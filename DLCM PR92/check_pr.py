from matplotlib import pyplot as plt
from hil_access.maport import MAPort
from utils import VP, messages, dts
import time
from hil_access.stimulus import simple_write
from bokeh.plotting import show, output_file, figure
from bokeh.layouts import column
import pprint

hil = "CASSIOPEIA"
sdf = r"N:\Users\HIL Tester\Projects\P15253010_Ford_DLCM\P15253010_Ford_DLCM_CFD\Application_DLCM\Build Results\Application_DLCM.sdf"
p = MAPort(hil, sdf, VP)


def make_write_data(payload):
    write_data = {}
    for name, data in payload.items():
        write_data[name] = [[0], simple_write([data])]
    write_data['TC1 Kickout'] = [[3, 3.2], simple_write([1, 0])]
    return write_data

def power_cycle():
    p.write({
        'Battery Voltage': 0
    })
    time.sleep(1)
    p.write({
        'Battery Voltage': 13.7
    })
    time.sleep(1)


def get_average(values):
    maximum = max(values)
    data_before = values[0:3999]
    average = sum(data_before) / len(data_before)
    diff = maximum - average
    return maximum, average, diff


if __name__ == '__main__':
    power_cycle()
    for name, message in messages:
        print 'sending: ', name
        with open(name+'.txt', 'w') as f:
            data = p.run_stimulus(
                write_data=make_write_data(message),
                #write_data={},
                verify_signals=dts,
                length=5,
            )
            times = data.pop('time')
            plt.figure(name)
            fig=[]
            for index, (dt, values) in enumerate(sorted(data.items())):
                # import json           # save data to files named after each message
                # with open(dt+'.json', 'w') as d:
                #     d.write(json.dumps(values))
                maximum, average, diff = get_average(values)
                emphasis = '<-------' if diff > 0.02 else ''
                print "{}\tmax: {:.5f}\taverage: {:.5f}\tdifference {:.5f}\t{}".format(dt, maximum, average, diff, emphasis)
                f.write("{}\tmax: {:.5f}\taverage: {:.5f}\tdifference {:.5f}\t{}\n".format(dt, maximum, average, diff, emphasis))
                plt.subplot(7, 4, index+1)
                plt.plot(times, values, label=dt)
                plt.legend()
                output_file('plot.html')
                t=figure(width=1000, height=250, title='plot', x_axis_label='time', y_axis_label=dt)
                t.line(times, values, line_width=2)
                fig.append(t)
            show(column(children=fig, sizing_mode="scale_width"))


    plt.show()

