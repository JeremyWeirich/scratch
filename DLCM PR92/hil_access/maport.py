"""
A simplified implementation of the dSPACE MAPort, which allows model variables to be written to and read from.
"""

# Copyright (c) Omron Automotive Electronics Co.Ltd.
# All rights reserved.
#
# The use, disclosure, reproduction, modification, transfer, or transmittal
# of this work for any purpose in any form or by any means without the written
# permission of Omron Automotive Electronics is strictly prohibited.
#
# Confidential, Unpublished Property of Omron Automotive Electronics
# Use and Distribution Limited Solely to Authorized Personnel.


import time
import shutil
import os
import logging
from xml.etree.ElementTree import Element, SubElement

# pragma pylint: disable=relative-import
from _utils import prettify, create_subelement_with_text, catch_xil_exception
from _utils import HIL_ACCESS_BASE, XIL_STRINGS
from stimulus import StimulusManager
# pragma pylint: enable=relative-import

#  pragma pylint: disable=import-error, wrong-import-position, no-member
import clr
clr.AddReference(XIL_STRINGS['testbench_ref'])
clr.AddReference(XIL_STRINGS['interface_ref'])

from ASAM.XIL.Implementation.TestbenchFactory.Testbench import TestbenchFactory
from ASAM.XIL.Interfaces.Testbench.Common.Error import TestbenchPortException
from ASAM.XIL.Interfaces.Testbench.Common.ValueContainer.Enum import DataType
#  pragma pylint: enable=import-error, wrong-import-position, no-member


CONFIGS_DIR = 'maport_configs'
FLOAT = DataType.eFLOAT
INT = DataType.eINT
UINT = DataType.eUINT
LOGGER = logging.getLogger(__name__)


class MAPort(object):
    """Creates and manages a model access port, used to write to and read from the HIL.

    Args:
        :hil_name: string name of the HIL to run tests on
        :sdf: string absolute path of the application .sdf file
        :variable_pool: dict mapping signal names as string to variable paths as string
        :default_pool: (optional, default=None) dict mapping signal names as string to default values as Number
        :sample_time_name: (optional, default='Fixed Step Size') signal name for model sample time
        :logger: (optional, default=None) logger to use for reporting events

    .. note:: if no logger is provided, logging.getLogger(__name__) will be used

    Attributes:
        :port: MAPort instance
        :bench: testbench for instantiating dSPACE factories
        :hil_name: string name of the HIL to run tests on
        :variable_pool: dict mapping signal names as string to variable paths as string
        :default_pool: dict mapping signal names as string to default values as Number
        :logger: event reporting logger
        :config_path: path to temp dir where files will be stored
        :sample_time: model sample time in seconds
    """
    def __init__(self, hil_name, sdf, variable_pool, default_pool=None, sample_time_name='Fixed Step Size',
                 logger=None):
        """Inits an MAPort"""
        self.port = None
        self.bench = None
        self._make_type = None
        self.hil_name = hil_name
        self.variable_pool = variable_pool
        self.default_pool = default_pool if default_pool else {}
        self.logger = logger if logger else LOGGER

        self.config_path = self._create_temp_dir()
        maport_config_file = self._create_maport_config_xml(sdf)
        self._create_maport(maport_config_file)
        self._create_type_functions()
        self.sample_time = self.read([sample_time_name])[sample_time_name]

    @staticmethod
    def _create_temp_dir():
        config_path = os.path.join(HIL_ACCESS_BASE, CONFIGS_DIR)
        if not os.path.isdir(config_path):
            os.makedirs(config_path)
        return config_path

    def _create_maport_config_xml(self, sdf):
        """Create an MAPort configuration xml file"""
        params = XIL_STRINGS['xml_params']
        maport_config = Element(params['port'])
        config = SubElement(maport_config, params['maport'])
        create_subelement_with_text(config, params['sdf'], sdf)
        create_subelement_with_text(config, params['hil'], self.hil_name)
        location = os.path.join(self.config_path, '{}.maportconfig'.format(self.hil_name))
        with open(location, 'w') as config_file:
            config_file.write(prettify(maport_config))
        return location

    def _create_maport(self, config_file):
        """Creates the MAPort object, configures it and starts it"""
        self.logger.info("configuring MAPort")
        test_bench_factory = TestbenchFactory()
        self.bench = test_bench_factory.CreateVendorSpecificTestbench(*XIL_STRINGS['testbench_params'])
        maport_factory = self.bench.MAPortFactory
        self.port = maport_factory.CreateMAPort('_')
        maport_config = self.port.LoadConfiguration(config_file)
        start = time.time()
        self.port.Configure(maport_config, False)
        self.logger.info("configuring took %s", time.time() - start)
        self.port.StartSimulation()

    def _create_type_functions(self):
        """Creates functions for making dSPACE types"""
        value_factory = self.bench.ValueFactory
        self._make_type = {
            INT: value_factory.CreateIntValue,
            UINT: value_factory.CreateUintValue,
            FLOAT: value_factory.CreateFloatValue,
        }

    def write_defaults(self):
        """Write all defaults using the default pool"""
        self.write(self.default_pool)

    @catch_xil_exception(TestbenchPortException)
    def write(self, write_values):
        """Writes value(s), coercing if needed.  Warns if a conversion might result in losing data

        Args:
            :write_values: dict mapping signal names to values to write
        """
        for signal_name, signal_value in write_values.items():
            path = self.variable_pool[signal_name]
            given = self._get_naive_type(signal_value)
            needed = self.port.GetDataType(path)
            self._warn(given, needed, signal_name)
            value = self._make_type[needed](signal_value)
            self.port.Write(self.variable_pool[signal_name], value)

    @catch_xil_exception(TestbenchPortException)
    def read(self, signals):
        """Read value(s) for model signals

        Args:
            :signals: signal name or sequence of signal names to read
        Returns:
            :results: dict mapping signal names to signal values
        """
        results = {}
        if isinstance(signals, (str, unicode)):
            signals = (signals, )  # convert single element to iterable
        for signal_name in signals:
            results[signal_name] = self.port.Read(self.variable_pool[signal_name]).Value
        return results

    def run_stimulus(self, write_data, verify_signals, length):
        """Execute a stimulus test

        Args:
            :write_data: dict mapping signal names as string to write data as ([times], [values])
            :verify_signals: sequence of verify signal names
            :length: length of stimulus in seconds

        Returns:
            :results: dict mapping signal names to capture value vectors.  Time data is mapped to 'time' key.
        """
        with StimulusManager(self, write_data, verify_signals, length) as stimulus:
            stimulus.run()
            return stimulus.read()

    @staticmethod
    def _get_naive_type(value):
        """Guess the type of user-provided data."""
        if isinstance(value, (int, long)) or value.is_integer():
            if value >= 0:
                return UINT
            return INT
        return FLOAT

    def _warn(self, given, needed, name):
        """Warn if type coercion loses information"""
        if given != needed:
            if given == FLOAT or given == INT and needed == UINT:
                self.logger.warn("Unsafe type coercion for %s", name)

    def cleanup(self):
        """Disposes the MAPort and removes the configs directory"""
        try:
            shutil.rmtree(self.config_path, ignore_errors=True)
        except WindowsError:
            self.logger.warn('temporary folder %s may not have been deleted', self.config_path)
        if self.port is not None:
            self.port.Dispose()
            self.port = None
