"""
Copyright (c) Omron Automotive Electronics Co.Ltd.
All rights reserved.

The use, disclosure, reproduction, modification, transfer, or transmittal
of this work for any purpose in any form or by any means without the written
permission of Omron Automotive Electronics is strictly prohibited.

Confidential, Unpublished Property of Omron Automotive Electronics
Use and Distribution Limited Solely to Authorized Personnel.
"""
# pylint: disable=C0301

def zero_appender(hex_message):
    """Appends zero's to the remainder if the length of remainder hex message including the suffix prefix '0x' is less than 10
    Args:
        hex_message: Any hex message whose length is less than 10 can be passed as an argument
    Returns:
        Returns a hex message with appropriate no of zero's appended to it.
    """
    fixed_len = 10
    if len(hex_message) < 10:
        no_of_zeros_to_be_appended = fixed_len - len(hex_message)
        for i in range(no_of_zeros_to_be_appended):
            if i == 0:
                message = hex_message.replace('0x', "")
            message = '0' + message
        message = '0x' + message
        return message


def utl_crcinit():
    """Prepares UTL_Crc_Table for calculating checksum for both record and checksum.
    """
    utl_crc_table = []
    polynomial = int('0x04C11DB7', 16)

    for j in range(256):
        remainder = j << ((32-8))

        for i in xrange(8, 0, -1):
            if remainder & (1 << ((32) - 1)) != 0:
                remainder = (remainder << 1) ^ polynomial
            else:
                remainder = (remainder << 1)

        remainder = hex(remainder)
        remainder = remainder.strip('L')

        if j != 0:
            if 4 <= j <= 15:
                remainder = '0x'+remainder[3:]
            else:
                remainder = '0x'+remainder[4:]

        utl_crc_table.append(int(remainder, 16))
    return utl_crc_table


def utl_crcreflect(data, nbits, reflection):
    """Reorder the bits of a binary sequence, by reflecting them about the middle position.
         The reflection of the original data will be returned
    """
    for i in range(nbits):
        if (data & 01) == 1:
            reflection = reflection | (1 << (nbits - 1) - i)
        data = (data >> 1)
    return reflection


#can_message_129 [['31', '36', '31', '54', '70', '00', '00', '00'], ['6F', 'C6', 'D5', '7B', '00', '00', '00', '00']]#response for core0 and type 16
#provide this message as input to header_checksum_calculator
def header_checksum_calculator(message):
    """Calculates chesksum for header.
    Args:
        message: List of two CAN 129 messages should be provided as input
        [['01','02','03','04','05','06,'07','08'],['01','02','03','04','05','06,'07','08']]
    Return:
        Returns header checksum(hex message)
    """
    utl_crc_table = utl_crcinit()
    xor_value = '0xFFFFFFFF'
    remainder = '0xFFFFFFFF'

    for can_messages in range(len(message)):
        if can_messages == 0:#considers all bytes in first list
            for byte in message[can_messages]:
                byte = int(byte, 16)
                data = utl_crcreflect(byte, 8, int('0x00000000', 16)) ^ (int(remainder, 16) >> (24))
                remainder = utl_crc_table[data] ^ (int(remainder, 16) << 8)
                remainder = hex(remainder)
                remainder = remainder.strip('L')
                remainder = '0x'+ remainder[4:]

        elif can_messages == 1:
            for byte in range(len(message[can_messages])):
                if byte < 4:#consider only first four bytes in second list
                    data = utl_crcreflect(int(message[can_messages][byte], 16), 8, int('0x00000000', 16)) ^ (int(remainder, 16) >> (24))
                    remainder = utl_crc_table[data] ^ (int(remainder, 16) << 8)
                    remainder = hex(remainder)
                    remainder = remainder.strip('L')
                    remainder = '0x'+remainder[4:]

            return_value = (utl_crcreflect(int(remainder, 16), 32, int('0x00000000', 16))) ^ int(xor_value, 16)
            return_header_checksum = hex(return_value).upper()
            return return_header_checksum.strip('L')

#response for core0 and type 16
#provide this message as input to record_checksum_calculator
def record_checksum_calculator(message):
    """Calculates checksum for record.
    Args:
        message: Provide list of list hex messages as inputs
        ex: [['01','02','03','04','05','06,'07','08'],['01','02','03','04','05','06,'07','08'],.......]
    Return:
        Returns record checksum(hex message)
    """
    utl_crc_table = utl_crcinit()
    xor_value = '0xFFFFFFFF'
    remainder = '0xFFFFFFFF'

    for can_messages in range(len(message)):
        print message[can_messages]

        if can_messages <= 26:#consider all bytes of first 26 messages
            for byte in message[can_messages]:
                byte = int(byte, 16)
                data = utl_crcreflect(byte, 8, int('0x00000000', 16)) ^ (int(remainder, 16) >> (24))
                remainder = utl_crc_table[data] ^ (int(remainder, 16) << 8)
                remainder = hex(remainder)
                remainder = remainder.strip('L')
                if len(remainder) < 10:
                    remainder = zero_appender(remainder)
                    remainder = '0x' + remainder[-8:]
                else:
                    remainder = '0x' + remainder[-8:]


        elif can_messages == 27:#consider first four bytes of 27th message
            for byte in range(len(message[can_messages])):
                if byte < 4:
                    data = utl_crcreflect(int(message[can_messages][byte], 16), 8, int('0x00000000', 16)) ^ (int(remainder, 16) >> (24))
                    remainder = utl_crc_table[data] ^ (int(remainder, 16) << 8)
                    remainder = hex(remainder)
                    remainder = remainder.strip('L')
                    remainder = '0x' + remainder[-8:]
            return_value = (utl_crcreflect(int(remainder, 16), 32, int('0x00000000', 16))) ^ int(xor_value, 16)

            return_record_checksum = hex(return_value).upper()
            print "&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@return_value", return_record_checksum
            return return_record_checksum.strip('L')



if __name__ == '__main__':
    can_message_128 = [
        ['16', '00', '01', '00', '00', '00', '00', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00'],
        ['6F', 'C6', 'D5', '7B', '00', '00', '40', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00'],
        ['00', '00', '00', '00', '01', '01', '00', '00'],
        ['03', 'A6', '2F', '00', '00', '00', '00', '00'],
        ['00', '00', '08', '00', '00', '00', '00', '00'],
        ['00', '00', '00', '00', '84', '0A', '00', '08'],
        ['83', '0A', '00', '08', '78', '02', '3D', '00'],
        ['79', '02', '3D', '00', '75', '02', '0D', '00'],
        ['84', 'FC', '01', 'A0', '8C', '81', '02', 'A0'],
        ['5C', '02', '30', 'A0', '01', '00', '00', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00'],
        ['E0', 'A0', '00', '70', '5C', '02', '30', 'A0'],
        ['8E', '67', '06', 'A0', '3E', 'A5', '00', '50'],
        ['80', 'DF', '00', '70', '01', '00', '00', '00'],
        ['06', '00', '00', '00', 'D1', '5F', '2D', '32'],
        ['00', '00', '00', '00', '77', '02', '3D', '00'],
        ['85', '0A', '00', '08', 'E8', '41', '00', 'D0'],
        ['50', 'BB', '01', 'A0', '07', 'C8', 'B9', '10'],
        ['F0', '41', '00', 'D0', 'C0', '9E', '00', 'D0'],
        ['00', '00', '00', '00', '48', '06', '30', 'A0'],
        ['60', 'A7', '00', '50', '40', '06', '30', 'A0'],
        ['44', 'A7', '00', '50', '02', '00', '00', '00'],
        ['00', '80', '00', '00', '17', '80', '00', '00'],
        ['00', '00', '00', '00', '00', '00', '00', '00']
    ]
    import pprint
    pprint.pprint(record_checksum_calculator(can_message_128))
