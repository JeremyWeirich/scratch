import numpy as np,math
import matplotlib.patches as patches
from matplotlib import pyplot as plt
from matplotlib import animation
import foc_algs
import foc_plt

switch_vector = range(0, 360, 60)

ma = 1.0
fs = 16000.0
ts = 1 / fs
vd = 12
vref_max = 12 / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3

fig = plt.figure()
fig.set_size_inches(20, 10, forward=True)
ax1 = plt.subplot2grid((4, 5), (0, 0), rowspan=4, colspan=2)
ax2 = plt.subplot2grid((4, 5), (1, 2), rowspan=2, colspan=3)

ax1.set_title("Space Vector Diagram")
foc_plt.init_svd(ax1)
graph_settings = [10, 12]
foc_plt.plot_block_com_template(ax1, vd, graph_settings)

ax2.set_title("Phase Voltage Vs Angle")
foc_plt.init_block_phase_duty(ax2)
foc_plt.phase_voltage_template(ax2)

arrows = []
plotcols1 = ["black", "red", "blue"]
arrow_color = ['green', 'blue']
for index in range(2):
    init = patches.FancyArrowPatch([0, 0], [0, 0], lw=2, arrowstyle='simple', mutation_scale=15,
                                   edgecolor=arrow_color[index], facecolor=arrow_color[index])
    arrows.append(init)

angle_line = ax2.plot([], [], 'k--', lw=2, color='black')[0]
plot = [arrows, angle_line]


def init():
    for arrow in arrows:
        ax1.add_patch(arrow)
    plot[1].set_data([], [])
    return arrows


def animate(sweep_angle):
    alpha_value = []
    beta_value = []
    sweep_angle *= 2
    stator_angle = foc_algs.get_block_stator_angle(sweep_angle)
    valpha, vbeta = foc_algs.clarke_transform(math.radians(stator_angle), vref_max)
    alpha_value.append(valpha)
    beta_value.append(vbeta)

    rotor_angle = sweep_angle
    temp_alpha, temp_beta = foc_algs.clarke_transform(math.radians(rotor_angle), (vd*0.25))
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    # JMW I think you can just zip through all three of these at once:
    # for arrow, alpha, beta in zip(plot[0], alpha_value, beta_value):
    #     arrow.set_positions([0, 0], alpha, beta)
    for vector_index, arrow in enumerate(plot[0]):
        arrow.set_positions([0, 0], [alpha_value[vector_index], beta_value[vector_index]])

    plot[1].set_data([sweep_angle, sweep_angle], [0, 9])
    return arrows


anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=181, interval=20, blit=False, repeat=True)

plt.show()
