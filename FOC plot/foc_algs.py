import math


def clarke_transform(angle_in_radians, reference_voltage_vector):
    v_alpha = reference_voltage_vector * math.cos(angle_in_radians)
    v_beta = reference_voltage_vector * math.sin(angle_in_radians)
    return v_alpha, v_beta


def p1(t_zero, t_one, t_two, period):
    return (100 * (t_zero / 2)) / period


def p2(t_zero, t_one, t_two, period):
    return (100 * (t_one + (t_zero / 2))) / period


def p3(t_zero, t_one, t_two, period):
    return (100 * (t_two + (t_zero / 2))) / period


def p4(t_zero, t_one, t_two, period):
    return (100 * (t_one + t_two + (t_zero / 2))) / period


def calculate_sector(v_alpha_ref, v_beta_ref, tx, ty, tz):
    if (v_beta_ref >= 0) and (v_alpha_ref >= 0):
        if tz <= 0:
            sector = 1
        else:
            sector = 2
    elif (v_beta_ref >= 0) and (v_alpha_ref < 0):
        if ty <= 0:
            sector = 3
        else:
            sector = 2
    elif (v_beta_ref < 0) and (v_alpha_ref >= 0):
        if ty <= 0:
            sector = 5
        else:
            sector = 6
    else:
        if tz <= 0:
            sector = 5
        else:
            sector = 4
    return sector


class Sector(object):
    def __init__(self, daf, dbf, dcf, t1, t2, period, angle, next_angle, voltage):
        if (t1+t2) > period:
            self.t1 = (t1*period)/(t1+t2)
            self.t2 = (t2*period)/(t1+t2)
            self.t0 = period - self.t1 - self.t2
        else:
            self.t1 = t1
            self.t2 = t2
            self.t0 = period - t1 - t2

        vector_max = (voltage*2)/3
        self.alpha = ((self.t1*vector_max*math.cos(math.radians(angle)))/period) + \
                     ((self.t2*vector_max*math.cos(math.radians(next_angle)))/period)
        self.beta = ((self.t1 * vector_max * math.sin(math.radians(angle))) / period) + \
                    ((self.t2 * vector_max * math.sin(math.radians(next_angle))) / period)

        self.daf = daf
        self.dbf = dbf
        self.dcf = dcf
        self.period = period

    def calculate_duty_cycle(self):
        da = self.daf(self.t0, self.t1, self.t2, self.period)
        db = self.dbf(self.t0, self.t1, self.t2, self.period)
        dc = self.dcf(self.t0, self.t1, self.t2, self.period)
        return da, db, dc, self.alpha, self.beta


def calculate_svm_duty_cycles(angle, vref, v_supply, period):
    v_alpha_ref, v_beta_ref = clarke_transform(math.radians(angle), vref)
    tx = (math.sqrt(3)*period*v_beta_ref)/v_supply
    ty = (period*((math.sqrt(3)*v_beta_ref)+(3*v_alpha_ref)))/(2*v_supply)
    tz = (period*((math.sqrt(3)*v_beta_ref)-(3*v_alpha_ref)))/(2*v_supply)

    S1 = Sector(p4, p3, p1, -tz, tx, period, 0, 60, v_supply)
    S2 = Sector(p2, p4, p1, ty, tz, period, 60, 120, v_supply)
    S3 = Sector(p1, p4, p3, tx, -ty, period, 120, 180, v_supply)
    S4 = Sector(p1, p2, p4, tz, -tx, period, 180, 240, v_supply)
    S5 = Sector(p3, p1, p4, -ty, -tz, period, 240, 300, v_supply)
    S6 = Sector(p4, p1, p2, -tx, ty, period, 300, 0, v_supply)

    sectors = [S1, S2, S3, S4, S5, S6]

    sector_index = calculate_sector(v_alpha_ref, v_beta_ref, tx, ty, tz)
    da, db, dc, new_alpha, new_beta = sectors[sector_index-1].calculate_duty_cycle()
    return da, db, dc, new_alpha, new_beta

#######################################################################################################################
# Start of sine wave modulation algorithms
#######################################################################################################################


def get_sine_mod_duty_sweep(ma):
    da = []
    db = []
    dc = []
    if ma > 1:
        ma = 1
    sva = range(0, 361, 1)
    for angle in sva:
        da.append(ma * 50 * math.cos(math.radians(angle)) + 50)
        db.append(ma * 50 * math.cos(math.radians(angle - 120)) + 50)
        dc.append(ma * 50 * math.cos(math.radians(angle - 240)) + 50)
    return da, db, dc


def get_sine_mod_duty(ma, angle):
    da = ma * 50 * math.cos(math.radians(angle)) + 50
    db = ma * 50 * math.cos(math.radians(angle - 120)) + 50
    dc = ma * 50 * math.cos(math.radians(angle - 240)) + 50
    return da, db, dc


def get_sine_mod_alpha_beta(da, db, dc, vmax):
    v_alpha = ((da * vmax * math.cos(math.radians(0))) / 100.0) + (
    (db * vmax * math.cos(math.radians(120))) / 100.0) + \
              ((dc * vmax * math.cos(math.radians(240))) / 100.0)
    v_beta = ((da * vmax * math.sin(math.radians(0))) / 100.0) + (
    (db * vmax * math.sin(math.radians(120))) / 100.0) + \
             ((dc * vmax * math.sin(math.radians(240))) / 100.0)
    return v_alpha, v_beta


#######################################################################################################################
# Start of block commutation algorithms
#######################################################################################################################

def get_block_stator_angle(rotor_angle):
    # JMW you can write this check like this:
    # if 0 <= rotor_angle < 30:
    if (rotor_angle >= 0) and (rotor_angle < 30):
        stator_angle = 90
    elif (rotor_angle >= 30) and (rotor_angle < 90):
        stator_angle = 150
    elif (rotor_angle >= 90) and (rotor_angle < 150):
        stator_angle = 210
    elif (rotor_angle >= 150) and (rotor_angle < 210):
        stator_angle = 270
    elif (rotor_angle >= 210) and (rotor_angle < 270):
        stator_angle = 330
    elif (rotor_angle >= 270) and (rotor_angle < 330):
        stator_angle = 30
    elif (rotor_angle >= 330) and (rotor_angle <= 360):
        stator_angle = 90
    return stator_angle


def get_block_duty(rotor_angle):
    if (rotor_angle >= 0) and (rotor_angle < 30):
        da = 7.0
        db = 5.0
        dc = 0.0
    elif (rotor_angle >= 30) and (rotor_angle < 90):
        da = 6.0
        db = 5.0
        dc = 1.0
    elif (rotor_angle >= 90) and (rotor_angle < 150):
        da = 6.0
        db = 4.0
        dc = 2.0
    elif (rotor_angle >= 150) and (rotor_angle < 210):
        da = 7.0
        db = 3.0
        dc = 2.0
    elif (rotor_angle >= 210) and (rotor_angle < 270):
        da = 8.0
        db = 3.0
        dc = 1.0
    elif (rotor_angle >= 270) and (rotor_angle < 330):
        da = 8.0
        db = 4.0
        dc = 0.0
    else:
        da = 7.0
        db = 5.0
        dc = 0.0
    return da, db, dc
#######################################################################################################################
# The following could be used for the actual embedded implementation of SVM
#def calculate_sector1_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_one + t_two + (t_zero / 2))) / period
#    db = (100 * (t_two + (t_zero / 2))) / period
#    dc = (100 * (t_zero / 2)) / period
#    return da, db, dc
#
#
#def calculate_sector2_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_one + (t_zero / 2))) / period
#    db = (100 * (t_one + t_two + (t_zero / 2))) / period
#    dc = (100 * (t_zero / 2)) / period
#    return da, db, dc
#
#
#def calculate_sector3_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_zero / 2)) / period
#    db = (100 * (t_one + t_two + (t_zero / 2))) / period
#    dc = (100 * (t_two + (t_zero / 2))) / period
#    return da, db, dc
#
#
#def calculate_sector4_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_zero / 2)) / period
#    db = (100 * (t_one + (t_zero / 2))) / period
#    dc = (100 * (t_one + t_two + (t_zero / 2))) / period
#    return da, db, dc
#
#
#def calculate_sector5_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_two + (t_zero / 2))) / period
#    db = (100 * (t_zero / 2)) / period
#    dc = (100 * (t_one + t_two + (t_zero / 2))) / period
#    return da, db, dc
#
#
#def calculate_sector6_duty_cycles(t_zero, t_one, t_two, period):
#    da = (100 * (t_one + t_two + (t_zero / 2))) / period
#    db = (100 * (t_zero / 2)) / period
#    dc = (100 * (t_one + (t_zero / 2))) / period
#    return da, db, dc
#
#
#get_duty_cycles = \
#    {1: calculate_sector1_duty_cycles,
#     2: calculate_sector2_duty_cycles,
#     3: calculate_sector3_duty_cycles,
#     4: calculate_sector4_duty_cycles,
#     5: calculate_sector5_duty_cycles,
#     6: calculate_sector6_duty_cycles, }
#
#
#def calculate_svm_duty_cycles(v_alpha_ref, v_beta_ref, v_supply, period):
#    tx = (math.sqrt(3)*period*v_beta_ref)/v_supply
#    ty = (period*((math.sqrt(3)*v_beta_ref)+(3*v_alpha_ref)))/(2*v_supply)
#    tz = (period*((math.sqrt(3)*v_beta_ref)-(3*v_alpha_ref)))/(2*v_supply)
#    if v_beta_ref >= 0:
#        if v_alpha_ref >= 0:
#            if tz <= 0:
#                sector = 1
#                t1 = -tz
#                t2 = tx
#            else:
#                sector = 2
#                t1 = ty
#                t2 = tz
#        else:
#            if ty <= 0:
#                sector = 3
#                t1 = tx
#                t2 = -ty
#            else:
#                sector = 2
#                t1 = ty
#                t2 = tz
#    else:
#        if v_alpha_ref >= 0:
#            if ty <= 0:
#                sector = 5
#                t1 = -ty
#                t2 = -tz
#            else:
#                sector = 6
#                t1 = -tx
#                t2 = ty
#        else:
#            if tz <= 0:
#                sector = 5
#                t1 = -ty
#                t2 = -tz
#            else:
#                sector = 4
#                t1 = tz
#                t2 = -tx
#    t0 = period-t1-t2
#    duty_a, duty_b, duty_c = get_duty_cycles[sector](t0, t1, t2, period)
#    return duty_a, duty_b, duty_c