import numpy as np, math
import matplotlib.patches as patches
from matplotlib import pyplot as plt
import foc_algs


def create_switch_graphs(figure):
    # create two subplots for each switch state. Total of 12 subplots; 3 rows; 4 columns
    # Each switching vector will have a plot that depicts the space vector diagram and a plot that depicts the phase
    # duty cycles.
    graphs = [[], [], [], [], [], []]
    figure.set_size_inches(20, 10, forward=True)
    graphs[0].append(plt.subplot2grid((3, 6), (0, 0)))
    graphs[0].append(plt.subplot2grid((3, 6), (0, 1), colspan=2))
    graphs[1].append(plt.subplot2grid((3, 6), (0, 3)))
    graphs[1].append(plt.subplot2grid((3, 6), (0, 4), colspan=2))
    graphs[2].append(plt.subplot2grid((3, 6), (1, 0)))
    graphs[2].append(plt.subplot2grid((3, 6), (1, 1), colspan=2))
    graphs[3].append(plt.subplot2grid((3, 6), (1, 3)))
    graphs[3].append(plt.subplot2grid((3, 6), (1, 4), colspan=2))
    graphs[4].append(plt.subplot2grid((3, 6), (2, 0)))
    graphs[4].append(plt.subplot2grid((3, 6), (2, 1), colspan=2))
    graphs[5].append(plt.subplot2grid((3, 6), (2, 3)))
    graphs[5].append(plt.subplot2grid((3, 6), (2, 4), colspan=2))
    return graphs


def init_switch_plot(graphs):
    for graph in graphs:
        # Init x and y axis for the space vector diagram
        init_svd(graph[0])
        # Init x and y axis for the phase duty cycle plot
        init_phase_duty(graph[1])


def init_svd(graph):
    graph.set_ylim(-8, 8)
    graph.set_xlim(-10, 10)
    graph.xaxis.set_visible(False)
    graph.yaxis.set_visible(False)
    graph.set_aspect('equal', adjustable='box')


def create_duty_sweep_graphs(figure):
    graphs = []
    figure.set_size_inches(20, 10, forward=True)
    graphs.append(plt.subplot2grid((3, 2), (0, 0)))
    graphs.append(plt.subplot2grid((3, 2), (0, 1)))
    graphs.append(plt.subplot2grid((3, 2), (1, 0)))
    graphs.append(plt.subplot2grid((3, 2), (1, 1)))
    graphs.append(plt.subplot2grid((3, 2), (2, 0)))
    graphs.append(plt.subplot2grid((3, 2), (2, 1)))
    return graphs


def init_phase_duty(graph):
    graph.set_ylim(0, 6)
    graph.set_xlim(0, 100)
    graph.xaxis.set_visible(False)
    graph.set_yticklabels([])
    graph.grid(which='major', color='black', ls='-')
    graph.yaxis.grid(True)
    graph.xaxis.grid(False)
    ytext = ['CL', 'CH', 'BL', 'BH', 'AL', 'AH']
    graph.set_yticklabels(ytext)


def init_duty_sweep(graph):
    sw_vect = range(0, 360, 60)
    graph.set_ylim(0, 105)
    graph.set_xlim(0, 360)
    graph.xaxis.set_ticks(np.arange(0, 390, 30))
    graph.yaxis.set_ticks(np.arange(0, 110, 10))
    graph.grid(which='major', color='black', ls='--')
    graph.yaxis.grid(True)
    graph.xaxis.grid(False)
    graph.set_aspect('equal', adjustable='box')
    for index, angle in enumerate(sw_vect):
        graph.plot([angle, angle], [0, 100], 'k--', lw=1, color="black")
        graph.text((angle + 30), 95, 'Sector %s' % (index + 1), horizontalalignment='center',
                   verticalalignment='center')


def plot_duty_sweep(graph, vref, vd, ts):
    sva = range(0, 361, 1)
    phase_a_duty = []
    phase_b_duty = []
    phase_c_duty = []
    for angle in sva:
        duty_a, duty_b, duty_c, alpha, beta = foc_algs.calculate_svm_duty_cycles(angle, vref, vd, ts)
        phase_a_duty.append(duty_a)
        phase_b_duty.append(duty_b)
        phase_c_duty.append(duty_c)
    graph.plot(sva, phase_a_duty, linewidth=2, color="blue")
    graph.plot(sva, phase_b_duty, linewidth=2, color="red")
    graph.plot(sva, phase_c_duty, linewidth=2, color="black")


def plot_svd_template(graph, vd, graph_settings):
    # This function plots the switching state vectors, the max limit hexagon and the continuous range circle on the
    # space vector diagram.
    vref_max = vd / math.sqrt(3)
    vector_max = (vd * 2) / 3
    switch_vector = range(0, 360, 60)
    ends = []
    for index, angle in enumerate(switch_vector):
        # Calculate the alpha and beta values for each angle in switch_vector
        v_a_sw = vector_max * math.cos(math.radians(angle))
        v_b_sw = vector_max * math.sin(math.radians(angle))
        # Store the vector endpoints into ends for plotting the hexagon
        ends.append([v_a_sw, v_b_sw])
        # Calculate intermediate vector points to plot the sector label
        sector_a = 0.7 * vector_max * math.cos(math.radians(angle + 30))
        sector_b = 0.7 * vector_max * math.sin(math.radians(angle + 30))
        # Check where the vector end point is and adjust the vertical or horizontal text position for the vector name
        # This prevents the label Vx from getting obstructed by the vector line.
        if v_b_sw < 0:
            vertical = 'top'
        else:
            vertical = 'bottom'
        if v_a_sw < 0:
            horizontal = 'right'
        else:
            horizontal = 'left'

        # Plot the vector arrow for the current line.
        arrow = patches.FancyArrowPatch([0, 0], [v_a_sw, v_b_sw], lw=1, arrowstyle='simple',
                                        mutation_scale=graph_settings[0], edgecolor='black', facecolor='black')
        graph.add_patch(arrow)
        # Plot the sector label
        graph.text(sector_a, sector_b, 'S%s' % (index + 1), ha='center', va='center', fontsize=graph_settings[1])
        # Plot the vector label
        graph.text(v_a_sw, v_b_sw, 'V%s' % (index + 1), horizontalalignment=horizontal, verticalalignment=vertical,
                   fontsize=graph_settings[1])

    ends.append(ends[0])

    # Use the vector endpoints to plot the hexagon
    for (old_x, old_y), (new_x, new_y) in zip(ends, ends[1:]):
        graph.plot([old_x, new_x], [old_y, new_y], 'k--', linewidth=1)
    # Plot the continuous operation circle
    circ = plt.Circle((0, 0), radius=vref_max, lw=1, fill=False, color='blue')
    graph.add_patch(circ)


def plot_vector_and_dc(graph, swvect, vref, vd, ts):
    # This function plots the stator vector on the space vector diagram and the phase duty cycles for each angle in the
    # swvect list.

    phase_y_values = [[], [], []]
    phase_null = [[], [], []]

    for index in range(3):
        # Create y values for Phase A, B and C PWMs
        # Phase A 0 - 1; Phase B 2 - 3; Phase C 4 - 5
        phase_y_values[index] = [index * 2, index * 2, (index * 2) + 1, (index * 2) + 1, index * 2, index * 2]
        phase_null[index] = [index * 2, index * 2, index * 2, index * 2, index * 2, index * 2]

    for index, angle in enumerate(swvect):
        duty_a, duty_b, duty_c, new_alpha, new_beta = foc_algs.calculate_svm_duty_cycles(angle, vref, vd, ts)
        phase = [duty_c, duty_b, duty_a]
        plot_phase_duty(graph[index][1], phase)

        arrow = patches.FancyArrowPatch([0, 0], [new_alpha, new_beta], lw=1, arrowstyle='simple', mutation_scale=15,
                                        edgecolor='green', facecolor='green')
        graph[index][0].add_patch(arrow)


def plot_phase_duty(graph, duty_cycles):
    plotcols1 = ["black", "red", "blue"]
    phase_x_values = [[], [], []]
    phase_y_values = [[], [], []]
    phase_null = [[], [], []]

    for index in range(3):
        # Create y values for Phase A, B and C PWMs
        # Phase A 0 - 1; Phase B 2 - 3; Phase C 4 - 5
        phase_y_values[index] = [index * 2, index * 2, (index * 2) + 1, (index * 2) + 1, index * 2, index * 2]
        phase_null[index] = [index * 2, index * 2, index * 2, index * 2, index * 2, index * 2]

    for phase_index, duty in enumerate(duty_cycles):
        start = (100 - duty) / 2
        end = duty + start
        phase_x_values[phase_index] = [0, start, start, end, end, 100]
        if duty < 0.1:
            graph.plot(phase_x_values[phase_index], phase_null[phase_index], color=plotcols1[phase_index], lw=1.5)
        else:
            graph.plot(phase_x_values[phase_index], phase_y_values[phase_index], color=plotcols1[phase_index], lw=1.5)
        graph.plot([start, start], [0, 6], 'k--', color='black', lw=1.0)
        graph.plot([end, end], [0, 6], 'k--', color='black', lw=1.0)

#######################################################################################################################
# Start of sine wave modulation plots
#######################################################################################################################


def plot_sine_mod_duty_sweep(graph, ma):
    sva = range(0, 361, 1)
    da, db, dc = foc_algs.get_sine_mod_duty_sweep(ma)
    graph.plot(sva, da)
    graph.plot(sva, db)
    graph.plot(sva, dc)

#######################################################################################################################
# Start of block commutation plots
#######################################################################################################################


def init_block_phase_duty(graph):
    sector_label = ['BC', 'BA', 'CA', 'CB', 'AB', 'AC']
    sector_angle = range(30, 360, 60)
    graph.set_ylim(0, 9)
    graph.set_xlim(0, 100)
    graph.set_yticklabels([])
    graph.grid(which='major', color='black', ls='-')
    graph.yaxis.grid(True)
    graph.xaxis.grid(False)
    graph.yaxis.set_ticks(np.arange(0, 10, 1))
    ytext = ['CL', 'CF', 'CH', 'BL', 'BF', 'BH', 'AL', 'AF', 'AH']
    graph.set_yticklabels(ytext)
    graph.xaxis.set_ticks(np.arange(0, 390, 30))
    graph.xaxis.set_visible(True)
    for index, angle in enumerate(sector_angle):
        print angle
        graph.plot([angle, angle], [0, 9], 'k--', lw=1, color='black')
        if (angle >= 30) and (angle < 330):
            graph.text(angle+30, 8.5, 'Sec. %s' % sector_label[index], ha='center')
        else:
            graph.text(angle + 15, 8.5, 'Sec. %s' % sector_label[index], ha='center')
            graph.text(15, 8.5, 'Sec. %s' % sector_label[index], ha='center')


def plot_block_com_template(graph, vd, graph_settings):
    # This function plots the switching state vectors, the max limit hexagon and the continuous range circle on the
    # space vector diagram.
    vref_max = vd / math.sqrt(3)
    switch_vector = range(30, 360, 60)
    for index, angle in enumerate(switch_vector):
        # Calculate the alpha and beta values for each angle in switch_vector
        v_a_sw = vref_max * math.cos(math.radians(angle))
        v_b_sw = vref_max * math.sin(math.radians(angle))
        # Calculate intermediate vector points to plot the sector label
        phase_x = vref_max * math.cos(math.radians(angle - 30))
        phase_y = vref_max * math.sin(math.radians(angle - 30))
        # Check where the vector end point is and adjust the vertical or horizontal text position for the vector name
        # This prevents the label Vx from getting obstructed by the vector line.
        if v_b_sw < 0:
            vertical = 'top'
        else:
            vertical = 'bottom'
        if v_a_sw < 0:
            horizontal = 'right'
        else:
            horizontal = 'left'

        if (angle == 90) or (angle == 270):
            s_horizontal = 'center'
        else:
            s_horizontal = horizontal

        # Plot the vector arrow for the current line.
        arrow = patches.FancyArrowPatch([0, 0], [v_a_sw, v_b_sw], lw=1, arrowstyle='simple',
                                        mutation_scale=graph_settings[0], edgecolor='black', facecolor='black')
        graph.add_patch(arrow)
        # Plot the sector label
        phase_label = ['A', '-C', 'B', '-A', 'C', '-B']
        phase_line = ['-', '--', '-', '--', '-', '--']
        graph.plot([0, phase_x], [0, phase_y], ls=phase_line[index], color='blue')
        graph.text(phase_x, phase_y, '%s' % phase_label[index], ha=horizontal, va=vertical, fontsize=graph_settings[1])
        # Plot the vector label
        sector_label = ['AC', 'BC', 'BA', 'CA', 'CB', 'AB']
        graph.text(v_a_sw, v_b_sw, 'Sec. %s' % sector_label[index], horizontalalignment=s_horizontal,
                   verticalalignment=vertical, fontsize=graph_settings[1])


def phase_voltage_template(graph):
    vect_sweep = range(0, 390, 30)
    phase_a = []
    phase_b = []
    phase_c = []
    sweep_angle = []
    for angle in vect_sweep:
        dty_a, dty_b, dty_c = foc_algs.get_block_duty(angle - 1)
        phase_a.append(dty_a)
        phase_b.append(dty_b)
        phase_c.append(dty_c)
        sweep_angle.append(angle)
        dty_a, dty_b, dty_c = foc_algs.get_block_duty(angle)
        phase_a.append(dty_a)
        phase_b.append(dty_b)
        phase_c.append(dty_c)
        sweep_angle.append(angle)

    graph.plot(sweep_angle, phase_a, color='blue', lw=2)
    graph.plot(sweep_angle, phase_b, color='red', lw=2)
    graph.plot(sweep_angle, phase_c, color='black', lw=2)