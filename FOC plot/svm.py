import numpy as math
import matplotlib.patches as patches
from matplotlib import pyplot as plt
from matplotlib import animation
import foc_algs
import foc_plt

switch_vector = range(0, 360, 60)

ma = 1.0
fs = 16000.0
ts = 1 / fs
vd = 12
vref_max = 12 / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3

# create a figure with two subplots
# fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
fig = plt.figure()
fig.set_size_inches(20, 10, forward=True)
ax1 = plt.subplot2grid((2, 5), (0, 0), rowspan=2, colspan=2)
ax2 = plt.subplot2grid((2, 5), (0, 2), colspan=3)
ax3 = plt.subplot2grid((2, 5), (1, 2), colspan=3)

ax1.set_title("Space Vector Diagram")
foc_plt.init_svd(ax1)
graph_settings = [10, 12]
foc_plt.plot_svd_template(ax1, vd, graph_settings)

ax2.set_title("Phase PWMs")
foc_plt.init_phase_duty(ax2)

ax3.set_title("Phase Duty Cycle Vs Angle")
foc_plt.init_duty_sweep(ax3)
foc_plt.plot_duty_sweep(ax3, vref, vd, ts)

sva = range(0, 361, 1)
plotcols1 = ["black", "red", "blue"]
arrow_color = ['green', 'blue']
arrows = []
lines = []
phase_y_values = [[], [], []]
phase_null = [[], [], []]

for index in range(2):
    init = patches.FancyArrowPatch([0, 0], [0, 0], lw=2, arrowstyle='simple', mutation_scale=15,
                                   edgecolor=arrow_color[index], facecolor=arrow_color[index])
    arrows.append(init)

for index in range(3):
    # Create y values for Phase A, B and C PWMs
    # Phase A 0 - 1; Phase B 2 - 3; Phase C 4 - 5
    phase_y_values[index] = [index*2, index*2, (index*2)+1, (index*2)+1, index*2, index*2]
    phase_null[index] = [index*2, index*2, index*2, index*2, index*2, index*2]
    # Init PWM lines
    lobj = ax2.plot([], [], lw=3, color=plotcols1[index])[0]
    lines.append(lobj)

angle_line = ax3.plot([], [], 'k--', lw=2, color="black")[0]

plot = [arrows, lines, angle_line]


def init():
    for arrow in plot[0]:
        ax1.add_patch(arrow)
    for line in plot[1]:
        line.set_data([], [])
    plot[2].set_data([], [])
    return plot


def animate(sweep_angle):
    sweep_angle *= 2
    alpha_value = []
    beta_value = []
    phase_x_values = [[], [], []]

    duty_a, duty_b, duty_c, new_alpha, new_beta = foc_algs.calculate_svm_duty_cycles(sweep_angle, vref, vd, ts)
    alpha_value.append(new_alpha)
    beta_value.append(new_beta)
    phase = [duty_c, duty_b, duty_a]
    for phase_index, duty in enumerate(phase):
        start = (100-duty)/2
        end = duty + start
        phase_x_values[phase_index] = [0, start, start, end, end, 100]

    rotor_angle = sweep_angle - 90
    temp_alpha, temp_beta = foc_algs.clarke_transform(math.radians(rotor_angle), (vd*0.25))
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    for vector_index, arrow in enumerate(plot[0]):
        arrow.set_positions([0, 0], [alpha_value[vector_index], beta_value[vector_index]])

    for lnum, line in enumerate(plot[1]):
        if phase[lnum] < 0.1:
            line.set_data(phase_x_values[lnum], phase_null[lnum])
        else:
            line.set_data(phase_x_values[lnum], phase_y_values[lnum])

    plot[2].set_data([sweep_angle, sweep_angle], [0, 105])
    return plot


anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=181, interval=20, blit=False, repeat=True)

plt.show()
