import numpy as np, math
from matplotlib import pyplot as plt
import foc_plt

# List contains the switching vector angles
switch_vector = range(0, 360, 60)
# List contains the intermediate stator vector angles
int_sw_vect = range(30, 390, 60)

ma = 0.75
fs = 16000.0
ts = 1 / fs
vd = 12
vref_max = vd / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3
switch_state = ['[100]', '[110]', '[010]', '[011]', '[001]', '[101]']

#######################################################################################################################
# Create first figure for switching vector duty cycles
fig = plt.figure()
# Create two subplots for each switching state.
plots = foc_plt.create_switch_graphs(fig)
# Initialize x and y axis for each subplot.
foc_plt.init_switch_plot(plots)
# Plot the six switching vectors, the hexagon and the circle on the space vector diagram along with labels.
graph_settings = [5, 8]
for plot_index, plot in enumerate(plots):
    foc_plt.plot_svd_template(plot[0], vd, graph_settings)
    plot[0].set_title("V%s %s" % (plot_index+1, switch_state[plot_index]))
# Plot the stator vector and the phase duty cycles for each angle in switch_vector.
foc_plt.plot_vector_and_dc(plots, switch_vector, vref, vd, ts)

#######################################################################################################################
# Create second figure for intermediate switching vector duty cycles
fig2 = plt.figure()
# Create two subplots for each switching state.
plots1 = foc_plt.create_switch_graphs(fig2)
# Initialize x and y axis for each subplot.
foc_plt.init_switch_plot(plots1)
# Plot the six switching vectors, the hexagon and the circle on the space vector diagram along with labels.
graph_settings = [5, 8]
for plot in plots1:
    foc_plt.plot_svd_template(plot[0], vd, graph_settings)
# Plot the stator vector and the phase duty cycles for each angle in int_sw_vect.
foc_plt.plot_vector_and_dc(plots1, int_sw_vect, vref, vd, ts)
# Set title for each space vector diagram
for plot_index, plot in enumerate(plots1):
    if plot_index+1 >= len(plots1):
        vecnum = 1
        new_index = 0
    else:
        vecnum = plot_index+2
        new_index = plot_index+1
    plot[0].set_title("V%s %s & V%s %s" % (plot_index+1, switch_state[plot_index], vecnum, switch_state[new_index]))

#######################################################################################################################
# Create third figure for modulation index duty cycle sweep.
fig3 = plt.figure()
plots2 = foc_plt.create_duty_sweep_graphs(fig3)
mod_index = [0, 0.25, 0.5, 0.75, 1.0, 1.25]

for index, plot in enumerate(plots2):
    plot.set_title('Phase Duty Cycle Vs Angle: ma = %s' % mod_index[index])
    foc_plt.init_duty_sweep(plot)
    foc_plt.plot_duty_sweep(plot, mod_index[index] * vref_max, vd, ts)

plt.show()
