import numpy as np, math, matplotlib.patches as patches
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib import pyplot as plt
from matplotlib import animation
from math import pi as pi
import test_2


def to_radian(angle_in_degrees):
    return angle_in_degrees*(pi/180)


def to_degree(angle_in_radians):
    return angle_in_radians*(180/pi)

# Create figure
fig = plt.figure()
ax = fig.gca()

ax.set_xticklabels([])
ax.set_ylim(-8,8)
ax.yaxis.set_ticks(np.arange(-8, 9, 1))
ax.set_yticklabels([])
ax.set_xlim(-10,10)
ax.xaxis.set_ticks(np.arange(-10, 11, 1))
# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_position('center')
ax.spines['top'].set_position('center')

plt.gca().set_aspect('equal', adjustable='box')
arrow_color = ['green','blue']
arrows = []

for index in range(2):
    init = patches.FancyArrowPatch([0, 0], [0, 0], lw=2, arrowstyle='simple', mutation_scale=15, edgecolor=arrow_color[index], facecolor=arrow_color[index] )
    arrows.append(init)

#patch = patches.FancyArrowPatch([0, 0], [0, 0], lw=2, arrowstyle='simple', mutation_scale=20, edgecolor='none', facecolor='green' )


def init():
    for arrow in arrows:
        ax.add_patch(arrow)
    return arrows

def animate(angle):
    #global patch
    alpha_value = []
    beta_value = []
    temp_alpha, temp_beta = test_2.clarke_transform(to_radian(angle), vref)
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    rotor_angle = angle - 90
    temp_alpha, temp_beta = test_2.clarke_transform(to_radian(rotor_angle), (vd*0.25))
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    for index,arrow in enumerate(arrows):
        arrow.set_positions([0, 0], [alpha_value[index], beta_value[index]])
    return arrows

    #temp_alpha, temp_beta = test_2.clarke_transform(to_radian(angle), vref)

    #patch.set_positions([0, 0], [temp_alpha, temp_beta])

    #return patch,

switch_vector = range(0, 360, 60)

ma = 0.75
fs = 16000.0
ts = 1 / fs
vd = 12
vref_max = 12 / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3

v_a = []
v_b = []
ends = []

#vector_value = ['V1 \n [100]', 'V2 \n [110]', 'V3 \n [010]', 'V4 \n [011]', 'V5 \n [001]', 'V6 \n [101]']

for index,angle in enumerate(switch_vector):
    v_a_sw = vector_max * math.cos(to_radian(angle))
    v_b_sw = vector_max * math.sin(to_radian(angle))
    print v_a_sw, v_b_sw, angle
    plt.plot([0, v_a_sw], [0, v_b_sw], 'k-', linewidth=2)
    ends.append([v_a_sw, v_b_sw])
    if v_b_sw < 0:
        vertical = 'top'
    else:
        vertical = 'bottom'
    if v_a_sw < 0:
        horizontal = 'right'
    else:
        horizontal = 'left'
    plt.text(v_a_sw, v_b_sw, 'V%s' % (index+1), horizontalalignment=horizontal, verticalalignment=vertical)


ends.append(ends[0])

for (old_x, old_y), (new_x, new_y) in zip(ends, ends[1:]):
    plt.plot([old_x, new_x], [old_y, new_y], 'k--', linewidth=2)

circ = plt.Circle((0,0), radius=vref_max, lw=2, fill=False, color='blue' )
ax.add_patch(circ)


anim = animation.FuncAnimation(fig, animate,
                               init_func=init,
                               interval=2,
                               frames=360,
                               blit=False)

fig1 = plt.figure()
ax1 = fig1.gca()
ax1.set_ylim(0,6)
ax1.set_xlim(0,100)

plotcols1 = ["black","red","blue"]
lines = []
phase_null = [[4, 4, 4, 4, 4, 4],
              [2, 2, 2, 2, 2, 2],
              [0, 0, 0, 0, 0, 0]]
phase_a_y = [4, 4, 5, 5, 4, 4]
phase_b_y = [2, 2, 3, 3, 2, 2]
phase_c_y = [0, 0, 1, 1, 0, 0]
phase_y_values = [phase_a_y, phase_b_y, phase_c_y]

for index in range(3):
    lobj = ax1.plot([], [], lw=3, color=plotcols1[index])[0]
    lines.append(lobj)


# initialization function: plot the background of each frame
def init_svm():
    for line in lines:
        line.set_data([], [])
    return lines


def svm(angle):
    v_alpha, v_beta = test_2.clarke_transform(to_radian(angle), vref)
    duty_a, duty_b, duty_c = test_2.calculate_svm_duty_cycles(v_alpha, v_beta, vd, ts)
    phase = [duty_a, duty_b, duty_c]
    a_start = (100-duty_a)/2
    a_end = duty_a + a_start
    b_start = (100-duty_b)/2
    b_end = duty_b + b_start
    c_start = (100-duty_c)/2
    c_end = duty_c + c_start
    phase_x_values = [[0,a_start,a_start,a_end,a_end,100],
                      [0,b_start,b_start,b_end,b_end,100],
                      [0,c_start,c_start,c_end,c_end,100]]
    for lnum,line in enumerate(lines):
        if phase[lnum] < 0.1:
            line.set_data(phase_x_values[lnum], phase_null[lnum])
        else:
            line.set_data(phase_x_values[lnum], phase_y_values[lnum])
    return lines


#plt.plot([0,40,40,60,60,100],[0,0,1,1,0,0])
#plt.plot([0,40,40,60,60,100],[2,2,3,3,2,2])
#plt.plot([0,40,40,60,60,100],[4,4,5,5,4,4])
anim1 = animation.FuncAnimation(fig1, svm, init_func=init_svm,
                               frames=360, interval=2, blit=True)


plt.show()