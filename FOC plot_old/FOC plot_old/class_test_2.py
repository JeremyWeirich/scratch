import math
PERIOD = 1/16000.0


def p1(t_zero, t_one, t_two, period):
    return (100 * (t_zero / 2)) / period


def p2(t_zero, t_one, t_two, period):
    return (100 * (t_one + (t_zero / 2))) / period


def p3(t_zero, t_one, t_two, period):
    return (100 * (t_two + (t_zero / 2))) / period


def p4(t_zero, t_one, t_two, period):
    return (100 * (t_one + t_two + (t_zero / 2))) / period


def calculate_sector(v_alpha_ref, v_beta_ref, tx, ty, tz):
    # NOT DONE
    return 1


class Sector(object):
    def __init__(self, daf, dbf, dcf, t1, t2, period):
        self.daf = daf
        self.dbf = dbf
        self.dcf = dcf
        self.t1 = t1
        self.t2 = t2
        self.period = period
        self.t0 = period - t1 - t2

    def calculate_duty_cycle(self):
        da = self.daf(self.t0, self.t1, self.t2, self.period)
        db = self.dbf(self.t0, self.t1, self.t2, self.period)
        dc = self.dcf(self.t0, self.t1, self.t2, self.period)
        return da, db, dc


def calculate_svm_duty_cycles(v_alpha_ref, v_beta_ref, v_supply, period):
    tx = (math.sqrt(3)*period*v_beta_ref)/v_supply
    ty = (period*((math.sqrt(3)*v_beta_ref)+(3*v_alpha_ref)))/(2*v_supply)
    tz = (period*((math.sqrt(3)*v_beta_ref)-(3*v_alpha_ref)))/(2*v_supply)

    # NOT DONE    # NOT DONE    # NOT DONE    # NOT DONE    # NOT DONE
    S1 = Sector(p4, p3, p1, -tz, tx, PERIOD)
    S2 = Sector(p2, p3, p1, -tz, tx, PERIOD)
    S3 = Sector(p1, p3, p2, -tz, tx, PERIOD)
    S4 = Sector(p1, p2, p3, -tz, tx, PERIOD)
    S5 = Sector(p2, p1, p3, -tz, tx, PERIOD)
    S6 = Sector(p3, p1, p2, -tz, tx, PERIOD)
    # NOT DONE    # NOT DONE    # NOT DONE    # NOT DONE    # NOT DONE

    sectors = [S1, S2, S3, S4, S5, S6]

    sector_index = calculate_sector(v_alpha_ref, v_beta_ref, tx, ty, tz)
    da, db, dc = sectors[sector_index].calculate_duty_cycle()