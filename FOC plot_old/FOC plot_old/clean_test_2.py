from math import sqrt


def p1(t0, t1, t2, period):
    del t1, t2
    return (100 * (t0 / 2)) / period


def p2(t0, t1, t2, period):
    del t2
    return (100 * (t1 + (t0 / 2))) / period


def p3(t0, t1, t2, period):
    del t1
    return (100 * (t2 + (t0 / 2))) / period


def p4(t0, t1, t2, period):
    return (100 * (t1 + t2 + (t0 / 2))) / period


def calculate_duty_cycles(period, t, duty_cycle_funcs):
    t1, t2 = t
    t0 = period - t1 - t2
    return [f(t0, t1, t2, period) for f in duty_cycle_funcs]


def calculate_t_params(period, v_alpha_ref, v_beta_ref, v_supply):
    tx = (sqrt(3) * period * v_beta_ref) / v_supply
    ty = (period * ((sqrt(3) * v_beta_ref)+(3 * v_alpha_ref))) / (2 * v_supply)
    tz = (period * ((sqrt(3) * v_beta_ref)-(3 * v_alpha_ref))) / (2 * v_supply)
    return tx, ty, tz


def calculate_svm_duty_cycles(v_alpha_ref, v_beta_ref, v_supply, period):
    tx, ty, tz = calculate_t_params(v_alpha_ref, v_beta_ref, v_supply, period)
    if v_beta_ref >= 0:
        if v_alpha_ref >= 0:
            if tz <= 0:  # Sector 1
                return calculate_duty_cycles(
                    period=period,
                    t=(1, 2),
                    duty_cycle_funcs=(1, 2, 3)
                )
            else:  # Sector 2
                return calculate_duty_cycles(
                    period=period,
                    t=(1, 2),
                    duty_cycle_funcs=(1, 2, 3)
                )
        else:
            if ty <= 0:  # Sector 3
                return calculate_duty_cycles(
                    period=period,
                    t=(1, 2),
                    duty_cycle_funcs=(1, 2, 3)
                )
            else:  # Sector 2
                return calculate_duty_cycles(
                    period=period,
                    t=(1, 2),
                    duty_cycle_funcs=(1, 2, 3)
                )
    else:
        if v_alpha_ref >= 0:
            if ty <= 0:
                sector = 5
                t1 = -ty
                t2 = -tz
            else:
                sector = 6
                t1 = -tx
                t2 = ty
        else:
            if tz <= 0:
                sector = 5
                t1 = -ty
                t2 = -tz
            else:
                sector = 4
                t1 = tz
                t2 = -tx
