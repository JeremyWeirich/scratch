import math
from math import pi as pi
from matplotlib import animation
import matplotlib.pyplot as plt


def to_radian(angle_in_degrees):
    return angle_in_degrees*(pi/180)


def clarke_transform(angle_in_radians, reference_voltage_vector):
    v_alpha = reference_voltage_vector * math.cos(angle_in_radians)
    v_beta = reference_voltage_vector * math.sin(angle_in_radians)
    return v_alpha, v_beta


def calculate_sector1_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_one + t_two + (t_zero / 2))) / period
    db = (100 * (t_two + (t_zero / 2))) / period
    dc = (100 * (t_zero / 2)) / period
    return da, db, dc


def calculate_sector2_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_one + (t_zero / 2))) / period
    db = (100 * (t_one + t_two + (t_zero / 2))) / period
    dc = (100 * (t_zero / 2)) / period
    return da, db, dc


def calculate_sector3_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_zero / 2)) / period
    db = (100 * (t_one + t_two + (t_zero / 2))) / period
    dc = (100 * (t_two + (t_zero / 2))) / period
    return da, db, dc


def calculate_sector4_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_zero / 2)) / period
    db = (100 * (t_one + (t_zero / 2))) / period
    dc = (100 * (t_one + t_two + (t_zero / 2))) / period
    return da, db, dc


def calculate_sector5_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_two + (t_zero / 2))) / period
    db = (100 * (t_zero / 2)) / period
    dc = (100 * (t_one + t_two + (t_zero / 2))) / period
    return da, db, dc


def calculate_sector6_duty_cycles(t_zero, t_one, t_two, period):
    da = (100 * (t_one + t_two + (t_zero / 2))) / period
    db = (100 * (t_zero / 2)) / period
    dc = (100 * (t_one + (t_zero / 2))) / period
    return da, db, dc


get_duty_cycles = {
    1: calculate_sector1_duty_cycles,
    2: calculate_sector2_duty_cycles,
    3: calculate_sector3_duty_cycles,
    4: calculate_sector4_duty_cycles,
    5: calculate_sector5_duty_cycles,
    6: calculate_sector6_duty_cycles,
}


def calculate_svm_duty_cycles(v_alpha_ref, v_beta_ref, v_supply, period):
    tx = (math.sqrt(3)*period*v_beta_ref)/v_supply
    ty = (period*((math.sqrt(3)*v_beta_ref)+(3*v_alpha_ref)))/(2*v_supply)
    tz = (period*((math.sqrt(3)*v_beta_ref)-(3*v_alpha_ref)))/(2*v_supply)
    if v_beta_ref >= 0:
        if v_alpha_ref >= 0:
            if tz <= 0:
                sector = 1
                t1 = -tz
                t2 = tx
            else:
                sector = 2
                t1 = ty
                t2 = tz
        else:
            if ty <= 0:
                sector = 3
                t1 = tx
                t2 = -ty
            else:
                sector = 2
                t1 = ty
                t2 = tz
    else:
        if v_alpha_ref >= 0:
            if ty <= 0:
                sector = 5
                t1 = -ty
                t2 = -tz
            else:
                sector = 6
                t1 = -tx
                t2 = ty
        else:
            if tz <= 0:
                sector = 5
                t1 = -ty
                t2 = -tz
            else:
                sector = 4
                t1 = tz
                t2 = -tx
    t0 = period-t1-t2
    duty_a, duty_b, duty_c = get_duty_cycles[sector](t0, t1, t2, period)
    return duty_a, duty_b, duty_c