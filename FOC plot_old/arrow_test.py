import numpy as np
import math
import matplotlib.patches as patches
from matplotlib import pyplot as plt
from matplotlib import animation
from math import pi as pi
import test_2

plt.close('all')


def to_radian(angle_in_degrees):
    return angle_in_degrees*(pi/180)


def to_degree(angle_in_radians):
    return angle_in_radians*(180/pi)

# Create figure
fig = plt.figure()
ax = fig.gca()

# Axes labels and title are established
ax.set_xticklabels([])
ax.set_ylim(-8, 8)
ax.yaxis.set_ticks(np.arange(-8, 9, 1))
ax.set_yticklabels([])
ax.set_xlim(-10, 10)
ax.xaxis.set_ticks(np.arange(-10, 11, 1))

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
for axis in ['left', 'bottom', 'right', 'top']:
    ax.spines[axis].set_position('center')

plt.gca().set_aspect('equal', adjustable='box')


def init():
    ax.add_patch(patch)
    return patch,

patch = patches.FancyArrowPatch([0, 0], [0, 0], lw=2, arrowstyle='simple', mutation_scale=70, edgecolor='none')


def animate(angle):
    global patch
    temp_alpha, temp_beta = test_2.clarke_transform(to_radian(angle), vref)
    patch.set_positions([0, 0], [temp_alpha, temp_beta])
    return patch,

switch_vector = range(0, 360, 60)

ma = 1
fs = 16000
ts = 1 / fs
vd = 12
vref_max = 12 / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3

v_a = []
v_b = []
ends = []

for index,angle in enumerate(switch_vector):
    v_a_sw = vector_max * math.cos(to_radian(angle))
    v_b_sw = vector_max * math.sin(to_radian(angle))
    print v_a_sw, v_b_sw, angle
    plt.plot([0, v_a_sw], [0, v_b_sw], 'k-', linewidth=2)
    ends.append([v_a_sw, v_b_sw])
    if v_b_sw < 0:
        vertical = 'top'
    else:
        vertical = 'bottom'
    if v_a_sw < 0:
        horizontal = 'right'
    else:
        horizontal = 'left'
    plt.text(v_a_sw, v_b_sw, 'V%s' % index, horizontalalignment=horizontal, verticalalignment=vertical)


ends.append(ends[0])

# make hexagon
for (old_x, old_y), (new_x, new_y) in zip(ends, ends[1:]):
    plt.plot([old_x, new_x], [old_y, new_y], 'k--', linewidth=2)

# make circle
circ = plt.Circle((0, 0), radius=vref_max, lw=2, fill=False, color='blue' )
ax.add_patch(circ)

anim = animation.FuncAnimation(fig, animate,
                               init_func=init,
                               interval=20,
                               frames=360,
                               blit=False)

plt.show()
