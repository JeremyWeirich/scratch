import math
import numpy as np
from math import pi as pi
from matplotlib import animation
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import test_2


def to_radian(angle_in_degrees):
    return angle_in_degrees*(pi/180)


def to_degree(angle_in_radians):
    return angle_in_radians*(180/pi)


fig = plt.figure()
plt.axis("equal")
ax = plt.axes(xlim=(-10, 10), ylim=(-8, 8))
ax.xaxis.set_ticks(np.arange(-10, 11, 1))
ax.set_xticklabels([])
ax.yaxis.set_ticks(np.arange(-8, 9, 1))
ax.set_yticklabels([])


plotlays, plotcols = [2], ["black","red"]
lines = []
for index in range(2):
    lobj = ax.plot([],[],lw=3,color=plotcols[index])[0]
    #lobj = patches.Arrow(0,0,4,0)
    lines.append(lobj)

print ax.plot([],[],lw=3,color=plotcols[index])
# initialization function: plot the background of each frame
def init():
    for line in lines:
        line.set_data([], [])
    return lines

def animate(angle):
    alpha_value = []
    beta_value = []
    temp_alpha, temp_beta = test_2.clarke_transform(to_radian(angle), vref)
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    rotor_angle = angle - 90
    temp_alpha, temp_beta = test_2.clarke_transform(to_radian(rotor_angle), (vd*0.25))
    alpha_value.append(temp_alpha)
    beta_value.append(temp_beta)

    for lnum,line in enumerate(lines):
        line.set_data([0, alpha_value[lnum]], [0, beta_value[lnum]])
    return lines

sva = range(0, 360, 1)
switch_vector = range(0, 360, 60)

ma = 1
fs = 16000
ts = 1 / fs
vd = 12
vref_max = 12 / math.sqrt(3)
vref = ma * vref_max
vector_max = (vd * 2)/3

v_a = []
v_b = []
for angle in sva:
    curr_v_a, curr_v_b = test_2.clarke_transform(to_radian(angle), vref)
    v_a.append(curr_v_a)
    v_b.append(curr_v_b)

ends = []

for index,angle in enumerate(switch_vector):
    v_a_sw = vector_max * math.cos(to_radian(angle))
    v_b_sw = vector_max * math.sin(to_radian(angle))
    print v_a_sw, v_b_sw, angle
    plt.plot([0, v_a_sw], [0, v_b_sw], 'k-', linewidth=2)
    ends.append([v_a_sw, v_b_sw])
    if v_b_sw < 0:
        vertical = 'top'
    else:
        vertical = 'bottom'
    if v_a_sw < 0:
        horizontal = 'right'
    else:
        horizontal = 'left'
    plt.text(v_a_sw, v_b_sw, 'V%s' % index, horizontalalignment=horizontal, verticalalignment=vertical)


ends.append(ends[0])

for (old_x, old_y), (new_x, new_y) in zip(ends, ends[1:]):
    plt.plot([old_x, new_x], [old_y, new_y], 'k--', linewidth=2)

circ = plt.Circle((0,0), radius=vref_max, lw=2, fill=False, color='blue' )
ax.add_patch(circ)

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_position('center')
ax.spines['top'].set_position('center')

#plt.arrow(0, 0, 2, 2, length_includes_head=True, width=0.01)
#plt.plot([0,-2],[0,-2])
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=360, interval=20, blit=True)
plt.show()