import math
from math import pi as pi
from matplotlib import animation
import matplotlib.pyplot as plt


def clarke_transform(angle_in_radians, reference_voltage_vector):
    v_alpha = reference_voltage_vector * math.cos(angle_in_radians)
    v_beta = reference_voltage_vector * math.sin(angle_in_radians)
    return v_alpha, v_beta