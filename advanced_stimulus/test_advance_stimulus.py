import re
stimulus_parts_regex = re.compile(r'^\[\[([\d. ,]+)\], \[([\w.\-,\(\) ]+)\]\]$')
segment_regex = re.compile(r'^(?:ramp|slope|sine|pulse|saw|noise|exp)\((?:-?\d+(?:\.\d+)? ?)+\)$')

sample_input = r'[[0, 1, 2, 3], [sine(4 0 4 11.2), 2.12, ramp(2 5), -5.3224]]'

match = re.match(stimulus_parts_regex, sample_input)
times, values = match.groups()

def split_and_strip(s):
    return [i.strip() for i in s.split(',')]

times = split_and_strip(times)
values = split_and_strip(values)

# print 'times', times
# print 'values', values

for value in values:
    try:
        print float(value)
    except ValueError:
        match = re.match(segment_regex, value)
        if match:

            category, data_string = value.split('(')
            data_string = data_string[:-1]
            print 'category:', category
            print 'data_string:', data_string
        else:
            print value, 'is invalid'