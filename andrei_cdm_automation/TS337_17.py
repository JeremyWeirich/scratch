import EDIABASLIB
import time
from pprint import pprint as eprint


# Init Status
INIT_RUNNING = 1
INIT_DONE = 2
INIT_DONE_NOK = 3
# Positions for KFS
RETRACTED = 0
EXTENDED = 1
INTERMEDIATE = 2
ASSEMBLED = 3
# Movements for KFS
NOT_MOVING = 0
EXTENDING = 1
RETRACTING = 2
# Length of results list values for movement and for an assembly
MOVE_LENGTH = -2
ASSEMBLY_LENGTH = 1
# Timeouts for movements and initialization
MOVE_TIMEOUT = 5  # seconds for movement
INIT_TIMEOUT = 10  # seconds for init
# Job Status Checks
JOB_ERROR = 'ERROR_ECU_BUSY_REPEAT_REQUEST'
JOB_OKAY = 'OKAY'
# Passing and Failing Condition Checks
JOB_PASSED = True
PRG = r'CDM02'
TS = "TS337_17"
Objective = """Objective 17: KFS Diagnostic Jobs - 8 EDIABAS Sequences
This objective tests the following KFS diagnostics job:
STEUERN_ROUTINE_HOOD_ORNAMENT
The following diagnostic job must also be completed in TS337_7:
STATUS_LESEN_HOTEL_MODE
The following diagnostic job must also be completed in TS337_46:
STEUERN_ROUTINE_HOOD_ORNAMENT
"""


# call the rrr status and return it in a dictionary
def get_status():
    status = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;HOOD_ORNAMENT;RRR')
    return dict(
        job_status=status[1]['JOB_STATUS'],
        movement=status[1]['STAT_HOOD_ORNAMENT_MOVEMENT'],
        position=status[1]['STAT_HOOD_ORNAMENT_POSITION'],
        initialization=status[1]['STAT_HOOD_ORNAMENT_INITIALISATION'],
    )


# call specified movement and then call rrr status over and over in a loop until position is desired
def run_job(job, arg, end, timeout, target_parameter, exp_status):
    print job, arg
    results = []
    # get the status before actually running the job
    initial_rrr_response = get_status()
    results.append(initial_rrr_response)
    # run the job
    str_response = EDIABASLIB.apiJob(PRG, job, arg)
    job_status = str_response[1]['JOB_STATUS']
    if job_status == exp_status:
        rrr_response = dict(
            job_status=None,
            movement=None,
            position=None,
            initialization=None,
        )
        start = time.time()
        # while the rrr status position is not the intended final position
        # check the rrr status continually and store all the data in results list for later
        while rrr_response[target_parameter] != end and time.time() - start < timeout:
            rrr_response = get_status()
            results.append(rrr_response)
            if rrr_response['job_status'] != 'OKAY':
                print 'This job status failed during the movement!'
        return results
    else:
        print 'Job {} with argument {} failed with response {}.'.format(job, arg, job_status)


# function to only run a movement job
def run_move_job(job, arg):
    print job, arg
    # get the status before actually running the job
    # run the job
    str_response = EDIABASLIB.apiJob(PRG, job, arg)
    move_job_status = str_response[1]['JOB_STATUS']
    with open(TS + '.txt', 'a') as f:
        f.write("Job {} with argument {} had status: {}\n\n".format(job, arg, move_job_status))
    return move_job_status


def run_status_job(move_job_status, end, timeout, target_parameter, exp_status):
    results = []
    if move_job_status == exp_status:
        rrr_response = dict(
            job_status=None,
            movement=None,
            position=None,
            initialization=None,
        )
        start = time.time()
        # while the rrr status position is not the intended final position
        # check the rrr status continually and store all the data in results list for later
        while rrr_response[target_parameter] != end and time.time() - start < timeout:
            rrr_response = get_status()
            results.append(rrr_response)
            if rrr_response['job_status'] != 'OKAY':
                print 'This job status failed during the movement!'
        return results
    else:
        print 'The selected job and argument failed with status {}.'.format(move_job_status)


def run_diag_job(job, arg, result):
    print job, arg
    response = EDIABASLIB.apiJob(PRG, job, arg)
    if response[1]['JOB_STATUS'] == result:
        print "Diagnostic job passed"
        return result
    else:
        print "Diagnostic job failed"
    with open(TS + '.txt', 'a') as f:
        f.write("Job {} with argument {} is running:\n\n".format(job, arg))
        f.write("Result is {}\n\n".format(response[1]['JOB_STATUS']))


def verify(condition, message):
    if not condition:
        print message
    return condition


def movement_check(status_data, init_position, no_movement, mid_position, mid_movement, end_position, length):
    # initial position and movement status report verification
    passed = True
    passed &= verify(status_data[0]['position'] == init_position, 'initial position is wrong')
    passed &= verify(status_data[0]['movement'] == no_movement, 'initial movement is wrong')
    # middle position and movement status report verification
    if length == -2:
        for index, middle_entry in enumerate(status_data[1:length]):
            passed &= verify(middle_entry['position'] == mid_position, 'middle position is wrong')
            passed &= verify(middle_entry['movement'] == mid_movement, 'middle movement is wrong')
    elif length == 1:
        passed &= verify(status_data[1]['position'] == mid_position, 'middle position is wrong')
        passed &= verify(status_data[1]['movement'] == mid_movement, 'middle movement is wrong')
    else:
        print 'THIS IS NOT A VALID TEST, PLEASE REVIEW!'
    # end position and movement status report verification
    passed &= verify(status_data[-1]['position'] == end_position, 'end position is wrong')
    passed &= verify(status_data[-1]['movement'] == no_movement, 'end movement is wrong')
    print "Movement passed"


def initialization_check(status_data, init_start, init_end):
    passed = True
    passed &= verify(status_data[0]['initialization'] == init_start, 'beginning initialization is wrong')
    passed &= verify(status_data[-1]['initialization'] == init_end, 'end initialization is wrong')
    print "Initialization passed"


def rrr_results_text(data_text, job_text, arg_text):
    with open(TS + '.txt', 'a') as f:
        f.write("Job {} with argument {} is running with status reads:\n\n".format(job_text, arg_text))
        for entry in data_text:
            for key, value in entry.items():
                f.write("{}: {}\n".format(key, value))
            f.write("\n")


def move_results_text(move_status, job_text, arg_text):
    with open(TS + '.txt', 'a') as f:
        f.write("Job {} with argument {} had status: {}\n\n".format(job_text, arg_text, move_status))


def standard_movement_tests():
    # validate initialization
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION', INIT_DONE, INIT_TIMEOUT, 'initialization'
                   , JOB_OKAY)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_DONE, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    # validate retract data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;RETRACT', RETRACTED, MOVE_TIMEOUT, 'position', JOB_OKAY)
    movement_check(data, EXTENDED, NOT_MOVING, INTERMEDIATE, RETRACTING, RETRACTED, MOVE_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;RETRACT')

    # validate extend data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;EXTEND', EXTENDED, MOVE_TIMEOUT, 'position', JOB_OKAY)
    movement_check(data, RETRACTED, NOT_MOVING, INTERMEDIATE, EXTENDING, EXTENDED, MOVE_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;EXTEND')

    # validate assembly data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;ASSEMBLING_POSITION', ASSEMBLED, MOVE_TIMEOUT, 'position'
                   , JOB_OKAY)
    movement_check(data, EXTENDED, NOT_MOVING, INTERMEDIATE, RETRACTING, ASSEMBLED, ASSEMBLY_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;ASSEMBLING_POSITION')

    # validate retract data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;RETRACT', RETRACTED, MOVE_TIMEOUT, 'position', JOB_OKAY)
    movement_check(data, ASSEMBLED, NOT_MOVING, INTERMEDIATE, RETRACTING, RETRACTED, MOVE_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;RETRACT')

    # validate assembly data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;ASSEMBLING_POSITION', ASSEMBLED, MOVE_TIMEOUT, 'position',
                   JOB_OKAY)
    movement_check(data, RETRACTED, NOT_MOVING, INTERMEDIATE, EXTENDING, ASSEMBLED, ASSEMBLY_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;ASSEMBLING_POSITION')

    # validate extend data
    data = run_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;EXTEND', EXTENDED, MOVE_TIMEOUT, 'position', JOB_OKAY)
    movement_check(data, ASSEMBLED, NOT_MOVING, INTERMEDIATE, EXTENDING, EXTENDED, MOVE_LENGTH)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;EXTEND')

    # validate diagnostic stop routine
    run_diag_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STPR', 'ERROR_ECU_CONDITIONS_NOT_CORRECT')


def init_advanced_tests():
    run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_DONE, INIT_TIMEOUT, 'initialization', JOB_ERROR)
    initialization_check(data, INIT_RUNNING, INIT_DONE)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_RUNNING, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_diag_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STPR', 'OKAY')
    data = run_status_job(movement, INIT_RUNNING, INIT_RUNNING, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_RUNNING, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_diag_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STPR', 'OKAY')
    data = run_status_job(movement, INIT_RUNNING, INIT_RUNNING, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_RUNNING, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_diag_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STPR', 'OKAY')
    data = run_status_job(movement, INIT_RUNNING, INIT_RUNNING, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_RUNNING, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_diag_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STPR', 'OKAY')
    data = run_status_job(movement, INIT_RUNNING, INIT_RUNNING, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_RUNNING)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')

    movement = run_move_job('steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')
    data = run_status_job(movement, INIT_DONE_NOK, INIT_TIMEOUT, 'initialization', JOB_OKAY)
    initialization_check(data, INIT_RUNNING, INIT_DONE_NOK)
    rrr_results_text(data, 'steuern_routine', 'ARG;HOOD_ORNAMENT;STR;INITIALISATION')


if __name__ == '__main__':
    # clear text file by opening in write mode
    with open(TS + '.txt', 'w') as f:
        pass
    # Function standard_movement_tests() is for TS337_17_1-16 manual tests.
    # To run hotel mode tests for TS337_17_158, set Hotel mode manually
    # and re-run standard_movement_tests() function
    standard_movement_tests()
    init_advanced_tests()
    # validate initialization again with new setup

