import json
import glob
import os
import time

from bokeh.plotting import figure
from bokeh.embed import components


#  --------------------------------------------------
RESULTS_PATH = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\bokeh_plot\shrunk_results\\'
OUTPUT = r'george_results.html'
#  --------------------------------------------------


seen_reqs = []
seen_objs = []


def get_results():
    for f in glob.glob('{}*.txt'.format(RESULTS_PATH)):
        at = os.path.split(f)[1][:-4]
        with open(f, 'r') as log:
            yield at, json.loads(log.read())


def make_header(f):
    f.write('''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Report</title>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link
        href="http://cdn.pydata.org/bokeh/release/bokeh-0.12.3.min.css"
        rel="stylesheet" type="text/css">
    <link
        href="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.3.min.css"
        rel="stylesheet" type="text/css">

    <script src="http://cdn.pydata.org/bokeh/release/bokeh-0.12.3.min.js"></script>
    <script src="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.3.min.js"></script>
    <style>
        pre {font-family: "Verdana"}
    </style>
</head>
<body>
<script async="async">
    function openTest(id) {
        var test_info = document.getElementById(id);
        if (test_info.className.indexOf("w3-show") == -1) {
            test_info.className += " w3-show";
        } else {
            test_info.className = test_info.className.replace(" w3-show", "");
        }
    }
</script>
\n
<h1 class="w3-container w3-padding-4">Placeholder Text</h1>
<hr>
<div class="w3-container">\n
''')


def make_footer(f):
    f.write('''
</div>
</body>
</html>''')


def add_at(f, at, passes):
    req, obj, test = at.split('_')
    obj = req + '_' + obj
    if req not in seen_reqs:
        seen_reqs.append(req)
        f.write('''
<h2>{}</h2>
'''.format(req))
    if obj not in seen_objs:
        seen_objs.append(obj)
        f.write('''
<h4>{}</h4>
'''.format(obj))

    if all([value is True for value in passes.values()]):
        color = 'light-green'
    else:
        color = 'red'
    f.write('''
<div class="w3-accordion w3-padding-4">
<div onclick="openTest('{0}')" class="w3-btn-block w3-left-align w3-{1}">{0}</div>
<div id="{0}" class="w3-accordion-content w3-container">'''.format(at, color))


def close_accordion(f):
    f.write('''
</div>
</div>
''')


def add_description(f, desc):
    try:
        f.write('<pre class="w3-container"><strong>Description:</strong><br/>{}</pre><hr>\n'.format(desc))
    except UnicodeEncodeError:
        f.write('<p>desc has some weird stuff</p>\n')


def add_data(f, write, verify):
    f.write('''<div class=w3-container>
<div class="w3 container w3-third">
    <table class="w3-table w3-striped">
        <tr>
            <th><strong>Write Variable</strong></th>
            <th><strong>Value</strong></th>
        </tr>''')
    for signal in write.values():
        f.write('''
        <tr>
            <td>{0}</td>
            <td>{1}</td>
        </tr>'''.format(signal['name'], signal['value']))
    f.write('''
    </table>
</div>\n\n''')

    f.write('''<div class="w3 container w3-twothird">
    <table class="w3-table w3-striped">
        <tr>
            <th><strong>Verify Variable</strong></th>
            <th><strong>Operator</strong></th>
            <th><strong>Value</strong></th>
            <th><strong>Tolerance</strong></th>
        </tr>''')
    for signal in verify.values():
        f.write('''
        <tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
        </tr>'''.format(signal['name'], signal['operator'], signal['value'], signal['tolerance']))
    f.write('''
    </table>
</div>
</div>
<hr>\n''')


def add_results(f, results, verify):
    for signal, (times, data) in results.items():
        verify_sig = verify[signal]
        p = figure(title=signal, width=1200, plot_height=300, tools=[])
        p.line(times, data)

        if verify_sig['operator'] in ['>=', '>']:
            p.triangle(verify_sig['value'][0], verify_sig['value'][1], color='red', size=6)
        elif verify_sig['operator'] in ['<=', '<']:
            p.inverted_triangle(verify_sig['value'][0], verify_sig['value'][1], color='red', size=6)
        elif verify_sig['operator'] in ['!=']:
            p.x(verify_sig['value'][0], verify_sig['value'][1], color='red', size=6)
        else:
            p.circle(verify_sig['value'][0], verify_sig['value'][1], color='red', size=6)
        errs = [i * verify_sig['tolerance'] for i in verify_sig['value'][1]]
        y_err_x = []
        y_err_y = []
        for px, py, err in zip(verify_sig['value'][0], verify_sig['value'][1], errs):
            y_err_x.append((px, px))
            y_err_y.append((py - err, py + err))
            p.multi_line(y_err_x, y_err_y, color='red')

        script, div = components(p)
        f.write(div)
        f.write('\n')
        f.write(script)
        f.write('\n')


def add_result(f, r, at):
    add_at(f, at, r['passes'])
    add_description(f, r['description'])
    add_data(f, r['write'], r['verify'])
    add_results(f, r['results'], r['verify'])
    close_accordion(f)


def main():
    start = time.time()
    with open(OUTPUT, 'w') as report:
        make_header(report)
        for at, result in get_results():
            add_result(report, result, at)
        make_footer(report)

    print 'time taken: {}s'.format(round(time.time() - start, 2))


if __name__ == '__main__':
    main()
