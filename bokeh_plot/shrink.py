import json
import glob
import os


def get_results():
    p = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\bokeh_plot\new_json_results\\'
    for f in glob.glob('{}*.txt'.format(p)):
        at = os.path.split(f)[1][:-4]
        with open(f, 'r') as log:
            yield at, json.loads(log.read())


def shrink(times, values):
    new_times = [times[0]]
    new_values = [values[0]]
    for c_time, n_time, c_val, n_val in zip(times, times[1:], values, values[1:]):
        if c_val != n_val:
            new_times.extend([c_time, n_time])
            new_values.extend([c_val, n_val])
    new_times += [times[-1]]
    new_values += [values[-1]]
    return new_times, new_values


def shrink_dict(results_dict):
    shrunk_dict = {}
    for signal, (times, values) in results_dict.items():
        shrunk_dict[signal] = shrink(times, values)
    return shrunk_dict


for at, data in get_results():
    print 'working on {}'.format(at)
    new_path = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\bokeh_plot\shrunk_results\\' + at + '.txt'
    data['results'] = shrink_dict(data['results'])
    with open(new_path, 'w') as log:
        log.write(json.dumps(data))
