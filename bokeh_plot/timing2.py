from bokeh.plotting import figure, output_file, show
from bokeh.layouts import row

y0 = [
    0.5, 8.2005, 7.2005, 5.7005, 5.7005, 5.7005, 5.7005, 5.7005, 5.7005, 0.5, 8.2005, 6.2005, 34.1005,
    48.200500000000005, 34.1005, 13.100499999999998, 0.5, 8.2005, 9.2005, 9.2005, 0.5, 8.2005, 11.2005, 5.7005,
    11.2005, 0.5, 8.2005, 27.200499999999998]
x = range(len(y0))

y1 = [0.61, 7.51, 7.35, 6.38, 6.22, 6.03, 6.2, 6.32, 6.22, 0.61, 7.79, 6.63, 35.14, 48.73, 34.18, 12.9, 0.51, 7.49, 9.86, 9.63, 0.61, 7.69, 11.76, 5.98, 11.76, 0.51, 7.6, 28.25]

diff = [j - i for i, j in zip(y0, y1)]

output_file('lines2.html')

p1 = figure(title="expected vs actual time", x_axis_label='test #', y_axis_label='duration')

p1.line(x, y0, line_color='blue', legend='expected')
p1.circle(x, y0, fill_color='white', line_color='blue', size=6)
p1.line(x, y1, line_color='red', legend='actual')
p1.circle(x, y1, fill_color='white', line_color='red', size=6)

p2 = figure(title="additional time per test", x_axis_label='test #', y_axis_label='delta')
p2.line(x, diff)

p3 = figure(title="additional time of tests", x_axis_label='test length [s]', y_axis_label='additional duration [s]')
scaled_diff = [x for (y, x) in sorted(zip(y0, diff))]
p3.line(sorted(y0), scaled_diff)
p3.circle(sorted(y0), scaled_diff, fill_color='white', line_color='blue', size=6)

show(row(p1, p2, p3))
