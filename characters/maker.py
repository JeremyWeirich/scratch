from random import choice, sample


FIRSTS = {
    'm': [
        'andrei',
        'barros',
        'carra',
        'danik',
        'elvic',
        'fathil',
    ],
    'f': [
        'alia',
        'basha',
        'cursha',
        'danika'
    ],
}
LASTS = [
    'cantemir',
    'moldovar',
    'vesa',
    'guran',
]
BODY = [
    'skinny',
    'thin',
    'healthy',
    'athletic',
    'portly',
    'fat',
    'obese'
]
HEIGHT = [
    'short',
    'tiny',
    'medium',
    'average',
    'standard',
    'tall',
    'huge',
]
APPEARANCES = {
    'm': [
        'a walrus mustache',
        'a bald head',
        'a thick beard',
    ],
    'f': [
        'dark hair',
        'pimples',
        'a gentle smile',
    ],
}
TRAITS = [
    'coughs a lot',
    'blushes',
    'stares at handsome men',
    'loves animals',
    'distracted easily',
    'trails off sentences',
    'interrupts',
    'nervous laugh',
    'incorrectly predicts how you will finish sentence',
]


def make(gender=None):
    gender = choice(['m', 'f']) if not gender else gender
    name = choice(FIRSTS[gender]) + ' ' + choice(LASTS)
    appearance = '{} and {} with {} and {}'.format(choice(BODY), choice(HEIGHT), *sample(APPEARANCES[gender], 2))
    trait = choice(TRAITS)
    return '[%s] %s\n%s\n%s\n' % (gender, name, appearance, trait)


if __name__ == '__main__':
    for _ in range(3):
        print make()