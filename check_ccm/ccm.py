import os
import glob

p = r'N:\Users\Jeremy Weirich\Testing\CDM\I503\2_6_0\CoSi Automated Test Results\SVC CAN logs'


def check(si):
    i = int(si, 16)
    masked = i & 60
    shifted = masked >> 2
    return shifted == 10


def read_file(fpath):
    print 'reading', fpath
    with open(fpath, 'r') as f:
        for line in f.readlines()[6:-3]:
            mode = line.split(' ')[6]
            if mode == '40':
                time = float(line.split(' ')[0])
                byte_3 = line.split(' ')[9]
                if not check(byte_3):
                    print time

if __name__ == '__main__':
    for n in glob.glob('{}/*'.format(p)):
        read_file(n)