import ast
from numbers import Number
from openpyxl import load_workbook, Workbook


# -----------------------------------------------------------------------------------------------------------
# ----------------------           Change these lines                ----------------------------------------
# -----------------------------------------------------------------------------------------------------------
filename = r"N:\Users\Andrei Vesa\ELSDM\ELSDM scripts\Jeremy Tests\VTC-P14253004_FORD_U55X_ELSDM_0.8.4.xlsx"
sheet_name = "Functional Auto"
# -----------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------


wb = load_workbook(filename, use_iterators=True, data_only=True)
sheet = wb.get_sheet_by_name(sheet_name)

new_wb = Workbook()
new_sheet = new_wb.active


def dump_test_data(at, description, write_dict, verify_dict):
    if None in write_dict:
        del write_dict[None]
    if None in verify_dict:
        del verify_dict[None]
    # write dict -------------------------------------------------------------------------------------
    for w_var, w_val in write_dict.items():
        if w_var == "TX_RAW_CAN_HEX":
            kickout_times = {}
            kickouts = {}
            w_val = ast.literal_eval(w_val)
            times, values = w_val
            for index, value in enumerate(values):
                prefix = None
                msg_id_hex, msg_data = value
                msg_id = int(msg_id_hex, 16)
                if msg_id == int("795", 16):
                    prefix = "PhysReq_"
                elif msg_id == int("7DF", 16):
                    prefix = "FuncReq_"
                else:
                    assert False, "Write dict has ID other than 795/7DF: {}".format(msg_id_hex)
                if prefix:
                    kicktype = prefix + "Kickout"
                    if kicktype not in kickout_times:
                        kickout_times[kicktype] = []
                        kickouts[kicktype] = []
                    kickout_times[kicktype].append(times[index])
                    kickout_times[kicktype].append(round(times[index]+0.0005, 5))
                    kickouts[kicktype].append(1)
                    kickouts[kicktype].append(0)

                msg_data = msg_data.split()
                msg_data = [int(i, 16) for i in msg_data]
                for i in range(8):
                    var = prefix + str(i+1)
                    if var not in write_dict:
                        write_dict[var] = [[], []]
                    write_dict[var][0].append(times[index])
                    write_dict[var][1].append(msg_data[i])

            for name, times in kickout_times.items():
                write_dict[name] = [times, kickouts[name]]

    # verify dict -------------------------------------------------------------------------------------
    for v_var, temp_val in verify_dict.items():
        v_val, tol = temp_val
        if v_var == "RX_RAW_CAN_HEX":
            v_val = ast.literal_eval(v_val)
            times, values = v_val
            for index, value in enumerate(values):
                prefix = None
                msg_id_hex, msg_data = value
                msg_id = int(msg_id_hex, 16)
                if msg_id == int("79D", 16):
                    prefix = "PhysRes_"
                else:
                    assert False, "Verify dict has ID other than 79D: {}".format(msg_id_hex)

                msg_data = msg_data.split()
                msg_data = [int(i, 16) for i in msg_data]
                for i in range(8):
                    var = prefix + str(i+1)
                    if var not in verify_dict:
                        verify_dict[var] = [[[], []], tol]
                    verify_dict[var][0][0].append(times[index])
                    verify_dict[var][0][1].append(msg_data[i])

    if "TX_RAW_CAN_HEX" in write_dict:
        del write_dict["TX_RAW_CAN_HEX"]
    if "RX_RAW_CAN_HEX" in verify_dict:
        del verify_dict["RX_RAW_CAN_HEX"]

    # add data to excel --------------------------------------------------------------------------------------------
    is_firstline = True
    firstline = [None, None, at, description, None, None, None, None, None, None, None]

    num_writes = len(write_dict)
    num_verifies = len(verify_dict)
    biggest = max(num_writes, num_verifies)
    nextlines = []
    for i in range(biggest-1):
        nextlines.append([None, None, None, None, None, None, None, None, None, None, None])
    counter = -1
    for key in sorted(write_dict.keys()):
        key, value = key, write_dict[key]
        if is_firstline:
            firstline[4] = key
            if isinstance(value, Number):
                firstline[5] = value
            else:
                firstline[5] = str(value)
            is_firstline = False
        else:
            nextlines[counter][4] = key
            if isinstance(value, Number):
                nextlines[counter][5] = value
            else:
                nextlines[counter][5] = str(value)
        counter += 1

    is_firstline = True
    counter = -1
    for key in sorted(verify_dict.keys()):
        key, temp_value = key, verify_dict[key]
        value, tol = temp_value
        if is_firstline:
            firstline[6] = key
            firstline[7] = "'=="
            if isinstance(value, Number):
                firstline[8] = value
            else:
                firstline[8] = str(value)
            firstline[9] = tol
            is_firstline = False
        else:
            nextlines[counter][6] = key
            nextlines[counter][7] = "'=="
            if isinstance(value, Number):
                nextlines[counter][8] = value
            else:
                nextlines[counter][8] = str(value)
            nextlines[counter][9] = tol
        counter += 1

    new_sheet.append(firstline)
    for line in nextlines:
        new_sheet.append(line)

store_at = None
store_desc = None
store_tol = None
in_test = False
write_dict = {}
verify_dict = {}
for format_row in sheet.iter_rows():
    row = [cell.value for cell in format_row]
    row.insert(7, None)
    at = row[2]
    description = row[3]
    write_var = row[4]
    write_val = row[5]
    verify_var = row[6]
    verify_val = row[8]
    tolerance = row[9]
    row_group = row
    if at:
        if at.startswith("AT"):
            if at.count("_") == 2:
                # New test header
                if in_test:
                    dump_test_data(store_at, store_desc, write_dict, verify_dict)
                write_dict = {}
                verify_dict = {}
                store_at = at
                store_desc = description
                in_test = True

                write_dict[write_var] = write_val
                verify_dict[verify_var] = [verify_val, tolerance]
            else:
                if in_test:
                    dump_test_data(store_at, store_desc, write_dict, verify_dict)
                    in_test = False
                new_sheet.append(row_group)
    else:
        if in_test:
            write_dict[write_var] = write_val
            verify_dict[verify_var] = [verify_val, tolerance]


dump_test_data(store_at, store_desc, write_dict, verify_dict)
new_wb.save(r"N:\Users\Andrei Vesa\ELSDM\ELSDM scripts\Jeremy Tests\converted_functional_auto.xlsx")
