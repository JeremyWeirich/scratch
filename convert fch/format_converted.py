from openpyxl import Workbook, load_workbook
from openpyxl.styles import Font, PatternFill, Alignment, Border, Side
import time

start = time.clock()

LEFT = Alignment(horizontal='left', vertical='top')
CENTER = Alignment(horizontal='center', vertical='center')
RIGHT = Alignment(horizontal='right', vertical='center')

WHITE = PatternFill(start_color='FFFFFF', end_color='FFFFFF', fill_type='solid')
ORANGE = PatternFill(start_color='FF8001', end_color='FF8001', fill_type='solid')
YELLOW = PatternFill(start_color='FFFF00', end_color='FFFF00', fill_type='solid')
GREEN = PatternFill(start_color='92D050', end_color='92D050', fill_type='solid')
CYAN = PatternFill(start_color='00B0F0', end_color='00B0F0', fill_type='solid')

TITLE = Font(size=12, bold=True, name='Times New Roman')
REG = Font(size=12, name='Times New Roman')

STHICK = Side(border_style='thick', color='000000')
STHIN = Side(border_style='thin', color='000000')
THICK = Border(
    left=STHICK,
    right=STHICK,
    top=STHICK,
    bottom=STHICK,
)
THIN = Border(
    left=STHIN,
    right=STHIN,
    top=STHIN,
    bottom=STHIN,
)

doc = r'N:\Users\Andrei Vesa\ELSDM\ELSDM scripts\Jeremy Tests\format\part_II_before.xlsx'
savepath = r'N:\Users\Andrei Vesa\ELSDM\ELSDM scripts\Jeremy Tests\format\part_II_after.xlsx'

wb = load_workbook(doc)

ws = wb.active


def apply_border(cell_range_string, side):
    rows = list(ws.iter_rows(cell_range_string))
    max_y = len(rows) - 1  # index of the last row
    for pos_y, cells in enumerate(rows):
        max_x = len(cells) - 1  # index of the last cell
        for pos_x, cell in enumerate(cells):
            border = Border(
                left=cell.border.left,
                right=cell.border.right,
                top=cell.border.top,
                bottom=cell.border.bottom
            )
            if pos_x == 0:
                border.left = side
            if pos_x == max_x:
                border.right = side
            if pos_y == 0:
                border.top = side
            if pos_y == max_y:
                border.bottom = side

            # set new border only if it's one of the edge cells
            if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
                cell.border = border


def format_requirement(r):
    r[1].fill = WHITE
    r[1].font = REG
    r[1].alignment = CENTER
    r[1].border = THIN
    r[2].fill = GREEN
    r[2].font = TITLE
    r[2].alignment = CENTER
    r[2].border = THIN
    for i in range(3, 11):
        r[i].fill = GREEN
        r[i].alignment = LEFT
        r[i].font = REG
        r[i].border = THIN
    ws.merge_cells('{}:{}'.format(r[4].coordinate, r[9].coordinate))


def format_objective(r):
    r[1].fill = WHITE
    r[1].font = REG
    r[1].alignment = CENTER
    r[1].border = THIN
    r[2].fill = YELLOW
    r[2].font = TITLE
    r[2].alignment = CENTER
    r[2].border = THIN
    r[3].fill = YELLOW
    r[3].font = REG
    r[3].alignment = LEFT
    r[3].border = THIN
    for i in range(4, 6):
        r[i].fill = ORANGE
        r[i].alignment = CENTER
        r[i].font = TITLE
        r[i].border = THIN
    for i in range(6, 10):
        r[i].fill = CYAN
        r[i].alignment = CENTER
        r[i].font = TITLE
        r[i].border = THIN
    r[10].fill = WHITE
    r[10].font = REG
    r[10].alignment = CENTER
    r[10].border = THIN


def format_test(r):
    for i in range(1, 11):
        r[i].fill = WHITE
        r[i].font = REG
        if i != 3:
            r[i].alignment = CENTER


in_test = False
current_range = ['', '']


def end_test_block():
    # disabled
    cellrange = '{}:{}'.format(current_range[0], current_range[1]).replace('C', 'B')
    apply_border(cellrange, STHIN)
    ws.merge_cells(cellrange)
    # at
    cellrange = '{}:{}'.format(current_range[0], current_range[1])
    apply_border(cellrange, STHIN)
    ws.merge_cells(cellrange)
    # req
    cellrange = '{}:{}'.format(current_range[0], current_range[1]).replace('C', 'D')
    apply_border(cellrange, STHIN)
    ws.merge_cells(cellrange)
    # write
    cellrange = '{}:{}'.format(current_range[0], current_range[1]).replace('C', 'F')
    cellrange = 'E' + cellrange[1:]
    apply_border(cellrange, STHIN)
    # verify
    cellrange = '{}:{}'.format(current_range[0], current_range[1]).replace('C', 'J')
    cellrange = 'G' + cellrange[1:]
    apply_border(cellrange, STHIN)
    # PR
    cellrange = '{}:{}'.format(current_range[0], current_range[1]).replace('C', 'K')
    apply_border(cellrange, STHIN)
    ws.merge_cells(cellrange)

for row in ws.iter_rows(row_offset=4):
    AT = row[2].value
    if AT is not None:
        if AT.startswith('AT'):
            if AT.count('_') == 0:
                format_requirement(row)
                if in_test:
                    in_test = False
                    end_test_block()
            elif AT.count('_') == 1:
                format_objective(row)
                if in_test:
                    in_test = False
                    end_test_block()
            elif AT.count('_') == 2:
                if in_test:
                    end_test_block()
                in_test = True
                current_range[0] = row[2].coordinate
                format_test(row)
    else:
        if in_test:
            current_range[1] = row[2].coordinate
            format_test(row)
end_test_block()

wb.save(savepath)
end = time.clock()
print "Finished in {} seconds".format(end - start)
