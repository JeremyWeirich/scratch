import glob
import pprint
import os
from openpyxl import load_workbook

p = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM'

count = {}

def its_a_satellite_tab(tab):
    if 'sat' in tab.lower():
        return True
    if 'LS' in tab:
        return True
    if 'RS' in tab:
        return True
    return False

for filename in glob.glob('{}/*.xlsx'.format(p)):
    simple = os.path.basename(filename)
    print 'checking', simple
    wb = load_workbook(filename, read_only=True)
    count[simple] = {
        'asil': 0,
        'normal': 0,
        'total': 0,
    }
    for sheet_name in wb.sheetnames:
        if "Auto" in sheet_name and its_a_satellite_tab(sheet_name):
            asil = False
            sheet = wb.get_sheet_by_name(sheet_name)
            for frow in sheet.iter_rows():
                row = [cell.value for cell in frow]
                if row[2]:
                    if row[2].startswith("AT"):
                        if row[2].count("_") == 0:
                            asil = row[3] and row[3].startswith("ASIL")
                        elif row[2].count("_") == 2:
                            count[simple]['total'] += 1
                            if asil:
                                count[simple]['asil'] += 1
                            else:
                                count[simple]['normal'] += 1
print '\n'
print '{:40}{:>7}{:>7}'.format('Function', 'Normal', 'ASIL')
for function, values in count.items():
    print '{:40}{:7}{:7}'.format(function, values['total'], values['asil'])
