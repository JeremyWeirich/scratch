from openpyxl import load_workbook
import os

os.chdir(r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Results\Executed_Testcases\VTC-P14106001_BMW_CDM-2_1_0')
filename = r'VTC-P14106001_BMW_CDM_CoSi-2_1_0.xlsx'

num_asil = 0
num_reg = 0

wb = load_workbook(filename, use_iterators=True, read_only=True, data_only=True)
for sheet_name in wb.sheetnames:
    asil = False
    if "Auto" in sheet_name:
        sheet = wb.get_sheet_by_name(sheet_name)
        for frow in sheet.iter_rows():
            row = [cell.value for cell in frow]
            if row[2]:
                if row[2].startswith("AT"):
                    if row[2].count("_") == 0:
                        if row[3] and row[3].startswith("ASIL A Requirement"):
                            asil = True
                        else:
                            asil = False
                    elif row[2].count("_") == 2:
                        if asil:
                            num_asil += 1
                        else:
                            num_reg += 1

print "ASIL: {}\nReg: {}".format(num_asil, num_reg)
