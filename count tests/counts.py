import glob
import os
import copy
from openpyxl import load_workbook, Workbook


# ----------------------------------------------------------------------------------------------------------------------
CVS_PATH = r'C:\Users\Jeremy Weirich\Desktop\CVS_personal\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM'
# ----------------------------------------------------------------------------------------------------------------------


count = dict(satellite={}, high={}, all={})
last_lines = {}


def its_a_satellite_tab(tab):
    if 'sat' in tab.lower():
        return True
    if 'LS' in tab:
        return True
    if 'RS' in tab:
        return True
    return False


def get_count(path):
    for filename in glob.glob('{}/VTC*.xlsx'.format(path)):
        function = os.path.basename(filename)
        last_lines[function] = {}
        print 'checking', function
        wb = load_workbook(filename, read_only=True)

        categories = {'asil': 0,'normal': 0,'total': 0}
        count['satellite'][function] = copy.copy(categories)
        count['high'][function] = copy.copy(categories)
        count['all'][function] = copy.copy(categories)

        for sheet_name in wb.sheetnames:
            last_lines[function][sheet_name] = 0
            sheet = wb.get_sheet_by_name(sheet_name)
            if "Auto" in sheet_name:
                satellite = its_a_satellite_tab(sheet_name)
                asil = False
                for frow in sheet.iter_rows():
                    last_lines[function][sheet_name] += 1
                    row = [cell.value for cell in frow]
                    if row[2]:
                        if row[2].startswith("AT"):
                            if row[2].count("_") == 0:
                                asil = row[3] and row[3].startswith("ASIL")
                            elif row[2].count("_") == 2:
                                count['all'][function]['total'] += 1
                                if satellite:
                                    count['satellite'][function]['total'] += 1
                                else:
                                    count['high'][function]['total'] += 1

                                if asil:
                                    count['all'][function]['asil'] += 1
                                    if satellite:
                                        count['satellite'][function]['asil'] += 1
                                    else:
                                        count['high'][function]['asil'] += 1

                                else:
                                    count['all'][function]['normal'] += 1
                                    if satellite:
                                        count['satellite'][function]['normal'] += 1
                                    else:
                                        count['high'][function]['normal'] += 1
            else:
                for _ in sheet.iter_rows():
                    last_lines[function][sheet_name] += 1
                last_lines[function][sheet_name] -= 1
    return count, last_lines


def add_last_lines(wb, last_lines):
    sheet = wb.create_sheet('last lines')
    row = 0
    for function, tabs in last_lines.items():
        row += 1
        sheet['A{}'.format(row)] = function
        for name, line in tabs.items():
            row += 1
            sheet['A{}'.format(row)] = name
            sheet['B{}'.format(row)] = line
        row += 1 # blank line between


def _make_excel_sheet(all_counts, last_lines):
    wb = Workbook()
    for category, count in all_counts.items():
        sheet = wb.create_sheet(category)
        sheet['B1'] = 'normal'
        sheet['C1'] = 'asil'
        sheet['D1'] = 'total'
        row = 1
        for function, counts in count.items():
            row += 1
            sheet['A%s'%(row)] = function
            sheet['B%s'%(row)] = counts['normal']
            sheet['C%s'%(row)] = counts['asil']
            sheet['D%s'%(row)] = counts['total']
    add_last_lines(wb, last_lines)
    wb.save('counts.xlsx')


if __name__ == '__main__':
    count, last_lines = get_count(CVS_PATH)
    _make_excel_sheet(count, last_lines)
