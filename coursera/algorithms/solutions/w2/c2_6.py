# python3

def fib_last(n):
    if n <= 1:
        return n
    n += 1
    a = [0 for _ in range(n)]
    a[1] = 1
    for i in range(2, n):
        a[i] = (a[i-2] + a[i-1]) % 10
    return a[-1]

def fib_sum_last(n):
    r = fib_last(n+2) - 1
    if r == -1:
        r = 9
    return r

n = int(input())
print(fib_sum_last(n))
