# last digit of a sum of fibonacci numbers

def fib(n):
    if n <= 1:
        return n
    n += 1
    a = [0 for _ in range(n)]
    a[1] = 1
    for i in range(2, n):
        a[i] = a[i-2] + a[i-1]
    return a[-1]

def fib_sum(n):
    n += 1
    total = 0
    for i in range(n):
        total += fib(i)
    return total

def fib_last(n):
    if n <= 1:
        return n
    n += 1
    a = [0 for _ in range(n)]
    a[1] = 1
    for i in range(2, n):
        a[i] = (a[i-2] + a[i-1]) % 10
    return a[-1]

def fib_sum_last(n):
    a = fib_last(n+2) - 1
    if a == -1:
        a = 9
    return a


for i in range(15):
    print '{}\t{}\t{}\t{}'.format(i, fib_last(i), fib_sum(i), fib_sum_last(i))