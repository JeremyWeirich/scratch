# goal: fibonacci value n modulo a large number m


def fib(n):
    if n <= 1:
        return n
    n += 1
    a = [0 for _ in range(n)]
    a[1] = 1
    for i in range(2, n):
        a[i] = a[i-2] + a[i-1]
    return a[-1]


for i in range(10):
    print i, fib(i)