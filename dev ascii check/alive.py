import os

BASE = os.path.dirname(os.path.realpath(__file__))
logfile = 'log.txt'
ALIVES = [hex(i)[-1].upper() for i in range(15)]  # generates 0-E

with open(logfile, 'w'):
    pass


def asc(files):
    for filename in files:
        if filename.endswith('.asc'):
            yield filename


def get_filtered():
    for root, dirs, files in os.walk(BASE):
        for filename in asc(files):
            filepath = os.path.join(root, filename)
            print '#  checking {}'.format(filename)
            with open('log.txt', 'a') as f:
                f.write('\n#  checking {}'.format(filename))
            with open(filepath, 'r') as log:
                previous_index = None
                previous_time = None
                for line in log:
                    split = line.split()
                    if len(split) > 6 and split[2] == '36C':
                        # check timing
                        current_time = float(split[0])
                        if previous_time is not None:
                            if current_time - previous_time > .12:
                                with open('log.txt', 'a') as f:
                                    f.write('alive increment time exceeded at {} in {}\n'.format(current_time, filepath))
                        previous_time = current_time
                        # check increment
                        alive = split[7][-1]
                        assert alive in ALIVES, 'found invalid alive: {}'.format(alive)
                        if previous_index is not None:
                            predicted_index = previous_index + 1 if previous_index < 14 else 0
                            if alive != ALIVES[predicted_index]:
                                with open('log.txt', 'a') as f:
                                    f.write('expected alive to be {}, was {} at time {} in {}\n'.format(ALIVES[predicted_index], alive, current_time, filepath))
                        previous_index = ALIVES.index(alive)

if __name__ == '__main__':
    get_filtered()
