import sys
import glob
import os

BASE = os.path.dirname(os.path.realpath(__file__))

def asc(files):
    for filename in files:
        if filename.endswith('.asc'):
            yield filename

def get_filtered(ids):
    found = []
    for root, dirs, files in os.walk(BASE):
        for filename in asc(files):
            filepath = os.path.join(root, filename)
            print 'checking {} for'.format(filename), ids
            with open(filepath, 'r') as log:
                for line in log:
                    split = line.split()
                    if len(split) >= 5 and split[1] == '1':
                        if split[2].lower() in ids:
                            if root not in found:
                                found.append(root)
    with open('found.txt', 'w') as out:
        out.write('\n'.join(found))

if __name__ == '__main__':
    ids = [i.lower() for i in sys.argv[1:]]
    get_filtered(ids)
