# *===========================================================================*
# * Name:      EDIABAS_script.py                                              *
# * Version:   1.0                                                            *
# * Author:    bence.kerekes                                                  *
# * Copyright: ThyssenKrupp-Presta                                            *
# *---------------------------------------------------------------------------*
# * Content:  Python script for testing EDIABAS scripts.                      *
# *                                                                           *
# *---------------------------------------------------------------------------*
# * Usage: Start the script without any argument.                             *
# *                                                                           *
# *   > python EDIABAS_script.py                                              *
# *                                                                           *
# *---------------------------------------------------------------------------*
# * History:                                                                  *
# *                                                                           *
# *  Rev. 1.0:                                                                *
# *  Initial revision created                                                 *
# *                                                                           *
# *---------------------------------------------------------------------------*
# * Language: Python                                                          *
# * Compiler: -                                                               *
# * Target:   Python 2.5 & EDIABASLIB module                                  *
# *===========================================================================*

# import standard modules
from win32api import Sleep

import EDIABASLIB

# Necessary function definitions are copied from other modules

###############################################################################
# Print Dictionary                                                            #
###############################################################################
def printDict(inputObject):
    """ This function is used to print a dictionary object in human readable 
        format.

        Input:  a dictionary object (other types of objects are accepted but 
                not formatted)
        Output: a formatted string
    """

    # check if object exists
    if inputObject != None:

        # the object is a dictionary
        if type(inputObject) == dict:

            # create ouput buffer
            strOut = ''

            # get the keys
            dictKeys = inputObject.keys()

            # sort the keys
            dictKeys.sort()

            # go through all elements in alphabetical order
            for i in dictKeys:

                # the actual element is not a dictionary
                if type(inputObject[i]) != dict:
                    strOut += "  %s: %s,\n" % (str(i), unicode(inputObject[i]) )

                # the actual element is a dictionary
                else:
                    # print the actual element of the dictionary
                    strOut += "  %s:\n" % (str(i))

                    # Sort the keys of dictionary
                    sortedKeys = inputObject[i].keys()
                    sortedKeys.sort()

                    # print the sub elements of actual element of the dictionary
                    for j in sortedKeys:
                        strOut += "    %s: %s,\n" % (str(j), unicode(inputObject[i][j]))

            # print out the buffer
            strOut += '\n'
            print("\n" + strOut)

            # return the formatted strings
            return strOut

        # the object is not a dictionary
        else:
            # convert to unicode and print it out
            print("  " + unicode(inputObject) + '\n')

            # return the input
            return inputObject



###############################################################################
# Execute a diagnostic job and read back response                             #
###############################################################################
def CheckDiagJob(jobname, argument, checkStatus=True, prgfile="CDM02", verbose=False):
    """ This function is used to execute a diagnostic job. It rasies an 
        exception if the job status is not okay.

        Input:  - the job name
                - argument of the job
                - check the status of the job? (optional default: true)
                - external or internal prg (optional default: external)
                - enable verbose mode (optional)
        Output: - 
    """

    # satus indicator
    statusOK = False

    # select SGBD file (the default is external)
    sgbdsel = prgfile

    # execute diag job
    resultSet = EDIABASLIB.apiJob(sgbdsel, jobname, argument)

    # check job status
    for i in range(0, resultSet[0]["SAETZE"] + 1):

        if resultSet[i].has_key("JOB_STATUS") and resultSet[i]["JOB_STATUS"] == "OKAY": statusOK = True

    # raise exception if status not okay
    if not statusOK and checkStatus:

        # print information into the log
        print "<a style=\"color:990000;font-weight:bold\">JOB_STATUS not OKAY!</a>"

        # print received response into the log
        printDict(resultSet)

        # raise exception
        raise Exception

    # check job status
    for i in range(0, resultSet[0]["SAETZE"] + 1):

        # split long response hex value (it is found in the last SAETZE)
        if resultSet[i].has_key("_RESPONSE"):

            # split hex string along spaces and clear string
            res = resultSet[i]["_RESPONSE"].split(' ')
            resultSet[i]["_RESPONSE"] = ''

            # compile string
            for j in range(0, len(res)):

                # append element
                resultSet[i]["_RESPONSE"] += res[j]

                # break line and indent after every tenth item
                if j % 10 == 9:
                    resultSet[i]["_RESPONSE"] += '\n               '

                # put a space after every other item except the last one
                elif j != len(res) - 1:
                    resultSet[i]["_RESPONSE"] += ' '

    # print to output only in verbose mode
    if verbose: printDict(resultSet)

    # return result
    return resultSet

###############################################################################

prgfile = r'CDM02'
# execute job
printDict(CheckDiagJob(jobname="svk_lesen", argument="", checkStatus=True, prgfile=prgfile))

# *===========================================================================*
# * end of file EDIABAS_script.py                                             *
# *===========================================================================*
