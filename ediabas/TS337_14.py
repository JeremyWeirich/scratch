import EDIABASLIB
PRG = r'CDM02'

TS = "TS337_14"
SUMMARY = """Objective 14: CoSi Diagnostic Jobs - 8 EDIABAS Sequences
This objective tests the following CoSi diagnostics jobs:
STEUERN_ROUTINE_COACHDOOR_SECURE
STATUS_LESEN_COACHDOORS
The following diagnostic jobs must also be completed in TS337_25:
STATUS_LESEN_CHILDLOCK
STATUS_LESEN_COACHDOORS
STATUS_LESEN_DOORS_PRECATCH_FRONT
STATUS_LESEN_ZV_HALLSENSORS
The following diagnostic jobs must also be completed in TS337_27:
STATUS_LESEN_COACHDOORS
STATUS_LESEN_DOORS_PRECATCH_FRONT
The following diagnostic jobs must also be completed in TS337_31:
STATUS_LESEN_COACHDOORS
STATUS_LESEN_DOORS_PRECATCH_FRONT
STATUS_LESEN_ZV_HALLSENSORS
The following diagnostic jobs must also be completed in TS337_40:
STATUS_LESEN_COACHDOORS
STATUS_LESEN_DOORS_PRECATCH_FRONT
"""

INSERTED = 1
NOT_INSERTED = 0
ERRORS = []


def run(job, args):
    result = EDIABASLIB.apiJob(PRG, job, args)
    status = result[1]['JOB_STATUS']
    print 'the status is {} when i ran {} {}'.format(status, job, args)
    return status


def verify(text, condition):
    print text, ': ', condition
    if not condition:
        ERRORS.append('failed: ' + text)


def setup():
    print '-' * 50
    EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')


def expect(text, condition):
    conditions = {
        'e/e': dict(left=INSERTED, right=INSERTED),
        'd/e': dict(left=NOT_INSERTED, right=INSERTED),
        'e/d': dict(left=INSERTED, right=NOT_INSERTED),
        'd/d': dict(left=NOT_INSERTED, right=NOT_INSERTED),
    }
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    verify(text, status[1]['STAT_COSI_LEFT'] == conditions[condition]['left'])
    verify(text, status[1]['STAT_COSI_RIGHT'] == conditions[condition]['right'])


def standard_commands():
    """TS337_14_1-14"""
    setup()

    # engage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    expect('CoSi engaged on both sides', 'e/e')

    # disengage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')

    # engage left only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    expect('CoSi engaged left only', 'e/d')

    # disengage left only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')

    # engage right only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    expect('CoSi engaged right only', 'd/e')

    # disengage right only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')


def duplicate_back_to_back_commands():
    """TS337_14_15-34"""
    setup()

    # engage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    expect('CoSi engaged on both sides', 'e/e')

    # disengage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')

    # engage left only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    expect('CoSi engaged left only', 'e/d')

    # disengage left only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')

    # engage right only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    expect('CoSi engaged right only', 'd/e')

    # disengage right only
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')


def opposite_back_to_back_commands():
    """TS337_14_35-63"""
    setup()

    # - BOTH --------------
    # engage/disengage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')
    # engage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    expect('CoSi engaged on both sides', 'e/e')
    # disengage/engage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    expect('CoSi engaged on both sides', 'e/e')
    # disengage both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')

    # - LEFT --------------
    # engage/disengage left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    expect('CoSi disengaged on left side', 'd/d')
    # engage left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    expect('CoSi engaged on left side', 'e/d')
    # disengage/engage left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    expect('CoSi engaged on left side', 'e/d')
    # disengage left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    expect('CoSi disengaged on left side', 'd/d')

    # - RIGHT --------------
    # engage/disengage right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    expect('CoSi disengaged on right side', 'd/d')
    # engage right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    expect('CoSi engaged on right side', 'd/e')
    # disengage/engage right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    expect('CoSi engaged on right side', 'd/e')
    # disengage right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    expect('CoSi disengaged on right side', 'd/d')


def STPR():
    """TS337_14_64-111"""
    setup()

    # - BOTH ---------
    # engage/STPR both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on both sides', 'e/e')
    # disengage/STPR both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on both sides', 'd/d')
    # stop while engaged both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    expect('CoSi engaged on both sides', 'e/e')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on both sides', 'e/e')
    # stop while disengaged both
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    expect('CoSi disengaged on both sides', 'd/d')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on both sides', 'd/d')

    # - LEFT ---------
    # engage/STPR left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on left side', 'e/d')
    # disengage/STPR left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on left side', 'd/d')
    # stop while engaged left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    expect('CoSi engaged on left sides', 'e/d')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on left side', 'e/d')
    # stop while disengaged left
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    expect('CoSi disengaged on left side', 'd/d')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on left side', 'd/d')

    # - RIGHT ---------
    # engage/STPR right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on right side', 'd/e')
    # disengage/STPR right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on right side', 'd/d')
    # stop while engaged right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    expect('CoSi engaged on right side', 'd/e')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi engaged on right side', 'd/e')
    # stop while disengaged right
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    expect('CoSi disengaged on right side', 'd/d')
    run('steuern_routine', 'ARG;COACHDOOR_SECURE;STPR;')
    expect('CoSi disengaged on right side', 'd/d')


def RRR():
    """TS337_14_64-70"""
    setup()

    status = run('steuern_routine', 'ARG;COACHDOOR_SECURE;RRR;')
    if status != 'ERROR_ECU_SUB_FUNCTION_NOT_SUPPORTED':
        ERRORS.append('RRR: did not receive ERROR_ECU_SUB_FUNCTION_NOT_SUPPORTED')


if __name__ == '__main__':
    import time
    start = time.time()
    print TS
    print SUMMARY
    # standard_commands()
    # duplicate_back_to_back_commands()
    # STPR()
    RRR()
    print ERRORS
    print 'took', time.time() - start
