# based on script written by bence.kerekes, copyright ThyssenKrupp-Presta

import json

import EDIABASLIB


def printDict(inputObject):
    """ This function is used to print a dictionary object in human readable 
        format.

        Input:  a dictionary object (other types of objects are accepted but 
                not formatted)
        Output: a formatted string
    """

    # check if object exists
    if inputObject and isinstance(inputObject, dict):
        strOut = ''
        dictKeys = inputObject.keys()
        dictKeys.sort()
        for i in dictKeys:
            if type(inputObject[i]) != dict:
                strOut += "  %s: %s,\n" % (str(i), unicode(inputObject[i]) )
            else:
                strOut += "  %s:\n" % (str(i))
                sortedKeys = inputObject[i].keys()
                sortedKeys.sort()
                for j in sortedKeys:
                    strOut += "    %s: %s,\n" % (str(j), unicode(inputObject[i][j]))
        strOut += '\n'
        print("\n" + strOut)
        return strOut

    else:
        print("  " + unicode(inputObject) + '\n')
        return inputObject



###############################################################################
# Execute a diagnostic job and read back response                             #
###############################################################################
def CheckDiagJob(jobname, argument, checkStatus=True, prgfile="CDM02", verbose=False):
    """ This function is used to execute a diagnostic job. It rasies an 
        exception if the job status is not okay.

        Input:  - the job name
                - argument of the job
                - check the status of the job? (optional default: true)
                - external or internal prg (optional default: external)
                - enable verbose mode (optional)
        Output: - 
    """
    status_ok = False
    result_set = EDIABASLIB.apiJob(prgfile, jobname, argument)
    with open('{}.txt'.format(jobname), 'w') as f:
        f.write(argument)
        f.write(json.dumps(result_set))
    for i in range(0, result_set[0]["SAETZE"] + 1):
        if 'JOB_STATUS' in result_set[i] and result_set[i]["JOB_STATUS"] == "OKAY":
            status_ok = True
    if not status_ok and checkStatus:
        print "<a style=\"color:990000;font-weight:bold\">JOB_STATUS not OKAY!</a>"
        printDict(result_set)
        raise Exception
    for i in range(0, result_set[0]["SAETZE"] + 1):
        if result_set[i].has_key("_RESPONSE"):
            res = result_set[i]["_RESPONSE"].split(' ')
            result_set[i]["_RESPONSE"] = ''
            for j in range(0, len(res)):
                result_set[i]["_RESPONSE"] += res[j]
                if j % 10 == 9:
                    result_set[i]["_RESPONSE"] += '\n               '
                elif j != len(res) - 1:
                    result_set[i]["_RESPONSE"] += ' '
    if verbose:
        printDict(result_set)
    return result_set


if __name__ == '__main__':
    prgfile = r'CDM02'
    jobname = 'steuern_routine'
    arg = 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED'
    result_set = EDIABASLIB.apiJob(prgfile, jobname, arg)
    with open('{}_{}.txt'.format(jobname, arg), 'w') as f:
        f.write(json.dumps(result_set))

