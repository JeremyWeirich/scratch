import EDIABASLIB

PRG = r'CDM02'


def check_cosi_engage_both():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;ENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


def check_cosi_disengage_both():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_BOTH;DISENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


def check_cosi_engage_left():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;ENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


def check_cosi_disengage_left():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_LEFT;DISENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


def check_cosi_engage_right():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;ENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


def check_cosi_disengage_right():
    result = EDIABASLIB.apiJob(PRG, 'steuern_routine', 'ARG;COACHDOOR_SECURE;STR;COACHDOOR_RIGHT;DISENGAGED')
    print 'job status:', result[1]['JOB_STATUS']
    status = EDIABASLIB.apiJob(PRG, 'status_lesen', 'ARG;COACHDOORS')
    print 'left:', status[1]['STAT_COSI_LEFT_TEXT']
    print 'right:', status[1]['STAT_COSI_RIGHT_TEXT']


if __name__ == '__main__':
    check_cosi_engage_both()
    check_cosi_disengage_both()
    check_cosi_engage_left()
    check_cosi_disengage_left()
    check_cosi_engage_right()
    check_cosi_disengage_right()
