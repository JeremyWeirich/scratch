from openpyxl import load_workbook


DOC = 'effort.xlsx'
TASKS = ['A','B','C','D','E','F']


def get_task_data(workbook_path):
    wb = load_workbook(DOC, read_only=True)
    sheet = wb['effort']

    tasks = {task: 0 for task in TASKS}
    people = {}

    new_person = True
    tasks_left = len(TASKS)
    v_off = 1
    for f_row in sheet.iter_rows():
        while v_off:
            v_off -= 1
            continue
        row = [cell.value for cell in f_row]
        if new_person:
            new_person = False
            person = row[0]
            people[person] = {}
        else:
            row = [cell if cell else 0 for cell in row]
            task = row[0]
            if task in TASKS:
                effort = sum(row[1:])
                people[person][task] = effort
                tasks[task] += effort
                tasks_left -= 1
                if tasks_left == 0:
                    new_person = True
    return tasks, people

if __name__ == '__main__':
    tasks, people = get_task_data(DOC)
    import pprint
    pprint.pprint(tasks)
    pprint.pprint(people)