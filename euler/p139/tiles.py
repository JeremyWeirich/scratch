import cProfile
import timeit
from time import time

from triangles import *


def hole_size_works(s1, s2, s3):
    triangle_square_area = s3 ** 2
    center_size = sqrt(triangle_square_area - 2 * (s1 * s2))
    return s3 % center_size == 0


def main():
    #for i in [100, 1000, 5000, 10000]:
        i = 10000
        start = time()
        count = 0
        # should be 1003 when i = 10,000
        for triangle in t3(i):
            if hole_size_works(*triangle):
                count += 1
        print count, '\n'
        print i, time() - start

if __name__ == '__main__':
    # cProfile.run('create_html_report()')
    main()
