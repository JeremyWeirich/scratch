from math import sqrt




def t1(perimeter):
    # 6.397s
    triangles = []
    for s1 in range(1, perimeter):
        for s2 in range(1, perimeter):
            s3 = sqrt(s1 ** 2 + s2 ** 2)
            if s1 + s2 + s3 > perimeter:
                break
            if s3.is_integer():
                t = sorted({s1, s2, s3})
                if t not in triangles:
                    triangles.append(t)
    return triangles


def t2(perimeter):
    # rick
    # 2.81s
    triangles = []
    for s1 in range(1, perimeter):
        for s2 in range(1, s1 + 1):
            s3 = sqrt(s1 ** 2 + s2 ** 2)
            if s1 + s2 + s3 > perimeter:
                break
            if s3.is_integer():
                t = (s1, s2, s3)
                triangles.append(t)
    return triangles
