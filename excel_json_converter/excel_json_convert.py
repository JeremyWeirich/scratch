FILEPATH = r'N:\Users\Jeremy Weirich\repos\perseus\scan\tests\functional\functional_tests.xlsx'


class JSONConvert(object):
    def __init__(self):
        self.wb = None
        self.sheet_names = None

    @staticmethod
    def get_sheet_names(path):
        from openpyxl import load_workbook
        return load_workbook(path, read_only=True, data_only=True).sheetnames

    def load_workbook(self, file_path):
        from openpyxl import load_workbook
        self.wb = load_workbook(file_path, read_only=True, data_only=True)

    def get_json_sheet_data(self, sheet, column_names):
        s = self.wb.get_sheet_by_name(sheet)
        sheet_data = []
        for f_row in s.iter_rows():
            row_data = {}
            for index, cell in enumerate(f_row):
                try:
                    column_name = column_names[index]
                    if column_name:
                        row_data[column_name] = cell.value
                except IndexError:
                    pass  # do not save data for columns that aren't provided
            sheet_data.append(row_data)
        return sheet_data

    def get_json_workbook_data(self, column_names_dict):
        workbook_data = {}
        for sheet_name, column_names in column_names_dict.items():
            workbook_data[sheet_name] = self.get_json_sheet_data(sheet_name, column_names)
        return workbook_data


ORDERING = [
    None,
    'Enabled',
    'AT',
    'Description',
    'Write Variable',
    'Write Value',
    'Verify Variable',
    'Verify Operator',
    'Verify Value',
    'Verify Tolerance',
    'PR'
]
converter = JSONConvert()
converter.load_workbook(FILEPATH)
import pprint
pprint.pprint(converter.get_json_workbook_data({'report': ORDERING, 'debug': ORDERING}))
