color = 'red'

def unsafe_exec(code):
    color = 'green'
    exec(code)
    print 'function color is', color


def safer_exec(code):
    color = 'green'
    namespace = {}
    exec(code) in namespace
    print 'function color is', color


if __name__ == '__main__':
    code = """
# global color
color = 'pink'
print 'exec color is', color
"""
    # safer_exec(code)
    # print 'global color is', color
    c = compile(code, '<string>', 'exec')
    print c
    safer_exec(c)

