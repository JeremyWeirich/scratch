import clr
import time
import os
import shutil
from functools import wraps

from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from xml.dom import minidom

from System import Array
from System.Collections.Generic import Dictionary

# Load ASAM assemblies from the global assembly cache (GAC)
clr.AddReference(
    "ASAM.XIL.Implementation.TestbenchFactory, Version=2.0.1.0, Culture=neutral, PublicKeyToken=fc9d65855b27d387")
clr.AddReference("ASAM.XIL.Interfaces, Version=2.0.1.0, Culture=neutral, PublicKeyToken=bf471dff114ae984")

# Import XIL API .NET classes from the .NET assemblies
from ASAM.XIL.Implementation.TestbenchFactory.Testbench import TestbenchFactory
from ASAM.XIL.Interfaces.Testbench.Common.Error import TestbenchPortException
from ASAM.XIL.Interfaces.Testbench.Common.ValueContainer.Enum import DataType
from ASAM.XIL.Interfaces.Testbench.EESPort.Error import EESPortException
from ASAM.XIL.Interfaces.Testbench.EESPort.Enum import EESPortState, ErrorCategory, ErrorType, LoadType, TriggerType


TASK = 'Periodic Task 1'  # should always be this task????
MODEL_PREFIX = 'Model Root/Test/{}/Value'

EESPORT_CONFIG_FILE_PATH = r'N:\Users\Jeremy Weirich\EESPORT.portconfig'

eFLOAT = DataType.eFLOAT
eINT = DataType.eINT
eUINT = DataType.eUINT

FIU_Trigger_variable = "FIU_Trigger"
FIU_Trigger_path = "Platform()://Model Root/HIL/FIU_Trigger/Value"

class FIUError(object):
    CATEGORIES = dict(
        power='SHORT_CIRCUIT_UBATT',
        common='PIN_2_PIN',
        ground='SHORT_CIRCUIT_GROUND',
        open='INTERRUPT',
    )
    ACCEPTABLE_LOAD_CATEGORIES = [
        'WITH_LOAD',
        'WITHOUT_LOAD'
    ]

    def __init__(self, category, fiu, signal, load_type='WITHOUT_LOAD', software_trigger=False):
        assert category in self.CATEGORIES, 'Invalid FIU error category {}, must be in {}'.format(category, self.CATEGORIES)
        assert load_type in self.ACCEPTABLE_LOAD_CATEGORIES, 'Invalid FIU error load type {}, must be in {}'.format(category, self.ACCEPTABLE_LOAD_CATEGORIES)
        self.category = self.CATEGORIES[category]
        self.name = '{}\{}'.format(fiu, signal)
        self.load_type = load_type
        self.software_trigger = 'SOFTWARE' if software_trigger else 'MANUAL'

# - create configuration xml files -------------------------------------------------------------------------------------
def prettify(elem):
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='  ')


def create_ees_config_xml(hil, sdf, csv, com):
    assert isinstance(com, int), 'COM Port {} needs to be an int'.format(com)
    body = Element('PortConfigurations')
    config = SubElement(body, 'EESPortConfig', Logging='false', OfflineMode='false', Version='2016-B')

    # --- hardware config
    hardware_configurations = SubElement(config, 'HardwareConfigurations')
    # drivers
    drivers = SubElement(hardware_configurations, 'Drivers')
    driver = SubElement(drivers, 'Driver', ID='0', DriverType='RS232')
    com_port = SubElement(driver, 'COMPort')
    com_port.text = 'COM{}'.format(com)
    # hardware configuration
    SubElement(hardware_configurations, 'HardwareConfiguration', ID='1', SignalListPath=csv, DriverId='0', EcuVersion='1')
    # potential mapping
    SubElement(hardware_configurations, 'PotentialMapping')
    # signal mapping
    SubElement(hardware_configurations, 'SignalMapping')

    # --- real time config
    platform_name = '{}_PU'.format(hil)
    real_time_configuration = SubElement(config, 'RealTimeConfiguration', PlatformName=platform_name, SystemDescriptionFilePath=sdf)
    # tracing
    tracing = SubElement(real_time_configuration, 'Tracing', Enabled='false')
    SubElement(tracing, 'Variable', Value='{0}()://XIL API/EESPort/Error Activated'.format(hil), Type='ErrorActivated')
    SubElement(tracing, 'Variable', Value='{0}()://XIL API/EESPort/Active ErrorSet'.format(hil), Type='ActiveErrorSet')
    SubElement(tracing, 'Variable', Value='{0}()://XIL API/EESPort/Error Switching'.format(hil), Type='ErrorSwitching')
    SubElement(tracing, 'Variable', Value='{0}()://XIL API/EESPort/Flags'.format(hil), Type='Flags')
    SubElement(tracing, 'Variable', Value='{0}()://XIL API/EESPort/Trigger'.format(hil), Type='Trigger')
    # software trigger
    SubElement(real_time_configuration, 'SoftwareTrigger', PollingInterval='0.001')
    return prettify(body)


def write_ees_config_file(hil, sdf, csv, com):
    with open(EESPORT_CONFIG_FILE_PATH, 'w') as f:
        f.write(create_ees_config_xml(hil, sdf, csv, com))
    return EESPORT_CONFIG_FILE_PATH


def create_EES_error_group_xml(errors):
    # for 5355 controller, you can only do one of the following:
    # interrupt any number of channels and/or short any number of channels to common
    # short a single channel to gnd/vbatt
    error_configuration_attributes = {
        'name': 'ErrorConfiguration',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation': 'http://www.asam.net/XILAPI/EESConfiguration/2.0 EESConfiguration.xsd',
        'xmlns': 'http://www.asam.net/XILAPI/EESConfiguration/2.0',
    }
    body = Element('ErrorConfiguration')
    for attribute, value in error_configuration_attributes.items():
        body.set(attribute, value)

    # safety assertions
    all_names = []
    unique_names = set()
    pin_2_pin_count = 0
    for error in errors:
        all_names.append(error.name)
        unique_names.add(error.name)
        if error.category == 'PIN_2_PIN':
            pin_2_pin_count += 1
        if error.category in ['SHORT_CIRCUIT_GROUND', 'SHORT_CIRCUIT_UBATT']:
            assert len(errors) == 1, 'Only one error of type short to ground/power is allowed'
    assert len(all_names) == len(unique_names), 'A channel may only be used once in a single error set'
    assert pin_2_pin_count != 1, 'At least two channels are required for Pin to Pin '

    # make error xml file
    first_pin_to_pin = True
    pin_to_pin_element = None
    for error in errors:
        error_set_element = SubElement(body, 'ErrorSet', name='ErrorSet1', triggerType=error.software_trigger)
        if error.category == 'PIN_2_PIN':
            if first_pin_to_pin:
                if pin_2_pin_count > 2:
                    error.category = 'MULTI_PIN_2_PIN'
                first_pin_to_pin = False
                pin_to_pin_element = SubElement(error_set_element, 'SimpleError', errorCategory=error.category)
            SubElement(pin_to_pin_element, 'Signal', name=error.name, loadType=error.load_type)
        else:
            category_element = SubElement(error_set_element, 'SimpleError', errorCategory=error.category)
            signal_element = SubElement(category_element, 'Signal', name=error.name)
            if error.category != 'INTERRUPT':
                signal_element.set('loadType', error.load_type)

        if error.software_trigger == 'SOFTWARE':
            for each in ['ErrorSet1','ErrorSet2']:
                if each=='ErrorSet2':
                    error_set_element = SubElement(body, 'ErrorSet', name=each, triggerType= error.software_trigger)
                soft_trigger = SubElement(error_set_element, 'SoftwareTrigger', type= 'ConditionWatcher')
                timeout = SubElement(soft_trigger, 'Timeout')
                timeout.text = '4000'
                condition = SubElement(soft_trigger, 'Condition')
                condition.text = FIU_Trigger_variable+'==1' if each=='ErrorSet1' else FIU_Trigger_variable+'==0'
                defines = SubElement(soft_trigger, 'Defines')
                SubElement(defines,'Link',key=FIU_Trigger_variable, value=FIU_Trigger_path)

    return prettify(body)


def make_ees_error_group_file(path, errors):
    with open(path, 'w') as f:
        f.write(create_EES_error_group_xml(errors))
    return path


# - convenience classes ------------------------------------------------------------------------------------------------
def catch_XIL_exception(exception_type):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception_type as e:
                print '\n'
                print 'Exception {} occurred'.format(exception_type)
                print 'Code:', e.CodeDescription
                print 'Vendor Code:', e.VendorCodeDescription
                print '\n'
                raise
        return wrapper
    return decorator


class _EESPortConvenience(object):
    def __init__(self, port, bench):
        self.port = port
        self.bench = bench

    @catch_XIL_exception(EESPortException)
    def run(self, xml):
        error_configuration = self.bench.CreateErrorConfiguration('Anything')
        reader = error_configuration.CreateEESConfigurationFileReader(xml)
        error_configuration.Load(reader)
        self.port.SetErrorConfiguration(error_configuration)
        self.port.Download()
        print 'state = ', self.port.get_State()
        self.port.Activate()
        # self.port.Trigger()

    @catch_XIL_exception(EESPortException)
    def end(self):
        self.port.Deactivate()
        self.port.Unload()



class EESPortMaker(object):
    """Creates EESPort"""
    def __init__(self, hil, sdf, csv, com):
        self.config = write_ees_config_file(hil, sdf, csv, com)
        self.port = None
        self.bench = None

    @catch_XIL_exception(EESPortException)
    def __enter__(self):
        print 'creating EESPort'
        test_bench_factory = TestbenchFactory()
        test_bench = test_bench_factory.CreateVendorSpecificTestbench('dSPACE GmbH', 'XIL API', '2016-B')
        self.bench = test_bench.EESPortFactory
        self.port = self.bench.CreateEESPort('EESPORT')
        eesport_config = self.port.LoadConfiguration(self.config)
        start = time.time()
        self.port.Configure(eesport_config)
        print 'configuring took', time.time() - start
        return _EESPortConvenience(self.port, self.bench)

    @catch_XIL_exception(EESPortException)
    def __exit__(self, *args):
        self.config = None
        if self.port != None:
            self.port.Dispose()
            self.port = None


if __name__ == '__main__':
    # eesport stuff
    hil = 'AURIGA'
    com = 3
    sdf = r'N:\Users\HIL Tester\Projects\P14106001_BMW_CDM_Left\P14106001_BMW_CDM_Left_CFD\Application_P14106001_BMW_CDM_Left\Build Results\Application_P14106001_BMW_CDM_Left.sdf'
    csv = r'N:\Users\HIL Tester\FIU Pin out\AP1398_HW_SL_FIU_Only_v2p00p00.CSV'
    # error set stuff
    fiu = 'HC_FIU_01'
    signals = [
        'HC_Rack1_FIU_01',
        'HC_Rack1_FIU_02',
        'HC_Rack1_FIU_03',
        'HC_Rack1_FIU_04',
    ]
    errors = [
        FIUError('open', fiu, signals[0], software_trigger=True),
    ]

    a = make_ees_error_group_file(path='N:\Users\Jeremy Weirich\set2.xml', errors=errors)
    print a
    with EESPortMaker(hil=hil, com=com, csv=csv, sdf=sdf) as EESPort:
        EESPort.run(a)
        time.sleep(1000)
        EESPort.end()
