from random import choice, sample

def get_random_name(adjectives, nouns):
    s = ''.join([i.title() for i in sample(adjectives, 2)])
    noun = choice(nouns).title()
    return s+noun





if __name__ == '__main__':
    with open('adjectives', 'r') as f:
        adjectives = f.read().split('\n')
    with open('nouns', 'r') as f:
        nouns = f.read().split('\n')
    for _ in range(10):
        print get_random_name(adjectives, nouns)

