# Veena's code
import json
import os,glob,sys
import win32com.client
import win32com
import string

#http://dmcritchie.mvps.org/excel/colors.htm

GREY = 15
WHITE = 2
GREEN = 35
BCK_BLUE = 11
BRD_YELLOW = 19

TXT_BIG = 14
TXT_MED = 10
TXT_BLUE = 55
TXT_BLACK = 1#-4105

STS_RED = 3
STS_DARKGREEN = 10
STS_YELLOW = 27
STS_ORANGE = 46
STS_BLUE = 28

CENTRE = -4108
RIGHT = -4152
LEFT = -4131

SLD_LINE = 1
THIN_LINE = 2
THICK_LINE = 3

with open('result.json', 'r') as f:
    rls_items = json.loads(f.read())
rls = dict(
    ProjectName = '',
    Reviewdate='',
    PhaseOfproject = 'Testing',
    ReviewItem = 'TBD',
    ReferenceMaterials = 'NA',
    ScopeOfReview = 'Testing',
    Moderator = 'Veena',
    Authors = 'Jeremy',
    Reader = 'All',
    Reviewers = 'x,y',
    TypeOfReview = 'Automation',
    rls_items = rls_items
)



def set_column_widths():
    widths = [
        2.14,
        2.14,
        6.86,
        24.14,
        15.29,
        41.86,
        14,
        15.14,
        9.29,
        10.57,
        36.14,
        2.14
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        ws.Cells(1,column).ColumnWidth = width

def Format_Cell(Cel_Str, Cel_Stp, Val_list, CelColr, TxtSiz, TxtColr, Ver, Hor, TxtBold = False, Merge = False):

    if(Merge):
        ws.Range(ws.Range(Cel_Str), ws.Range(Cel_Stp)).Merge()
        ws.Range(Cel_Str).Value = Val_list
        ws.Range(Cel_Str).Interior.ColorIndex = CelColr
        ws.Range(Cel_Str).Font.Size = TxtSiz
        ws.Range(Cel_Str).Font.ColorIndex = TxtColr
        ws.Range(Cel_Str).Font.Bold = TxtBold
        ws.Range(Cel_Str).VerticalAlignment = Ver
        ws.Range(Cel_Str).HorizontalAlignment = Hor
    else:
        for each in Val_list:
            ws.Range(Cel_Str).Value = each
            ws.Range(Cel_Str).Interior.ColorIndex = CelColr
            ws.Range(Cel_Str).Font.Size = TxtSiz
            ws.Range(Cel_Str).Font.ColorIndex = TxtColr
            ws.Range(Cel_Str).Font.Bold = TxtBold
            ws.Range(Cel_Str).VerticalAlignment = Ver
            ws.Range(Cel_Str).HorizontalAlignment = Hor
            Cel_Str = unichr((ord(Cel_Str[0]))+1) + Cel_Str[1:]


def make_header():

    Format_Cell('C3','K3', ['Review Log Sheet'], GREY, TXT_BIG, TXT_BLUE, CENTRE, CENTRE, True, True)
    Format_Cell('C4', 'K4', ['Review Details'], GREEN, TXT_MED, TXT_BLACK, CENTRE, CENTRE, True, True)
    #review details
    Text = ['Project Name- Version', 'Review Date', 'Phase of the Project', 'Review Item', 'Reference Materials', 'Scope of Review' ]
    Cell = [5,6,7,8,9,10]
    for Text,Row in zip(Text,Cell):
        Format_Cell('C'+str(Row), 'E'+str(Row), Text, GREY, TXT_MED, TXT_BLACK, CENTRE, LEFT, True, True)
    #review details
    Text = ['Moderator', 'Author/s', 'Reader', 'Reviewer(s)', 'Reviewer(s)', 'Type of Review']
    Cell = [5, 6, 7, 8, 9, 10]
    ws.Range(ws.Range('G8'), ws.Range('G9')).Merge()
    ws.Range(ws.Range('J8'), ws.Range('J9')).Merge()
    for Text, Row in zip(Text, Cell):
        Format_Cell('G' + str(Row), 'I' + str(Row), Text, GREY, TXT_MED, TXT_BLACK, CENTRE, LEFT, True, True)

    #spl format for  Cell 'Reviewer's
    for each in Cell:
        Format_Cell('J' + str(each), 'K' + str(each), [''], WHITE, TXT_MED, TXT_BLACK, CENTRE, LEFT, False, True)
    # defect log
    Format_Cell('C11', 'K11', ['Defect Log'], GREY, TXT_MED, TXT_BLACK, CENTRE, CENTRE, True, True)

    Text = ['Sl.No', 'Location of Defect', 'Line #/ Section #', 'Description of Defect', 'Assigned to', 'Log in Problem Tracking System', 'PR #', 'Status','Comments']
    Format_Cell('C12', 'K12', Text, GREEN, TXT_MED, TXT_BLACK, CENTRE, CENTRE, True, False)


def make_footer(curr, num_defects):
    Row = num_defects + 13
    Format_Cell('C'+str(curr), 'G'+str(curr), ['Number of defects logged in the Problem Tracking System'], 15, 10, TXT_BLACK, CENTRE, RIGHT, True, True)
    Format_Cell('H'+str(curr), 'H'+str(curr), ['0'], 15, 10, TXT_BLACK, CENTRE, CENTRE, True, False)
    Format_Cell('I'+str(curr), 'J'+str(curr), ['Total defects'], 15, 10, TXT_BLACK, CENTRE, CENTRE, True, True)
    Formula = '=COUNTA(J13:J' + str(Row) + ')-COUNTIF(J13:J' + str(Row) + ',"Not a Defect")'
    Format_Cell('K'+str(curr), 'K'+str(curr), [Formula], 15, 10, TXT_BLACK, CENTRE, CENTRE, True, False)
    curr += 1
    Format_Cell('C'+str(curr), 'K'+str(curr), ['Verification Details (To be filled after all the defects have been fixed or deferred)'], 35, 10, TXT_BLACK, CENTRE, CENTRE, True, True)
    curr += 1
    Format_Cell('C' + str(curr), 'E' + str(curr), ['Verification Date'], 15, 10, TXT_BLACK, CENTRE, CENTRE, True, True)
    Format_Cell('G' + str(curr), 'J' + str(curr), ['Defects Closure Verified By'], 15, 10, TXT_BLACK, CENTRE, CENTRE, True, True)


def draw_border():
    StartRow = 2
    used = ws.UsedRange
    FinalRow = used.Row + used.Rows.Count - 1
    StartCol = 2
    FinalCol = used.Column + used.Columns.Count - 1
    RowThick = [2,3,4,10,11,12, FinalRow-3, FinalRow-2, FinalRow-1, FinalRow]
    ColThick = [2, FinalCol]
    # Draw all Rows
    for row in range(StartRow, FinalRow + 1):
        for col in range(StartCol+1, FinalCol + 1):
            ws.Cells(row, col).Select()
            if row in RowThick:
                excel.Selection.Borders(9).LineStyle = SLD_LINE
                excel.Selection.Borders(9).Weight = 3
            else:
                excel.Selection.Borders(9).LineStyle = SLD_LINE
                excel.Selection.Borders(9).Weight = 2

    #Draw all Columns
    for col in range(StartCol, FinalCol + 1):
        for row in range(StartRow+1, FinalRow + 1):
            ws.Cells(row, col).Select()
            if col in ColThick:
                excel.Selection.Borders(10).LineStyle = SLD_LINE
                excel.Selection.Borders(10).Weight = THICK_LINE
            else:
                excel.Selection.Borders(10).LineStyle = SLD_LINE
                excel.Selection.Borders(10).Weight = THIN_LINE


def Fill_Col():
    StartRow = 2
    used = ws.UsedRange
    FinalRow = used.Row + used.Rows.Count - 1
    StartCol = 2
    FinalCol = used.Column + used.Columns.Count - 1
    for row in range(StartRow, (FinalRow + 1)):
        for col in range(StartCol, (FinalCol + 1)):
            if row == StartRow:
                ws.Cells(row, col).Interior.ColorIndex = BRD_YELLOW
            if col == StartCol:
                ws.Cells(row, col).Interior.ColorIndex = BRD_YELLOW
            if row == FinalRow:
                ws.Cells(row+1, col).Interior.ColorIndex = BRD_YELLOW
            if col == FinalCol:
                ws.Cells(row, col+1).Interior.ColorIndex = BRD_YELLOW
            if row == FinalRow and col == FinalCol:
                ws.Cells(row+1, col+1).Interior.ColorIndex = BRD_YELLOW
            if ws.Cells(row, col).Interior.ColorIndex == BCK_BLUE:
                ws.Cells(row, col).Interior.ColorIndex = 2


def Fill_Defect(rls):
    count = 0
    Curr_Row = 13

    ws.Range('F5').Value = rls['ProjectName']
    ws.Range('F6').value = rls['Reviewdate']


    '''for section, cell in zip(['Location', 'Line', 'Description'], [4, 5, 6]):
        ws.Cells(Str_Row, cell).Value = rls['rls_items'][section]'''

    for items in rls['rls_items']:
        count += 1
        ws.Cells(Curr_Row, 3).Value = count
        ws.Cells(Curr_Row, 4).Value = items['Location']
        ws.Cells(Curr_Row, 5).Value = items['Line']
        ws.Cells(Curr_Row, 6).Value = items['Description']
        Curr_Row += 1
    return Curr_Row, count

def Data_validate(end):
    ###Type = xlValidateList/3, AlertStyle = xlvalidAlertStop/1, Operator=xlBetween/1, Formula1="=ValidStatus"
    sts_list = ['Open', 'Not a Defect', 'Deferred', 'Fixed', 'Verified']
    sts_clr = [STS_RED, STS_YELLOW, STS_ORANGE, STS_BLUE, STS_DARKGREEN]
    sts_cnt = [1,2,3,4,5]
    ws.Range('J13','J'+str(end)).Validation.Add(3,1,1, '\''+ sts_list[0] +','+ sts_list[1]+','+ sts_list[2]+','+ sts_list[3]+','+ sts_list[4]+'\'')#'Open, Not a Defect, Deferred, Fixed, Verified'
    ws.Range('J13','J'+str(end)).Value = sts_list[0]
    for list,clr,cnt in zip(sts_list,sts_clr,sts_cnt):
        ws.Range('J13','J'+str(end)).FormatConditions.Add(1, 3, list)# 1:xlCellValue, 3:xlEqual
        ws.Range('J13','J'+str(end)).FormatConditions(cnt).Interior.ColorIndex = clr # RED

    ws.Range('H13', 'H' + str(end)).Validation.Add(3, 1, 1, 'Yes, No')


def Text_Align(end):
    end -= 1
    cols = string.ascii_uppercase[2:11]
    txt = [CENTRE,LEFT,LEFT,LEFT,CENTRE,CENTRE,CENTRE,CENTRE,LEFT]
    for col, fmat in zip(cols, txt):
        ws.Range(col+'13', col + str(end)).HorizontalAlignment = fmat


if __name__ == '__main__':
    RLSNAME = 'Generated RLS.xlsx'
    excel = win32com.client.DispatchEx('Excel.Application')
    excel.Visible = False
    path = os.getcwd()
    New_wb = excel.Workbooks.Add()
    ws = excel.Worksheets(1)
    ws.Name = 'Review Log Sheet'
    ws.Cells.Interior.ColorIndex = BCK_BLUE  # 6299648

    set_column_widths()
    make_header()
    end,count = Fill_Defect(rls)
    make_footer(end, count)
    Data_validate(end)
    draw_border()
    Fill_Col()
    Text_Align(end)

    if os.path.isfile(RLSNAME):
        os.remove(path+'\\'+RLSNAME)
    New_wb.SaveAs(path+'\\'+RLSNAME)
    New_wb.Close(True)
    excel.Application.Quit()
