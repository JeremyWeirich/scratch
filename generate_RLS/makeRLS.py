import string
import datetime

from openpyxl import Workbook
from openpyxl.formatting import Rule
from openpyxl.styles import Font, PatternFill, Alignment, Border, Side
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.worksheet.datavalidation import DataValidation


STATUS = DataValidation(type='list', formula1='"Open,Not a Defect,Deferred,Fixed,Verified"', allow_blank=True)

CENTER = Alignment(horizontal='center', vertical='center')
RIGHT = Alignment(horizontal='right', vertical='center')

GRAY = PatternFill(start_color='C0C0C0', end_color='C0C0C0', fill_type='solid')
GREEN = PatternFill(start_color='CCFFCC', end_color='CCFFCC', fill_type='solid')
CRED = PatternFill(start_color='FF3F3F', end_color='FF3F3F', fill_type='solid')
CORANGE = PatternFill(start_color='FF8001', end_color='FF8001', fill_type='solid')
CYELLOW = PatternFill(start_color='FFFF00', end_color='FFFF00', fill_type='solid')
CGREEN = PatternFill(start_color='00B050', end_color='00B050', fill_type='solid')
CBLUE = PatternFill(start_color='00B0F0', end_color='00B0F0', fill_type='solid')

TITLE = Font(size=10, bold=True)
REG = Font(size=10)

THICK = Side(border_style='thick', color='000000')
THIN = Side(border_style='thin', color='000000')

wb = Workbook()
ws = wb.active
ws.title = 'Review Log Sheet'
ws.add_data_validation(STATUS)


class Defect(object):
    def __init__(self, location, line, description):
        self.location = location
        self.line = line
        self.description = description


def make_sample_defects(n):
    defects = []
    for i in range(n):
        defects.append(Defect(
            'location',
            'line #',
            'description'
        ))
    return defects


def iter_cells(cell_range_string):
    rows = ws.iter_rows(cell_range_string)
    for row in rows:
        for cell in row:
            yield cell


def apply_border(cell_range_string, side):
    rows = list(ws.iter_rows(cell_range_string))
    max_y = len(rows) - 1  # index of the last row
    for pos_y, cells in enumerate(rows):
        max_x = len(cells) - 1  # index of the last cell
        for pos_x, cell in enumerate(cells):
            border = Border(
                left=cell.border.left,
                right=cell.border.right,
                top=cell.border.top,
                bottom=cell.border.bottom
            )
            if pos_x == 0:
                border.left = side
            if pos_x == max_x:
                border.right = side
            if pos_y == 0:
                border.top = side
            if pos_y == max_y:
                border.bottom = side

            # set new border only if it's one of the edge cells
            if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
                cell.border = border


def apply_conditional_formatting_to_cell(cell):
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Deferred",{})))'.format(cell)], operator='containsText', text='Deferred', dxf=DifferentialStyle(fill=CORANGE)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Verified",{})))'.format(cell)], operator='containsText', text='Verified', dxf=DifferentialStyle(fill=CGREEN)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Not a Defect",{})))'.format(cell)], operator='containsText', text='Not a Defect', dxf=DifferentialStyle(fill=CYELLOW)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Open",{})))'.format(cell)], operator='containsText', text='Open', dxf=DifferentialStyle(fill=CRED)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Fixed",{})))'.format(cell)], operator='containsText', text='Fixed', dxf=DifferentialStyle(fill=CBLUE)))


def apply_individual_border(cell_range_string, side):
    border = Border(
        left=side,
        right=side,
        top=side,
        bottom=side,
    )
    for cell in iter_cells(cell_range_string):
        cell.border = border


def make_header():
    apply_individual_border('C3:K11', THIN)
    # Review log sheet
    ws.merge_cells('C3:K3')
    ws['C3'] = 'Review Log Sheet'
    apply_border('C3:K3', THICK)
    ws['C3'].alignment = CENTER
    ws['C3'].font = Font(size=14, bold=True)
    ws['C3'].fill = GRAY

    # Review details
    ws.merge_cells('C4:K4')
    ws['C4'].alignment = CENTER
    ws['C4'] = 'Review Details'
    ws['C4'].font = TITLE
    ws['C4'].fill = GREEN
    apply_border('C4:K4', THICK)

    # project label
    apply_border('C5:F9', THICK)
    p_labels = [
        'Project Name - Version',
        'Review Date',
        'Project Phase',
        'Review Item',
        'Scope of Review'
    ]
    for label, row in zip(p_labels, range(5, 10)):
        ws.merge_cells('C{}:E{}'.format(row, row))
        first = ws['C{}'.format(row)]
        first.font = TITLE
        first.value = label
        first.fill = GRAY
    ws['F6'].value = datetime.datetime.now().strftime('%B %d, %Y')
    ws['F6'].font = REG

    # reviewer label
    apply_border('G5:K9', THICK)
    r_labels = [
        'Moderator',
        'Author(s)',
        'Reader',
        'Reviewer(s)',
        'Review Type',
    ]
    for label, row in zip(r_labels, range(5, 10)):
        ws.merge_cells('G{}:I{}'.format(row, row))
        first = ws['G{}'.format(row)]
        first.font = TITLE
        first.value = label
        first.fill = GRAY

    # reviewer info
    ws.merge_cells('J5:K5')
    ws.merge_cells('J6:K6')
    ws.merge_cells('J7:K7')
    ws.merge_cells('J8:K8')
    ws.merge_cells('J9:K9')
    ws['J8'].value = 'VTC SCAN'
    ws['J8'].font = REG
    ws['J9'].value = 'Automated Review'
    ws['J9'].font = REG

    # Defect log
    ws.merge_cells('C10:K10')
    apply_border('C10:K10', THICK)
    ws['C10'].alignment = CENTER
    ws['C10'] = 'Defect Log'
    ws['C10'].font = TITLE
    ws['C10'].fill = GRAY

    # Defect h
    headers = [
        'SI. No',
        'Location of Defect',
        r'Line #/Section #',
        'Description of Defect',
        'Assigned to',
        'Log in Problem Tracking System',
        'PR #',
        'Status',
        'Comments',
    ]
    cols = string.ascii_uppercase[2:11]
    for header, column in zip(headers, cols):
        cell = ws['{}11'.format(column)]
        cell.value = header
        cell.font = TITLE
        cell.fill = GREEN
        cell.alignment = CENTER
    apply_border('C11:K11', THICK)


def add_defects(defects):
    count = 0
    start_row = 11
    apply_individual_border('C{}:K{}'.format(start_row, start_row + len(defects)), THIN)
    for defect in defects:
        count += 1
        ws['C{}'.format(count + start_row)].value = count
        ws['C{}'.format(count + start_row)].alignment = CENTER
        ws['D{}'.format(count + start_row)].value = defect.location
        ws['E{}'.format(count + start_row)].value = defect.line
        ws['F{}'.format(count + start_row)].value = defect.description
        ws['J{}'.format(count + start_row)].value = 'Open'
        STATUS.add(ws['J{}'.format(count + start_row)])
        apply_conditional_formatting_to_cell('J{}'.format(count + start_row))
        for cell in iter_cells('C{}:K{}'.format(count + start_row, count + start_row)):
            cell.font = REG
        for cell in iter_cells('G{}:J{}'.format(count + start_row, count + start_row)):
            cell.alignment = CENTER
    apply_border('C{}:K{}'.format(start_row, start_row + len(defects)), THICK)
    return start_row + count, count


def make_footer(curr, num_defects):
    curr += 1
    # Number label
    ver_range = 'C{}:G{}'.format(curr, curr)
    ws.merge_cells(ver_range)
    apply_border(ver_range, THICK)
    ws['C{}'.format(curr)].alignment = RIGHT
    ws['C{}'.format(curr)].value = 'Number of defects logged in the Problem Tracking System'
    ws['C{}'.format(curr)].font = TITLE
    ws['C{}'.format(curr)].fill = GRAY
    # Number
    ver_range = 'H{}:H{}'.format(curr, curr)
    ws.merge_cells(ver_range)
    apply_border(ver_range, THICK)
    ws['H{}'.format(curr)].alignment = CENTER
    ws['H{}'.format(curr)].value = 0
    ws['H{}'.format(curr)].font = TITLE
    ws['H{}'.format(curr)].fill = GRAY
    # Total label
    ver_range = 'I{}:J{}'.format(curr, curr)
    ws.merge_cells(ver_range)
    apply_border(ver_range, THICK)
    ws['I{}'.format(curr)].alignment = CENTER
    ws['I{}'.format(curr)].value = 'Total Defects'
    ws['I{}'.format(curr)].font = TITLE
    ws['I{}'.format(curr)].fill = GRAY
    # Total
    ver_range = 'K{}:K{}'.format(curr, curr)
    ws.merge_cells(ver_range)
    apply_border(ver_range, THICK)
    ws['K{}'.format(curr)].alignment = CENTER
    ws['K{}'.format(curr)].value = num_defects
    ws['K{}'.format(curr)].font = TITLE
    ws['K{}'.format(curr)].fill = GRAY

    # Verification details label
    curr += 1
    ver_range = 'C{}:K{}'.format(curr, curr)
    ws.merge_cells(ver_range)
    apply_border(ver_range, THICK)
    ws['C{}'.format(curr)].alignment = CENTER
    ws['C{}'.format(curr)].value = 'Number of defects logged in the Problem Tracking System'
    ws['C{}'.format(curr)].font = TITLE
    ws['C{}'.format(curr)].fill = GREEN

    # Verification details label
    curr += 1
    ver_range = 'C{}:K{}'.format(curr, curr)
    apply_individual_border(ver_range, THIN)
    # date
    r = 'C{}:E{}'.format(curr, curr)
    ws.merge_cells(r)
    ws['C{}'.format(curr)].value = 'Verification Date'
    ws['C{}'.format(curr)].font = TITLE
    ws['C{}'.format(curr)].fill = GRAY

    # who
    r = 'G{}:J{}'.format(curr, curr)
    ws.merge_cells(r)
    ws['G{}'.format(curr)].value = 'Defects Closure Verified By'
    ws['G{}'.format(curr)].font = TITLE
    ws['G{}'.format(curr)].fill = GRAY

    apply_border(ver_range, THICK)


def set_column_widths():
    widths = [
        2.14,
        2.14,
        6.86,
        24.14,
        15.29,
        41.86,
        14,
        15.14,
        9.29,
        10.57,
        36.14,
        2.14
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        ws.column_dimensions[column].width = width

set_column_widths()
make_header()
end, count = add_defects(make_sample_defects(10))
make_footer(end, count)


wb.save(r'N:\Users\Jeremy Weirich\Python Projects\Scratch\generate_RLS\generated_RLS.xlsx')
