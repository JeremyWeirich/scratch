import re
import os
import json
from itertools import count

from openpyxl import load_workbook

FILENAME = r'N:\Users\Imran Mohammed\DLCM\CD539 SW-V129\Test_Result\VTC-P15253010_FORD_DLCM-V1_2_6-V1_2_7-V1_2_8-V1_2_9.xlsx'
MODIFY_REGEX = re.compile(r'[Mm]odify( [Tt]est)?( [Cc]ase)?( *)?:( *)?(\n*)?([\s\S]*)$')


def get_rls_items(filename):
    rls_items = []
    basename = os.path.basename(filename)
    print basename
    wb = load_workbook(filename, data_only=True, read_only=True)
    for sheet in wb.worksheets:
        found_in_this_tab = []
        for r_index, row in zip(count(), sheet.iter_rows()):
            at, comment = None, None
            if 'auto' in sheet.title.lower():
                if len(row) > 10:
                    at = row[2].value
                    comment = row[10].value
            else:
                if len(row) > 11:
                    at = row[3].value
                    comment = row[11].value
            if comment and isinstance(comment, basestring):
                description = re.search(MODIFY_REGEX, comment)
                if description:
                    found_in_this_tab.append({
                        'Location': sheet.title,
                        'Line': '{} / {}'.format(r_index + 1, at),
                        'Description': description.group(6),
                    })
        print 'checked tab {} [{}]'.format(sheet.title, len(found_in_this_tab))
        rls_items.extend(found_in_this_tab)
    return rls_items


if __name__ == '__main__':
    items = get_rls_items(FILENAME)
    print 'found:', len(items)
    with open('result.json', 'w') as f:
        f.write(json.dumps(items))
