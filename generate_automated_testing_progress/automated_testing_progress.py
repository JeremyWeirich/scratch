from openpyxl import load_workbook
import os
import operator
import pprint
import string
import datetime

from openpyxl import Workbook
from openpyxl.formatting import Rule
from openpyxl.styles import Font, PatternFill, Alignment, Border, Side
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.worksheet.datavalidation import DataValidation


# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# the path of the folder containing the VTCs and the project part of the VTC title
base = r'C:\Users\George Alvarez\Desktop\George Alvarez CVS\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM'
vtc_title_root = r'VTC-P14106001_BMW_CDM_'
# for each column in the final document, add a dictionary to the 'documents' list. the keys of the dictionary are:
# function: the final part of the name of the document.  this value added to the end of vtc_title_root should spell the name of the document
# name: the name of the column
# sheets: which manual and automated sheets go in the column
documents = [
    {'function': 'CoSi', 'name': 'CoSi 4DNP', 'sheets': ['4DNP Manual', '4DNP Auto CP1']},
    {'function': 'CoSi', 'name': 'CoSi 4DWP', 'sheets': ['4DWP Manual', '4DWP Auto CP2', '4DWP Auto CP3', '4DWP Auto CP4']},
    {'function': 'CoSi', 'name': 'CoSi 2D', 'sheets': ['2D Manual', '2D Auto CP5']},
    {'function': 'CID', 'name': 'CID RR11', 'sheets': ['RR11 Man', 'RR11 Auto CP1', 'RR11 Auto CP2', 'RR11 Auto CP3']},
    {'function': 'CID', 'name': 'CID RR31', 'sheets': ['RR31 Man']},
    {'function': 'HSK', 'name': 'HSK RR11', 'sheets': ['RR11 Man', 'RR11 Auto CP1', 'RR11 Auto CP2', 'RR11 Auto CP3']},
    {'function': 'HSK', 'name': 'HSK RR31', 'sheets': ['RR31 Man']},
    {'function': 'HPW', 'name': 'HPW RR12', 'sheets': ['RR12 Man', 'RR12 Auto CP1', 'RR12 Auto CP2', 'RR12 Auto CP3']},
    {'function': 'Curtain', 'name': 'Curtains 4D', 'sheets': ['High Man 4 Door', 'High Auto CP1', 'High Auto CP2', 'High Auto CP3']},
    {'function': 'Curtain', 'name': 'Curtains 2D', 'sheets': ['High Man 2 Door']},
    {'function': 'Curtain', 'name': 'Curtains LSat', 'sheets': ['LSat Man', 'LSat Auto CP1', 'LSat Auto CP2', 'LSat Auto CP3']},
    {'function': 'Curtain', 'name': 'Curtains RSat', 'sheets': ['RSat Man', 'RSat Auto CP1', 'RSat Auto CP2', 'RSat Auto CP3']},
    {'function': 'KFS', 'name': 'KFS', 'sheets': ['KFS Man', 'KFS Auto CP1', 'KFS Auto CP2', 'KFS Auto CP3']},
    {'function': 'Volume', 'name': 'Volume LSat', 'sheets': ['Volume Left Sat']},
    {'function': 'Volume', 'name': 'Volume RSat', 'sheets': ['Volume Right Sat']},
    {'function': 'General', 'name': 'General 4D', 'sheets': ['High Variant - 4DNP Man']},
    {'function': 'General', 'name': 'General 2D', 'sheets': ['High Variant - 4DWP&2D Man']},
    {'function': 'General', 'name': 'General LSat', 'sheets': ['Left Door Satellite Man']},
    {'function': 'General', 'name': 'General RSat', 'sheets': ['Right Door Satellite Man']},
    {'function': 'ZuZi', 'name': 'ZuZi 4D', 'sheets': ['High Variant - 4 Door', 'Hi 4D Auto CP1', 'Hi 4D Auto CP2', 'Hi 4D Auto CP3', 'Hi 4D Auto CP4', 'Hi 4D Auto CP5', 'Hi 4D Auto CP6', 'Hi 4D Auto CP7', 'Hi 4D Auto CP8']},
    {'function': 'ZuZi', 'name': 'ZuZi 2D', 'sheets': ['High Variant - 2 Door', 'Hi 2D Auto CP9', 'Hi 2D Auto CP10', 'Hi 2D Auto CP11']},
    {'function': 'ZuZi', 'name': 'ZuZi LSat', 'sheets': ['Left Door Satellite', 'LS 4D Auto CP1', 'LS 4D Auto CP4', 'LS 4D Auto CP6', 'LS 4D Auto CP7', 'LS 4D Auto CP8']},
    {'function': 'ZuZi', 'name': 'ZuZi RSat', 'sheets': ['Right Door Satellite', 'RS 4D Auto CP1', 'RS 4D Auto CP4', 'RS 4D Auto CP6', 'RS 4D Auto CP7', 'RS 4D Auto CP8']},
]
# output_path = r'N:\Users\Software Validation Team\Tools\Generated RLS\CDM_Automated_Testing_Progress.xlsx'
output_path = r'CDM_Automated_Testing_Progress.xlsx'
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


STATUS = DataValidation(type='list', formula1='"Verified,Not Implemented,Not Planned"', allow_blank=True)

CENTER = Alignment(horizontal='center', vertical='center')
RIGHT = Alignment(horizontal='right', vertical='center')
LEFT = Alignment(horizontal='left', vertical='center')

GRAY = PatternFill(start_color='696969', end_color='696969', fill_type='solid')
GREEN = PatternFill(start_color='92D050', end_color='92D050', fill_type='solid')
RED = PatternFill(start_color='E6B8B7', end_color='E6B8B7', fill_type='solid')

TITLE = Font(size=16, name='Calibri')
MINI_TITLE = Font(size=11, name='Calibri', bold=True)
REG = Font(size=11, name='Calibri')

THICK = Side(border_style='thick', color='000000')
THIN = Side(border_style='thin', color='000000')


def apply_conditional_formatting_to_cell(ws, cell):
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Verified",{})))'.format(cell)], operator='containsText', text='Verified',
                                                  dxf=DifferentialStyle(fill=GREEN)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Not Implemented",{})))'.format(cell)], operator='containsText', text='Not Implemented',
                                                  dxf=DifferentialStyle(fill=RED)))
    ws.conditional_formatting.add(cell, Rule(type='containsText', formula=['NOT(ISERROR(SEARCH("Not Planned",{})))'.format(cell)], operator='containsText', text='Not Planned',
                                                  dxf=DifferentialStyle(fill=GRAY)))


def apply_border(ws, cell_range_string, side):
    rows = list(ws.iter_rows(cell_range_string))
    max_y = len(rows) - 1  # index of the last row
    for pos_y, cells in enumerate(rows):
        max_x = len(cells) - 1  # index of the last cell
        for pos_x, cell in enumerate(cells):
            border = Border(
                left=cell.border.left,
                right=cell.border.right,
                top=cell.border.top,
                bottom=cell.border.bottom
            )
            if pos_x == 0:
                border.left = side
            if pos_x == max_x:
                border.right = side
            if pos_y == 0:
                border.top = side
            if pos_y == max_y:
                border.bottom = side

            # set new border only if it's one of the edge cells
            if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
                cell.border = border


def set_column_widths(worksheet):
    widths = [
        12.9,
        9.6,
        140,
        9.6,
        19,
        70,
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        worksheet.column_dimensions[column].width = width


def iter_cells(worksheet, cell_range_string):
    rows = worksheet.iter_rows(cell_range_string)
    for r in rows:
        for c in r:
            yield c


new_wb = Workbook()
overview = new_wb.active
overview.title = 'Overview'
overview_columns = string.ascii_uppercase
overview_column = 0

# Make overview intro
overview['A2'].value = 'Objectives'
overview['A3'].value = 'Verified'
overview['A4'].value = 'Not Implemented'
overview['A5'].value = 'Not Planned'
overview['A6'].value = 'Automated [Possible]'
overview['A7'].value = 'Automated [Total]'
overview.column_dimensions['A'].width = 19.8
overview_column += 1

# MAIN LOOP -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
for document in documents:
    requirements = {}
    input_file = base + '\\' + vtc_title_root + document['function'] + '.xlsx'
    title = document['name']
    sheets = document['sheets']

    print 'adding objectives from {}'.format(title)

    wb = load_workbook(input_file, data_only=True, read_only=True)
    for sheet in sheets:
        try:
            location = int(sheet.split('CP')[1])
        except:
            location = None
        ws = wb.get_sheet_by_name(sheet)
        for frow in ws.iter_rows():
            row = [cell.value for cell in frow]
            if 'Auto' in sheet:
                ts = row[2]
                start = 'AT'
                desc = row[3]
            else:
                ts = row[3]
                start = 'TS'
                desc = row[4]
            if ts:
                if isinstance(ts, basestring):
                    if ts.startswith(start):
                        if ts.count('_') == 1:
                            req = int(ts.split('_')[0][2:])
                            obj = int(ts.split('_')[1])
                            desc = desc.split(': ')[1] if ': ' in desc else desc

                            if req not in requirements:
                                requirements[req] = {}
                            requirements[req][obj] = dict(
                                desc=desc,
                                cp=location,
                            )

    sorted_reqs = sorted(requirements)

    new_ws = new_wb.create_sheet()
    new_ws.title = title

    # Overview ---------------------------------------------------
    col = overview_columns[overview_column]
    apply_border(overview, '{}1:{}7'.format(col, col), THIN)
    for c in iter_cells(overview, '{}1:{}7'.format(col, col)):
        c.alignment = CENTER
        c.font = REG
    overview['{}1'.format(col)] = title
    overview['{}1'.format(col)].font = MINI_TITLE
    overview.column_dimensions[col].width = 10
    overview_column += 1
    overview['{}2'.format(col)] = r"=SUM({}3:{}5)".format(col, col)
    for i in [3, 4, 5]:
        overview['{}{}'.format(col, i)] = r"=COUNTIF('{}'!$E:$E, $A${})".format(title, i)
    overview['{}6'.format(col)] = r'=IF(({}2-{}5)>0, {}3/({}2-{}5), 1)'.format(col, col, col, col, col)
    overview['{}6'.format(col)].number_format = '0.0%'
    overview['{}7'.format(col)] = r'={}3/{}2'.format(col, col)
    overview['{}7'.format(col)].number_format = '0.0%'
    # End overview -----------------------------------------------

    new_ws.add_data_validation(STATUS)
    set_column_widths(new_ws)
    rnum = 3
    new_ws['A1'].value = '  ' + title
    new_ws['A1'].alignment = LEFT
    new_ws['A1'].font = TITLE
    new_ws.merge_cells('A1:F2')
    new_ws['A3'].value = 'Requirement'
    new_ws['A3'].alignment = CENTER
    new_ws['B3'].value = 'Objective'
    new_ws['B3'].alignment = CENTER
    new_ws['C3'].value = 'Description'
    new_ws['C3'].alignment = CENTER
    new_ws['D3'].value = 'CP'
    new_ws['D3'].alignment = CENTER
    new_ws['E3'].value = 'Status'
    new_ws['E3'].alignment = CENTER
    new_ws['F3'].value = 'Notes'
    new_ws['F3'].alignment = CENTER

    for column in 'ABCDEF':
        apply_border(new_ws, '{}3:{}3'.format(column, column), THIN)
    apply_border(new_ws, 'A1:F2', THIN)

    for requirement in sorted_reqs:
        objectives = requirements[requirement]
        sorted_objs = sorted(objectives)
        for objective in sorted_objs:
            data = objectives[objective]
            desc = data['desc']
            cp = data['cp']
            rnum += 1
            new_ws['A{}'.format(rnum)] = requirement
            new_ws['A{}'.format(rnum)].alignment = CENTER
            new_ws['B{}'.format(rnum)] = objective
            new_ws['B{}'.format(rnum)].alignment = CENTER
            new_ws['C{}'.format(rnum)] = desc
            new_ws['D{}'.format(rnum)] = cp
            new_ws['D{}'.format(rnum)].alignment = CENTER
            if cp:
                new_ws['E{}'.format(rnum)] = 'Verified'
            else:
                new_ws['E{}'.format(rnum)] = 'Not Implemented'
            new_ws['E{}'.format(rnum)].alignment = CENTER
            apply_conditional_formatting_to_cell(new_ws, 'E{}'.format(rnum))
            STATUS.add(new_ws['E{}'.format(rnum)])
            new_ws['F{}'.format(rnum)] = None

    top = None
    bottom = None
    current = None
    rnum = 3
    for frow in new_ws.iter_rows(row_offset=rnum):
        rnum += 1
        row = [cell.value for cell in frow]
        req = row[0]
        if req != current:
            if top:
                if bottom:
                    new_ws.merge_cells('A{}:A{}'.format(top, bottom))
                    for column in 'ABCDEF':
                        apply_border(new_ws, '{}{}:{}{}'.format(column, top, column, bottom), THIN)
            top = rnum
            bottom = rnum
            current = req
        else:
            bottom = rnum


new_wb.save(output_path)
