import pandas as pd
from bokeh.charts import Bar, show
from bokeh.charts.attributes import color

import json

data = dict(
    Suite=[],
    rank=[],
    Objective=[]
)

with open('suites.txt', 'r') as s:
    suites = json.loads(s.read())

for tab, reqs in suites.items():
    manual = 0
    automated = 0
    for req, objs in reqs.items():
        for obj, params in objs.items():
            if params['cp']:
                automated += 1
            else:
                manual += 1
    data['Suite'].extend([tab, tab])
    data['rank'].extend(['Automated', 'Manual'])
    data['Objective'].extend([automated, manual])


df = pd.DataFrame(data)
p = Bar(
    df,
    label='Suite',
    values='Objective',
    stack='rank',
    legend='top_right',
    color=color(palette=['green', 'red']),
)
show(p)
