import sys
from xml.dom import minidom
import xml.etree.ElementTree as ET


def att(doc, tag, attribute):
    return doc.getElementsByTagName(tag)[0].attributes[attribute].value


def val(doc, category, tag):
    return doc.getElementsByTagName(category)[0].getElementsByTagName(tag)[0].firstChild.nodeValue


def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


class Merger(object):
    def __init__(self, files):
        self.id_num = 0
        self.files = files
        self.doc = None
        self.variants = []
        self.tal = None

    def get_id(self):
        self.id_num += 1
        return 'tl_{}'.format(self.id_num)

    def make_head(self):
        self.tal = ET.Element('tal', {
            'status': "Executable",
            'refSchemaFile': "tal.xsd",
            'schemaVersion': "5.0.0",
            'xmlns': "http://bmw.com/2005/psdz.data.tal",
        })
        ET.SubElement(self.tal, 'ExecProperties', {'supportsParallelMostFlash': 'false'})
    
    def add_section(self, cat):
        def add_property(parent, name):
            a = ET.SubElement(parent, name)
            a.text = val(doc, cat, name)
        for filename in self.files:
            doc = minidom.parse(filename)
            tal_line = ET.SubElement(self.tal, 'talLine', {
                'id': self.get_id(),
                'status': 'Executable',
                'baseVariant': 'CDM2',
                'diagAddress': att(doc, 'lineECU', 'diagAddress'),
            })
            outer = ET.SubElement(tal_line, cat, {'status': 'Executable'})
            outerTA = ET.SubElement(outer, '{}TA'.format(cat), {'status': 'Executable'})
            sgbmid = ET.SubElement(outerTA, 'sgbmid')
            properties = [
                'processClass',
                'id',
                'mainVersion',
                'subVersion',
                'patchVersion',
            ]
            for p in properties:
                add_property(sgbmid, p)

    def make_tail(self):
        ET.SubElement(self.tal, 'executionTime', {
            'plannedStartTime': "0",
            'plannedEndTime': "0",
            'actualStartTime': "0",
            'actualEndTime': "0",
        })
        ET.SubElement(self.tal, 'installedECUList_Ist')
        ET.SubElement(self.tal, 'installedECUList_Soll')

    def save(self, name):
        indent(self.tal)
        output = ET.ElementTree(self.tal)
        output.write(name, encoding='UTF-8')


def main():
    files = sys.argv[2:]
    output_name = sys.argv[1]
    # files = ['high_generated_tal.xml', 'left_generated_tal.xml', 'right_generated_tal.xml']
    # output_name = 'output.xml'
    sections = [
        'blFlash',
        'swDeploy',
        'cdDeploy',
    ]
    merger = Merger(files)
    merger.make_head()
    for section in sections:
        merger.add_section(section)
    merger.make_tail()
    merger.save(output_name)

if __name__ == '__main__':
    main()
