from xml.dom import minidom
import lxml.etree as ET

tals = [
    'high_generated_tal.xml',
    'left_generated_tal.xml',
    'right_generated_tal.xml'
]

global id_num
id_num = 0
def get_id():
    global id_num
    id_num += 1
    return 'tl_{}'.format(id_num)


def att(doc, tag, att):
    return doc.getElementsByTagName(tag)[0].attributes[att].value


def val(doc, cat, tag):
    a = doc.getElementsByTagName(cat)[0]
    b = a.getElementsByTagName(tag)[0].firstChild.nodeValue
    return b


class Section(object):
    def __init__(self):
        self.processClass = '0'
        self.id = '0'
        self.mainVersion = '0'
        self.subVersion = '0'
        self.patchVersion = '0'


class SubTal(object):
    def __init__(self):
        self.status = 'Executable'
        self.baseVariant = 'CDM2'
        self.diagAddress = '0'

        self.blFlash = Section()
        self.swDeploy = Section()
        self.cdDeploy = Section()


sub_tals = []

for filename in tals:
    st = SubTal()
    sub_tals.append(st)
    doc = minidom.parse(filename)
    st.diagAddress = att(doc, 'lineECU', 'diagAddress')
    # BTLD
    cat = 'blFlash'
    st.blFlash.processClass = val(doc, cat, 'processClass')
    st.blFlash.id = val(doc, cat, 'id')
    st.blFlash.mainVersion = val(doc, cat, 'mainVersion')
    st.blFlash.subVersion = val(doc, cat, 'subVersion')
    st.blFlash.patchVersion = val(doc, cat, 'patchVersion')
    # SWD
    cat = 'swDeploy'
    st.swDeploy.processClass = val(doc, cat, 'processClass')
    st.swDeploy.id = val(doc, cat, 'id')
    st.swDeploy.mainVersion = val(doc, cat, 'mainVersion')
    st.swDeploy.subVersion = val(doc, cat, 'subVersion')
    st.swDeploy.patchVersion = val(doc, cat, 'patchVersion')
    # SWD
    cat = 'cdDeploy'
    st.cdDeploy.processClass = val(doc, cat, 'processClass')
    st.cdDeploy.id = val(doc, cat, 'id')
    st.cdDeploy.mainVersion = val(doc, cat, 'mainVersion')
    st.cdDeploy.subVersion = val(doc, cat, 'subVersion')
    st.cdDeploy.patchVersion = val(doc, cat, 'patchVersion')


tal = ET.Element('tal')
tal.attrib.update({
    'status': "Executable",
    'refSchemaFile': "tal.xsd",
    'schemaVersion': "5.0.0",
    'xmlns': "http://bmw.com/2005/psdz.data.tal",
})

exec_prop = ET.SubElement(tal, 'ExecProperties', {'supportsParallelMostFlase': 'false'})

for st in sub_tals:
    # BTLD
    line = ET.SubElement(tal, 'talLine')
    line.attrib.update({
        'id': get_id(),
        'status': 'Executable',
        'baseVariant': st.baseVariant,
        'diagAddress': st.diagAddress,
    })
    blFlash = ET.SubElement(line, 'blFlash', {'status': 'Executable'})
    blFlashTA = ET.SubElement(blFlash, 'blFlashTA', {'status': 'Executable'})
    sgbmid = ET.SubElement(blFlashTA, 'sgbmid')
    processClass = ET.SubElement(sgbmid, 'processClass')
    processClass.text = st.blFlash.processClass
    id = ET.SubElement(sgbmid, 'id')
    id.text = st.blFlash.id
    mainVersion = ET.SubElement(sgbmid, 'mainVersion')
    mainVersion.text = st.blFlash.mainVersion
    subVersion = ET.SubElement(sgbmid, 'subVersion')
    subVersion.text = st.blFlash.subVersion
    patchVersion = ET.SubElement(sgbmid, 'patchVersion')
    patchVersion.text = st.blFlash.patchVersion

for st in sub_tals:
    # SWD
    line = ET.SubElement(tal, 'talLine')
    line.attrib.update({
        'id': get_id(),
        'status': 'Executable',
        'baseVariant': st.baseVariant,
        'diagAddress': st.diagAddress,
    })
    swDeploy = ET.SubElement(line, 'swDeploy', {'status': 'Executable'})
    swDeployTA = ET.SubElement(swDeploy, 'swDeployTA', {'status': 'Executable'})
    sgbmid = ET.SubElement(swDeployTA, 'sgbmid')
    processClass = ET.SubElement(sgbmid, 'processClass')
    processClass.text = st.swDeploy.processClass
    id = ET.SubElement(sgbmid, 'id')
    id.text = st.swDeploy.id
    mainVersion = ET.SubElement(sgbmid, 'mainVersion')
    mainVersion.text = st.swDeploy.mainVersion
    subVersion = ET.SubElement(sgbmid, 'subVersion')
    subVersion.text = st.swDeploy.subVersion
    patchVersion = ET.SubElement(sgbmid, 'patchVersion')
    patchVersion.text = st.swDeploy.patchVersion

for st in sub_tals:
    # SWD
    line = ET.SubElement(tal, 'talLine')
    line.attrib.update({
        'id': get_id(),
        'status': 'Executable',
        'baseVariant': st.baseVariant,
        'diagAddress': st.diagAddress,
    })
    cdDeploy = ET.SubElement(line, 'cdDeploy', {'status': 'Executable'})
    cdDeployTA = ET.SubElement(cdDeploy, 'cdDeployTA', {'status': 'Executable'})
    sgbmid = ET.SubElement(cdDeployTA, 'sgbmid')
    processClass = ET.SubElement(sgbmid, 'processClass')
    processClass.text = st.cdDeploy.processClass
    id = ET.SubElement(sgbmid, 'id')
    id.text = st.cdDeploy.id
    mainVersion = ET.SubElement(sgbmid, 'mainVersion')
    mainVersion.text = st.cdDeploy.mainVersion
    subVersion = ET.SubElement(sgbmid, 'subVersion')
    subVersion.text = st.cdDeploy.subVersion
    patchVersion = ET.SubElement(sgbmid, 'patchVersion')
    patchVersion.text = st.cdDeploy.patchVersion

eT = ET.SubElement(tal, 'executionTime')
eT.attrib.update({
    'plannedStartTime': "0",
    'plannedEndTime': "0",
    'actualStartTime': "0",
    'actualEndTime': "0",
})

iECUIst = ET.SubElement(tal, 'installedECUList_Ist')
iECUSoll = ET.SubElement(tal, 'installedECUList_Soll')

output = ET.ElementTree(tal)
output.write('output.xml', encoding='UTF-8', method='xml', pretty_print=True)
