import binascii
import glob
import os

# TRAPS_TO_COUNT = 3

# dump parameters
HEADER_FLAG = [
    '40000028'
]
RECORD_FLAG = [
    '400001E0',
    '400002E0',
    '400003E0',
]
HEADER_GAP = 93
RECORD_GAP = 539

# log parameters
HEADER_ID = '122'
RECORD_ID = '129'


def check_hex_dump(filename):
    begin_counting_for_header = False

    def rearrange(s):  # reverse byte order: 01234567 --> 67452301
        output = s[6:8] + s[4:6] + s[2:4] + s[0:2]
        return output

    with open(filename, "rb") as dump:
        header_results, record_results = [], []
        read_by = 1
        byte = dump.read(read_by)
        count = 0
        records_found = 0
        header_index = None
        record_index = None

        data = ''
        while byte != '':
            count += 1
            num = binascii.hexlify(byte)
            num_in_hex = num.decode('hex')
            if len(data) < 8:
                data += num_in_hex
            else:
                data = data[read_by:] + num_in_hex

            if data in HEADER_FLAG:
                # skip the first header found
                if begin_counting_for_header:
                    header_index = count
                else:
                    begin_counting_for_header = True

            elif data in RECORD_FLAG:
                record_index = count

            if header_index and count - header_index == HEADER_GAP:
                header_results.append(rearrange(data))
                header_index = None
            if record_index and count - record_index == RECORD_GAP:
                record_results.append(rearrange(data))
                record_index = None
                records_found += 1

            byte = dump.read(read_by)

    return header_results, record_results


def check_can_log(filename):
    header_results, record_results = [], []
    with open(filename, 'r') as log:
        for line in log.readlines():
            split_line = line.split()
            if len(split_line) == 23:
                can_id = split_line[2]
                start = ''.join(split_line[6:10])
                end = ''.join(split_line[10:14])
                if can_id == HEADER_ID:
                    header_results.append(end)
                elif can_id == RECORD_ID:
                    record_results.append(start)
    return header_results, record_results


def compare_multiple():
    report = ''
    all_pass = True

    for dump_filename in glob.glob('logs\*.hex'):
        current = ''
        base_name = os.path.basename(dump_filename).split('.hex')[0]
        can_filename = 'logs\{}.asc'.format(base_name)

        current += 'reading {}\n'.format(dump_filename)
        dump_headers, dump_records = check_hex_dump(dump_filename)
        current += 'reading {}\n'.format(can_filename)
        try:
            can_headers, can_records = check_can_log(can_filename)
        except IOError:
            error = '\nCould not find/open {}\n\n'.format(can_filename)
            print error
            current += error
            report += current
            continue

        all_results = [dump_headers, can_headers, dump_records, can_records]
        longest = max([len(i) for i in all_results])
        for result in all_results:
            while len(result) < longest:
                result.append('   NA   ')

        all_pass &= (dump_headers == can_headers)
        all_pass &= (dump_records == can_records)

        current += 'Location\t\t{0}\t\t{1}\t\tMatches?\n'.format(os.path.basename(dump_filename), os.path.basename(can_filename))
        for count in range(longest):
            current += 'Header {0}\t\t{1}\t\t{2}\t\t{3}\n'.format(count + 1, dump_headers[count], can_headers[count], dump_headers[count] == can_headers[count])
            current += 'Record {0}\t\t{1}\t\t{2}\t\t{3}\n'.format(count + 1, dump_records[count], can_records[count], dump_records[count] == can_records[count])

        print current
        report += current
        report += '\n'
    report += 'All match: {}'.format(all_pass)
    with open('results.txt', 'w') as f:
        f.write(report)


if __name__ == '__main__':
    compare_multiple()
