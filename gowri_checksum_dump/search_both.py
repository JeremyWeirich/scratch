import binascii

# --------------------------------------------------------------------------------------------------------------------
DUMP_TO_CHECK = r'dump_to_check.hex'
LOG_TO_CHECK = r'elog_CAN_Trace.asc'
SAVE_PATH = 'results.txt'
# --------------------------------------------------------------------------------------------------------------------

h = [[], []]
r = [[], []]

# dump parameters
HEADER_FLAG = [
    '40000028'
]
RECORD_FLAG = [
    '400001E0',
    '400002E0',
    '400003E0',
]
HEADER_GAP = 93
RECORD_GAP = 539

begin_counting_for_header = False

# log parameters
HEADER_ID = '122'
RECORD_ID = '129'


# checking dump
def rearrange(s):  # reverse byte order: 01234567 --> 67452301
    output = s[6:8] + s[4:6] + s[2:4] + s[0:2]
    return output


with open(DUMP_TO_CHECK, "rb") as dump:
    print 'reading hex dump:', DUMP_TO_CHECK
    read_by = 1
    byte = dump.read(read_by)
    count = 0
    header_count = None
    record_count = None

    data = ''
    while byte != '':
        count += 1
        num = binascii.hexlify(byte)
        num_in_hex = num.decode('hex')
        if len(data) < 8:
            data += num_in_hex
        else:
            data = data[read_by:] + num_in_hex

        if data in HEADER_FLAG:
            # skip the first header found
            if begin_counting_for_header:
                header_count = count
            else:
                begin_counting_for_header = True

        elif data in RECORD_FLAG:
            record_count = count

        if header_count and count - header_count == HEADER_GAP:
            h[0].append(rearrange(data))
            header_count = None
        if record_count and count - record_count == RECORD_GAP:
            r[0].append(rearrange(data))
            record_count = None

        byte = dump.read(read_by)

# checking log
with open(LOG_TO_CHECK, 'r') as log:
    print 'reading CAN log:', LOG_TO_CHECK
    for line in log.readlines():
        split_line = line.split()
        if len(split_line) == 23:
            can_id = split_line[2]
            start = ''.join(split_line[6:10])
            end = ''.join(split_line[10:14])
            if can_id == HEADER_ID:
                h[1].append(end)
            elif can_id == RECORD_ID:
                r[1].append(start)


# save results
results = ''
results += 'Location\tHex Dump\tCAN Data\tMatches?\n'
for count in range(3):
    results += 'Header {0}\t{1}\t{2}\t{3}\n'.format(count + 1, h[0][count], h[1][count], h[0][count] == h[1][count])
    results += 'Record {0}\t{1}\t{2}\t{3}\n'.format(count + 1, r[0][count], r[1][count], r[0][count] == r[1][count])

print results
with open(SAVE_PATH, 'w') as f:
    f.write(results)
