import binascii

FILE_TO_CHECK = r'dump_to_check.hex'

HEADER_FLAG = [
    '40000028'
]
RECORD_FLAG = [
    '400001E0',
    '400002E0',
    '400003E0',
]
HEADER_GAP = 93
RECORD_GAP = 539

begin_counting_for_header = False


def rearrange(s):
    assert len(s) == 8, 'Rearrange can only be called on strings of length 8'
    output = s[6:8] + s[4:6] + s[2:4] + s[0:2]
    return output


with open(FILE_TO_CHECK, "rb") as dump:
    print 'reading hex dump:', FILE_TO_CHECK
    read_by = 1
    byte = dump.read(read_by)
    count = 0
    header_count = None
    record_count = None

    data = ''
    while byte != '':
        count += 1
        num = binascii.hexlify(byte)
        num_in_hex = num.decode('hex')
        if len(data) < 8:
            data += num_in_hex
        else:
            data = data[read_by:] + num_in_hex

        if data in HEADER_FLAG:
            if begin_counting_for_header:
                header_count = count
            else:
                begin_counting_for_header = True

        elif data in RECORD_FLAG:
            record_count = count

        if header_count and count - header_count == HEADER_GAP:
            print 'header checksum:', rearrange(data)
            header_count = None
        if record_count and count - record_count == RECORD_GAP:
            print 'record checksum:', rearrange(data)
            record_count = None

        byte = dump.read(read_by)
