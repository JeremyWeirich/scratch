LOG_TO_CHECK = r'elog_CAN_Trace.asc'

break_next = False
with open(LOG_TO_CHECK, 'r') as log:
    print 'reading CAN log:', LOG_TO_CHECK
    for line in log.readlines():
        sline = line.split()
        if len(sline) == 23:
            id = sline[2]
            start = ''.join(sline[6:10])
            end = ''.join(sline[10:14])
            if id == '122':
                print 'header checksum:', end
            elif id == '129':
                print 'record checksum:', start



