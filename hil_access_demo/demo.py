from hil_access.maport import MAPort

SDF = r'N:\Users\HIL Tester\Projects\P15253010_Ford_DLCM\P15253010_Ford_DLCM_CFD\Application_DLCM\Build Results\Application_DLCM.sdf'
VP = {
    'A': r'Model Root/HIL/Test/A/Value',
    'B': r'Model Root/HIL/Test/B/Value',
    'Fixed Step Size': r'Model Root/HIL/Fixed Step Size/Fixed Step Size',
    'Capture_Trigger': r'Model Root/HIL/Capture_Trigger/Value',
}

if __name__ == '__main__':
    p = MAPort('AURIGA', sdf=SDF, variable_pool=VP)

    def convert_simple(values):
        return [dict(category='const', parameters={'value': value}) for value in values]

    print p.read(['A', 'B'])


    p.write({
        'A': 20,
        'B': 10,
    })

    print p.read(['A', 'B'])

    result = p.run_stimulus(
        write_data={
            'A': [
                [.1, .2, .3, .4], # times
                convert_simple([5, 3, 4, 23])  # values
            ],
        },
        verify_signals=['A', 'B'],
        length=0.4,
    )

    import json
    with open('d.json', 'w') as f:
        f.write(json.dumps(result))
