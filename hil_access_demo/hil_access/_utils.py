# Copyright (c) Omron Automotive Electronics Co.Ltd.
# All rights reserved.
#
# The use, disclosure, reproduction, modification, transfer, or transmittal
# of this work for any purpose in any form or by any means without the written
# permission of Omron Automotive Electronics is strictly prohibited.
#
# Confidential, Unpublished Property of Omron Automotive Electronics
# Use and Distribution Limited Solely to Authorized Personnel.

import os
from functools import wraps
from xml.etree import ElementTree
from xml.dom import minidom
from xml.etree.ElementTree import SubElement


HIL_ACCESS_BASE = os.path.dirname(os.path.abspath(__file__))
XIL_STRINGS = dict(
    testbench_ref="ASAM.XIL.Implementation.TestbenchFactory, Version=2.0.1.0, Culture=neutral, "
                  "PublicKeyToken=fc9d65855b27d387",
    interface_ref="ASAM.XIL.Interfaces, Version=2.0.1.0, Culture=neutral, PublicKeyToken=bf471dff114ae984",
    testbench_params=('dSPACE GmbH', 'XIL API', '2016-B'),
    xml_params=dict(
        port='PortConfigurations',
        maport='MAPortConfig',
        sdf='SystemDescriptionFile',
        hil='PlatformName',
    ),
    task='Periodic Task 1',
    triggers=dict(
        base='Trigger',
        start='posedge(Trigger, 0.5)',
        stop='negedge(Trigger, 0.5)',
    ),
    error_configuration_params={
        'name': 'ErrorConfiguration',
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation': 'http://www.asam.net/XILAPI/EESConfiguration/2.0 EESConfiguration.xsd',
        'xmlns': 'http://www.asam.net/XILAPI/EESConfiguration/2.0',
    }
)


def prettify(elem):
    """Convert an xml element into a string suitable for a text file"""
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='  ')


def create_subelement_with_text(parent, name, text):
    """Create a xml subelement and then add text to it"""
    sub = SubElement(parent, name)
    sub.text = text
    return sub


def catch_xil_exception(exception_type):
    """Decorator: Wraps a dSPACE function call in a try/except and extracts vendor information"""
    def decorator(func):  # pylint: disable=missing-docstring
        @wraps(func)
        def wrapper(*args, **kwargs): # pylint: disable=missing-docstring
            try:
                return func(*args, **kwargs)
            except exception_type as dspace_exception:
                print '\n'
                print 'Exception {} occurred'.format(exception_type)
                print 'Code:', dspace_exception.CodeDescription
                print 'Vendor Code:', dspace_exception.VendorCodeDescription
                print '\n'
                raise
        return wrapper
    return decorator
