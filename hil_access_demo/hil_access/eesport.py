# Copyright (c) Omron Automotive Electronics Co.Ltd.
# All rights reserved.
#
# The use, disclosure, reproduction, modification, transfer, or transmittal
# of this work for any purpose in any form or by any means without the written
# permission of Omron Automotive Electronics is strictly prohibited.
#
# Confidential, Unpublished Property of Omron Automotive Electronics
# Use and Distribution Limited Solely to Authorized Personnel.

import time
import logging
import os
import shutil

from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement

import _utils  # pylint: disable=relative-import

#  pragma pylint: disable=import-error, wrong-import-position, no-member
import clr
clr.AddReference(
    "ASAM.XIL.Implementation.TestbenchFactory, Version=2.0.1.0, Culture=neutral, PublicKeyToken=fc9d65855b27d387")
clr.AddReference("ASAM.XIL.Interfaces, Version=2.0.1.0, Culture=neutral, PublicKeyToken=bf471dff114ae984")
from ASAM.XIL.Implementation.TestbenchFactory.Testbench import TestbenchFactory
from ASAM.XIL.Interfaces.Testbench.EESPort.Error import EESPortException
#  pragma pylint: enable=import-error, wrong-import-position, no-member


EESPORT_BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SIGNAL_MAPPING = os.path.join(EESPORT_BASE_DIR, r'fiu_signal_mapping.xml')

FIU_CHANNEL_MAPPING = r'../AP1398_HW_SL_FIU_Only_v2p00p00.CSV'
STIMULUS_FIU_REGEX = r'^\[(?P<time>\[(\d+\.)?\d+, (\d+\.)?\d+\]), \[(?P<category>PWR|PWRL|GND|GNDL|COM|COML|OPNL), OFF\]\]$'  # pylint: disable=line-too-long
CONFIGS_DIR = 'eesport_configs'
FIU_TRIGGER = "FIU_Trigger"
FIU_PREFIX = 'fiu'
FIU_TRIGGER_PATH = "Model Root/HIL/FIU_Trigger/Value"
VALID_FIU_CHANNELS = range(1, 10)
VALID_FIU_NUMBERS = range(1, 4)
VALID_COM_PORTS = range(1, 257)
MINIMUM_FIU_INTERVAL = 0.1 # 100ms
FIU_WAIT_SCALING = 1.5  # wait for fault multiplier: 2 = wait twice the expected duration
FIU_WAIT_OFFSET = 3  # wait for fault offset in seconds
ERROR_SET_PATH = os.path.join(CONFIGS_DIR, r'temp.xml')

LOGGER = logging.getLogger(__name__)


def is_fiu_signal(name):
    """Check if signal name starts with the FIU prefix"""
    return name.lower().startswith(FIU_PREFIX)


class EESPort(object):
    """Electrical error simulation (EES) port, used to control a dSPACE fault insertion unit (FIU)

    Attributes:
        port: dSPACE EESPort
        factory: dSPACE EESPort factory
        hil_name: name of the HIL that the EESPort is configured for
        fiu: FIU number
        com: COM port for communicating with FIU
        variable_pool: variable pool of associated model
        logger: logger to send messages to
    """
    def __init__(self, hil_name, sdf, csv, fiu_pool, variable_pool, logger=LOGGER):
        self.port = None
        self.factory = None
        self.hil_name = hil_name
        self.fiu = fiu_pool['fiu']
        self.com = fiu_pool['com']
        self.variable_pool = variable_pool
        self.logger = logger

        self.config_path = self._create_temp_dir()
        eesport_config_file = self._create_eesport_config_xml(self.config_path, hil_name, sdf, csv, self.com)
        self._create_eesport(eesport_config_file)

    @property
    def fiu_trigger_path(self):
        return self.hil_name + "()://" + FIU_TRIGGER_PATH

    @staticmethod
    def _create_temp_dir():
        config_path = os.path.join(_utils.HIL_ACCESS_BASE, CONFIGS_DIR)
        if not os.path.isdir(config_path):
            os.makedirs(config_path)
        return config_path

    @staticmethod
    def _create_eesport_config_xml(config_path, hil_name, sdf, csv, com):
        """Create an MAPort configuration xml file"""
        body = Element('PortConfigurations')
        config = SubElement(body, 'EESPortConfig', Logging='false', OfflineMode='false', Version='2016-B')

        # --- hardware config
        hardware_configurations = SubElement(config, 'HardwareConfigurations')
        drivers = SubElement(hardware_configurations, 'Drivers')
        driver = SubElement(drivers, 'Driver', ID='0', DriverType='RS232')
        _utils.create_subelement_with_text(driver, 'COMPort', 'COM{}'.format(com))
        SubElement(hardware_configurations, 'HardwareConfiguration', ID='1', SignalListPath=csv, DriverId='0',
                   EcuVersion='1')
        SubElement(hardware_configurations, 'PotentialMapping')
        mapping = ElementTree.parse(SIGNAL_MAPPING)
        hardware_configurations.append(mapping.getroot())
        # --- real time config
        platform_name = '{}_PU'.format(hil_name)
        real_time_configuration = SubElement(config, 'RealTimeConfiguration', PlatformName=platform_name,
                                             SystemDescriptionFilePath=sdf)
        tracing = SubElement(real_time_configuration, 'Tracing', Enabled='true')
        traces = ['Error Activated', 'Active ErrorSet', 'Error Switching', 'Flags', 'Trigger']
        for trace in traces:
            SubElement(tracing, 'Variable', Value='Platform()://XIL API/EESPort/{0}'.format(trace),
                       Type=trace.replace(' ', ''))
        SubElement(real_time_configuration, 'SoftwareTrigger', PollingInterval='0.001')
        location = os.path.join(config_path, '{}.eesportconfig'.format(hil_name))

        with open(location, 'w') as config_file:
            config_file.write(_utils.prettify(body))
        return location

    @_utils.catch_xil_exception(EESPortException)
    def _create_eesport(self, config_file):
        """Creates the MAPort object, configures it and starts it"""
        self.logger.info("configuring EESPort")
        test_bench_factory = TestbenchFactory()
        bench = test_bench_factory.CreateVendorSpecificTestbench('dSPACE GmbH', 'XIL API', '2016-B')
        self.factory = bench.EESPortFactory
        self.port = self.factory.CreateEESPort('_')
        eesport_config = self.port.LoadConfiguration(config_file)
        start = time.time()
        self.port.Configure(eesport_config)
        self.logger.info("configuring took %s", time.time() - start)

    @staticmethod
    def _get_signal_name(fiu, channel):
        rack_map = {1: 'HC_Rack1', 2: 'Rack3', 3: 'Rack4'}
        return r'HC_FIU_0{}\{}_FIU_0{}'.format(fiu, rack_map[fiu], channel)

    def _create_errorset_xml(self, errors):
        """For 5355 controller, you can only do one of the following:
        1) interrupt any number of channels and/or short any number of channels to common
        2) short a single channel to gnd or pwr

        Args:
            errors: dictionary mapping error categories to lists of errors
        """
        start = (errors.pop('start') + FIU_WAIT_OFFSET) * FIU_WAIT_SCALING
        length = (errors.pop('length') + FIU_WAIT_OFFSET) * FIU_WAIT_SCALING

        # safety assertions
        assert 0 <= len(errors['SHORT_CIRCUIT_GROUND']) <= 1, "Short to ground cannot be combined with other faults"
        assert 0 <= len(errors['SHORT_CIRCUIT_UBATT']) <= 1, "Short to power cannot be combined with other faults"
        assert len(errors['PIN_2_PIN']) == 0 or 2 <= len(errors['PIN_2_PIN']) <= VALID_FIU_CHANNELS[-1], \
            "Invalid Pin 2 Pin"
        all_channels = [value['channel'] for values in errors.values() for value in values]
        assert len(all_channels) == len(set(all_channels)), "Cannot use a channel more than once"

        # make error xml file
        body = Element('ErrorConfiguration')
        for attribute, value in _utils.XIL_STRINGS['error_configuration_params'].items():
            body.set(attribute, value)

        def add_fiu_trigger(element, trigger_value, timeout):
            """Add the software trigger to an errorset"""
            trigger = SubElement(element, 'SoftwareTrigger', type='ConditionWatcher')
            _utils.create_subelement_with_text(trigger, 'Timeout', str(timeout))
            _utils.create_subelement_with_text(trigger, 'Condition', '{}=={}'.format(FIU_TRIGGER, trigger_value))
            defines = SubElement(trigger, 'Defines')
            SubElement(defines, 'Link', key=FIU_TRIGGER, value=self.fiu_trigger_path)

        def add_signal_to_error(parent, error, category):
            """Add a signal to an error"""
            signal = SubElement(parent, 'Signal', name=self._get_signal_name(self.fiu, error['channel']))
            if category != 'INTERRUPT':
                signal.set('loadType', 'WITH_LOAD' if error['load'] else 'WITHOUT_LOAD')

        # create set
        set_fault = SubElement(body, 'ErrorSet', name='set', triggerType='SOFTWARE')
        add_fiu_trigger(set_fault, 1, start)
        for category, category_errors in errors.items():
            if category == 'PIN_2_PIN':
                if len(category_errors) > 2:
                    category = 'MULTI_PIN_2_PIN'
                if category_errors:
                    error_element = SubElement(set_fault, 'SimpleError', errorCategory=category)
                    for category_error in category_errors:
                        add_signal_to_error(error_element, category_error, category)
            else:
                for category_error in category_errors:
                    error_element = SubElement(set_fault, 'SimpleError', errorCategory=category)
                    add_signal_to_error(error_element, category_error, category)

        # create clear
        clear_fault = SubElement(body, 'ErrorSet', name='clear', triggerType='SOFTWARE')
        add_fiu_trigger(clear_fault, 0, length)

        errorset_xml_path = os.path.join(EESPORT_BASE_DIR, ERROR_SET_PATH)
        self.logger.debug("Creating errorset xml at %s", errorset_xml_path)
        with open(errorset_xml_path, 'w') as error_file:
            error_file.write(_utils.prettify(body))
        return errorset_xml_path

    @_utils.catch_xil_exception(EESPortException)
    def prime(self, errors):
        """Prepare a fault on the FIU so that the change in FIU_Trigger will trigger the fault"""
        xml = self._create_errorset_xml(errors)
        error_configuration = self.factory.CreateErrorConfiguration("EC")
        reader = error_configuration.CreateEESConfigurationFileReader(xml)
        error_configuration.Load(reader)
        self.port.SetErrorConfiguration(error_configuration)
        self.port.Download()
        self.port.Activate()

    @_utils.catch_xil_exception(EESPortException)
    def unload(self):
        """Remove a prepared fault from the FIU"""
        self.port.Deactivate()
        self.port.Unload()

    def cleanup(self):
        """Removes the MAPort and removes the configs directory"""
        try:
            shutil.rmtree(self.config_path, ignore_errors=True)
        except WindowsError:
            self.logger.warn('temporary folder %s may not have been deleted', self.config_path)
        if self.port is not None:
            self.port.Dispose()
            self.port = None
        self.factory = None


if __name__ == '__main__':
    print EESPORT_BASE_DIR
