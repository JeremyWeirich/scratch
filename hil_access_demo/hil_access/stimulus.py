"""
A simplified general-purpose implementation of a stimulus with attached capture.  Designed to be used by the MAPort's
run_stimulus function.
"""

# Copyright (c) Omron Automotive Electronics Co.Ltd.
# All rights reserved.
#
# The use, disclosure, reproduction, modification, transfer, or transmittal
# of this work for any purpose in any form or by any means without the written
# permission of Omron Automotive Electronics is strictly prohibited.
#
# Confidential, Unpublished Property of Omron Automotive Electronics
# Use and Distribution Limited Solely to Authorized Personnel.

import time
import os

from _utils import XIL_STRINGS  # pylint: disable=relative-import

#  pragma pylint: disable=import-error, unused-import
import clr
from System import Array
from System.Collections.Generic import Dictionary
#  pragma pylint: enable=import-error, unused-import


DELAY_BEFORE_STARTING_GENERATOR = 0.5  # second between starting capture and signal generator


class Stimulus(object):
    """Runs a stimulus test and captures data.  Should be accessed through :py:class:`StimulusManager`

    Args:
        :maport: model access port object
        :write_data: dict mapping signal names as string to write data as ([times], [values])
        :verify_signals: signal or sequence of signals to capture results from
        :length: length of stimulus in seconds
        :capture_trigger_name: (optional, default='Capture_Trigger') signal name of capture trigger

    Attributes:
        :signal_generator: generator signals on the HIL
        :capture: reads model variable values during stimulus
    """
    def __init__(self, maport, write_data, verify_signals, length, capture_trigger_name='Capture_Trigger'):  # pylint: disable=too-many-arguments
        self._maport = maport
        self.signal_generator = None
        self.capture = None
        self._capture_writer = None
        self._capture_paths = None
        self._capture_trigger = capture_trigger_name
        self._capture_result = None

        self._init_signal_generator(write_data, length)
        self._init_capture(verify_signals)

    def _init_signal_generator(self, write_data, length):
        """Creates a stimulus signal generator

        Creates factories for signal generator components, adds capture trigger to write data, and creates
        signal descriptions for each signal.  Combines all signal descriptions into a signal generator.
        """
        stz_generator_factory = self._maport.bench.SignalGeneratorFactory
        signal_factory = self._maport.bench.SignalFactory
        symbol_factory = self._maport.bench.SymbolFactory
        self.signal_generator = self._maport.port.CreateSignalGenerator()
        signal_description_set = signal_factory.CreateSignalDescriptionSet()
        assignments = Dictionary[str, str]()

        def _create_signal_description(name, value):
            """Create a signal description for a signal

            Args:
                name: signal name
                value: signal data as ([times], [values])
            """
            signal_description = signal_factory.CreateSegmentSignalDescriptionByName(name)
            time_vector, data_vector = value
            new_time = time_vector[:]
            new_time.append(new_time[-1] + self._maport.sample_time)  # duration of last data value is one sample point
            if new_time[0] != 0:  # idle until first instruction
                idle_segment = signal_factory.CreateIdleSegment()
                idle_segment.Duration = symbol_factory.CreateConstSymbolByValue(new_time[0])
                signal_description.Add(idle_segment)
            for index, data in enumerate(data_vector):  # create segments for each instruction
                parameters = data['parameters']
                if data['category'] == 'const':
                    segment = signal_factory.CreateConstSegment()
                    segment.Value = symbol_factory.CreateConstSymbolByValue(parameters['value'])
                elif data['category'] == 'ramp':
                    segment = signal_factory.CreateRampSegment()
                    segment.Start = symbol_factory.CreateConstSymbolByValue(parameters['start'])
                    segment.Stop = symbol_factory.CreateConstSymbolByValue(parameters['stop'])
                elif data['category'] == 'slope':
                    segment = signal_factory.CreateRampSlopeSegment()
                    segment.Offset = symbol_factory.CreateConstSymbolByValue(parameters['offset'])
                    segment.Slope = symbol_factory.CreateConstSymbolByValue(parameters['slope'])
                elif data['category'] == 'sine':
                    segment = signal_factory.CreateSineSegment()
                    segment.Offset = symbol_factory.CreateConstSymbolByValue(parameters['offset'])
                    segment.Period = symbol_factory.CreateConstSymbolByValue(parameters['period'])
                    segment.Amplitude = symbol_factory.CreateConstSymbolByValue(parameters['amplitude'])
                    segment.Phase = symbol_factory.CreateConstSymbolByValue(parameters['phase'])
                elif data['category'] == 'pulse':
                    segment = signal_factory.CreatePulseSegment()
                    segment.Offset = symbol_factory.CreateConstSymbolByValue(parameters['offset'])
                    segment.Period = symbol_factory.CreateConstSymbolByValue(parameters['period'])
                    segment.Amplitude = symbol_factory.CreateConstSymbolByValue(parameters['amplitude'])
                    segment.Phase = symbol_factory.CreateConstSymbolByValue(parameters['phase'])
                    segment.DutyCycle = symbol_factory.CreateConstSymbolByValue(parameters['duty_cycle'])
                elif data['category'] == 'saw':
                    segment = signal_factory.CreateSawSegment()
                    segment.Offset = symbol_factory.CreateConstSymbolByValue(parameters['offset'])
                    segment.Period = symbol_factory.CreateConstSymbolByValue(parameters['period'])
                    segment.Amplitude = symbol_factory.CreateConstSymbolByValue(parameters['amplitude'])
                    segment.Phase = symbol_factory.CreateConstSymbolByValue(parameters['phase'])
                    segment.DutyCycle = symbol_factory.CreateConstSymbolByValue(parameters['duty_cycle'])
                elif data['category'] == 'noise':
                    segment = signal_factory.CreateNoiseSegment()
                    segment.Mean = symbol_factory.CreateConstSymbolByValue(parameters['mean'])
                    segment.Sigma = symbol_factory.CreateConstSymbolByValue(parameters['sigma'])
                    segment.Seed = symbol_factory.CreateConstSymbolByValue(parameters['seed'])
                elif data['category'] == 'exp':
                    segment = signal_factory.CreateExpSegment()
                    segment.Start = symbol_factory.CreateConstSymbolByValue(parameters['start'])
                    segment.Stop = symbol_factory.CreateConstSymbolByValue(parameters['stop'])
                    segment.Tau = symbol_factory.CreateConstSymbolByValue(parameters['tau'])
                else:
                    raise Exception("Cannot handle segment category {}".format(data['category']))
                duration = new_time[index + 1] - new_time[index]
                segment.Duration = symbol_factory.CreateConstSymbolByValue(duration)
                signal_description.Add(segment)
            return signal_description

        capture_data = [
            {'category': 'const', 'parameters': {'value': 1.0}}, {'category': 'const', 'parameters': {'value': 0.0}}
        ]
        write_data[self._capture_trigger] = [[0, length + self._maport.sample_time], capture_data]
        for signal_name, signal_value in write_data.iteritems():
            assignments.Add(signal_name, self._maport.variable_pool[signal_name])
            signal_description_set.Add(_create_signal_description(signal_name, signal_value))
        self.signal_generator.SignalDescriptionSet = signal_description_set
        self.signal_generator.Assignments = assignments
        stz_file_path = os.path.join(self._maport.config_path, '{}.stz'.format(self._maport.hil_name))
        stz_writer = stz_generator_factory.CreateSignalGeneratorSTZWriterByFileName(stz_file_path)
        stz_writer.Save(self.signal_generator)

    def _init_capture(self, verify_signals):
        """Creates a capture whose duration is controlled by capture trigger."""
        def _make_capture_paths():
            """Add verify signal paths and capture trigger to capture paths."""
            variable_pool = self._maport.variable_pool
            capture_paths = {variable_pool[signal_name]: signal_name for signal_name in verify_signals}
            capture_paths[variable_pool[self._capture_trigger]] = self._capture_trigger
            return capture_paths

        self._capture_paths = _make_capture_paths()
        self.capture = self._maport.port.CreateCapture(XIL_STRINGS['task'])
        self.capture.Variables = Array[str](self._capture_paths.keys())
        self._capture_writer = self._maport.bench.CapturingFactory.CreateCaptureResultMemoryWriter()
        triggers = XIL_STRINGS['triggers']

        def _make_capture_trigger():
            """Adds the capture trigger to the capture paths."""
            trigger = Dictionary[str, str]()
            for path, name in self._capture_paths.iteritems():
                if name == self._capture_trigger:
                    trigger.Add(triggers['base'], path)
            return trigger

        capture_trigger = _make_capture_trigger()
        start_delay, end_delay = 0, 0
        watcher_factory = self._maport.bench.WatcherFactory
        start_watcher = watcher_factory.CreateConditionWatcher(triggers['start'], capture_trigger)
        self.capture.SetStartTriggerCondition(start_watcher, start_delay)
        end_watcher = watcher_factory.CreateConditionWatcher(triggers['stop'], capture_trigger)
        self.capture.SetStopTriggerCondition(end_watcher, end_delay)

    def run(self):
        """Starts a stimulus test and saves data when it completes.  Call 'read' to collect results."""
        self.signal_generator.LoadToTarget()
        self.capture.Start(self._capture_writer)
        time.sleep(DELAY_BEFORE_STARTING_GENERATOR)  # capture needs a moment for the start trigger to fire
        self.signal_generator.Start()
        self._capture_result = self.capture.Fetch(True)  # monitor and collect data when stimulus finishes

    def read(self):
        """Get data from a stimulus that has finished executing.

        Returns:
            :results: dict mapping signal names to capture value vectors.  Time data is mapped to 'time' key.
        """
        stimulus_results = {}
        group_value = self._capture_result.GetSignalGroupValue(XIL_STRINGS['task'])
        time_vector = list(group_value.XVector.Value)
        for capture_path in self._capture_result.GetVariableNames(XIL_STRINGS['task']):
            signal_values = group_value.ExtractSignalValue(capture_path).FcnValues
            signal_name = self._capture_paths[capture_path]
            stimulus_results[signal_name] = list(signal_values.Value)

        def _slice_results(results):  # TODO(Jeremy) may be obsolete if duration triggers work reliably
            """Removes data from before signal generator started and after signal generator ended."""
            ref = results.pop(self._capture_trigger)
            capture_samples = ref.count(1)
            assert ref[-1] == 0, "Capture for {} too short for signal generator to finish"
            sliced_time = time_vector[:capture_samples]
            sliced_time = [round(i, 6) for i in sliced_time]
            for name, raw_data in results.items():
                assert len(ref) == len(time_vector) == len(raw_data), "Capture time length != capture data length"
                sliced_data = [round(raw_data[i], 6) for i in range(len(raw_data)) if ref[i]]
                assert capture_samples == len(sliced_data) == len(sliced_time), "Slices not equal length"
                results[name] = sliced_data
            results['time'] = sliced_time
            return results
        return _slice_results(stimulus_results)


class StimulusManager(object):  # pylint: disable=too-few-public-methods
    """Context manager for a dSPACE MAPort to ensure cleanup"""
    def __init__(self, maport, write_data, verify_signals, length):
        self.stimulus = Stimulus(maport, write_data, verify_signals, length)

    def __enter__(self):
        return self.stimulus

    def __exit__(self, *args):
        if self.stimulus.capture is not None:
            self.stimulus.capture.Dispose()
            self.stimulus.capture = None
        if self.stimulus.signal_generator is not None:
            self.stimulus.signal_generator.DestroyOnTarget()
            self.stimulus.signal_generator = None
