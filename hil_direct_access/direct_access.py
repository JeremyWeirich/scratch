import sys
import os

needed_paths = [
    r'N:\Users\Jeremy Weirich\repos\orion',
    r'C:\Program Files (x86)\dSPACE Python Extensions 1.5',
    r'C:\Program Files (x86)\dSPACE Python Extensions 1.5\Main\bin',
    r'C:\Anaconda\Lib\site-packages\dSPACECommon',
    r'C:\Anaconda\pkgs\openpyxl-2.3.2-py27_0\Lib\site-packages',
    r'C:\Anaconda\pkgs\et_xmlfile-1.0.1-py27_0\Lib\site-packages',
    r'C:\Anaconda\pkgs\jdcal-1.2-py27_0\Lib\site-packages',
]

for path in needed_paths:
    while path in sys.path:
        sys.path.remove(path)
    sys.path.insert(0, path)

try:
    from src.config import Config, FileInfo
    from src.parse.error import create_error_file, get_errors
    from src.importer import ExcelImport
    from src.parse_excel import Parser
    from src.execute import Executor
    from src.report import report_errors
except:
    import traceback
    print traceback.format_exc()
    raise

from ASAM.HILAPI.dSPACE.MAPort import MAPort

import dspace.com
import win32com.client


HIL_IP = "10.150.107.49"


class PlatformManagement(object):
    def __init__(self):
        self.pfm = None
        self.enums = None

    def initialize_hil(self):
        self.pfm = win32com.client.Dispatch('DSPlatformManagementAPI2')
        self.pfm.RefreshPlatformConfiguration()
        self.enums = dspace.com.Enums(self.pfm)


def main():
    print 'getting platform'
    pfm = PlatformManagement()
    pfm.initialize_hil()
