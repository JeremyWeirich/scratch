import sys
from pprint import pprint
import inspect

needed_paths = [
    r'C:\Program Files (x86)\dSPACE Python Extensions 1.5\Main\bin',
]

config_dict = dict(
    ApplicationPath=r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Tools\HIL_Testing\BMW_CDM_CFD\Application_High\Build Results\Application_High.sdf',
    PlatformIdentifier='10.150.107.49',
)

for path in needed_paths:
    while path in sys.path:
        sys.path.remove(path)
    sys.path.insert(0, path)

from ASAM.HILAPI.dSPACE.MAPort import MAPort

pprint(inspect.getmembers(MAPort))
