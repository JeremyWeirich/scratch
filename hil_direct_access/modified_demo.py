# -*- coding: cp1252 -*-

"""
File:           PlatformManagementDemo_Scalexio.py

Description:    This script shows how to use Platform Management API to manage Scalexio platform. 
                - Register platform
                - Load RealTimeApplication to the platform
                - Start simulation, Stop simulation
                - Unload RealTimeApplication

Tip/Remarks:    The script command line arguments are the following:
                '-c' or '--console': Print everything to the console.
                '-q' or '--quiet'  : Don't show the dialog boxes.

Limitations:    -

Version:        2.0

Date:           July 2013

                dSPACE GmbH shall not be liable for errors contained herein or
                direct, indirect, special, incidental, or consequential damages
                in connection with the furnishing, performance, or use of this
                file.
                Brand names or product names are trademarks or registered
                trademarks of their respective companies or organizations.

Copyright (c) 2013 by dSPACE GmbH
"""

import sys

# sys.path.append('C:\Program Files (x86)\dSPACE Python Extensions 1.5')
# sys.path.append('C:\Anaconda\Lib\site-packages\dSPACECommon')
import dspace.com
import win32com.client

#########################################################################
# Start of configuration
# Before executing this script you have to specify the following variables

# IPAddress of the your Scalexio platform
IPAddress = "10.150.107.49"

# End of configuration
#########################################################################

ScalexioAppPath = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Tools\HIL_Testing' \
                  r'\BMW_CDM_CFD\Application_High\Build Results\Application_High.sdf'


class PlatformManagement():
    def __init__(self):
        # the PlatformManagement object
        self.PlatformManagement = None

        # the enums for PlatformManagement object model
        self.Enums = None

    def Initialize(self):
        # get PlatformManagementobject
        self.PlatformManagement = win32com.client.Dispatch("DSPlatformManagementAPI2")
        self.PlatformManagement.RefreshPlatformConfiguration()
        # get the enums object
        self.Enums = dspace.com.Enums(self.PlatformManagement)

    def GetInfoOfRegisteredPlatforms(self):
        print "\nRegistered platforms count: {}".format(self.PlatformManagement.Platforms.Count)
        print 50*"#"
        for eachPlatform in self.PlatformManagement.Platforms:
            print "# UniqueName  : %s" %(eachPlatform.UniqueName) + (33-len(eachPlatform.UniqueName))*" " + "#"
            PType = "%s" %(self.Enums.PlatformType(eachPlatform.Type))
            print "# PlatformType: %s" %(self.Enums.PlatformType(eachPlatform.Type)) + (33-len(PType))*" " + "#"
            print "#" + 48*" " + "#"
        print 50*"#" + "\n"

    def CheckIfPlatformAlreadyRegistered(self, IPAddress, PlatformType):
        RegisteredPlatform = None
        for platform in self.PlatformManagement.Platforms:
            if platform.Type == PlatformType:
                if platform.Identification.IPAddress == IPAddress:
                    RegisteredPlatform = platform
        return RegisteredPlatform

    def RegisterSCALEXIO(self, IPAddress):
        SCALEXIOPlatform = self.CheckIfPlatformAlreadyRegistered(IPAddress, self.Enums.PlatformType.SCALEXIO)
        if SCALEXIOPlatform is None:
            RegInfo = self.PlatformManagement.CreatePlatformRegistrationInfo(self.Enums.PlatformType.SCALEXIO)
            RegInfo.NetClient = IPAddress
            SCALEXIOPlatform = self.PlatformManagement.RegisterPlatform(RegInfo)
        return SCALEXIOPlatform
    
        

def ExecuteDemo():
    myPlatformManagement = None
    ScalexioPlatform     = None

    myPlatformManagement = PlatformManagement()
    myPlatformManagement.Initialize()
    myPlatformManagement.GetInfoOfRegisteredPlatforms()

    # register SCALEXIO
    print "- Register SCALEXIO platform\n"
    ScalexioPlatform = myPlatformManagement.RegisterSCALEXIO(IPAddress)
    myPlatformManagement.GetInfoOfRegisteredPlatforms()

    if ScalexioPlatform is not None:
        print "- Load realtimeapplication"
        ScalexioPlatform.LoadRealtimeApplication(ScalexioAppPath)
        print "Application state: %s\n" %(myPlatformManagement.Enums.ApplicationState(ScalexioPlatform.RealTimeApplication.State))
        print "- Stop application"
        ScalexioPlatform.RealTimeApplication.Stop()
        print "Application state: %s\n" %(myPlatformManagement.Enums.ApplicationState(ScalexioPlatform.RealTimeApplication.State))
        print "- Start application"
        ScalexioPlatform.RealTimeApplication.Start()
    #     print "Application state: %s\n" %(myPlatformManagement.Enums.ApplicationState(ScalexioPlatform.RealTimeApplication.State))
    #     print "- Unload application"
    #     ScalexioPlatform.RealTimeApplication.Unload()



# Main program
if __name__ == "__main__":
    ExecuteDemo()