import requests

# "https://" + region + ".api.battle.net/wow/character/" + server + "/" + name + "?fields=items&?locale-en_US&apikey=" + apikey)


def make_url(region, server, character):
    return 'https://{0}.api.battle.net/wow/character/{1}/{2}'.format(
        region, server, character
    )


if __name__ == '__main__':
    character = 'Sungar'
    server = 'rexxar'
    region = 'us'

    r = requests.get(
        url=make_url(region, server, character),
        params={
            'fields': ['items', 'statistics'],
            'apikey': 'b7ubvkt8hs8u7hx5aa4qfwerz9qky9k6'
        }
    )
    print r.url
    print r.json()