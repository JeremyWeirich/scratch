import json
from openpyxl import load_workbook

def get_raw_tabs(path):
    data = {}
    wb = load_workbook(path, read_only=True)
    for sheet in wb.worksheets:
        data[sheet.title] = []
        for line in sheet.rows:
            data[sheet.title].append([i.value for i in line])
    return data


def make_vp(raw_vp_data):
    vp = {
        'read': [],
        'write': [],
    }
    ordering = {
        'category': 0,
        'name': 3,
        'default': 5,
        'path': 6,
    }
    for line in raw_vp_data[1:]:
        category = line[ordering['category']].lower()
        name = line[ordering['name']]
        default = line[ordering['default']]
        path = line[ordering['path']]
        variable = {
            'name': name,
            'path': path,
        }
        if default:
            variable['default'] = default
        vp[category].append(variable)
    return vp


if __name__ == '__main__':
    path = 'curtains.xlsx'
    raw_vp = get_raw_tabs(path)["VariablePool"]
    vp = make_vp(raw_vp)
    import pprint
    pprint.pprint(vp)
    with open('vp.json', 'w') as f:
        f.write(json.dumps(vp))