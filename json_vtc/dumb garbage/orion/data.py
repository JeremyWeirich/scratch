import re


REQUIREMENT_REGEX = re.compile(r'^AT\d+$')
OBJECTIVE_REGEX = re.compile(r'^AT\d+_\d+$')
TEST_HEADER_REGEX = re.compile(r'^AT\d+_\d+_\d+[a-zA-Z]?$')  # optional single alphabet character at the end

AT_COLUMN = 1  # column location of component AT number - used to determine component category
ORDERING = {
    0: {  # Requirement
        "disable_flag": 0,
        "description": 2,
        "explanation": 3,
        "spec_ref": 9,
    },
    1: {  # Objective
        "disable_flag": 0,
        "description": 2,
    },
    2: {  # Test header
        "disable_flag": 0,
        "description": 2,
        "write_var": 3,
        "write_val": 4,
        "verify_var": 5,
        "verify_operator": 6,
        "verify_val": 7,
        "verify_tol": 8,
        "pr": 9,
    },
    3: {  # Test body
        "write_var": 3,
        "write_val": 4,
        "verify_var": 5,
        "verify_operator": 6,
        "verify_val": 7,
        "verify_tol": 8,
    },
}


def _largest_index():
    """Get the minimum row length for the test data based on the largest index in ORDERING"""
    return max(max(value for value in category.values()) for category in ORDERING.values())

def _categorize(row):
    """Determines the category of a component (requirement/objective/test header/test body)

    At some point you have to know how to tell the difference between a requirement, objective, test header, or test
    body.  We use the AT to make this decision.  There are the following options:

    """
    at = row[AT_COLUMN]
    if at:
        pass

def convert(raw_test_data):
    """Converts raw test data as list of lists of cells to JSON VTC
    """
    if not all(len(row) > 2):
        pass
    for row in raw_test_data:
        pass


if __name__ == '__main__':
    print _largest_index()

