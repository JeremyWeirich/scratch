# Reads an Excel script and converts to a JSON suite

from openpyxl import load_workbook

def read_excel_data(path):
    """Given a path to an Excel file, extract data as a dict mapping sheet names to a list of lists of cells"""
    data = {}
    wb = load_workbook(path, read_only=True, data_only=True)
    for sheet in wb.worksheets:
        data[sheet.title] = [[cell.value for cell in row] for row in sheet.iter_rows()]
    return data


def apply_offsets(raw_sheet_data, left=0, top=0, right=0, bottom=0):
    """Given a list of lists of cells, apply the requested left/top/right/bottom offsets"""
    # "or None" handles cases where y is 0
    return [[cell for cell in row[left:-right or None]] for row in raw_sheet_data[top:-bottom or None]]


if __name__ == '__main__':
    import pprint
    OFFSETS = {
        "left": 1,
        "top": 3,
        "right": 0,
        "bottom": 0,
    }
    excel_script = r'../curtains.xlsx'
    raw = read_excel_data(excel_script)
    offset = {name: apply_offsets(value, **OFFSETS) for name, value in raw.iteritems()}
    pprint.pprint(offset)