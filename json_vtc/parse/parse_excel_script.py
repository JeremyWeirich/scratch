import json
import ast

from fastnumbers import fast_real
from openpyxl import load_workbook

OFFSETS = {
    'TolerancePool': [0, 1],
    'Suite': [1, 4],
    'FIUPool': [0, 0],
}
ORDERING = {
    'requirement': {
        'disable_flag': 0,
        'at': 1,
        'description': 2,
        'explanation': 3,
        'spec_ref': 9,
    },
    'objective': {
        'disable_flag': 0,
        'at': 1,
        'description': 2,
    },
    'test_header': {
        'disable_flag': 0,
        'at': 1,
        'description': 2,
        'write_var': 3,
        'write_val': 4,
        'verify_var': 5,
        'verify_operator': 6,
        'verify_val': 7,
        'verify_tol': 8,
        'pr': 9,
    },
    'test_body': {
        'write_var': 3,
        'write_val': 4,
        'verify_var': 5,
        'verify_operator': 6,
        'verify_val': 7,
        'verify_tol': 8,
    },
}
DURATIONS = {
    'suite': 0.1,
    'requirement': 0.1,
    'objective': 0.1,
    'sequence': 0.5,
}

def get_common_props(category, parent=None, name=None, description='', asil=None, map=None, duration=None,
                      repeat=1, properties=None):
    if not properties:
        properties = {}  # can't put mutable objects in function declaration
    if parent:
        asil = parent['asil']
        map = parent['map']
    if not duration:
        duration = DURATIONS.get(category, 0)
    return dict(category=category, name=name, description=description, asil=asil, map=map, duration=duration,
                repeat=repeat, properties=properties)


def get_raw_tabs(path):
    data = {}
    wb = load_workbook(path, read_only=True)
    for sheet in wb.worksheets:
        data[sheet.title] = []
        for line in sheet.rows:
            data[sheet.title].append([i.value for i in line])
    return data


def apply_offsets(tab, horizontal, vertical):
    results = tab[vertical:]
    results = [line[horizontal:] for line in results]
    return results


def categorize(line):
    mapping = {0: 'requirement', 1: 'objective', 2: 'test_header'}
    at = line[ORDERING['requirement']['at']]
    if at and isinstance(at, basestring):
        return mapping[at.count('_')]
    return 'test_body'


def add_element(line):
    category = categorize(line)
    props = {name: line[value] for name, value in ORDERING[category].items()}
    return props


def make_requirement(line):
    at = line[1]
    ordering = [
        ('description', 2),
        ('explanation', 3),
        ('spec_ref', 9),
    ]
    props = [{'property': name, 'value': line[value]} for name, value in ordering]
    component = {
        'category': 'requirement',
        'properties': props,
        'children': [],
    }
    component['common'] = get_common_props('requirement', name=at)
    return component


def make_objective(line):
    at = line[1]
    ordering = [
        ('description', 2),
    ]
    props = [{'property': name, 'value': line[value]} for name, value in ordering]
    component = {
        'category': 'objective',
        'properties': props,
        'children': [],
    }
    component['common'] = get_common_props('objective', name=at)
    return component


def get_signal_values(vals_string):
    if isinstance(vals_string, basestring):
        vals = ast.literal_eval(vals_string)
        result = []
        for t, v in zip(vals[0], vals[1]):
            result.append({
                'time': t,
                'value': v,
            })
        return result
    else:
        return [{
            'time': None,
            'value': fast_real(vals_string),
        }]


def add_line_to_test(test, line):
    write_var = line[3]
    write_val = line[4]
    if write_var and write_val:
        test['write_signals'].append({
            'signal': write_var,
            'values': get_signal_values(write_val),
        })

    verify_var = line[5]
    verify_operator = line[6]
    verify_val = line[7]
    verify_tol = line[8]
    if verify_var and verify_operator and verify_val and verify_tol:
        test['verify_signals'].append({
            'signal': verify_var,
            'tolerance': verify_tol,
            'operator': verify_operator,
            'values': get_signal_values(verify_val)
        })


def make_test(line):
    at = line[1]
    ordering = [
        ('description', 2),
    ]
    props = [{'property': name, 'value': line[value]} for name, value in ordering]
    component = {
        'category': 'test',
        'properties': props,
        'write_signals': [],
        'verify_signals': [],
    }
    add_line_to_test(component, line)
    component['common'] = get_common_props('test', name=at)
    return component


def attach_children(suite, data):
    suite['children' ] = []
    children = suite['children']
    current_req = None
    current_obj = None
    current_test = None
    for line in data:
        category = categorize(line)
        if category == 'requirement':
            current_req = make_requirement(line)
            children.append(current_req)
        if category == 'objective':
            current_obj = make_objective(line)
            current_req['children'].append(current_obj)
        if category == 'test_header':
            current_test = make_test(line)
            current_obj['children'].append(current_test)
        if category == 'test_body':
            add_line_to_test(current_test, line)








if __name__ == '__main__':
    import pprint
    import os
    # location = r'C:\Users\HIL Tester\Documents\perseus-runner'
    location = ''
    filename = 'curtains.xlsx'
    tab = 'High Auto CP2'
    raw_tab = get_raw_tabs(filename)[tab]
    data = apply_offsets(raw_tab, *OFFSETS['Suite'])
    categorized_data = build_json_suite(data)
    pprint.pprint(categorized_data)
    with open(os.path.join(location, 'tests.json'), 'w') as f:
        f.write(json.dumps(categorized_data))