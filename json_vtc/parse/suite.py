def attach_pools(suite, data):
    suite['pools'] = {
            'variables': {
                'motor a': 'Signal Chain/I/O Function View/Basic I/O/Analog Input/Polarity',
            },
            'tolerances': {
                'telephone': dict(category='relative', value=10),
                'calculator': dict(category='relative', value='frog'),
            },
            'defaults': {
                'motor a': 10,
            },
            'fiu': {
                'settings': {
                    'fiu_number': 1,
                    'com_port': 1,
                },
                'channels': {},
            },
            'globals': {
                'frog': 10,
                'cat': 200,
                'dog': 23452
            },
    }

def build_json_suite(data):
    json_suite = {
        'config': {
            'setting 1': 20,
        },
        'application': 'Application_DLCM',
        'properties': {},
        'database': {
            'release': 8,
        }
    }
    json_suite['common'] = get_common_props('suite', name='my cool suite')
    attach_pools(json_suite, data)
    attach_children(json_suite, data)
    return json_suite