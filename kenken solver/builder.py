from kenken import Board


BOARDS = []

# Board 1
b = Board(6)
b.add_cage('a', '11 21 31', 11)
b.add_cage('a', '41 51 52', 7)
b.add_cage('a', '61 62', 7)
b.add_cage('a', '32 42', 6)
b.add_cage('a', '34 35', 5)

b.add_cage('m', '22 23', 15)
b.add_cage('m', '33 43', 6)
b.add_cage('m', '64 65', 12)
b.add_cage('m', '45 55', 10)
b.add_cage('m', '36 46', 4)

b.add_cage('s', '12 13', 2)
b.add_cage('s', '53 63', 3)
b.add_cage('s', '14 24', 4)
b.add_cage('s', '44 54', 2)
b.add_cage('s', '56 66', 1)

b.add_cage('d', '15 16', 3)
b.add_cage('d', '25 26', 3)

b.check_cages()
BOARDS.append(b)



# Board 2
b = Board(7)
b.add_cage('a', '21 22 23', 6)
b.add_cage('a', '51 61', 11)
b.add_cage('a', '42 52', 11)
b.add_cage('a', '73 74 75', 10)
b.add_cage('a', '15 16 17', 7)
b.add_cage('a', '25 26 27', 18)
b.add_cage('a', '66 76 77', 14)
b.add_cage('a', '57 67', 10)
b.add_cage('a', '63', 4)
b.add_cage('a', '54', 5)

b.add_cage('m', '11 12 13', 105)
b.add_cage('m', '71 62 72', 72)
b.add_cage('m', '35 36 37 47', 72)

b.add_cage('s', '31 41', 5)
b.add_cage('s', '32 33', 4)
b.add_cage('s', '14 24', 2)
b.add_cage('s', '34 44', 4)
b.add_cage('s', '45 55', 1)
b.add_cage('s', '64 65', 1)

b.add_cage('d', '43 53', 3)
b.add_cage('d', '46 56', 2)
b.check_cages()
BOARDS.append(b)



# Board 3
b = Board(6)
b.add_cage('a', '13 23 14 15', 13)
b.add_cage('a', '51 52 42 43', 13)
b.add_cage('a', '21 22 12', 15)
b.add_cage('a', '31 41 32', 10)
b.add_cage('a', '61 62 63', 11)
b.add_cage('a', '33 34 35', 11)
b.add_cage('a', '53 54 64', 7)
b.add_cage('a', '44 45 46', 11)
b.add_cage('a', '55 65 56', 13)
b.add_cage('a', '16 26 36', 9)
b.add_cage('a', '24 25', 7)
b.add_cage('a', '11', 1)
b.add_cage('a', '66', 5)
b.check_cages()
BOARDS.append(b)

