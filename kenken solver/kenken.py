import itertools
import time
import re
from utils import add_combinations, prod_combinations, sub_combinations, div_combinations


COMBINATION_FINDER = dict(
    add=add_combinations,
    mul=prod_combinations,
    sub=sub_combinations,
    div=div_combinations,
)
line_regex = re.compile(r'^(\w+) (\d+) ((\d+ ?)+)')


def coordinate_string_to_coordinates(s):
    coordinates = []
    points = s.split()
    for point in points:
        coordinates.append((int(point[0]), int(point[1])))
    return coordinates


class InvalidSolution(Exception):
    """Invalid board state after a guess"""


class Board(object):
    def __init__(self, path):
        self.size = None
        self.all_values = None
        self.cells = None
        self.cages = []
        self.solve_iterations = 0
        self.guess_count = 0
        self.previously_solved = 0
        self.previously_unsolved = None
        self.guesses = []
        self.create_from_file(path)

    @property
    def finished(self):
        for cell in self.cells:
            if len(cell.value) > 1:
                return False
        return True

    @property
    def solved_count(self):
        count = 0
        for cell in self.cells:
            if len(cell.value) == 1:
                count += 1
        return count

    @property
    def unsolved_count(self):
        return self.size * self.size - self.solved_count

    @property
    def invalid(self):
        for cell in self.cells:
            if not cell.value:
                return True
        for cage in self.cages:
            if cage.invalid:
                return True
        return False

    @property
    def board_state(self):
        return [cell.value for cell in self.cells]

    def create_from_file(self, path):
        def check_cages():
            missing = []
            for row in self.all_values:
                for column in self.all_values:
                    if not self.get_cell(column, row).cage:
                        missing.append((column, row))
            assert not missing, 'no cages found for {}'.format(missing)

        with open(path) as f:
            lines = f.readlines()
        self.size = int(lines.pop(0)) + 1  # one-indexed from now on
        self.all_values = [i for i in range(1, self.size)]
        self.cells = [Cell(column, row, self.size) for row in self.all_values for column in self.all_values]
        for line in lines:
            if line:
                match = re.match(line_regex, line)
                category, target, cells, _ = match.groups()
                target = int(target)
                coordinates = coordinate_string_to_coordinates(cells)
                cells = [self.get_cell(column, row) for column, row in coordinates]
                self.cages.append(Cage(cells, target, self, category))
        check_cages()

    def display(self):
        if SHOW_WORK:
            offset = '    '
            div = ' ' * self.size
            top = offset + div.join([str(i) for i in self.all_values])
            print top
            print offset + '-' * len(top)
            for index, row in enumerate(self.all_values):
                current = '{}|  '.format(index+1)
                for column in self.all_values:
                    values = self.get_cell(column, row).value
                    current += ''.join([str(i) for i in values])
                    current += ' ' * (self.size - len(values) + 1)
                print current
            print ''

    def get_cell(self, column, row):
        for cell in self.cells:
            if cell.row == row and cell.column == column:
                return cell

    def by_row(self, row):
        return [cell for cell in self.cells if cell.row == row]

    def by_column(self, column):
        return [cell for cell in self.cells if cell.column == column]

    def find_paired_cages(self):
        """Identify pairs of 'certain' cages that lock out values for other cells in their row.
        For example, the two cages
        2/5 2/5
        5/6 5/6
        Locks out 5 from all cells sharing a column with the cells
        """
        FPC_SHOW_WORK = True and SHOW_WORK

        def get_aligned_cages(cages):
            """Find cages that are aligned either by column or row.  Only aligned cages are useful for this check."""
            aligned = {}
            for d in ('row', 'column'):
                current = {}
                for c in cages:
                    span_string = c.span_string(d)
                    if span_string not in current:
                        current[span_string] = []
                    current[span_string].append(c)
                aligned[d] = current
            return aligned

        certain_cages = [cage for cage in self.cages if cage.certain]
        certain_aligned_cages = get_aligned_cages(certain_cages)
        for direction, alignments in certain_aligned_cages.items():
            for align_path, aligned_cages in alignments.items():
                if len(aligned_cages) > 1 and len(aligned_cages) >= max([len(cage.cells) for cage in aligned_cages]):
                    for i in self.all_values:
                        if all([i in cage.possible_values for cage in aligned_cages]):
                            if FPC_SHOW_WORK:
                                print 'removing {} from {}s {} because cages {} match'.format(i, direction, align_path, aligned_cages)
                            if direction == 'column':
                                remove_from = [self.by_column(int(c)) for c in align_path]
                            else:
                                remove_from = [self.by_row(int(c)) for c in align_path]
                            for lane in remove_from:
                                for cell in lane:
                                    if all([cell not in cage for cage in aligned_cages]):
                                        cell.remove(i)

    def restore_board_state(self, state):
        for main_cell, state_value in zip(self.cells, state):
            main_cell.value = state_value

    def solve_puzzle(self):
        while not self.finished:
            self.solve_iterations += 1
            for cage in self.cages:
                cage.get_options()
            # self.find_paired_cages()
            if self.invalid:
                self.handle_invalid_guess()
            if self.unsolved_count != self.previously_unsolved:
                self.previously_unsolved = self.unsolved_count
            else:
                self.display()
                self.make_new_guess()
            if SHOW_WORK:
                print '{}: {} solved'.format(self.solve_iterations, self.solved_count)
        self.display()
        print 'solved after {} solve iterations and {} guesses'.format(b.solve_iterations, b.guess_count)
        print time.time() - s

    def make_new_guess(self):
        def get_first_smallest_unsolved_cell():
            for i in self.all_values[1:]:  # cells with 1 value are solved already
                for cell in self.cells:
                    if len(cell.value) == i:
                        return cell

        unsolved_cell = get_first_smallest_unsolved_cell()
        self.guesses.append((self.board_state, unsolved_cell, unsolved_cell.value[:]))
        if SHOW_WORK:
            print 'begin guessing for cell', unsolved_cell, 'starting with', unsolved_cell.value[0]
        self.apply_guess()

    def apply_guess(self):
        state, cell, possible_values = self.guesses[-1]
        if len(possible_values) == 1:  # only one option left for the cell
            if SHOW_WORK:
                print 'locking in', possible_values[0], 'for', cell
            cell.value = [possible_values[0]]
            self.guesses = self.guesses[:-1]
        else:
            self.guess_count += 1
            if SHOW_WORK:
                print 'making guess', possible_values[0], 'for', cell
            cell.value = [possible_values.pop(0)]

    def handle_invalid_guess(self):
        state, cell, possible_values = self.guesses[-1]
        self.display()
        self.restore_board_state(state)
        self.apply_guess()


class Cage(object):
    def __init__(self, cells, target, board, category):
        self.category = category
        self.cells = cells
        self.direction = self.get_direction()
        self.combinations = []
        self.combination_finder = COMBINATION_FINDER[category]
        self.target = target
        self.board = board
        self.possible_values = [i for i in range(1, self.board.size)]
        self.changed = True
        for cell in self.cells:
            cell.cage = self

    def __str__(self):
        return '{} cage: '.format(self.category) + ' '.join(['{}{}'.format(cell.column, cell.row) for cell in self.cells])

    def __repr__(self):
        return '{} cage: '.format(self.category) + ' '.join(['{}{}'.format(cell.column, cell.row) for cell in self.cells])

    def __iter__(self):
        return iter(self.cells)

    def span_string(self, direction):
        x = set()
        for cell in self.cells:
            if direction == 'row':
                x.add(str(cell.row))
            elif direction == 'column':
                x.add(str(cell.column))
            else:
                assert False, 'invalid direction for span string'
        return ''.join(x)

    def get_direction(self):
        if len(set([cell.row for cell in self.cells])) == 1:
            return 'row'
        if len(set([cell.column for cell in self.cells])) == 1:
            return 'column'
        return 'mixed'

    @property
    def invalid(self):
        cell_values = [cell.value for cell in self.cells]
        options, _ = self.combination_finder(cell_values, self.target)
        if SHOW_WORK and len(options) == 0:
            print self, 'cannot reach', self.target, 'with', [cell.value for cell in self.cells]
        return len(options) == 0

    @property
    def certain(self):
        values = set()
        for cell in self.cells:
            values.update(set(cell.value))
        return len(values) <= len(self.cells)

    def remove_certain(self):
        for cell in self.cells:
            if len(cell.value) == 1:
                for other_cell in self.board.by_row(cell.row):
                    if other_cell != cell and cell.value not in other_cell.value:
                        other_cell.value = [i for i in other_cell.value if i not in cell.value]
                        other_cell.cage.changed = True
                for other_cell in self.board.by_column(cell.column):
                    if other_cell != cell and cell.value not in other_cell.value:
                        other_cell.value = [i for i in other_cell.value if i not in cell.value]
                        other_cell.cage.changed = True

        if self.certain:
            if self.direction == 'row':
                row = self.cells[0].row
                for cell in self.board.by_row(row):
                    if cell not in self.cells:
                        cell.value = [value for value in cell.value if value not in self.possible_values]
                        cell.cage.changed = True
            if self.direction == 'column':
                column = self.cells[0].column
                for cell in self.board.by_column(column):
                    if cell not in self.cells:
                        cell.value = [value for value in cell.value if value not in self.possible_values]
                        cell.cage.changed = True

    def get_options(self):
        cell_values = [cell.value for cell in self.cells]
        options, reduced_options = self.combination_finder(cell_values, self.target)
        self.combinations = reduced_options
        self.apply_new_options(options)

    def apply_new_options(self, options):
        if self.direction in ('row', 'column'):
            reduced_options = []
            for option in options:
                keep_option = True
                for x, y in itertools.combinations(option, 2):
                    if x == y:
                        keep_option = False
                if keep_option:
                    reduced_options.append(option)
            options = reduced_options
        for index, elements in enumerate(zip(*options)):
            cell = self.cells[index]
            cell.value = [i for i in cell.value if i in elements]
        self.possible_values = list(set(itertools.chain(*options)))
        self.remove_certain()


class Cell(object):
    def __init__(self, column, row, max_value):
        self.value = [i for i in range(1, max_value)]
        self.cage = None
        self.row = row  # one indexed, counting from upper left
        self.column = column  # one indexed, counting from upper left

    def __str__(self):
        return '({},{}), v={}'.format(self.column, self.row, ''.join([str(i) for i in self.value]))

    def __repr__(self):
        return '({},{}), v={}'.format(self.column, self.row, ''.join([str(i) for i in self.value]))

    def remove(self, to_remove):
        assert isinstance(to_remove, int)
        self.value = [value for value in self.value if value != to_remove]


if __name__ == '__main__':
    SHOW_WORK = True
    import os
    puzzle_dir = r'stolen_puzzles'
    name = 'adds.kk'
    b = Board(os.path.join(puzzle_dir, name))
    s = time.time()
    b.solve_puzzle()
    print name
