import glob
import os

MAPPING = [
    ('(', ''),
    (')', ''),
    (',', ''),
    ('0', '1'),
    ('1', '2'),
    ('2', '3'),
    ('3', '4'),
    ('4', '5'),
    ('5', '6'),
    ('6', '7'),
    ('7', '8'),
    ('8', '9'),
    ('9', '10'),

]

steal_from = r'C:\Users\Jeremy\PycharmProjects\scratch\other kenkens\kenkensolve-master\kenkensolve-master\puzzles'
for i in glob.glob('{}\*.kk'.format(steal_from)):
    name = os.path.basename(i)
    with open(i) as source:
        lines = source.readlines()
    with open(name, 'w') as dest:
        dest.write(lines.pop(0))
        for line in lines:
            split = line.split(' ')
            if split[0] == 'con':
                split[0] = 'add'
            new = split[0] + ' ' + split[1]
            coords = ' '.join(split[2:])
            for o, n in MAPPING[::-1]:
                coords = coords.replace(o, n)
            dest.write(new + ' ' + coords)
