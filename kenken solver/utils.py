import itertools
from numpy import prod, sum


def add_combinations(cell_values, target):
    options = []
    reduced_options = []
    for combination in list(itertools.product(*cell_values)):
        if sum(combination) == target:
            options.append(combination)
            values = sorted(combination, reverse=True)
            if values not in reduced_options:
                reduced_options.append(values)
    return options, reduced_options


def sub_combinations(cell_values, target):
    options = []
    reduced_options = []
    for combination in list(itertools.product(*cell_values)):
        f_combination = sorted(combination, reverse=True)
        d = f_combination[0]
        for i in f_combination[1:]:
            d -= i
        if d == target:
            options.append(combination)
            values = sorted(combination, reverse=True)
            if values not in reduced_options:
                reduced_options.append(values)
    return options, reduced_options


def prod_combinations(cell_values, target):
    options = []
    reduced_options = []
    for combination in list(itertools.product(*cell_values)):
        if prod(combination) == target:
            options.append(combination)
            values = sorted(combination, reverse=True)
            if values not in reduced_options:
                reduced_options.append(values)
    return options, reduced_options


def div_combinations(cell_values, target):
    options = []
    reduced_options = []
    for combination in list(itertools.product(*cell_values)):
        f_combination = sorted([float(i) for i in combination], reverse=True)
        d = f_combination[0]
        for i in f_combination[1:]:
            d /= i
        if d == target:
            options.append(combination)
            values = sorted(combination, reverse=True)
            if values not in reduced_options:
                reduced_options.append(values)
    return options, reduced_options


if __name__ == '__main__':
    c = [i for i in range(1, 10)]
    c = add_combinations([c, c, c, c], 27)[1]
    print c