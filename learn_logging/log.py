import logging

import secondary



def create_primary_loggers():
    logging.basicConfig(
        filename='1.log',
        filemode='w',
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)-8s %(name)-30s %(message)s',
        datefmt='%y-%m-%d %H:%M',
    )
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)-8s %(name)-30s: %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


def get_new_logger(path):
    logger = logging.getLogger(path[0])
    logger.propagate = False
    handler = logging.FileHandler(filename=path)
    logger.addHandler(handler)
    return logger

if __name__ == '__main__':
    create_primary_loggers()
    logger = logging.getLogger('root')
    logger.info('message a')
    logger2 = get_new_logger('2.log')
    logger3 = get_new_logger('3.log')
    logger4 = get_new_logger('4.log')
    logger2.info('message b')
    logger3.info('message c')
    logger4.info('message d')
    logger2.handlers = []
    logger2.info('message e')
    print logger2.handlers
