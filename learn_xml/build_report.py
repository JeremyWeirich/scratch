import shutil
import os
from time import time
import sys

from xml.etree import ElementTree
from xml.dom import minidom

from suite_reader import read_files

CSS_HARDCODED_LOCATION = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\learn_xml\css\report.css'


def _create_dir(path):
    try:
        shutil.rmtree(path)
    except WindowsError:
        pass
    if not os.path.exists(path):
        os.makedirs(path)


def _add_css(CSS):
    with open(CSS_HARDCODED_LOCATION) as f:
        data = f.read()
    css_path = os.path.join(CSS, 'report.css')
    with open(css_path, 'w') as f:
        f.write(data)


def _get_path_and_file(*paths):
    d = os.path.join(*paths)
    p = '{}.html'.format(d)
    return d, p


def _prettify(elem):
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='  ', encoding='utf-8', xml_declaration=True)


def _make_section(section, parent_path, parent_html):
    section_html = section.generate_html(parent_html, section)
    section_dir, section_path = _get_path_and_file(parent_path, section.at)
    if section.at.count('_') < 2:
        _create_dir(section_dir)
        for i in section:
            _make_section(i, section_dir, section_html)
    with open(section_path, 'w') as f:
        # f.write(_prettify(section_html))
        section_tree = ElementTree.ElementTree(element=section_html)
        section_tree.write(f, encoding='UTF-8', method='html')


def create_html_report(results_dir):
    RESULTS = results_dir
    OUTPUT = os.path.join(RESULTS, 'report')
    CSS = os.path.join(OUTPUT, 'css')
    start = time()
    sys.stdout.write('making path ')
    _create_dir(OUTPUT)
    _create_dir(CSS)
    print str(time() - start)
    _add_css(CSS)
    start = time()
    sys.stdout.write('reading files ')
    suite = read_files(RESULTS)
    print str(time() - start)
    start = time()
    sys.stdout.write('making report ')
    _make_section(suite, OUTPUT, None)
    print str(time() - start)

if __name__ == '__main__':
    create_html_report(r'C:\Users\HIL Tester\Documents\HIL Test Logs\ANDROMEDA\2017\1489075914_88 - test')
