from generate import write_index, write_requirement, write_objective, write_test


class TestingSummary(object):  # it's not used at all
    def __init__(self):
        #  regular/asil
        #  requirement/objective/test
        self.count = [[0, 0, 0], [0, 0, 0]]
        self.status = [[0, 0, 0], [0, 0, 0]]
        self.failed = [[0, 0, 0], [0, 0, 0]]

    def update(self, form):
        if isinstance(form, Requirement):
            level = 0
        elif isinstance(form, Objective):
            level = 1
        else:
            level = 2
        if form.asil:
            safety = 1
        else:
            safety = 0
        self.count[safety][level] += 1
        if form.status:
            self.status[safety][level] += 1
        else:
            self.failed[safety][level] += 1

    def as_lists(self):
        return [self.count[0] + self.count[1], self.status[0] + self.status[1], self.failed[0] + self.failed[1]]


class Section(object):
    def __init__(self, name, data):
        self.contents = []
        self.at = name
        self.parent = None
        self.data = data
        self.previous_at = None
        self.next_at = None
        self.status = True
        self.execution_status = None
        self.generate_html = None
        self.section_type = None
        self.personalize()
        self.time = None

    def add(self, child):
        self.contents.append(child)
        self.status &= child.status
        if self.status is False:
            self.fail_parent()

    def set_parent(self, parent):
        self.parent = parent

    def __iter__(self):
        return iter(self.contents)

    def personalize(self):
        pass

    def fail_parent(self):
        self.status = False
        if self.parent:
            self.parent.status = False


class Suite(Section):
    def personalize(self):
        self.generate_html = write_index
        self.section_type = 'Suite'


class Requirement(Section):
    def personalize(self):
        self.generate_html = write_requirement
        self.section_type = 'Requirement'
        self.description = self.data['description']
        self.explanation = self.data['explanation']
        self.spec_ref = self.data['spec_ref']
        self.asil = self.data['asil']
        self.execution_status = self.data['status']
        self.time = self.data['time']


class Objective(Section):
    def personalize(self):
        self.generate_html = write_objective
        self.section_type = 'Objective'
        self.description = self.data['description']
        self.asil = self.data['asil']
        self.execution_status = self.data['status']
        self.time = self.data['time']


class Test(Section):
    def personalize(self):
        self.generate_html = write_test
        self.section_type = 'Test'
        self.write = self.data['write']
        self.verify = self.data['verify']
        self.category = self.data['category']
        self.time = self.verify.pop('time') if 'time' in self.verify else None
        self.description = self.data['description']
        self.asil = self.data['asil']
        self.execution_status = self.data['status']
        self.time = self.data['time']

        self.set_pass_status_and_category()

    def set_pass_status_and_category(self):
        for signal in self.verify.values():
            self.status &= signal['pass']
