import datetime

from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement

from bokeh.plotting import figure
from bokeh.models import HoverTool, PanTool, ResizeTool, SaveTool, BoxZoomTool, ResetTool
from bokeh.embed import components

COLS = [
    'Measured',
    'Operator',
    'Expected',
    'Upper',
    'Lower',
    'Tolerance',
    'Status',
]


def add_text_element(parent, element_type, text, **properties):
    properties = {'class' if key == 'cls' else key: value for key, value in properties.items()}
    e = SubElement(parent, element_type, properties)
    e.text = text
    return e


def _safe(at):
    return '{}.html'.format(at) if at else '#" class="invalid'


def make_table(parent, columns):
    table = SubElement(parent, 'table')
    tr = SubElement(table, 'tr')
    for column_name in columns:
        add_text_element(tr, 'th', column_name)
    return table


def add_v_nav(body, section):
    nav = SubElement(body, 'div', {'class': 'title'})
    time_list = SubElement(nav, 'ul', {'class': 'time'})
    time_string = datetime.datetime.now().strftime('Executed %H:%M %m/%d/%y')
    add_text_element(time_list, 'li', time_string)

    links = SubElement(nav, 'ul')
    link_data = []
    href = '{}.html'.format(section.at)
    prefix = ''
    link_data.append((section.at, href))
    while section.parent:
        section = section.parent
        prefix += r'../'
        href = prefix + '{}.html'.format(section.at)
        link_data.append((section.at, href))
    for text, href in reversed(link_data):
        l = SubElement(links, 'li')
        add_text_element(l, 'a', text, href=href)

    return nav


def add_h_nav(body, section):
    h_nav = SubElement(body, 'div', {'class': 'AT_Header'})
    buttons = SubElement(h_nav, 'ul', {'class': 'navigation'})
    prev_b = SubElement(buttons, 'li')  # must be created to space title correctly
    if section.previous_at:
        add_text_element(prev_b, 'a', 'Previous', href='{}.html'.format(section.previous_at))
    next_b = SubElement(buttons, 'li')  # must be created to space title correctly
    if section.next_at:
        add_text_element(next_b, 'a', 'Next', href='{}.html'.format(section.next_at))
    add_text_element(h_nav, 'h1', '{} {}'.format(section.section_type, section.at))
    return h_nav


def add_listing(body, name):
    listing = SubElement(body, 'div', {'class': 'listing'})
    add_text_element(listing, 'h3', '{}s'.format(name))
    add_text_element(listing, 'p', 'Click a {} below for detailed information'.format(name.lower()))
    table = SubElement(listing, 'table')
    tr = SubElement(table, 'tr')
    for column_name in ['AT Number', 'ASIL', 'Status']:
        add_text_element(tr, 'th', column_name)


def update_parent_listing(parent_elem, section):
    asil = 'A' if section.asil else 'False'
    status = 'Pass' if section.status else 'Fail'
    req_table = parent_elem.find('body/div/table')
    reportname = parent_elem.find('head/title').text
    link = "document.location = '{0}/{1}.html';".format(reportname, section.at)
    tr = SubElement(req_table, 'tr', onclick=link)
    add_text_element(tr, 'td', section.at)
    add_text_element(tr, 'td', asil, cls='asil_{}'.format(asil))
    add_text_element(tr, 'td', status, cls='passed_{}'.format(status))


def make_sequence_verify(verify_data, signal):
    name, values = signal
    results = values['results']
    expected = values['expected']
    operator = values['operator']
    tolerance = '{:.1%}'.format(values['tolerance'])
    # measured_value, upper, lower, status = measured
    status = 'Pass' if results['pass'] else 'Fail'
    add_text_element(verify_data, 'h4', name, cls='header_{}'.format(status))
    table = make_table(verify_data, COLS)
    tr = SubElement(table, 'tr')
    for column in (results['measured'], operator, expected, results['upper'], results['lower'], tolerance, status):
        add_text_element(tr, 'td', str(column))


def make_stimulus_verify(signal_elem, signal, head, time_vector):
    name, values = signal
    results = values['results']
    expected = values['expected']
    operator = values['operator']
    tolerance = '{:.1%}'.format(values['tolerance'])
    status = 'Pass' if all([signal['pass'] for signal in results]) else 'Fail'
    header = SubElement(signal_elem, 'h4', {'class': 'header_{}'.format(status)})
    header.text = name
    label = SubElement(signal_elem, 'label', {'class': 'collapse', 'for': '{}_collapse'.format(name)})
    label.text = 'Show Details'
    SubElement(signal_elem, 'input', {'id': '{}_collapse'.format(name), 'type': 'checkbox'})
    cols = ['Time'] + COLS
    table = make_table(signal_elem, cols)
    for point, expected_value in zip(results, expected[1]):
        status = 'Pass' if status else 'Fail'
        tr = SubElement(table, 'tr')
        for column in (point['time'], point['measured'], operator, expected_value, point['upper'], point['lower'], tolerance, status):
            add_text_element(tr, 'td', str(column))

    script, div = make_bokeh_graph(name, values, time_vector)
    head.append(script)
    signal_elem.append(div)


def make_bokeh_graph(signal_name, signal, time):
    operator = signal['operator']
    expected = signal['expected']

    hover = HoverTool(tooltips=[('Time', '$x'), ('Value', '$y')])
    tools = [hover, PanTool(), ResizeTool(), SaveTool(), BoxZoomTool(), ResetTool()]

    p = figure(title=signal_name, plot_height=300, plot_width=1600, sizing_mode='stretch_both', tools=tools)
    p.line(*signal['graph_data'])
    p.background_fill_color = 'beige'
    for expected_time, expected_value in zip(expected[0], expected[1]):
        if operator in ['>=', '>']:
            fill_alpha = 0 if operator == '>' else 1
            p.triangle(expected_time, expected_value, fill_alpha=fill_alpha, color='red', size=8)
        elif operator in ['<=', '<']:
            fill_alpha = 0 if operator == '<' else 1
            p.inverted_triangle(expected_time, expected_value, fill_alpha=fill_alpha, color='red', size=8)
        elif operator in ['!=']:
            p.x(expected_time, expected_value, color='red', size=8)
        else:
            p.circle(expected_time, expected_value, color='red', size=8)

    errs = [i * signal['tolerance'] for i in expected[1]]
    y_err_x = []
    y_err_y = []
    for px, py, err in zip(expected[0], expected[1], errs):
        y_err_x.append((px, px))
        y_err_y.append((py - err, py + err))
        p.multi_line(y_err_x, y_err_y, color='black')

    script, div = components(p)
    script = ET.fromstring(script)
    div = ET.fromstring(div)
    return script, div


# ------------------------------ Generate html --------------------------------------------------------------------


def write_index(_, suite):
    index_elem = Element('html')
    head = SubElement(index_elem, 'head')
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='css/report.css')
    add_text_element(head, 'title', suite.at)
    body = SubElement(index_elem, 'body')

    # Nav
    add_v_nav(body, suite)

    # Next/Previous
    add_h_nav(body, suite)

    # Requirement listing
    add_listing(body, 'Requirement')

    return index_elem


def write_requirement(index_elem, requirement):
    req_elem = Element('html')
    head = SubElement(req_elem, 'head')
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='../css/report.css')
    add_text_element(head, 'title', requirement.at)
    body = SubElement(req_elem, 'body')

    # Nav
    add_v_nav(body, requirement)

    # Next/Previous
    add_h_nav(body, requirement)

    # Requirement stats
    stats = SubElement(body, 'div', {'class': 'stats'})
    stats_data = [('Description', requirement.description),
                  ('Explanation', requirement.explanation),
                  ('Spec Ref', requirement.spec_ref)]
    for name, data in stats_data:
        add_text_element(stats, 'h3', name)
        add_text_element(stats, 'p', data)

    # Objective listing
    add_listing(body, 'Objective')

    # Update parent listing
    update_parent_listing(index_elem, requirement)

    return req_elem


def write_objective(req_elem, objective):
    obj_elem = Element('html')
    head = SubElement(obj_elem, 'head')
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='../../css/report.css')
    add_text_element(head, 'title', objective.at)
    body = SubElement(obj_elem, 'body')
    
    # Nav
    add_v_nav(body, objective)

    # Next/Previous
    add_h_nav(body, objective)

    # Objective stats
    stats = SubElement(body, 'div', {'class': 'stats'})
    add_text_element(stats, 'h3', 'Description')
    add_text_element(stats, 'p', objective.description)
    
    # Test listing
    add_listing(body, 'Test')

    # Update parent listing
    update_parent_listing(req_elem, objective)

    return obj_elem


def write_test(obj_elem, test):
    test_elem = Element('html')
    head = SubElement(test_elem, 'head')
    add_text_element(head, 'title', test.at)
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='http://cdn.pydata.org/bokeh/release/bokeh-0.12.3.min.css')
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.3.min.css')
    SubElement(head, 'link', rel='stylesheet', type='text/css', href='../../../css/report.css')
    SubElement(head, 'script', src='http://cdn.pydata.org/bokeh/release/bokeh-0.12.4.min.js')
    SubElement(head, 'script', src='http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.4.min.js')
    body = SubElement(test_elem, 'body')

    # Nav
    add_v_nav(body, test)

    # Next/Previous
    add_h_nav(body, test)

    # Test stats
    stats = SubElement(body, 'div', {'class': 'stats'})
    for name, data in zip(
            ['Description'],
            [test.description]):
        add_text_element(stats, 'h3', name)
        add_text_element(stats, 'p', data)

    # Update parent listing
    update_parent_listing(obj_elem, test)

    # Write Data
    write_data = SubElement(body, 'div', {'class': 'write'})
    add_text_element(write_data, 'h3', 'Write Data')
    write_table = make_table(write_data, ['Write Variable', 'Write Value'])
    for signal_name, signal_value in test.write.items():
        signal_row = SubElement(write_table, 'tr')
        add_text_element(signal_row, 'td', signal_name)
        add_text_element(signal_row, 'td', str(signal_value))

    # Verify Data
    verify_data = SubElement(body, 'div', {'class': 'verify'})
    add_text_element(verify_data, 'h3', 'Verify Data')
    for signal in sorted(test.verify.items()):
        signal_elem = SubElement(verify_data, 'div', {'class': 'signal'})
        if test.category == 'SEQUENCE':
            make_sequence_verify(signal_elem, signal)
        elif test.category == 'STIMULUS':
            make_stimulus_verify(signal_elem, signal, head, test.time)
        else:
            assert False, 'Test is neither a sequence nor a stimulus?'

    return test_elem
