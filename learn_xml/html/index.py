import datetime



def index_open(index, report_name, report_description):
    time_string = datetime.datetime.now().strftime('Executed %H:%M %m/%d/%y')
    with open(index, 'w') as f:
        f.write("""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{0}</title>
<link rel="stylesheet" type="text/css" href="../css/report.css">
</head>
<body>
<div class="title">
<ul class="time">
<li>{2}</li>
</ul>
<ul>
<li><a href="#">{0}</a></li>
</ul>
</div>
""".format(report_name, report_description, time_string))


def index_statistics(index, summary):
    with open(index, 'a') as f:
            f.write("""<div class="statistics">
<h3>Test Summary</h3>
""")
    categories = ['Count', 'Passed', 'Failed']
    for category, data in zip(categories, summary.as_lists()):
        combined = data + [category]
        with open(index, 'a') as f:
            f.write("""<div class="{6}">
<table>
<tr>
<th>{6}</th>
<th>Total</th>
<th>ASIL</th>
</tr>
<tr>
<td>Requirement
<td>{0}
<td>{3}
</tr>
<tr>
<td>Objective
<td>{1}
<td>{4}
</tr>
<tr>
<td>Test
<td>{2}
<td>{5}
</tr>
</table>
</div>
""".format(*combined))
    with open(index, 'a') as f:
        f.write("""</div>
""")


def index_start_requirements(index):
    with open(index, 'a') as f:
        f.write("""<div class="listing">
<h3>Requirements</h3>
<p>Click a requirement below for detailed information</p>
<table>
<tr>
<th class="AT">AT Number</th>
<th class="ASIL">ASIL</th>
<th class="status">Status</th>
</tr>
""")


def link_requirement_to_index(index_path, r):
    asil = 'A' if r.asil else 'False'
    status = 'Pass' if r.status else 'Fail'
    with open(index_path, 'a') as f:
        f.write("""<tr onclick="document.location = 'RESOURCE/{0}.html';">
<td class="AT">{0}</td>
<td class="asil_{1}">{1}</td>
<td class="passed_{2}">{2}</td>
</tr>
""".format(r.at, asil, status))


def index_close(index):
    with open(index, 'a') as f:
        f.write("""</table>
</div>
</body>
</html>
""")
