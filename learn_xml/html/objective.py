import datetime


def _safe(at):
    return '{}.html'.format(at) if at else '#" class="invalid'


def objective_open(objective_path, report_name, requirement, objective):
    time_string = datetime.datetime.now().strftime('Executed %H:%M %m/%d/%y')
    with open(objective_path, 'w') as f:
        f.write("""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{0}</title>
<link rel="stylesheet" type="text/css" href="../../../css/report.css">
</head>
<body>
<div class="title">
<ul class="time">
<li>{3}</li>
</ul>
<ul>
<li><a href="../../{0}.html">{0}</a></li>
<li><a href="../{1}.html">{1}</a></li>
<li><a href="#">{2}</a></li>
</ul>
</div>
<div class="AT_header">
<ul class="navigation">
<li><a href="{4}">Previous</a></li>
<li><a href="{5}">Next</a></li>
</ul>
<h1>Objective {2}</h1>
</div>
""".format(report_name, requirement, objective.at, time_string, _safe(objective.prev_at), _safe(objective.next_at)))


def objective_description(objective_path, description):
    with open(objective_path, 'a') as f:
        f.write("""<div class="description">
<h3>Description</h3>
<p>{0}</p>
</div>
""".format(description))


def objectives_start_tests(objective_path):
    with open(objective_path, 'a') as f:
        f.write("""<div class="listing">
<h3>Tests</h3>
<p>Click a test below for detailed information</p>
<table>
<tr>
<th class="AT">AT Number</th>
<th class="ASIL">ASIL</th>
<th class="status">Status</th>
</tr>
""")


def link_test_to_objective(objective_path, o, t):
    asil = 'A' if t.asil else 'False'
    status = 'Pass' if t.status else 'Fail'
    with open(objective_path, 'a') as f:
        f.write("""<tr onclick="document.location = '{3}/{0}.html';">
<td class="AT">{0}</td>
<td class="asil_{1}">{1}</td>
<td class="passed_{2}">{2}</td>
</tr>
""".format(t.at, asil, status, o.at))


def objective_close(objective_path):
    with open(objective_path, 'a') as f:
        f.write("""</table>
</div>
</body>
</html>
""")
