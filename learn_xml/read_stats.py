import pstats

stats = pstats.Stats('stats.txt')

stats.strip_dirs().sort_stats('cumulative').print_stats(30)