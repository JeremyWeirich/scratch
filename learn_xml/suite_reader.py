import os
import json
import re

from form import Suite, Requirement, Objective, Test

BUILDER = [
    {'build': Requirement, 'parent': None, 'previous': None},
    {'build': Objective, 'parent': None, 'previous': None},
    {'build': Test, 'parent': None, 'previous': None},
    {'parent': None}
]


def _time_sorted_valid_paths(base):
    valid_path = re.compile(r'^AT(\d+)(_\d+)?(_\d+)?\.txt$')
    paths = [os.path.join(base, i) for i in os.listdir(base) if i.startswith('AT')]
    time_sorted = sorted(paths, key=os.path.getmtime)
    for p in time_sorted:
        if re.match(valid_path, os.path.basename(p)):
            yield p


def _set_previous(current_type, child):
    if current_type['previous']:
        current_type['previous'].next_at = child.at
        child.previous_at = current_type['previous'].at
    current_type['previous'] = child


def read_files(base):
    suite = Suite('New Style Report', None)
    BUILDER[0]['parent'] = suite  # set suite as parent of first requirement
    for p in _time_sorted_valid_paths(base):
        name = os.path.basename(p)[:-4]
        with open(p, 'r') as f:
            data = json.loads(f.read())
        current_type = BUILDER[name.count('_')]
        child = current_type['build'](name, data)
        BUILDER[name.count('_') + 1]['parent'] = child
        BUILDER[name.count('_') + 1]['previous'] = None
        child.set_parent(current_type['parent'])
        child.parent.add(child)
        _set_previous(current_type, child)
    return suite
