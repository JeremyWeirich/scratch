import os
import markovify
import random
from openpyxl import load_workbook

VTC_PATHS = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases' \
            r'\VTC-P14106001_BMW_CDM'

MARKOV = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases' \
         r'\VTC-P14106001_BMW_CDM\markov.xlsx'


def get_corpus():
    print 'making corpus'
    requirement_descriptions = ''
    requirement_explanations = ''
    objective_descriptions = ''
    test_descriptions = ''
    test_data = ''
    test_results = ''
    wb = load_workbook(os.path.join(VTC_PATHS, 'VTC-P14106001_BMW_CDM_CoSi.xlsx'), read_only=True, data_only=True)
    sheet = wb.get_sheet_by_name('4DNP Manual')
    for f_row in sheet.iter_rows(row_offset=14):
        at, description, data, results = f_row[3].value, f_row[4].value, f_row[5].value, f_row[6].value
        if at:
            if at.startswith('TS'):
                if at.count('_') == 0:
                    requirement_descriptions += description
                elif at.count('_') == 1:
                    objective_descriptions += description
                elif at.count('_') == 2:
                    test_descriptions += description
                    test_data += data
                    test_results += results
            elif 'Time:' in at:
                requirement_explanations += description

    return dict(
        requirement_descriptions=[requirement_descriptions],
        requirement_explanations=[requirement_explanations],
        objective_descriptions=[objective_descriptions],
        test_descriptions=[test_descriptions],
        test_data=[test_data],
        test_results=[test_results],
    )


def make_model(corpus):
    for key, value in corpus.items():
        print 'making model for', key
        if 'test' in key:
            value.append(markovify.NewlineText(value[0]))
        else:
            value.append(markovify.Text(value[0]))


def make_data(model, length, separator=' '):
    data = ''
    for i in range(length):
        generated = model.make_sentence()
        generated = generated if generated else ''
        data += generated
        data += separator
    return data


def create_markovified_vtcs(corpus):
    print 'creating markovified vtcs'
    wb = load_workbook(MARKOV)
    sheet = wb.get_sheet_by_name('4DNP Manual')
    for row in range(14, 60):
        print 'working on row', row
        row_type = 0   # req = 0, obj = 1, test = 2, req_exp = 3
        for column in 'DEFG':
            current = sheet['{}{}'.format(column, row)].value
            if column == 'D':
                if current:
                    if current.startswith('TS'):
                        row_type = current.count('_')
                    elif current.startswith('Time:'):
                        row_type = 3
            elif column == 'E':
                if row_type == 0:
                    sheet['{}{}'.format(column, row)].value = make_data(corpus['requirement_descriptions'][1], random.randrange(2, 10))
                elif row_type == 1:
                    sheet['{}{}'.format(column, row)].value = make_data(corpus['objective_descriptions'][1], 1)
                elif row_type == 2:
                    sheet['{}{}'.format(column, row)].value = make_data(corpus['test_descriptions'][1], random.randrange(1, 6), '\n')
            elif column == 'F':
                if row_type == 2:
                    sheet['{}{}'.format(column, row)].value = make_data(corpus['test_data'][1], random.randrange(2, 8), '\n')
            elif column == 'G':
                if row_type == 2:
                    sheet['{}{}'.format(column, row)].value = make_data(corpus['test_results'][1], random.randrange(3, 9), '\n')
    wb.save(MARKOV)


if __name__ == '__main__':
    corpus = get_corpus()
    make_model(corpus)
    create_markovified_vtcs(corpus)
