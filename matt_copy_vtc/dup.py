from openpyxl import load_workbook, Workbook

objective = 0
test = 0

copies = 50

wb = load_workbook('trimmed.xlsx', read_only=True, data_only=True)
for s in wb.worksheets:
    if s.title == 'CG2999 Stress Test':
        sheet = s
data = []
for row in sheet.iter_rows():
    data.append([cell.value for cell in row])

length = len(data)
print length

def handle_at(v):
    global objective, test
    if v.startswith('AT'):
        if v.count('_') == 1:
            test = 0
            objective += 1
            return 'AT12_{}'.format(objective)
        if v.count('_') == 2:
            test += 1
            return 'AT12_{}_{}'.format(objective, test)
    return v


new = Workbook()
ws = new.get_active_sheet()
for i in range(copies):
    start = i * length + 1
    stop = (i + 1) * length
    print start, stop
    for row, data_row in zip(ws.iter_rows(min_row=start, max_row=stop, max_col=14), data):
        for cell, value in zip(row, data_row):
            if value and isinstance(value, basestring):
                if '=' in value:
                    value = "'" + value
                value = handle_at(value)
            cell.value = value

new.save('new.xlsx')