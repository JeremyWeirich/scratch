from rls import rls
from openpyxl import load_workbook
from itertools import count
import re
import glob
import os

p = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Results\Executed_Testcases\VTC-P14106001_BMW_CDM-2_2_0'

modify_regex = re.compile(r'^[Mm]odify( [Tt]est)?( [Cc]ase)?( *)?:( *)?(\n*)?([\s\S]*)$')
for filename in glob.glob('{}\VTC*.xlsx'.format(p)):
    basename = os.path.basename(filename)
    print basename
    r = rls.RLS(project='P14106001_BMW_CDM', phase='Testing', subject=basename)
    wb = load_workbook(filename, data_only=True, read_only=True)
    for sheet in wb.worksheets:
        print 'reading tab {}'.format(sheet.title)
        for r_index, row in zip(count(), sheet.iter_rows()):
            if len(row) > 11:
                at = row[3].value
                comment = row[11].value
                if comment and isinstance(comment, basestring):
                    description = re.match(modify_regex, comment)
                    if description:
                        r.add(location=sheet.title, line='{} / {}'.format(r_index + 1, at), description=description.group(6))

    r.save(r'N:\Users\Software Validation Team\Tools\Python\lib\RLS_{}'.format(basename))
