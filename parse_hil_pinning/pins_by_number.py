import re
from openpyxl import load_workbook, Workbook


pinning = {}
regex = r'CDM \d\\(\d+) \[Rack \(\d+\)\]'

filename = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\parse_hil_pinning\Detailed Pinning.xlsx'
wb = load_workbook(filename, read_only=True, data_only=True)
for sheet in wb.worksheets:
    print sheet.title
    pinning[sheet.title] = {}
    name = None
    for row in sheet.iter_rows():
        name_test = row[6].value
        if name_test == 'Name':
            name = row[7].value
            continue
        pin = row[7].value
        if pin:
            matches = re.finditer(regex, pin)
            for i in matches:
                for group in i.groups():
                    group = int(group)
                    if group in pinning[sheet.title]:
                        pinning[sheet.title][group].append(name)
                    else:
                        pinning[sheet.title][group] = [name]
#print pinning
new = Workbook()
for slot, pins in pinning.items():
    ws = new.create_sheet(slot)
    for row, (pin, names) in enumerate(pins.items()):
        row += 1
        ws.cell(row=row, column=1, value=pin)
        for column, name in enumerate(names):
            column += 2
            ws.cell(row=row, column=column, value=name)
new.save('test.xlsx')
