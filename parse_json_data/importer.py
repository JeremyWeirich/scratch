class Importer(object):
    """Receives file information from ADInterface and uses openpyxl to load the Excel document.  Provides the document
    to Parser.  This is the only location where the openpyxl library should be used.
    """
    class TestFileInfo(object):
        """Holds information on the excel file to be parsed and used for testing.  Requires a dict containing the
        path, filename, and sheet containing tests."""
        def __init__(self, file_info):
            self.path = file_info["path"]
            self.filename = file_info["filename"]
            self.sheet = file_info["sheet"]
            self.is_valid = True
            self.error = None

    class RawTestData(object):
        """Holds test data extracted using openpyxl as dict, keys = sheet names, data = rows of internal values"""
        def __init__(self):
            self.sheets = {}

        def add_sheet(self, key, data):
            self.sheets.update({key: data})

        def get_sheet_names(self):
            return self.sheets.keys()

        def get_data_dict(self):
            return self.sheets

    def __init__(self, file_info):
        self.sheets_needed = ["VariablePool", "TolerancePool"]
        self.test_file = self.create_file_object(file_info)

    @staticmethod
    def critical_exit(warning_message):
        import ctypes
        ctypes.windll.user32.MessageBoxA(0, warning_message, "Critical Error", 0)
        quit()

    def create_file_object(self, file_info):
        """Creates test file object and checks it for validity.  If the file is invalid, ends the script and warns the
        tester.
        @param file_info: dict with keys "path", "filename", and "sheet"
        """
        def check_path(current_file):
            """Given test file object, test file path
            @param current_file: Test file object
            """
            if os.path.isdir(current_file.path):
                os.chdir(current_file.path)
            else:
                current_file.is_valid = False
                current_file.error_location = "path"
            return current_file

        def check_filename(current_file):
            """Given test file object, test file name.  Add ".xlsx" extension if necessary
            @param current_file: Test file object
            """
            if not current_file.filename.endswith(".xlsx"):
                current_file.filename = current_file.filename.split(".")[0] + ".xlsx"
            if os.path.isfile(current_file.filename):
                pass
            else:
                current_file.is_valid = False
                current_file.error_location = "filename"
            return current_file

        def check_sheet(current_file):
            """Given test file object, test sheet name.  Add to expected sheets if it is valid
            @param current_file: Test file object
            """
            not_allowed = ["\\", "/", "*", "[", "]", ":", "?"]
            for char in not_allowed:
                if char in current_file.sheet:
                    current_file.is_valid = False
                    current_file.error_location = "sheet"
            self.sheets_needed.append(test_file.sheet)
            return current_file

        import os
        test_file = self.TestFileInfo(file_info)
        check_functions = [check_path, check_filename, check_sheet]
        for func in check_functions:
            if test_file.is_valid:
                test_file = func(test_file)
            else:
                self.critical_exit("File data is not correct")
        return test_file

    def extract_raw_data(self):
        """Uses openpyxl library to load the excel workbook to memory and returns it.  If openpyxl is not already in
        the path, adds it.
        """

        def get_internal_values(openpyxl_sheet):
            """Iterates through sheet object and extracts internal values.  Returns internal values as list of lists.
            @param openpyxl_sheet: openpyxl sheet object"""
            raw_rows = []
            for row in openpyxl_sheet.iter_rows():
                raw_rows.append([cell.value for cell in row])
            return raw_rows

        def add_module_paths():
            """If module paths for excel import libraries are not part of the system path, adds them"""
            import sys
            path_openpyxl = "C:\Anaconda\pkgs\openpyxl-2.0.2-py27_0\Lib\site-packages"
            path_jdcal = "C:\Anaconda\pkgs\jdcal-1.0-py27_0\Lib\site-packages"
            module_paths = [path_openpyxl, path_jdcal]
            for module_path in module_paths:
                if module_path not in sys.path:
                    sys.path.append(module_path)

        def load_workbook_and_extract_raw_data():
            """Uses openpyxl to load data from the excel sheet using the load_workbook method.  Extracts cell values
            out of the workbook and stores in a dict of rows
            """
            from openpyxl import load_workbook
            data = self.RawTestData()
            wb = load_workbook(self.test_file.filename,  # name of file
                               data_only=True,  # results of formulas instead of literal data
                               use_iterators=True  # faster reading and loading
                               )
            for sheet in wb.worksheets:
                sheet_name = sheet.title
                if sheet_name == self.test_file.sheet or sheet_name in self.sheets_needed:
                    sheet_data = get_internal_values(sheet)
                    data.add_sheet(sheet_name, sheet_data)
            return data

        def all_sheets_present(raw_data_object):
            sheet_names = raw_data_object.get_sheet_names()
            error_with_sheets = False
            if not len(sheet_names) == len(self.sheets_needed):
                error_with_sheets = True
            for name in self.sheets_needed:
                if name not in sheet_names:
                    error_with_sheets = True
            if error_with_sheets:
                self.critical_exit("Sheet data is not correct")

        add_module_paths()
        raw_data = load_workbook_and_extract_raw_data()
        all_sheets_present(raw_data)
        return raw_data
