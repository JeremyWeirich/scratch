import os
from importer import Importer
from serialize.serializeAVTC import SerializeAVTC
# from validate.validateAVTC import ValidateAVTC
sheet_name = "High Auto CP1"

file_info = {"filename": "VTC-P14106001_BMW_CDM_Curtain.xlsx",
             "path": "C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM",
             "sheet": sheet_name}
prev_dir = os.getcwd()
importer = Importer(file_info)
data = importer.extract_raw_data().get_data_dict()
main = data[sheet_name]
trim = [row[1:] for row in main[4:]]
data[sheet_name] = trim
os.chdir(prev_dir)
print prev_dir
serializer = SerializeAVTC(data)
serializer.serialize()
json_avtc = serializer.save_data(sheet_name)

# json_avtc = "N:\Users\Jeremy Weirich\\repos\excel_automation\improve_parse\serialize\serialized_test_data\\AVTC.json"
#
# validator = ValidateAVTC()
# validator.load_data(json_avtc)
# errors = validator.validate()
#
# print "\n"*3
# errors.print_errors()
