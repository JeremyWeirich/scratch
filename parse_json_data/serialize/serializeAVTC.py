import json
import os
from serializers import serializers


class SerializeAVTC(object):
    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.avtc = {}

    def serialize(self):
        for sheet_name, sheet_data in self.raw_data.items():
            if sheet_name in serializers:
                self.avtc[sheet_name] = serializers[sheet_name](sheet_data)
            else:
                self.avtc[sheet_name] = serializers["TestData"](sheet_data)

    def save_data(self, name):
        data_string = json.dumps(self.avtc)
        file_path = os.path.abspath("serialize\serialized_test_data\{}.json".format(name))
        with open(file_path, "w") as serialized_avtc:
            serialized_avtc.write(data_string)
        return file_path
