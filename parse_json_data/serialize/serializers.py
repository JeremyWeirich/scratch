def _serialize_variable_pool(rows):
    ordering = dict(
        type=0,
        name=3,
        default=5,
        path=6,
    )
    data = []
    for row in rows:
        if not row.count(None) == len(row):
            current = {}
            for category, index in ordering.items():
                current.update({category: row[index]})
            data.append(current)
    return data


def _serialize_tolerance_pool(rows):
    ordering = dict(
        name=0,
        tolerance=1,
    )
    data = []
    for row in rows:
        if not row.count(None) == len(row):
            current = {}
            for category, index in ordering.items():
                current.update({category: row[index]})
            data.append(current)
    return data


def _serialize_test_data(rows):
    ordering = dict(
        requirement=dict(
            category="requirement",
            enabled=0,
            at=1,
            description=2,
            explanation=3,
            spec_ref=9
        ),
        objective=dict(
            category="objective",
            enabled=0,
            at=1,
            description=2,
        ),
        test_header=dict(
            category="th",
            enabled=0,
            at=1,
            description=2,
            write_variable=3,
            write_value=4,
            verify_variable=5,
            relational_operator=6,
            verify_value=7,
            verify_tolerance=8,
            pr=9
        ),
        test_body=dict(
            category="tb",
            write_variable=3,
            write_value=4,
            verify_variable=5,
            relational_operator=6,
            verify_value=7,
            verify_tolerance=8,
        ),
    )

    def categorize(at_number):
        valid_categories = ["requirement", "objective", "test_header"]
        if at_number is not None:
            result = valid_categories[at_number.count("_")]
        else:
            result = "test_body"
        return result

    data = []
    for row in rows:
        if not row.count(None) == len(row):
            at = row[1]
            category = categorize(at)
            current = {}
            current_ordering = ordering[category]
            for subcategory, index in current_ordering.items():
                if subcategory == "category":
                    current[subcategory] = index
                else:
                    current[subcategory] = row[index]
            data.append(current)
    return data


serializers = dict(
    VariablePool=_serialize_variable_pool,
    TolerancePool=_serialize_tolerance_pool,
    TestData=_serialize_test_data,
)
