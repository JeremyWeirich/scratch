class ErrorList(object):
    """Contains errors and functions related to handling them"""
    def __init__(self):
        self.list_of_errors = []

    class Error(object):
        """An error found in the test cases"""
        def __init__(self, location_row, description, location_sheet):
            self.sheet = location_sheet
            self.row = location_row
            self.description = description

    def __iter__(self):
        return iter(self.list_of_errors)

    def add(self, row, error_string, location="TestData"):
        """Add a new error to the list
        @param location: name of sheet that contains the error
        @param row: number of row that contains the error
        @param error_string: description of error
        """
        error = self.Error(row, error_string, location)
        self.list_of_errors.append(error)

    def count_errors(self, sheet_name="All"):
        """Returns a count of the number of errors
        @param sheet_name: optional parameter to filter by sheet
        """
        num_errors = 0
        for error in self.list_of_errors:
            if sheet_name == "All" or sheet_name == error.sheet:
                num_errors += 1
        return num_errors

    def print_errors(self, sheet_name="All"):
        """Debugging, prints the errors to console
        @param sheet_name: optional parameter to filter by sheet
        """
        for error in self.list_of_errors:
            if sheet_name == "All" or sheet_name == error.sheet:
                print "{} {}: {}".format(error.sheet, error.row, error.description)

