import json
from errorlist import ErrorList
import validationcontroller


class ValidateAVTC(object):
    def __init__(self):
        self.avtc = None
        self.errors = ErrorList()

    def load_data(self, file_path):
        with open(file_path, "r") as serialized_avtc:
            self.avtc = json.load(serialized_avtc)

    def validate(self):
        for sheet_name, sheet_data in self.avtc.items():
            if sheet_name not in validationcontroller.validate_functions:
                sheet_name = "TestData"
            for row_index, row in enumerate(sheet_data):
                for cell_category, cell_value in row.items():
                    validation_function = validationcontroller.get_validation_function(sheet_name=sheet_name, category=cell_category)
                    errors = validation_function(cell_value)
                    if errors:
                        for error in errors:
                            self.errors.add(row_index, error, sheet_name)
        return self.errors

