# Points validateAVTC towards the correct functions

from validators import variablepool, tolerancepool, testdata


validate_functions = dict(
    VariablePool=variablepool.validation_methods,
    TolerancePool=tolerancepool.validation_methods,
    TestData=testdata.validation_methods
)


def get_validation_function(sheet_name=None, category=None):
    if category:
        if sheet_name:
            return validate_functions[sheet_name][category]
