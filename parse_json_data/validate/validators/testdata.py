def validate_enabled(enabled):
    errors = []
    if enabled is not None:
        if enabled.lower() in ["disabled", "enabled"]:
            pass
        else:
            errors.append("Invalid enabled state")
    else:
        pass
    return errors


def validate_req_at(at):
    errors = []
    if at is not None:
        if isinstance(at, basestring):
            if at.startswith("AT"):
                if at.count("_") == 0:
                    pass
                else:
                    errors.append("Wrong category of AT number")
            else:
                errors.append("AT number does not begin with \"AT\"")
        else:
            errors.append("AT number is not a string")
    else:
        errors.append("AT number not found")
    return errors


def validate_obj_at(at):
    errors = []
    if at is not None:
        if isinstance(at, basestring):
            if at.startswith("AT"):
                if at.count("_") == 1:
                    pass
                else:
                    errors.append("Wrong category of AT number")
            else:
                errors.append("AT number does not begin with \"AT\"")
        else:
            errors.append("AT number is not a string")
    else:
        errors.append("AT number not found")
    return errors


def validate_th_at(at):
    errors = []
    if at is not None:
        if isinstance(at, basestring):
            if at.startswith("AT"):
                if at.count("_") == 2:
                    pass
                else:
                    errors.append("Wrong category of AT number")
            else:
                errors.append("AT number does not begin with \"AT\"")
        else:
            errors.append("AT number is not a string")
    else:
        errors.append("AT number not found")
    return errors


def validate_description(description):
    errors = []
    if description is not None:
        if isinstance(description, basestring):
            pass
        else:
            errors.append("Description must be a string")
    else:
        errors.append("Description not found")
    return errors


def validate_explanation(explanation):
    errors = []
    if explanation is not None:
        if isinstance(explanation, basestring):
            pass
        else:
            errors.append("Explanation must be a string")
    else:
        errors.append("Explanation not found")
    return errors


def validate_spec_ref(spec_ref):
    errors = []
    if spec_ref is not None:
        if isinstance(spec_ref, basestring):
            pass
        else:
            errors.append("Specification Reference must be a string")
    else:
        errors.append("Specification Reference not found")
    return errors


def validate_th_write_or_verify_value(th_value):
    errors = []
    if th_value is not None:
        pass  # TODO number or valid stimulus check goes in here
    else:
        errors.append("Test Header must contain a write and verify value")
    return errors


def validate_th_write_or_verify_variable(th_variable):
    errors = []
    if th_variable is not None:
        if isinstance(th_variable, basestring):
            pass
        else:
            errors.append("Write/Verify variable must be string")
    else:
        errors.append("Test Header must contain a write and verify variable")
    return errors


def validate_th_relational_operator(operator):
    errors = []
    acceptable_operators = ["==", "!=", ">", ">=", "<", "<="]
    if operator is not None:
        if operator in acceptable_operators:
            pass
        else:
            errors.append("Invalid relational operator")
    else:
        errors.append("Test Header must contain a relational operator")
    return errors


def validate_th_tolerance(tolerance):
    errors = []
    if tolerance is not None:
        if isinstance(tolerance, basestring):
            pass
        else:
            errors.append("Tolerance must be a string")
    else:
        errors.append("Test Header must contain a verify tolerance")
    return errors


def validate_pr(pr):
    print pr


validation_methods = dict(
    # Requirement methods
    req_enabled=validate_enabled,
    req_at=validate_req_at,
    req_description=validate_description,
    req_explanation=validate_explanation,
    req_spec_ref=validate_spec_ref,
    # Objective methods
    obj_enabled=validate_enabled,
    obj_at=validate_obj_at,
    obj_description=validate_description,
    # Test Header methods
    th_enabled=validate_enabled,
    th_at=validate_th_at,
    th_description=validate_description,
    th_write_variable=validate_th_write_or_verify_variable,
    th_write_value=validate_th_write_or_verify_value,
    th_verify_veriable=validate_th_write_or_verify_variable,
    th_relational_operator=validate_th_relational_operator,
    th_verify_value=validate_th_write_or_verify_value,
    th_verify_tolerance=validate_th_tolerance,
    th_pr=validate_pr,
    # Test Body methods
    tb_write_variable=validate_tb_write_or_verify_variable,
    tb_write_value=validate_tb_write_or_verify_value,
    tb_verify_veriable=validate_tb_write_or_verify_variable,
    tb_relational_operator=validate_tb_relational_operator,
    tb_verify_value=validate_tb_write_or_verify_value,
    tb_verify_tolerance=validate_tb_tolerance,
)
