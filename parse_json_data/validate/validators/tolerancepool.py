def validate_name(t_name):
    errors = []
    if t_name is not None:
        if isinstance(t_name, basestring):
            pass
        else:
            errors.append("Tolerance name is not a string")
    else:
        errors.append("Tolerance name is empty")
    return errors


def validate_tolerance(t_tolerance):
    errors = []
    if t_tolerance is not None:
        from numbers import Number
        if isinstance(t_tolerance, Number):
            if t_tolerance >= 0:
                pass
            else:
                errors.append("Tolerance value must be positive")
        else:
            errors.append("Tolerance value is not a number")
    else:
        errors.append("Tolerance value is empty")
    return errors


validation_methods = dict(
    name=validate_name,
    tolerance=validate_tolerance
)
