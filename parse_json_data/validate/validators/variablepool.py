from numbers import Number


def validate_type(v_type):
    errors = []
    if v_type is not None:
        if v_type in ["Write", "Read"]:
            pass
        else:
            errors.append("Variable type is invalid")
    else:
        errors.append("Variable type is empty")
    return errors


def validate_name(v_name):
    errors = []
    if v_name is not None:
        if isinstance(v_name, basestring):
            pass
        else:
            errors.append("Variable name is not a string")
    else:
        errors.append("Variable name is empty")
    return errors


def validate_default(v_default):
    errors = []
    if v_default is not None:
        if isinstance(v_default, Number):
            pass
        else:
            errors.append("Variable default is not a number")
    else:
        pass  # None acceptable if type == Read
    return errors


def validate_path(v_path):
    errors = []
    if v_path is not None:
        if isinstance(v_path, basestring):
            if v_path.startswith(("Model Root", "BusSystems")):
                pass
            else:
                errors.append("Variable path expected to start with Model Root or BusSystems")
        else:
            errors.append("Variable path is not a string")
    else:
        errors.append("Variable path is empty")
    return errors


validation_methods = dict(
    type=validate_type,
    name=validate_name,
    default=validate_default,
    path=validate_path
)
