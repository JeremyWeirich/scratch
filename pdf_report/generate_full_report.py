import re
import os
import json
import datetime

from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, PageBreak, Table, TableStyle
from reportlab.lib.units import inch
from reportlab.lib.colors import black, HexColor

GREEN = HexColor('#4CAF50')
RED = HexColor('#F44242')

from reportlab.lib.pagesizes import letter

from res import STYLES, HR_1, HR_2

PAGE_WIDTH, PAGE_HEIGHT = letter
MARGINS = dict(
    rightMargin=36,
    leftMargin=36,
    topMargin=36,
    bottomMargin=36,
)

a = set()
b = set()


def _time_sorted_valid_paths(base):
    valid_path = re.compile(r'^AT(\d+)(_\d+)?(_\d+)?([a-zA-Z])?\.txt$')
    paths = [os.path.join(base, i) for i in os.listdir(base) if i.startswith('AT')]
    time_sorted = sorted(paths, key=os.path.getmtime)
    for p in time_sorted:
        if re.match(valid_path, os.path.basename(p)):
            yield p


def _categorize(name):
    categories = [
        'REQUIREMENT',
        'OBJECTIVE',
        'TEST',
    ]
    return categories[name.count('_')]


def summarize(path):
    def update_table_data(table, category, form):
        indices = dict(
            requirement=0,
            objective=1,
            test=2,
        )
        if form['status'] == "Pass":
            table[indices[category]][1] += 1
        elif form['status'] == "Fail":
            table[indices[category]][2] += 1

    with open(os.path.join(path, 'status.txt'), 'r') as f:
        status = json.loads(f.read())
    table_data = [
        ['Requirement', 0, 0, 'NA'],
        ['Objective', 0, 0, 'NA'],
        ['Test', 0, 0, 'NA'],
    ]
    for requirement in status['testing_summary']:
        update_table_data(table_data, 'requirement', requirement)
        for objective in requirement['children']:
            update_table_data(table_data, 'objective', objective)
            for test in objective['children']:
                update_table_data(table_data, 'test', test)
    for row in table_data:
        try:
            row[3] = '{0:.0f}%'.format(100 * row[1] / (row[1] + row[2]))
        except ZeroDivisionError:
            pass
    return status, table_data


def add_title(story, status):
    story.append(Spacer(1, 2 * inch))
    story.append(Paragraph(status['name'], STYLES['title']))
    subtitles = [
        'Test Suite: {} from {}'.format(status['sheet'], status['filename']),
        'HIL: {}'.format(status['hil']),
        'Executed: {}'.format(status['start'])
    ]
    for subtitle in subtitles:
        story.append(Paragraph(subtitle, STYLES['subtitle']))
    story.append(PageBreak())
    return story


def add_summary(story, table_data):
    story.append(Paragraph('Summary:', STYLES['h1']))
    story.append(Spacer(1, 0.1 * inch))
    table_data.insert(0, ['', 'Pass', 'Fail', '%'])
    t = Table(table_data, hAlign='LEFT')
    t.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
        ('BOX', (0, 0), (-1, -1), 0.25, black),
    ]))
    story.append(t)
    story.append(Spacer(1, 0.2 * inch))
    return story


def add_links(story, status):
    story.append(Paragraph('Status:', STYLES['h1']))
    story.append(Spacer(1, 0.1 * inch))
    for requirement in status['testing_summary']:
        story.append(HR_1)
        story.append(Spacer(1, 0.1 * inch))
        story.append(Paragraph('<a name="top_{0}"/><a href="#{0}" color="blue">{0}</a>: {1}'.format(requirement['at'], requirement['status']), STYLES['default']))
        for objective in requirement['children']:
            story.append(Paragraph('<a name="top_{0}"/><a href="#{0}" color="blue">{0}</a>: {1}'.format(objective['at'], objective['status']), STYLES['indent_10']))
            for test in objective['children']:
                story.append(Paragraph('<a name="top_{0}"/><a href="#{0}" color="blue">{0}</a>\t{1}'.format(test['at'], test['status']), STYLES['indent_20']))
            story.append(Spacer(1, 0.1 * inch))
        story.append(Spacer(1, 0.1 * inch))
    story.append(PageBreak())
    return story


def _break(s):
    return s.replace('\n', '<br/>')


def convert_time(data):
    timestamp = data['time']
    data['time'] = datetime.datetime.fromtimestamp(timestamp).strftime(r'%b %d, %Y | %H:%M:%S')
    return data


def add_requirement(story, status, at):
    story.append(Spacer(1, 0.3 * inch))
    story.append(HR_2)
    story.append(Spacer(1, 0.2 * inch))
    story.append(Paragraph('<a href="#top_{0}" color="blue">Top</a>'.format(at), STYLES['tiny']))
    story.append(Paragraph('<a name="{0}"/>{0}'.format(at), STYLES['h1']))
    story.append(Paragraph(status['time'], STYLES['default']))
    story.append(Paragraph('Description:', STYLES['h4']))
    story.append(Paragraph(_break(status['description']), STYLES['default']))
    story.append(Paragraph('Explanation:', STYLES['h4']))
    story.append(Paragraph(_break(status['explanation']), STYLES['default']))
    story.append(Paragraph('Spec Ref:', STYLES['h4']))
    story.append(Paragraph(_break(status['spec_ref']), STYLES['default']))
    return story


def add_objective(story, status, at):
    story.append(Spacer(1, 0.2 * inch))
    story.append(Paragraph('<a href="#top_{0}" color="blue">Top</a>'.format(at), STYLES['tiny']))
    story.append(Paragraph('<a name="{0}"/>{0}'.format(at), STYLES['h2']))
    story.append(Paragraph(status['time'], STYLES['default']))
    story.append(Paragraph('Description:', STYLES['h4']))
    story.append(Paragraph(_break(status['description']), STYLES['default']))
    story.append(Spacer(1, 0.2 * inch))
    return story


def _create_write_table(write_data, category):
    if category == 'SEQUENCE':
        table_data = [
            ['Signal', 'Value']
        ]
        for signal, value in write_data.items():
            table_data.append([signal, value])
    elif category == 'STIMULUS':
        table_data = [
            ['Signal', 'Time', 'Value']
        ]
        for signal, values in write_data.items():
            for signal_time, signal_data in zip(values[0], values[1]):
                table_data.append([signal, signal_time, signal_data])
    else:
        table_data = []
    # print table_data
    t = Table(table_data, hAlign='LEFT')
    t.setStyle(TableStyle([
        ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
        ('BOX', (0, 0), (-1, -1), 0.25, black),
    ]))
    return t


def _create_verify_table(verify_data, category):
    table_style = [
        ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
        ('BOX', (0, 0), (-1, -1), 0.25, black),
    ]
    if category == 'SEQUENCE':
        table_data = [
            ['Signal', 'Measured', 'Operator', 'Expected', 'Lower', 'Upper', 'Status']
        ]
        for i, (signal, data) in enumerate(verify_data.items()):
            result = data['results']
            table_data.append([signal, result['measured'], result['operator'], result['expected'], result['lower'], result['upper'], result['status']])
            color = GREEN if result['status'] == 'Pass' else RED
            table_style.append(('BACKGROUND', (-1, i+1), (-1, i+1), color))
    elif category == 'STIMULUS':
        table_data = [
            ['Signal', 'Time', 'Measured', 'Operator', 'Expected', 'Lower', 'Upper', 'Status']
        ]
        i = 0
        for signal, data in verify_data.items():
            for result in data['results']:
                i += 1
                table_data.append([signal, result['time'], result['measured'], result['operator'], result['expected'], result['lower'], result['upper'], result['status']])
                color = GREEN if result['status'] == 'Pass' else RED
                table_style.append(('BACKGROUND', (-1, i), (-1, i), color))
    else:
        table_data = []
    t = Table(table_data, hAlign='LEFT')
    t.setStyle(TableStyle(table_style))
    return t


def add_test(story, status, at):
    story.append(Spacer(1, 0.2 * inch))
    story.append(Paragraph('<a href="#top_{0}" color="blue">Top</a>'.format(at), STYLES['tiny']))
    story.append(Paragraph('<a name="{0}"/>{0}'.format(at), STYLES['h3']))
    story.append(Paragraph(status['time'], STYLES['default']))
    story.append(Spacer(1, 0.1 * inch))
    story.append(Paragraph('Description:', STYLES['h4']))
    story.append(Paragraph(_break(status['description']), STYLES['default']))
    story.append(Spacer(1, 0.1 * inch))
    story.append(Paragraph('Write Data:', STYLES['h4']))
    story.append(_create_write_table(status['write'], status['category']))
    story.append(Spacer(1, 0.1 * inch))
    story.append(Paragraph('Verify Data:', STYLES['h4']))
    story.append(_create_verify_table(status['verify'], status['category']))
    story.append(Spacer(1, 0.1 * inch))
    return story


def make_pdf_report(rp, base):
    print 'working on', rp
    report_path = os.path.join(base, rp)
    doc = SimpleDocTemplate(
        filename='{}.pdf'.format(os.path.join(base, rp.split(' - ')[1])),
        # filename='{}.pdf'.format(rp.split(' - ')[1]),
        pagesize=letter,
        **MARGINS
    )
    status, table_data = summarize(report_path)
    story = []
    story = add_title(story, status)
    story = add_summary(story, table_data)
    story = add_links(story, status)
    for report_path in _time_sorted_valid_paths(report_path):
        report_file = os.path.basename(report_path)
        category = _categorize(report_file)
        at = report_file[:-4]
        with open(report_path, 'r') as f:
            data = json.loads(f.read())
        data = convert_time(data)
        if category == 'REQUIREMENT':
            story = add_requirement(story, data, at)
        elif category == 'OBJECTIVE':
            story = add_objective(story, data, at)
        elif category == 'TEST':
            story = add_test(story, data, at)
    doc.build(story)

if __name__ == '__main__':
    import time
    base_path = r
    start = time.time()
    make_pdf_report(rp, base_path)
    print 'length:', time.time() - start