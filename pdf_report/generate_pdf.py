import os
import json

from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, PageBreak, flowables
from reportlab.lib.units import inch

from reportlab.lib.pagesizes import letter

from res import STYLES, HR

PAGE_WIDTH, PAGE_HEIGHT = letter
MARGINS = dict(
    rightMargin=36,
    leftMargin=36,
    topMargin=36,
    bottomMargin=36,
)


def summarize(path):
    def update_table_data(table, category, form):
        indices = dict(
            requirement=0,
            objective=1,
            test=2,
        )
        if form['status'] == "Pass":
            table[indices[category]][1] += 1
        elif form['status'] == "Fail":
            table[indices[category]][2] += 1

    with open(os.path.join(path, 'status.txt'), 'r') as f:
        status = json.loads(f.read())
    table_data = [
        ['Requirement', 0, 0, 'NA'],
        ['Objective', 0, 0, 'NA'],
        ['Test', 0, 0, 'NA'],
    ]
    for requirement in status['testing_summary']:
        update_table_data(table_data, 'requirement', requirement)
        for objective in requirement['children']:
            update_table_data(table_data, 'objective', objective)
            for test in objective['children']:
                update_table_data(table_data, 'test', test)
    for row in table_data:
        try:
            row[3] = '{0:.0f}%'.format(100 * row[1] / (row[1] + row[2]))
        except ZeroDivisionError:
            pass
    return dict(
        status=status,
        table_data=table_data,
    )


def add_title(story, status):
    story.append(Spacer(1, 2 * inch))
    story.append(Paragraph(status['name'], STYLES['title']))
    subtitles = [
        'Test Suite: {} from {}'.format(status['sheet'], status['filename']),
        'HIL: {}'.format(status['hil']),
        'Executed: {}'.format(status['start'])
    ]
    for subtitle in subtitles:
        story.append(Paragraph(subtitle, STYLES['subtitle']))
    story.append(PageBreak())
    return story


def add_requirement(story, requirement):
    req_line = '{}: {}'.format(requirement['at'], requirement['status'])
    story.append(Paragraph(req_line, STYLES['h1']))
    return story


def add_objective(story, objective):
    obj_line = '{}: {}'.format(objective['at'], objective['status'])
    story.append(Paragraph(obj_line, STYLES['h2']))
    return story


def add_test(story, test):
    test_line = '{}: {}'.format(test['at'], test['status'])
    story.append(Paragraph(test_line, STYLES['default']))
    return story


def pass_fail_status(story, testing_summary):
    for requirement in testing_summary:
        story = add_requirement(story, requirement)
        for objective in requirement['children']:
            story = add_objective(story, objective)
            for test in objective['children']:
                story = add_test(story, test)
        story.append(Spacer(1, 0.25 * inch))
        story.append(HR)
        story.append(Spacer(1, 0.25 * inch))
    return story


def make_pdf_report(report_data):
    doc = SimpleDocTemplate(
        filename='test.pdf',
        pagesize=letter,
        **MARGINS
    )
    story = []
    story = add_title(story, report_data['status'])
    story = pass_fail_status(story, report_data['status']['testing_summary'])
    story = title_and_summary(canvas, )
    style = styles['Normal']
    story.append(Spacer(1, 2 * inch))
    for i in range(100):
        text = """This is a <a href="#{0}" color="blue">link</a> to {0}""".format(i)
        p = Paragraph(text, style)
        story.append(p)
    story.append(Spacer(1, 1 * inch))

    for i in range(100):
        text = ('<a name="{0}"/>This is paragraph {0}'.format(i)) * 20
        para = Paragraph(text, style)
        story.append(para)
        story.append(Spacer(1, 0.2 * inch))
    doc.build(story)


if __name__ == '__main__':
    p = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\pdf_report\1490019379_67 - CoSi 4DNP CP1'
    report_data = summarize(p)
    make_pdf_report(report_data)

