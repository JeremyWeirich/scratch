
from reportlab.platypus import flowables
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT
from reportlab.lib.colors import (
    black,
    green,
    red,
)


def stylesheet():
    styles = {
        'default': ParagraphStyle(
            'default',
            fontName='Times-Roman',
            fontSize=10,
            leading=12,
            leftIndent=0,
            rightIndent=0,
            firstLineIndent=0,
            alignment=TA_LEFT,
            spaceBefore=0,
            spaceAfter=0,
            bulletFontName='Times-Roman',
            bulletFontSize=10,
            bulletIndent=0,
            textColor=black,
            backColor=None,
            wordWrap=None,
            borderWidth=0,
            borderPadding=0,
            borderColor=None,
            borderRadius=None,
            allowWidows=1,
            allowOrphans=0,
            textTransform=None,  # 'uppercase' | 'lowercase' | None
            endDots=None,
            splitLongWords=1,
        ),
    }
    styles['title'] = ParagraphStyle(
        'title',
        parent=styles['default'],
        fontName='Helvetica-Bold',
        fontSize=24,
        leading=30,
        alignment=TA_CENTER,
        textTransform='uppercase',
    )
    styles['subtitle'] = ParagraphStyle(
        'subtitle',
        parent=styles['default'],
        fontName='Helvetica-Bold',
        fontSize=14,
        leading=20,
        alignment=TA_CENTER,
    )
    styles['h1'] = ParagraphStyle(
        'h1',
        parent=styles['default'],
        fontName='Times-Bold',
        fontSize=26,
        leading=30,
    )
    styles['h2'] = ParagraphStyle(
        'h2',
        parent=styles['default'],
        fontName='Times-Bold',
        fontSize=22,
        leading=26,
    )
    styles['h3'] = ParagraphStyle(
        'h3',
        parent=styles['default'],
        fontName='Times-Bold',
        fontSize=18,
        leading=22,
    )
    styles['h4'] = ParagraphStyle(
        'h4',
        parent=styles['default'],
        fontName='Times-Bold',
        fontSize=14,
        leading=18,
    )
    styles['tiny'] = ParagraphStyle(
        'tiny',
        parent=styles['default'],
        fontSize=8,
        leading=8,
    )
    styles['indent_20'] = ParagraphStyle(
        'indent_20',
        parent=styles['default'],
        firstLineIndent=20,
    )
    styles['indent_10'] = ParagraphStyle(
        'indent_10',
        parent=styles['default'],
        firstLineIndent=10,
    )
    return styles

STYLES = stylesheet()

HR_1 = flowables.HRFlowable(
    width='100%',
    thickness=1,
    lineCap='round',
    color='lightgrey',
    hAlign='CENTER',
    dash='None',
)

HR_2 = flowables.HRFlowable(
    width='100%',
    thickness=4,
    lineCap='round',
    color='lightblue',
    hAlign='CENTER',
    dash='None',
)