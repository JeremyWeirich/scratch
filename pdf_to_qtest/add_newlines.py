import re

SPEC = 'GB105.txt'
HEADER = re.compile(r'(?P<section>^3(\.\d)+) (?P<title>[a-zA-Z ]+)')

with open(SPEC, 'r') as f_read:
    with open('new.txt', 'w') as f_write:
        for line in f_read.readlines():
            f_write.write(line)
            match = re.match(HEADER, line)
            if match:
                f_write.write('\n')