import json
import re
from collections import OrderedDict
import pprint

SPEC_NAME =
# Customer ID
LINCS_REGEX = re.compile(r'\[(?P<customer_id>LINCS-\d+)\]')


def make_requirement_title(rnum, requirement):
    rnum += 1
    return "RAW_"


def handle_requirements(results):
    current_rnum = 0
    for header, data in sorted(results.items()):
        print 'new module:', header, data['title']
        if data['requirements']:
            for requirement in data['requirements']:
                print 'new requirement:', make_requirement_title(current_rnum, requirement)


if __name__ == '__main__':
    with open(SPEC) as f:
        results = json.loads(f.read())

    handle_requirements(results)

