import re
from collections import OrderedDict
import json
import pprint

SPEC = r'GB105.txt'
HEADER = re.compile(r'(?P<section>^3(\.\d)+) (?P<title>[a-zA-Z ]+)')
LINCS_REGEX = re.compile(r'\[(?P<customer_id>LINCS-\d+)\]')

RNUM = 0


def _find_parent(outer, section):
    for parent in outer:
        if section.startswith(parent):
            return _find_parent(outer[parent]['children'], section)
    return outer


def get_data(spec_path):
    # read file and get only reqs from it
    with open(spec_path, 'r') as f:
        data = f.read().split('3 Requirements\n')[1]

    results = OrderedDict()
    current = None
    for line in data.split('\n'):
        match =  re.match(HEADER, line)
        if match:
            section = match.group('section')
            title = match.group('title')
            if current:
                current_section = current.pop('section')
                _find_parent(results, current_section)[current_section] = current
            current = dict(title=title, requirements=[], children=OrderedDict(), section=section)
        else:
            if current and line:
                current['requirements'].append(line.decode('latin-1'))
    return results


def make_requirement_title(rnum, requirement):
    return "RAW_{}_{}".format(SPEC[:-4], rnum)


def handle_requirements(results):
    global RNUM
    for header, data in sorted(results.items()):
        print 'new module:', header, data['title']
        if data['requirements']:
            for requirement in data['requirements']:
                requirement = requirement.replace('??', '')
                RNUM += 1
                print '\tnew requirement:', make_requirement_title(RNUM, requirement)
                match = re.search(LINCS_REGEX, requirement)
                if match:
                    cust_id = match.group('customer_id')
                    print '\t\tcust_id:', cust_id
                    requirement = requirement.replace('[{}]'.format(cust_id), '')
                print '\t'*2, requirement
        if data['children']:
            handle_requirements(data['children'])

if __name__ == '__main__':
    data = get_data(SPEC)
    # pprint.pprint(dict(data))

    # with open('results_{}'.format(SPEC), 'w') as f:
    #     f.write(json.dumps(data))

    handle_requirements(data)



