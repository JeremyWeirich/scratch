from math import floor


def progress(t, p):
    progress = int(floor(100 * (t - p) / t))
    return progress


if __name__ == '__main__':
    scenarios = [
        [100, 50],
        [200, 100],
        [100, 100],
        [100, 1],
        [293, 1],
        [433, 1],
        [433, 0],
    ]
    for total, pending in scenarios:
        print progress(total, pending)
