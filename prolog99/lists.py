TEN = [i for i in range(10)]
PALINDROME = [1, 2, 3, 2, 1]
NESTED = [1, 2, [3, 4, [5, 6], 7], 8, 9]
COMPRESS = [1, 1, 1, 2, 2, 2, 3, 3, 4, 2, 5, 5, 5, 3, 3]
# 1: find the last element of a list
print '1:', TEN[-1]

# 2: find the second-to-last element of a list
print '2:', TEN[-2]

# 3: find the kth element of a list
print '3:'
for k in range(10):
    print TEN[k]

# 4: find the number of elements in a list
print '4:', len(TEN)

# 5: reverse a list
print '5:', TEN[::-1]

# 6: find if a list is a palindrome
print '6:', TEN[::-1] == TEN
print '6:', PALINDROME[::-1] == PALINDROME

# 7: flatten a list
out = []
def flatten(iterable):
    for element in iterable:
        if isinstance(element, list):
            flatten(element)
        else:
            out.append(element)
flatten(NESTED)
print '7:', out

# 8: compress a list
out = []
previous = None
for i in COMPRESS:
    if i != previous:
        out.append(i)
        previous = i
print '8:', out

# 9: pack compressed values of list into sublists
out = []
count, current = 1, None
for i in COMPRESS:
    if i != current:
        if current:
            out.append([count, current])
        current = i
        count = 1
    else:
        count += 1
out.append([count, current])
print '9:', out
