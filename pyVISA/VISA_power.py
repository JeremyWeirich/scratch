import time
import visa
inst = visa.ResourceManager().open_resource('GPIB0::6::INSTR')
inst.write("OUTP:STAT OFF")
time.sleep(2)
inst.write("OUTP:STAT ON")
time.sleep(2)
inst.write(":VOLT 8.0")
time.sleep(2)
inst.write(":VOLT 14.0")
time.sleep(2)
inst.write("OUTP:STAT OFF")