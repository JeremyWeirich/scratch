import time
import visa
inst = visa.ResourceManager().open_resource('USB0::0x0957::0x1607::MY50002444::0::INSTR')
time.sleep(2)

inst.write("FUNC SQU")
inst.write("FUNC:SQU:DCYC +40.0")
inst.write("FREQ +1.0E+04")
inst.write("VOLT:HIGH +2.0")
inst.write("VOLT: LOW +0.0")
inst.write("OUTP 1")
time.sleep(3)

inst.write("FUNC RAMP")
time.sleep(5)
inst.write("FUNC SQU")
time.sleep(5)
inst.write("FUNC SIN")
