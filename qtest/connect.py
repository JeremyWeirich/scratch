import requests
import json
import logging

from utils import RESPONSE_CODES
from secrets import CREDENTIALS


def handle_response(request, action):
    if request.status_code == 200:
        logging.info('-- %s successful --', action)
        return json.loads(request.content)
    else:
        logging.warn('-- %s failed --', action)
        logging.warn('%s: %s', request.status_code, RESPONSE_CODES[request.status_code])
        return None


def login(username, password):
    logging.info('-- logging in user {} --'.format(username))
    r = requests.post(
        "http://omronv.qtestnet.com/oauth/token",
        auth=(username, ''),
        params={
            'grant_type': 'password',
            'username': username,
            'password': password,
        },
        headers={
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    )
    return handle_response(r, 'login')


def logout():
    logging.info('-- logging out --')
    r = requests.post(
        "http://qtest-dev.qtestnet.com/oauth/revoke",
        params={},
        headers={
            'Authorization': 'bearer {}'.format(access['access_token'])
        },
    )
    return handle_response(r, 'logout')


def get_projects(access):
    logging.info('-- requesting project data --')
    r = requests.get(
        "http://omronv.qtestnet.com/api/v3/projects",
        params={},
        headers={
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer {}'.format(access['access_token'])
        },
    )
    return handle_response(r, 'request project data')


def get_requirements(project):
    logging.info('-- requesting project data --')
    r = requests.get(
        "http://omronv.qtestnet.com/api/v3/projects/{}/requirements".format(project['id']),
        params={},
        headers={
            'Content-Type': 'application/json',
            'Authorization': 'bearer {}'.format(access['access_token'])
        },
    )
    return handle_response(r, 'get requirement data for project {}'.format(project['id']))


def get_releases(project):
    logging.info('-- requesting release data --')
    r = requests.get(
        "http://omronv.qtestnet.com/api/v3/projects/{}/releases".format(project['id']),
        params={},
        headers={
            'Content-Type': 'application/json',
            'Authorization': 'bearer {}'.format(access['access_token'])
        },
    )
    return handle_response(r, 'get release data for project {}'.format(project['id']))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    access = login(*CREDENTIALS['jeremy'])
    if access:
        projects = get_projects(access)
        for project in projects:
            print 'Project:', project['name']
            releases = get_requirements(project)
            for release in releases:
                print 'Release:', release['name']
            print '\n'
    logout()
