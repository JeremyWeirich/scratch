RESPONSE_CODES = {
    200: r"qTest is able to query or update the information of the requested resource successfully.",
    201: r"qTest is able to create a resource with submitted information successfully.",
    400: r"The submitted data is invalid to qTest. This is most likely due to wrong format, wrong passed in data type or unknown properties inside the data. In this case, correct the data before retrying.",
    401: r"The request authentication information is missing. This is most likely due to the API client has not logged in, or the HTTP header Authorization is missing.",
    402: r"Your qTest Edition does not support using API. If you wish to upgrade your qTest, please contact our Sales Representatives.",
    403: r"The requester account does not have proper permissions to make the request. In this case, grant necessary permissions to the account on qTest admin.",
    404: r"No resource identified by the requested URI could be found. This is most likely due to a wrong URI, or the resource has been deleted.",
    405: r"The data format is not supported by qTest. In this case, check the HTTP headers Content-Type and Accept-Type.",
    412: r"Pre-conditions have failed. The likeliest reason is some of the required properties are missing.",
    500: r"qTest has failed to process the request unexpectedly. This is likely caused by a qTest's internal issue.",
    503: r"The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay."
}