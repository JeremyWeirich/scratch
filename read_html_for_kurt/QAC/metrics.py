"""
Jeremy Weirich 4/20/17

Invocation:
Place this file in the same directory as the xml files to be parsed.
To invoke this script, use the following command line prompt:
$ python <filename>.py

Dependencies:
This file requires openpyxl, which is not part of the default python installation and must be installed manually.
To install, use the following command line prompt (may need admin privilege):
$ pip install openpyxl
"""

import glob
import os
from string import ascii_uppercase
from collections import OrderedDict
from xml.etree import ElementTree as ET
from openpyxl import Workbook
from openpyxl.styles import PatternFill

# define important file metrics (add/subtract if you want)
TARGET_FILE_METRICS = OrderedDict([
    ('STTPP', 'Lines of code analyzed'),
    ('STM28', 'Lines of code'),
    ('STM22', 'Total length'),
])

# define important function metrics (add/subtract if you want)
TARGET_FUNCTION_METRICS = OrderedDict([
    ['STPTH', dict(
        desc='Estimated static path count',
        valid_range=[1, 80],
    )],
    ['STGTO', dict(
        desc='Number of gotos',
        valid_range=[0, 0],
    )],
    ['STCYC', dict(
        desc='Cyclomatic complexity',
        valid_range=[1, 15],
    )],
    ['STCAL', dict(
        desc='Number of distinct function calls',
        valid_range=[0, 7],
    )],
    ['STPAR', dict(
        desc='Number of function parameters',
        valid_range=[0, 5],
    )],
    ['STST3', dict(
        desc='Number of statements in function (variant 3)',
        valid_range=[1, 50],
    )],
    ['STMIF', dict(
        desc='Maximum nesting of control structure',
        valid_range=[0, 4],
    )],
    ['STM19', dict(
        desc='Number of exit points',
        valid_range=[0, 1],
    )],
])

SUMMARY_COLUMN_WIDTHS = [40, 20, 20, 20, 20, 20]
FUNCTION_COLUMN_WIDTHS = [80, 8, 8, 8, 8, 8, 8, 8]

RED = 'FF0000'
BLUE = '7B9CD1'


# Parse the XML --------------------------------------------------------------------------------------------------------
def get_metrics_from_entity(file_entity, metrics_definitions):
    metrics = {}
    for Metric in file_entity:
        metric_name = Metric.attrib['name']
        if metric_name in metrics_definitions:
            metrics[metric_name] = int(Metric.attrib['value'])
    return metrics


def parse_xml(xml_file):
    with open(xml_file, 'r') as f:
        tree = ET.fromstring(f.read())
    function_metrics, file_metrics = {}, {}
    for File in tree:
        for Entity in File:
            entity_name = Entity.attrib['name']
            entity_type = Entity.attrib['type']
            if entity_type == 'function':
                if not entity_name.startswith('_'):  # filter out functions that start with __
                    function_metrics[entity_name] = get_metrics_from_entity(Entity, TARGET_FUNCTION_METRICS)
            elif entity_type == 'file':
                file_metrics = get_metrics_from_entity(Entity, TARGET_FILE_METRICS)
    return dict(
        file=file_metrics,
        function=function_metrics
    )


def get_combined_metrics(content_path):
    combined_metrics = {}
    print 'reading files in', content_path
    for filename in glob.glob('{}/*.xml'.format(content_path)):
        simple_name = filename.split('SWC_')[1].split('_BAC')[0]
        combined_metrics[simple_name] = parse_xml(filename)
    print 'files read'
    return combined_metrics


# Create the workbook --------------------------------------------------------------------------------------------------
def create_metrics_workbook(combined_metrics):
    def update_summary_data(summary_data):
        summary_data['files'] += 1
        summary_data['lines'] += metrics['file']['STTPP']
    def make_file_metrics(sheet, row, metrics, summary_data):
        for column, width in zip(ascii_uppercase, FUNCTION_COLUMN_WIDTHS):
            sheet.column_dimensions[column].width = width
        COMF = float(metrics['file']['STM28']) / metrics['file']['STM22']
        if COMF <= 0.2:
            summary_data['COMF']['failures'] += 1
        if COMF < summary_data['COMF']['min']:
            summary_data['COMF']['min'] = COMF
        if COMF > summary_data['COMF']['max']:
            summary_data['COMF']['max'] = COMF

        titles = ['File Metric Description', 'Metric', 'Value']
        for column, title in zip(ascii_uppercase, titles):
            sheet['{}1'.format(column)] = title
            sheet['{}1'.format(column)].fill = PatternFill('solid', fgColor=BLUE)
        for title in TARGET_FILE_METRICS:
            row += 1
            sheet['A{}'.format(row)] = TARGET_FILE_METRICS[title]
            sheet['B{}'.format(row)] = title
            sheet['C{}'.format(row)] = metrics['file'][title]
        return row

    def make_function_metrics(sheet, row, metrics, summary_data):
        # make title row
        row += 2
        sheet['A{}'.format(row)] = 'Function Name'
        sheet['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        for column, title in zip(ascii_uppercase[1:], TARGET_FUNCTION_METRICS):
            sheet['{}{}'.format(column, row)] = title
            sheet['{}{}'.format(column, row)].fill = PatternFill('solid', fgColor=BLUE)
        # enter data
        start = row
        failures = {key: 0 for key in TARGET_FUNCTION_METRICS}
        for function_name, metrics in metrics['function'].items():
            row += 1
            summary_data['functions'] += 1
            sheet['A{}'.format(row)] = function_name
            for column, metric_name in zip(ascii_uppercase[1:], TARGET_FUNCTION_METRICS):
                metric_value = metrics[metric_name]
                if metric_value > summary_data[metric_name]['max']:
                    summary_data[metric_name]['max'] = metric_value
                if metric_value < summary_data[metric_name]['min']:
                    summary_data[metric_name]['min'] = metric_value
                sheet['{}{}'.format(column, row)] = metric_value
                min, max = TARGET_FUNCTION_METRICS[metric_name]['valid_range']
                if not min <= metric_value <= max:
                    summary_data[metric_name]['failures'] += 1
                    failures[metric_name] += 1
                    sheet['{}{}'.format(column, row)].fill = PatternFill('solid', fgColor=RED)
        end = row
        return start, end, failures

    def make_function_summary(sheet, start, end, failures):
        row = end
        # metrics: MIN
        row += 2
        sheet['A{}'.format(row)] = 'MIN'
        sheet['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        for column in ascii_uppercase[1:len(TARGET_FUNCTION_METRICS) + 1]:
            sheet['{}{}'.format(column, row)] = '=MIN({0}{1}:{0}{2})'.format(column, start + 1, end)

        # metrics: MAX
        row += 1
        sheet['A{}'.format(row)] = 'MAX'
        sheet['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        for column in ascii_uppercase[1:len(TARGET_FUNCTION_METRICS) + 1]:
            sheet['{}{}'.format(column, row)] = '=MAX({0}{1}:{0}{2})'.format(column, start + 1, end)

        # metrics: FAILURES
        row += 1
        sheet['A{}'.format(row)] = 'FAILURES'
        sheet['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        for column, name in zip(ascii_uppercase[1:], TARGET_FUNCTION_METRICS):
            sheet['{}{}'.format(column, row)] = failures[name]

    def create_summary_data():
        summary_data = {'lines': 0, 'functions': 0, 'files': 0, 'density': 0}
        metrics = TARGET_FUNCTION_METRICS.keys() + ['COMF']
        for metric in metrics:
            summary_data[metric] = {'failures': 0, 'min': float('inf'), 'max': float('-inf'),}
        return summary_data

    def create_summary_page(summary, summary_data):
        for column, width in zip(ascii_uppercase, SUMMARY_COLUMN_WIDTHS):
            summary.column_dimensions[column].width = width
        titles = ['Metric', 'QAC denomination', 'Valid range', 'Minimum value',
                  'Maximum value', 'Range violations']
        for column, title in zip(ascii_uppercase, titles):
            summary['{}1'.format(column)] = title
            summary['{}1'.format(column)].fill = PatternFill('solid', fgColor=BLUE)
        COMF_properties = ['Code Density', 'STM28/STM22', '>0.2', summary_data['COMF']['min'],
                           summary_data['COMF']['max'], summary_data['COMF']['failures']]
        for column, COMF_property in zip(ascii_uppercase, COMF_properties):
            summary['{}2'.format(column)] = COMF_property
        row = 2
        for metric_name in TARGET_FUNCTION_METRICS:
            row += 1
            desc = TARGET_FUNCTION_METRICS[metric_name]['desc']
            lower, upper = TARGET_FUNCTION_METRICS[metric_name]['valid_range']
            range = '{}-{}'.format(lower, upper) if lower != upper else str(lower)
            min = summary_data[metric_name]['min']
            max = summary_data[metric_name]['max']
            failures = summary_data[metric_name]['failures']
            row_data = [desc, metric_name, range, min, max, failures]
            for column, column_data in zip(ascii_uppercase, row_data):
                summary['{}{}'.format(column, row)] = column_data
        row += 1
        # total lines
        row += 1
        summary['A{}'.format(row)] = 'Lines of code analyzed'
        summary['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        summary['B{}'.format(row)] = summary_data['lines']
        # total functions
        row += 1
        summary['A{}'.format(row)] = 'Total number of functions'
        summary['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        summary['B{}'.format(row)] = summary_data['functions']
        # total files
        row += 1
        summary['A{}'.format(row)] = 'Total number of files'
        summary['A{}'.format(row)].fill = PatternFill('solid', fgColor=BLUE)
        summary['B{}'.format(row)] = summary_data['files']

    print 'building workbook'
    wb = Workbook()
    summary = wb.active
    summary.title = 'Summary'
    summary_data = create_summary_data()
    for file, metrics in combined_metrics.items():
        sheet = wb.create_sheet(title=file)
        update_summary_data(summary_data)
        row = make_file_metrics(sheet, 1, metrics, summary_data)
        start, end, failures = make_function_metrics(sheet, row, metrics, summary_data)
        make_function_summary(sheet, start, end, failures)
    create_summary_page(wb['Summary'], summary_data)
    print 'saving workbook'
    wb.save('metrics.xlsx')
    print 'finished'


if __name__ == '__main__':
    current_path = os.path.dirname(os.path.abspath(__file__))
    combined_metrics = get_combined_metrics(current_path)
    create_metrics_workbook(combined_metrics)
