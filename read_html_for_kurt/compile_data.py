import glob

from read_xml import get_xml_data
from read_html import get_html_data

from openpyxl import Workbook
from openpyxl.styles import PatternFill
from string import ascii_uppercase

titles = [
    'function name',
    'lines',
    'STMIF',
    'STST3',
    'STM19',
    'STPTH',
    'STCAL',
    'STGTO',
    'STCYC',
    'STPAR',
]

acceptable_ranges = {
    'STMIF': [0, 4],
    'STST3': [1, 50],
    'STM19': [0, 1],
    'STPTH': [1, 80],
    'STCAL': [0, 7],
    'STGTO': [0, 0],
    'STCYC': [1, 15],
    'STPAR': [0, 5],
}

wb = Workbook()

files = {}
for filename in glob.glob(r'N:\Users\Jeremy Weirich\Python Projects\Scratch\read_html_for_kurt\QAC\*'):
    a = filename.split('SWC_')[1]
    file = a.split('_BAC')[0]
    if file not in files:
        files[file] = dict(
            html=None,
            xml=None,
        )
    if filename[-5:] == '.html':
        files[file]['html'] = filename
    elif filename[-4:] == '.xml':
        files[file]['xml'] = filename


for f, paths in files.items():
    print 'working on', f

    html_f_info = get_html_data(paths['html'])
    xml_f_info = get_xml_data(paths['xml']) # we'll combine data into here

    # add the small amount of html into the xml metrics
    for key, metrics in xml_f_info.items():
        metrics['lines'] = html_f_info[key]

    sheet = wb.create_sheet(f)
    sheet.title = f
    failures = {}
    final_line = 2
    for index, title in enumerate(titles):
        column = ascii_uppercase[index]
        sheet['{}1'.format(column)] = title
    for index, (function_name, metrics) in enumerate(xml_f_info.items()):
        final_line += 1
        sheet['A{}'.format(index + 2)] = function_name
        for column, metric_name in zip(ascii_uppercase[1:], titles[1:]):
            metric_value = int(metrics[metric_name])
            sheet['{}{}'.format(column, index + 2)] = metric_value
            if metric_name != 'lines':
                if metric_name not in failures:
                    failures[metric_name] = 0
                min, max = acceptable_ranges[metric_name]
                if not min <= metric_value <= max:
                    failures[metric_name] += 1
                    sheet['{}{}'.format(column, index + 2)].fill = PatternFill('solid', fgColor='FF0000')
    # metrics: SUM
    working = final_line
    sheet['A{}'.format(working)] = 'SUM'
    sheet['B{}'.format(working)] = '=SUM(B2:B{0})'.format(final_line-1)
    # metrics: MIN
    working += 1
    sheet['A{}'.format(working)] = 'MIN'
    for column in ascii_uppercase[1:len(titles)]:
        sheet['{}{}'.format(column, working)] = '=MIN({0}2:{0}{1})'.format(column, final_line-1)
    # metrics: MAX
    working += 1
    sheet['A{}'.format(working)] = 'MAX'
    for column in ascii_uppercase[1:len(titles)]:
        sheet['{}{}'.format(column, working)] = '=MAX({0}2:{0}{1})'.format(column, final_line - 1)
    # metrics: MAX
    working += 1
    sheet['A{}'.format(working)] = 'FAILURES'
    sheet['B{}'.format(working)] = 'NA'
    for column, name in zip(ascii_uppercase[2:], titles[2:]):
        sheet['{}{}'.format(column, working)] = failures[name]


wb.save('test.xlsx')