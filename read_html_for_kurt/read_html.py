import re

from bs4 import BeautifulSoup
f_header_data = re.compile(r'Function: ([a-zA-Z0-9_]+)[\s\S]*Line Count: (\d+)')


def get_html_data(filename):
    with open(filename, 'r') as f:
        tree = BeautifulSoup(f.read(), 'html.parser')

    functions = {}


    content = tree.find(id='content')
    function_div = content.find('div', 'functions')
    for i in function_div.find_all('h3'):
        f_header_text = i.text
        if not f_header_text.startswith('Function: __'):
            re_data = re.match(f_header_data, f_header_text)
            if re_data:
                name = re_data.group(1)
                lines = re_data.group(2)
                functions[name] = lines

    return functions


if __name__ == '__main__':
    get_html_data(r'N:\Users\Jeremy Weirich\Python Projects\Scratch\read_html_for_kurt\QAC\SWC_ZuZi_BAC_Workspace_Can_0x05_CRR_18042017_122931.html')