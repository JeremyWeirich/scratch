from xml.etree import ElementTree as ET

def get_xml_data(filename):
    with open(filename, 'r') as f:
        tree = ET.fromstring(f.read())

    functions = {}
    relevant_metrics = [
        'STM19',
        'STM28',
        'STM22',
        'STM29',
        'STPTH',
        'STGTO',
        'STCYC',
        'STCAL',
        'STPAR',
        'STST3',
        'STMIF',
    ]

    for File in tree:
        for Entity in File:
            if Entity.attrib['type'] == 'function':
                if not Entity.attrib['name'].startswith('_'):
                    function_data = {}
                    for Metric in Entity:
                        metric_name = Metric.attrib['name']
                        if metric_name in relevant_metrics:
                            function_data[metric_name] = Metric.attrib['value']
                    functions[Entity.attrib['name']] = function_data

    return functions


if __name__ == '__main__':
    pass