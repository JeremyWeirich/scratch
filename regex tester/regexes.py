import re

TIME_REGEX = r'\[(\d+(?:\.\d+)?(?:, \d+(?:\.\d+)?)*)\]'
DATA_REGEX = r'\[(?:-?(?:\d+(?:\.\d+)?|0x\d+)(?:, -?(?:\d+(?:\.\d+)?|0x\d+))*)\]'

VERIFY_REGEX = '^\[{}, {}\]$'.format(TIME_REGEX, DATA_REGEX)

VALID_STRINGS = [
    r'[[0], [1]]',
    r'[[0, 1], [1, 2]]',
    r'[[0, 0.1, 23], [1.3, 2, 3.2]]',
    r'[[0, 0.1, 23], [0x3, 2, 3.2]]',
    r'[[0, 0.1, 23], [0x324, 2, 3.2]]',
    r'[[0, 0.1, 23], [-1.3, 2, 3.2]]',
    r'[[0, 0.1, 23], [-0x3, 2, 3.2]]',
]

INVALID_STRINGS = [
    r'[[], []]',
    r'[[0], []]',
    r'[[], [1]]',
    r'[[0, 1], 1, 2]]',
    r'[[0, 1.], 1, 2]]',
    r'[[0, 1], 1, 2.]]',
    r'[[0, 0.1, 23, 1.3, 2, 3.2]]',
    r'[[0, -0.1, 23], [-1.3, 2, 3.2]]',
    r'[[0, 0x1, 23], [-0x3, 2, 3.2]]',
    r'[[0, 0x1.1, 23], [0x324, 2, 3.2]]',
    r'[[0, 0.1, 23], [0x3.2, 2, 3.2]]',

]

if __name__ == '__main__':
    verify_regex = re.compile(VERIFY_REGEX)
    for s in VALID_STRINGS:
        if not re.match(verify_regex, s):
            print "{} should match but doesn't".format(s)
    for s in INVALID_STRINGS:
        if re.match(verify_regex, s):
            print "{} shouldn't match but does".format(s)