from itertools import combinations, combinations_with_replacement

def adds(target, num_cells, max_size, duplicates=False):
    r = range(1, max_size + 1)
    if duplicates:
        c = combinations_with_replacement(r, num_cells)
    else:
        c = combinations(r, num_cells)
    return [i for i in c if sum(i) == target]

TARGET = 16
NUM = 4
SIZE = 6
DUPES = True

for i in sorted([i[::-1] for i in adds(TARGET, NUM, SIZE, duplicates=DUPES)])[::-1]:
    print i