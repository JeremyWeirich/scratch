class Importer(object):
    """Extract data from the sheets in the excel workbook using xlrd. Provides a data dictionary with
    sheet name as key and data in the sheet as value. This is the only place where the xlrd library
    should be used
    """
    def __init__(self):
        self.raw_workbook_data = {}
        self.wb = None

    def load_workbook_data(self, path, sheet_keywords=None, sheet_offsets=None, target_sheets=None):
        """Returns processed data dictionary, based on the sheet names, keyword / offset provided.
        @param path: file path to the excel workbook from which data is requested
        @param sheet_keywords: dict mapping sheet names to a keyword that will be searched to find offsets
        @param sheet_offsets: dict mapping sheet names to a tuple of horizontal and vertical offsets
        @param target_sheets: list of sheet names which need to be processed
        """
        import xlrd
        try:
            self.wb = xlrd.open_workbook(path)
        except:
            raise IOError("Invalid file path")
        sheets = self.wb.sheet_names()

        # Sentinel values
        sheet_keywords = {} if sheet_keywords is None else sheet_keywords
        sheet_offsets = {} if sheet_offsets is None else sheet_offsets
        target_sheets = [] if target_sheets is None else target_sheets

        if target_sheets:
            sheets = [sheet for sheet in sheets if sheet in target_sheets]  # filter by target_sheets

        if sheets:
            if sheet_keywords:
                if sheet_offsets:
                    raise TypeError("Provide either sheet_keywords or sheet_offsets")
                else:
                    sheet_offsets = self._get_offset_from_keyword(sheet_keywords)
            self._update_data_dictionary(sheets, sheet_offsets)
        else:
            raise KeyError("Requested sheets not found in document")

    def get_raw_workbook_data(self):
        return self.raw_workbook_data

    def _get_offset_from_keyword(self, sheet_keywords):
        """Given a dict of sheet names and keywords, return a dict of the horizontal and vertical offsets of each.
        @param sheet_keywords: dict mapping sheet names to a keyword that will be searched to find offsets
        @return sheet_offsets: dict mapping sheet names to a tuple of horizontal and vertical offsets
        """
        sheet_offsets = {}
        for sheet_name, target_word in sheet_keywords.items():
            sheet_data = self.wb.sheet_by_name(sheet_name)
            for row_index in range(sheet_data.nrows):
                for col_index in range(sheet_data.ncols):
                    if target_word == sheet_data.cell(row_index, col_index).value:
                        sheet_offsets[sheet_name] = (row_index + 1, col_index)  # to get data after the title row
        return sheet_offsets

    def _update_data_dictionary(self, sheets, sheet_offsets):
        """Iterate through workbook sheets and import raw data into workbook_data.
        @param sheets: sheets to import into self.workbook_data
        @param sheet_offsets: dict mapping sheet names to a tuple of horizontal and vertical offsets
        """
        for sheet_name in sheets:
            h_offset, v_offset = sheet_offsets.get(sheet_name, (0, 0))
            sheet = self.wb.sheet_by_name(sheet_name)
            rows = [[sheet.cell(row_index, col_index).value
                    for col_index in range(h_offset, sheet.ncols)]
                    for row_index in range(v_offset, sheet.nrows)]
            self.raw_workbook_data[sheet_name] = rows

