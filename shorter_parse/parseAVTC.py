import json
import os

from errorlist import ErrorList
from parsers import parsevariablepool
from parsers import parsetolerancepool
from parsers import parsetestdata


class Parser(object):
    """Receives the raw workbook dict from Importer and extracts information.  Creates test data, requirements,
    objectives, and tests.  Performs basic error checking on cell values, but does not compare between cells.
    """

    sheet_parsers = dict(
        VariablePool=parsevariablepool.VariablePoolParser,
        TolerancePool=parsetolerancepool.TolerancePoolParser,
        TestData=parsetestdata.TestDataParser,
    )

    def __init__(self, raw_workbook_data):
        self.raw_data = raw_workbook_data
        self.errors = ErrorList()
        self.json_data = {}

        self.sheet_order = ["VariablePool", "TolerancePool", "TestData"]
        self.raw_data = {(key if key in self.sheet_order else "TestData"): value
                         for key, value in self.raw_data.items()}  # Replace unknown sheet name with "TestData"

    def parse_test_data(self):
        for sheet_name in self.sheet_order:
            data_to_parse = {"sheet_data": self.raw_data[sheet_name]}
            sheet_parser = self.sheet_parsers[sheet_name]()
            if sheet_name == "TestData":
                data_to_parse["VariablePoolJSON"] = self.json_data["VariablePool"]
            sheet_parser.parse_sheet(data_to_parse)
            self.errors += sheet_parser.get_errors()
            self.json_data[sheet_name] = sheet_parser.get_json_data()

    def get_errors_found(self):
        return self.errors

    def save_json_data(self):
        data_relative_path = "serialize\serialized_test_data\AVTC.json"
        data_string = json.dumps(self.json_data)
        file_path = os.path.abspath(data_relative_path)
        with open(file_path, "w") as serialized_avtc:
            serialized_avtc.write(data_string)
        return file_path
