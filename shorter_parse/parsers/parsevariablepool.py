from errorlist import ErrorList


class VariablePoolParser(object):
    def __init__(self):
        self.sheet_data = []
        self.errors = ErrorList()
        self.json_data = {}

        self.errors.add(14, "vperror", "VariablePool")
        self.errors.add(4, "vperror2", "VariablePool")

    def parse_sheet(self, data_to_parse):
        self.sheet_data = data_to_parse["sheet_data"]

    def get_errors(self):
        return self.errors

    def get_json_data(self):
        return self.json_data
