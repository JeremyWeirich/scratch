import os
import pprint
from importer import Importer
from parseAVTC import Parser

if __name__ == "__main__":
    path = "C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM"
    document = "VTC-P14106001_BMW_CDM_HSK.xlsx"
    filepath = os.path.join(path, document)
    importer = Importer()
    importer.load_workbook_data(filepath, target_sheets=["VariablePool", "TolerancePool", "RR11 Auto CP3"])
    raw_data = importer.get_raw_workbook_data()

    # parser = Parser(raw_data)
    # parser.parse_test_data()
    # errors = parser.get_errors_found()
    # errors.print_errors()

