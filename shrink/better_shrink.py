import matplotlib.pyplot as plt

def shrink(results):  # TODO this function should probably live somewhere else
    shrunk = {}
    time = results.pop('time')
    for signal, data in results.items():

        signal_time, signal_data = [], []
        signal_time.append(time[0])
        signal_data.append(data[0])
        signal_time.append(time[-1])
        signal_data.append(data[-1])

        #if data[0] == data[1]:
        #    signal_time, signal_data = [time[0]], [data[0]]
        #else:
        #    signal_time, signal_data = [], []
        #already_captured = False
        for cur_time, cur_data, next_time, next_data in zip(time, data, time[1:], data[1:]):
            if already_captured:
                already_captured = False
                continue
            else:
                if cur_data != next_data:
                    signal_time.append(cur_time)
                    signal_time.append(next_time)
                    signal_data.append(cur_data)
                    signal_data.append(next_data)
                    already_captured = True
        if data[-1] == data[-2]:
            signal_time.append(time[-1])
            signal_data.append(data[-1])
        shrunk[signal] = (signal_time, signal_data)
    return shrunk

data = dict(
    time=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    A=   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    B=   [5, 5, 5, 2, 2, 2, 2, 5, 5, 5],
    C=   [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    D=   [4, 4, 4, 3, 3, 3, 4, 5, 5, 6],
)


def plot_before(a):
    plt.figure(1)
    plt.plot(a['time'], data['A'], 'r')
    plt.plot(a['time'], data['B'], 'k')
    plt.plot(a['time'], data['C'], 'g')
    plt.plot(a['time'], data['D'], 'g')


def plot_after(a):
    plt.figure(2)
    for signal in a.values():
        plt.plot(*signal)

if __name__ == '__main__':
    plot_before(data)
    after = shrink(data)
    plot_after(after)
    import pprint
    pprint.pprint(after)
    plt.show()
