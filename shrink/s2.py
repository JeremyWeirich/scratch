import matplotlib.pyplot as plt

def shrink(results):  # TODO this function should probably live somewhere else
    shrunk = {}
    time_vector = results.pop('time')
    for signal, data in results.items():
        first_match = True
        signal_time, signal_data = [], []
        for cur_time, cur_data, next_time, next_data in zip(time_vector, data, time_vector[1:], data[1:]):
            if cur_data == next_data and first_match:
                signal_time.append(cur_time)
                signal_data.append(cur_data)
                first_match = False
            elif cur_data != next_data:
                signal_time.append(cur_time)
                signal_data.append(cur_data)
                first_match = True
        signal_time.append(time_vector[-1])
        signal_data.append(data[-1])
        shrunk[signal] = (signal_time, signal_data)
    return shrunk

data = dict(
    time=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    A=   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    B=   [5, 5, 5, 2, 2, 2, 2, 5, 5, 5],
    C=   [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    D=   [6, 6, 6, 6, 6, 6, 6, 6, 7, 8],
    #     0  1  2  3  4  5  6  7  8  9
)


def plot_before(a):
    plt.figure(1)
    plt.plot(a['time'], data['A'])
    plt.plot(a['time'], data['B'])
    plt.plot(a['time'], data['C'])
    plt.plot(a['time'], data['D'])


def plot_after(a):
    plt.figure(2)
    for signal in a.values():
        plt.plot(*signal)

if __name__ == '__main__':
    plot_before(data)
    after = shrink(data)
    plot_after(after)
    import pprint
    pprint.pprint(after)
    plt.show()