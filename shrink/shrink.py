import matplotlib.pyplot as plt
import json

SMALL = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\shrink\results\AT25_141_1.txt'
LARGE = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\shrink\results\AT25_141_7.txt'

# with open(SMALL) as log:
#     verify = json.loads(log.read())['verify']
#     time = verify.pop('time')
#     print len(time)
#     for signal_name, data in verify.items():
#         graph_data = data['graph_data']
#         print signal_name, len(graph_data)
#         plt.plot(time, graph_data)
# plt.show()


def shrink(results):
    shrunk = {}
    time = results.pop('time')
    for signal, data in results.items():
        if data[0] == data[1]:
            signal_time, signal_data = [time[0]], [data[0]]
        else:
            signal_time, signal_data = [], []
        already_captured = False
        for cur_time, cur_data, next_time, next_data in zip(time, data, time[1:], data[1:]):
            if already_captured:
                already_captured = False
                continue
            else:
                if cur_data != next_data:
                    signal_time.append(cur_time)
                    signal_time.append(next_time)
                    signal_data.append(cur_data)
                    signal_data.append(next_data)
                    already_captured = True
        if signal_time[-1] != time[-1] and signal_data[-1] != data[-1]:
            signal_time.append(time[-1])
            signal_data.append(data[-1])
        shrunk[signal] = (signal_time, signal_data)
    return shrunk


if __name__ == '__main__':
    with open(LARGE) as log:
        verify = json.loads(log.read())['verify']
        time = verify.pop('time')
        results = {signal_name: data['graph_data'] for signal_name, data in verify.items()}
        results['time'] = time
    shrunk = shrink(results)
    for signal, data in shrunk.items():
        plt.plot(data[0], data[1])
    plt.show()
