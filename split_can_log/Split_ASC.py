import os

csv_file = "N:\Users\Andrei Vesa\DCMR.csv"
write_dir = "N:\Users\Andrei Vesa\Hello3"
if not os.path.isdir(write_dir):
    os.mkdir(write_dir)
os.chdir(write_dir)
title_string = "ELSDM_CAN_Log_File_sw_v_0.8.4_"
excerpt = None

def create_title(message_string_with_dashes):
    message_string = message_string_with_dashes.replace("_", "")  # should convert 00 11 22 33 44 55 66 77 to 0011223344556677
    req_str = message_string[0:8]
    obj_str = message_string[8:16]

    req_num = int(req_str)
    obj_num = int(obj_str)

    return "{}AT{}_{}.asc".format(title_string, req_num, obj_num)

with open(csv_file) as log:
    for line in log.readlines()[4:-1]:

        #change theses lines so that you still get can_id as a string like this: "302"
        #and data as a string like this: "00 32 33 22 44 65 44 33 22"
        row = line.split(",")
        can_id = row[4]
        data = row[7]

        if can_id == "0":
            if excerpt:
                excerpt.close()
                print "Closing {}".format(title)
            title = create_title(data)
            print "Writing lines to {}".format(title)
            excerpt = open(title, "w")
        else:
            excerpt.write(line)





