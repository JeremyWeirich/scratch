def filtered(message_list, filter_ids=None, filter_dirs=None):
    filtered_list = []
    for msg in message_list:
        info = msg.split()
        if filter_ids:
            if filter_dirs:
                filtered_list = message_list
            else:
                for eachdir in filter_dirs:
                    if eachdir == info[3]:
                        filtered_list.append(msg)
        else:
            for passedinid in filter_ids:
                if passedinid == info[2]:
                    if filter_dirs == []:
                        filtered_list.append(msg)
                    else:
                        for eachdir in filter_dirs:
                            if eachdir == info[3]:
                                filtered_list.append(msg)
    print (filtered_list)


def convert_message_to_dict(linename):
    info = linename.split()
    # before
    # table = {}
    # table['Time'],table['Channel'],table['Id'],table['Dir'],table['Data'] = info[0],info[1], info[2], info[3], info[4:]
    # after (you can make a dictionary in a cleaner way like this)
    table = {
        'Time': info[0],
        'Channel': info[1],
        'Id': info[2],
        'Dir': info[3],
        'Data': info[4:],
    }
    return (table)

def get_message_data(filename):
    firstcharacter = False
    with open(filename) as f:
        update = []
        for entireline in f.readlines():
            entireline = entireline[:-1]

            entireline = entireline.replace(',',' ')

            for line in entireline:
                if line == 'T':
                    firstcharacter = True
                elif line == 'R':
                    firstcharacter = True
                elif firstcharacter == True:
                    if line == 'x':
                        update.append(entireline)


                    else:
                        firstcharacter = False
                    #nothing
    idlist = ['B5','1A1']
    directionlist = ['Rx','Tx']
    filtered(update, idlist, filter_dirs= directionlist)
    return update



get_message_data("CAN.asc")
