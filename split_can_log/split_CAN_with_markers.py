import os
import re

# Stuff to edit -------------------------------------------------------------------------------------------
log_file = r"N:\Users\Jeremy Weirich\Python Projects\Scratch\split_can_log\DCMR.csv"
write_dir = r"N:\Users\Jeremy Weirich\Python Projects\Scratch\split_can_log\\split_data"
title_string = r"ELSDM_CAN_Log_File_sw_v_0.11.4_"
# Stuff to edit -------------------------------------------------------------------------------------------

if not os.path.isdir(write_dir):
    os.mkdir(write_dir)
os.chdir(write_dir)

excerpt = None
regex = None
startline = None

ext = log_file[-4:]
if ext == ".asc":
    regex = re.compile(ur' 2 ([\da-fA-F]{1,3}) [TR]x d 8 (([\da-fA-F]{2} ?){8})')
    startline = 5
elif ext == ".csv":
    regex = re.compile(ur',2,(\d{1,4}),[0-1],8,(([\da-fA-F]{2}-?){8})')
    startline = 4
else:
    print "Invalid log file format{}".format(ext)
    quit()


def create_title(message_string_with_dashes):
    message_string = message_string_with_dashes.replace("-", "")
    req_str = message_string[0:8]
    obj_str = message_string[8:16]

    req_num = int(req_str)
    obj_num = int(obj_str)

    return "{}AT{}_{}{}".format(title_string, req_num, obj_num, ext)

with open(log_file) as log:
    for line in log.readlines()[startline:-1]:

        can_id, data, _ = re.findall(regex, line)[0] # third capturing group is the last pair, for some reason
        data = data.replace(" ", "")
        data = data.replace("-", "")

        if can_id == "0":
            if excerpt:
                excerpt.close()
                print "Closing {}".format(title)
            title = create_title(data)
            print "Writing lines to {}".format(title)
            excerpt = open(title, "w")
        else:
            excerpt.write(line)
