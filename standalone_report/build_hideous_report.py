import json
import shutil
import os
from time import time
from tqdm import tqdm

from generate_html.index import index_open, index_statistics, index_start_requirements, link_requirement_to_index, index_close
from generate_html.requirement import link_objective_to_requirement, requirement_open, requirement_start_objectives, requirement_description, requirement_explanation, requirement_spec_ref, requirement_close
from generate_html.objective import objective_open, objective_description, objectives_start_tests, link_test_to_objective, objective_close
from generate_html.test import test_open, test_close, test_description, test_write, test_verify

from form import TestingSummary, Requirement, Objective, Test

BASEDIR = os.path.dirname(__file__)

RESULTS = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\bokeh_plot\shrunk_results'
# RESULTS = os.path.join(BASEDIR, 'results')
OUTPUT = os.path.join(BASEDIR, 'output')
RESOURCE = os.path.join(OUTPUT, 'RESOURCE')
CSS = os.path.join(BASEDIR, 'css')


def create_path(path):
    try:
        shutil.rmtree(path)
    except WindowsError:
        pass
    if not os.path.exists(path):
        os.makedirs(path)


def add_css():
    for _, _, files in os.walk(CSS):
        for file in files:
            file_path = os.path.join(CSS, file)
            with open(file_path, 'r') as f:
                with open(os.path.join(RESOURCE, file), 'w') as new:
                    new.write(f.read())


def get_time_sorted_filenames(p):
    name_list = os.listdir(p)
    name_list = [i for i in name_list if i.startswith('AT')]
    full_list = [os.path.join(p, i) for i in name_list]
    time_sorted_list = sorted(full_list, key=os.path.getmtime)
    return [os.path.basename(i)[:-4] for i in time_sorted_list]


def summarize(requirements):
    summary = TestingSummary()
    for r in requirements:
        summary.update(r)
        for o in r:
            summary.update(o)
            for t in o:
                summary.update(t)

    return summary


def get_at(name):
    requirement, objective_only, test_only = name.split('_')
    test = '{}_{}_{}'.format(requirement, objective_only, test_only)
    objective = '{}_{}'.format(requirement, objective_only)
    return requirement, objective, test


def get_path_and_file(*paths):
    p = os.path.join(*paths)
    f = '{}.html'.format(p)
    return p, f


def read_files():
    report_name = 'Placeholder Report Name'
    report_description = 'Placeholder Report Description'
    requirements = []

    previous_requirement = None
    previous_objective = None
    previous_test = None

    print 'getting data'

    for filename in get_time_sorted_filenames(RESULTS):
        with open(os.path.join(RESULTS, '{}.txt'.format(filename))) as f:
            data = json.loads(f.read())
            if filename.count('_') == 0:
                current_requirement = Requirement(filename, data)
                requirements.append(current_requirement)
                if previous_requirement:
                    previous_requirement.next_at = current_requirement.at
                    current_requirement.prev_at = previous_requirement.at
                previous_objective = None
                previous_requirement = current_requirement
            elif filename.count('_') == 1:
                current_objective = Objective(filename, data)
                current_requirement.add_objective(current_objective)
                if previous_objective:
                    previous_objective.next_at = current_objective.at
                    current_objective.prev_at = previous_objective.at
                previous_test = None
                previous_objective = current_objective
            elif filename.count('_') == 2:
                current_test = Test(filename, data)
                current_objective.add_test(current_test)
                current_objective.status &= current_test.status
                current_requirement.status &= current_test.status
                if previous_test:
                    previous_test.next_at = current_test.at
                    current_test.prev_at = previous_test.at
                previous_test = current_test
            else:
                pass  # Ignore any other files in this folder

    print 'making report'
    index_path = os.path.join(OUTPUT, '{}.html'.format(report_name))
    index_open(index_path, report_name, report_description)
    index_statistics(index_path, summarize(requirements))
    index_start_requirements(index_path)
    for r in tqdm(requirements):
        link_requirement_to_index(index_path, r)
        r_path, r_file = get_path_and_file(OUTPUT, 'RESOURCE', r.at)
        requirement_open(r_file, report_name, r)
        requirement_description(r_file, r.description)
        requirement_explanation(r_file, r.explanation)
        requirement_spec_ref(r_file, r.spec_ref)
        requirement_start_objectives(r_file)
        create_path(r_path)
        for o in r:
            link_objective_to_requirement(r_file, r, o)
            o_path, o_file = get_path_and_file(r_path, o.at)
            objective_open(o_file, report_name, r.at, o)
            objective_description(o_file, o.description)
            objectives_start_tests(o_file)
            create_path(o_path)
            for t in o:
                link_test_to_objective(o_file, o, t)
                _, t_file = get_path_and_file(o_path, t.at)
                test_open(t_file, report_name, r.at, o.at, t)
                test_description(t_file, t.description)
                test_write(t_file, t.write)
                test_verify(t_file, t.verify)
                test_close(t_file)
            objective_close(o_file)
        requirement_close(r_file)
    index_close(index_path)


def main():
    start = time()
    print 'making path'
    create_path(RESOURCE)
    print 'adding css'
    add_css()
    print 'reading files'
    read_files()
    print 'time = {}'.format(time() - start)

if __name__ == '__main__':
    main()
