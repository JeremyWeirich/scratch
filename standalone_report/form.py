class TestingSummary(object):
    def __init__(self):
        #  regular/asil
        #  requirement/objective/test
        self.count = [[0, 0, 0], [0, 0, 0]]
        self.status = [[0, 0, 0], [0, 0, 0]]
        self.failed = [[0, 0, 0], [0, 0, 0]]

    def update(self, form):
        if isinstance(form, Requirement):
            level = 0
        elif isinstance(form, Objective):
            level = 1
        else:
            level = 2
        if form.asil:
            safety = 1
        else:
            safety = 0
        self.count[safety][level] += 1
        if form.status:
            self.status[safety][level] += 1
        else:
            self.failed[safety][level] += 1

    def as_lists(self):
        return [self.count[0] + self.count[1], self.status[0] + self.status[1], self.failed[0] + self.failed[1]]


class Requirement(object):
    def __init__(self, filename, data):
        self.at = filename
        self.objectives = []
        self.status = True
        self.description = data['description'].encode('utf-8')
        self.explanation = data['explanation'].encode('utf-8')
        self.spec_ref = data['spec_ref'].encode('utf-8')
        self.asil = data['asil']
        self.prev_at = None
        self.next_at = None

    def add_objective(self, objective):
        self.objectives.append(objective)

    def __iter__(self):
        return iter(self.objectives)


class Objective(object):
    def __init__(self, filename, data):
        self.at = filename
        self.tests = []
        self.status = True
        self.description = data['description'].encode('utf-8')
        self.asil = data['asil']
        self.prev_at = None
        self.next_at = None

    def add_test(self, test):
        self.tests.append(test)

    def __iter__(self):
        return iter(self.tests)


class Test(object):
    def __init__(self, filename, data):
        self.at = filename
        self.write = data['write']
        self.verify = data['verify']
        self.description = data['description'].encode('utf-8')
        self.asil = data['asil']
        self.status = True
        self.prev_at = None
        self.next_at = None

        self._set_pass_status()

    def _set_pass_status(self):
        for signal in self.verify.values():
            self.status &= signal['pass']
