import datetime


def _safe(at):
    return '{}.html'.format(at) if at else '#" class="invalid'


def requirement_open(requirement_path, report_name, requirement):
    time_string = datetime.datetime.now().strftime('Executed %H:%M %m/%d/%y')
    with open(requirement_path, 'w') as f:
        f.write("""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{0}</title>
<link rel="stylesheet" type="text/css" href="../../css/report.css">
</head>
<body>
<div class="title">
<ul class="time">
<li>{2}</li>
</ul>
<ul>
<li><a href="../{0}.html">{0}</a></li>
<li><a href="#">{1}</a></li>
</ul>
</div>
<div class="AT_header">
<ul class="navigation">
<li><a href="{3}">Previous</a></li>
<li><a href="{4}">Next</a></li>
</ul>
<h1>Requirement {1}</h1>
</div>
""".format(report_name, requirement.at, time_string, _safe(requirement.prev_at), _safe(requirement.next_at)))


def requirement_description(requirement_path, description):
    with open(requirement_path, 'a') as f:
        f.write("""<div class="description">
<h3>Description</h3>
<p>{0}</p>
</div>
""".format(description))


def requirement_explanation(requirement_path, explanation):
    with open(requirement_path, 'a') as f:
        f.write("""<div class="explanation">
<h3>Explanation</h3>
<p>{0}</p>
</div>
""".format(explanation))


def requirement_spec_ref(requirement_path, spec_ref):
    with open(requirement_path, 'a') as f:
        f.write("""<div class="explanation">
<h3>Spec Ref</h3>
<p>{0}</p>
</div>
""".format(spec_ref))


def requirement_start_objectives(requirement_path):
    with open(requirement_path, 'a') as f:
        f.write("""<div class="listing">
<h3>Objectives</h3>
<p>Click an objective below for detailed information</p>
<table>
<tr>
<th class="AT">AT Number</th>
<th class="ASIL">ASIL</th>
<th class="status">Status</th>
</tr>
""")


def link_objective_to_requirement(requirement_path, r, o):
    asil = 'A' if o.asil else 'False'
    status = 'Pass' if o.status else 'Fail'
    with open(requirement_path, 'a') as f:
        f.write("""<tr onclick="document.location = '{3}/{0}.html';">
<td class="AT">{0}</td>
<td class="asil_{1}">{1}</td>
<td class="passed_{2}">{2}</td>
</tr>
""".format(o.at, asil, status, r.at))


def requirement_close(requirement_path):
    with open(requirement_path, 'a') as f:
        f.write("""</table>
</div>
</body>
</html>
""")