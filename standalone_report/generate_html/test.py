import datetime

from bokeh.plotting import figure
from bokeh.models import HoverTool, PanTool, ResizeTool, SaveTool, BoxZoomTool, ResetTool
from bokeh.embed import components


def _safe(at):
    return '{}.html'.format(at) if at else '#" class="invalid'


def test_open(test_path, report_name, requirement, objective, test):
    time_string = datetime.datetime.now().strftime('Executed %H:%M %m/%d/%y')
    with open(test_path, 'w') as f:
        f.write("""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{0}</title>
<link rel="stylesheet" type="text/css" href="../../../../css/report.css">
<link rel="stylesheet" type="text/css" href="http://cdn.pydata.org/bokeh/release/bokeh-0.12.3.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.3.min.css">
<script src="http://cdn.pydata.org/bokeh/release/bokeh-0.12.3.min.js"></script>
<script src="http://cdn.pydata.org/bokeh/release/bokeh-widgets-0.12.3.min.js"></script>
</head>
<body>
<div class="title">
<ul class="time">
<li>{4}</li>
</ul>
<ul>
<li><a href="../../../{0}.html">{0}</a></li>
<li><a href="../../{1}.html">{1}</a></li>
<li><a href="../{2}.html">{2}</a></li>
<li><a href="#">{3}</a></li>
</ul>
</div>
<div class="AT_header">
<ul class="navigation">
<li><a href="{5}">Previous</a></li>
<li><a href="{6}">Next</a></li>
</ul>
<h1>Test {3}</h1>
</div>
<div class="data">
""".format(report_name, requirement, objective, test.at, time_string, _safe(test.prev_at), _safe(test.next_at)))


def test_description(test_path, description):
    with open(test_path, 'a') as f:
        f.write("""<div class="description">
<h3>Description:</h3>
<p>{0}</p>
</div>
""".format(description))


def test_write(test_path, write):
    with open(test_path, 'a') as f:
        f.write("""<div class="write">
<h3>Write Data</h3>
<table>
<tr>
<th>Write Variable</th>
<th>Value</th>
</tr>
""")
        for signal in write.values():
            f.write("""<tr>
<td>{0}</td>
<td>{1}</td>
</tr>
""".format(signal['name'], signal['value']))

        f.write("""</table>
</div>
""")


def make_bokeh_graph(signal_name, signal):
    operator = signal['operator']
    expected = signal['expected']

    hover = HoverTool(tooltips=[('Time', '$x'), ('Value', '$y')])

    tools = [hover, PanTool(), ResizeTool(), SaveTool(), BoxZoomTool(), ResetTool()]

    p = figure(title=signal_name, plot_height=300, plot_width=1600, responsive=False, tools=tools)
    p.line(*signal['graph_data'])
    for expected_time, expected_value in zip(expected[0], expected[1]):
        if operator in ['>=', '>']:
            fill_alpha = 0 if operator == '>' else 1
            p.triangle(expected_time, expected_value, fill_alpha=fill_alpha, color='red', size=8)
        elif operator in ['<=', '<']:
            fill_alpha = 0 if operator == '<' else 1
            p.inverted_triangle(expected_time, expected_value, fill_alpha=fill_alpha, color='red', size=8)
        elif operator in ['!=']:
            p.x(expected_time, expected_value, color='red', size=8)
        else:
            g = p.circle(expected_time, expected_value, color='red', size=8)
            p.tools[0].renderers.append(g)

    errs = [i * signal['tolerance'] for i in expected[1]]
    y_err_x = []
    y_err_y = []
    for px, py, err in zip(expected[0], expected[1], errs):
        y_err_x.append((px, px))
        y_err_y.append((py - err, py + err))
        p.multi_line(y_err_x, y_err_y, color='black')

    return components(p)


def test_verify(test_path, verify):
    with open(test_path, 'a') as f:
        f.write("""<div class="verify">
<h3>Verify Data</h3>
""")
        for name, values in verify.items():
            measured = values['measured']
            expected = values['expected']
            operator = values['operator']
            tolerance = '{:.1%}'.format(values['tolerance'])
            if isinstance(values['expected'], list):  # stimulus
                status = 'Pass' if all([measurement[3] for measurement in measured]) else 'Fail'
                f.write("""<div class="signal">
<h4 class=header_{1}>{0}</h4>
<label class="collapse" for="{0}_collapse">Show Details</label>
<input id="{0}_collapse" type="checkbox">
<div>
<table>
<tr>
<th>Time</th>
<th>Measured</th>
<th>Operator</th>
<th>Expected</th>
<th>Upper</th>
<th>Lower</th>
<th>Tolerance</th>
<th>Status</th>
</tr>
""".format(name, status))
                for (measured_value, upper, lower, status, time), expected_value in zip(measured, expected[1]):
                    status = 'Pass' if status else 'Fail'
                    f.write("""<tr>
<td>{0}</td>
<td>{1}</td>
<td>{2}</td>
<td>{3}</td>
<td>{4}</td>
<td>{5}</td>
<td>{6}</td>
<td class={7}>{7}</td>
</tr>
""".format(time, measured_value, operator, expected_value, upper, lower, tolerance, status))
                f.write("""</table>
</div>
""")

                script, div = make_bokeh_graph(name, values)
                f.write("""<div class="graph">""")
                f.write(div)
                f.write('\n')
                f.write(script)
                f.write('\n')
                f.write("""</div>
</div>
""")

            else:
                f.write("""<h4>{0}</h4>
<table>
<tr>
<th>Measured</th>
<th>Operator</th>
<th>Expected</th>
<th>Upper</th>
<th>Lower</th>
<th>Tolerance</th>
<th>Status</th>
</tr>
""".format(name))

                measured_value, upper, lower, status = measured
                status = 'Pass' if status else 'Fail'
                f.write("""<tr>
<td>{0}</td>
<td>{1}</td>
<td>{2}</td>
<td>{3}</td>
<td>{4}</td>
<td>{5}</td>
<td class={6}>{6}</td>
</tr>
""".format(measured_value, operator, expected, upper, lower, tolerance, status))

                f.write('</table>\n')


def test_close(test_path):
    with open(test_path, 'a') as f:
        f.write("""</div>
</div>
</body>
</html>
""")
