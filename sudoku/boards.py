BOARD_1 = [
    [5   , None, None,    1   , None, None,    None, None, None], #1
    [None, 9   , 6   ,    None, None, None,    8   , 2   , None], #2
    [None, None, None,    None, None, 7   ,    None, None, 9   ], #3

    [None, None, None,    None, None, 3   ,    None, None, 6   ], #4
    [None, 7   , 4   ,    None, None, None,    9   , 1   , None], #5
    [2   , None, None,    5   , None, None,    None, None, None], #6

    [7   , None, None,    6   , None, None,    None, None, None], #7
    [None, 8   , 3   ,    None, None, None,    5   , 7   , None], #8
    [None, None, None,    None, None, 4   ,    None, None, 1   ], #9
]

if __name__ == '__main__':
    for column_i, row in enumerate(BOARD_1):
        for row_i, value in enumerate(row):
            print row_i, column_i, value