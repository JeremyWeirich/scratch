def r(a):
    i=a.find('0')
    ~i or exit(a)
    numbers = '3814697265625'
    first = [(i-j) % 9 * (i / 9 ^ j / 9) * (i / 27 ^ j / 27 | i % 9 / 3 ^ j % 9 / 3) or a[j] for j in range(81)]
    [m in first or r(a[:i]+m+a[i+1:]) for m in numbers]


puzzle = "807000003602080000000200900040005001000798000200100070004003000000040108300000506"

print r(puzzle)