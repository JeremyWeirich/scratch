from boards import BOARD_1
from utils import NINE
import random

import copy


class InvalidSolution(Exception):
    """Solution is invalid"""


class Cell(object):
    """A single cell of a sudoku puzzle"""
    def __init__(self, board, row_index, column_index, value=None):
        self.row_index = row_index
        self.column_index = column_index
        self.value = {value} if value else set(NINE)
        self.sector = self.determine_sector(row_index, column_index)
        self.groups = []
        self.board = board

    def __repr__(self):
        return 'r={}, c={}, v={}'.format(self.row_index, self.column_index, self.value)

    @property
    def solved(self):
        return len(self.value) == 1

    @property
    def coords(self):
        return self.row_index, self.column_index

    def remove(self, to_remove):
        self.value = self.value.difference(to_remove)

    def determine_sector(self, row_index, column_index):
        sectors = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
        sector_row = (row_index - 1) / 3
        sector_column = (column_index - 1) / 3
        return sectors[sector_row][sector_column]

    def solve(self):
        for group in self.groups:
            group.reduce()


def solved(cells):
    for cell in cells:
        if cell.solved:
            yield cell

def unsolved(cells):
    for cell in cells:
        if not cell.solved:
            yield cell

class Group(object):
    """A row, column, or sector"""
    def __init__(self, category):
        # 1 2 3
        # 4 5 6
        # 7 8 9
        self.cells = []
        self.category = category

    def reduce(self):
        for solved_cell in solved(self.cells):
            for unsolved_cell in unsolved(self.cells):
                unsolved_cell.remove(solved_cell.value)


class Board(object):
    """A full Sudoku puzzle"""
    def __init__(self, board):
        self.cells = self.build_cells(board)
        self.assign_groups()

    def build_cells(self, board):
        cells = []
        for row_index, row in enumerate(board):
            for column_index, value in enumerate(row):
                cells.append(Cell(self, row_index + 1, column_index + 1, value))
        return cells

    def assign_groups(self):
        for row in NINE:
            g = Group('row')
            g.cells = self.by_row(row)
            for cell in self.by_row(row):
                cell.groups.append(g)
        for column in NINE:
            g = Group('column')
            g.cells = self.by_column(column)
            for cell in self.by_column(column):
                cell.groups.append(g)
        for sector in NINE:
            g = Group('sector')
            g.cells = self.by_sector(sector)
            for cell in self.by_sector(sector):
                cell.groups.append(g)

    def by_row(self, row_index):
        return [cell for cell in self.cells if cell.row_index == row_index]

    def by_column(self, column_index):
        return [cell for cell in self.cells if cell.column_index == column_index]

    def by_sector(self, sector):
        return [cell for cell in self.cells if cell.sector == sector]

    @property
    def solved(self):
        for cell in self.cells:
            if not cell.solved:
                return False
        return True

    @property
    def solved_count(self):
        count = 0
        for cell in self.cells:
            if cell.solved:
                count += 1
        return count

    def solve(self):
        for cell in self.cells:
            cell.solve()

    def get_cell(self, r, c):
        for cell in self.cells:
            if cell.row_index == r and cell.column_index == c:
                return cell

    def display(self):
        rows = []
        for r in NINE:
            row = []
            for c in NINE:
                row.append(str(list(self.get_cell(r, c).value)))
            rows.append(row)
        for i, row in enumerate(rows):
            output = ''
            for element in row:
                output += element
                output += ' '*(20 - len(element))
            print output
            if i in [2, 5]:
                print ''

    def guess(self):
        for cell in unsolved(self.cells):
            possible_values = cell.value
            for value in possible_values:
                cell.value = {value}
                cell.solve()
                if self.invalid():
                    possible_values = possible_values.difference({value})
                    cell.value = possible_values

    def invalid(self):
        for cell in solved(self.cells):
            for group in cell.groups:
                for group_cell in solved(group.cells):
                    if cell.value == group_cell.value:
                        return True
        return False


if __name__ == '__main__':
    b = Board(BOARD_1)
    b.solve()
    print '\n\n{} solved after initial round'.format(b.solved_count)
    backup = copy.deepcopy(b)
    guesses = 0
    while not b.solved and guesses < 10:
        guesses += 1
        b.guess()
        print '{} solved after guess {}'.format(b.solved_count, guesses)

    b.display()
