import requests
import keyring
import pprint

username = 'jeremy.weirich@omron.com'
password = keyring.get_password('qTest', 'jeremy.weirich@omron.com')

authenticate_request = requests.post(
    "http://omronv.qtestnet.com/oauth/token",
    auth=(username, ''),
    params={
        'grant_type': 'password',
        'username': username,
        'password': password,
    },
    headers={
        'Content-Type': 'application/x-www-form-urlencoded',
    },
)

pprint.pprint(authenticate_request.json())
print '\n' * 5
token = authenticate_request.json()['access_token']

data_request = requests.get(
    r'http://omronv.qtestnet.com/api/v3/projects',
    params={},
    headers={
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer {}'.format(token)
    },
)

pprint.pprint(data_request.json())
