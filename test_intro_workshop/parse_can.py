def convert_asc_to_dict(line):
    split = line.strip().split(' ')
    return {
        'id': split[2],
        'dir': split[3],
        'data': split[6:],
    }


def convert_csv_to_dict(line):
    split = line.split(',')
    return {
        'id': hex(int(split[4]))[2:].upper(),    # convert csv id dec to hex
        'dir': 'Tx' if int(split[5]) else 'Rx',  # convert csv dir 0/1 to Rx/Tx
        'data': split[7].split('-'),             # convert csv data str to list
    }

HANDLERS = {  # create an entry for each file extension that can be handled
    'asc': {'parser': convert_asc_to_dict, 'start': 5, 'stop': -1},  # need a line-to-dict converter and # lines to skip at start/end
    'csv': {'parser': convert_csv_to_dict,'start': 4, 'stop': -1},
}


def get_message_data(filename):
    ext = filename.split('.')[-1]  # get the file extension
    h = HANDLERS[ext]  # use extension to get handler
    with open(filename, 'r') as f:
        return [h['parser'](line) for line in f.readlines()[h['start']:h['stop']]]  # using handler to get data and start/stop


def filtered(messages, filter_id=None, filter_dir=None):
    filt = []
    for message in messages:
        if filter_id and message['id'] not in filter_id:  # short circuit if no filter_id
            continue  # skip line
        if filter_dir and message['dir'] not in filter_dir:  # short circuit if no dir
            continue  # skip line
        filt.append(message)
    return filt

if __name__ == '__main__':
    all_messages = get_message_data('test_CAN.asc')
    filtered_messages = filtered(all_messages, filter_dir='Tx', filter_id=['2B2', '26E'])
    print filtered_messages


