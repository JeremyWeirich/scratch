import unittest
# This is a file that will test the get_message_data() and convert_message_to_dict() functions
# Change the name XXX below to the name of your python file
from XXX import get_message_data, convert_message_to_dict
# Then, run this file


class TestConvertMessageToDict(unittest.TestCase):
    def test_short_rx_message(self):
        line = '4787.965499 1 C0 Rx d 2 F4 80\n'
        d = convert_message_to_dict(line)
        target = dict(
            id='C0',
            dir='Rx',
            data=['F4', '80'],
        )
        self.assertDictEqual(d, target)

    def test_short_tx_message(self):
        line = '4788.002675 1 2CA Tx d 2 78 00\n'
        d = convert_message_to_dict(line)
        target = dict(
            id='2CA',
            dir='Tx',
            data=['78', '00'],
        )
        self.assertDictEqual(d, target)

    def test_long_rx_message(self):
        line = '4788.056500 1 6E Rx d 8 00 9F 0F A0 0F 00 00 FF\n'
        d = convert_message_to_dict(line)
        target = dict(
            id='6E',
            dir='Rx',
            data=['00', '9F', '0F', 'A0', '0F', '00', '00', 'FF'],
        )
        self.assertDictEqual(d, target)

    def test_long_tx_message(self):
        line = '4787.940029 1 B5 Tx d 8 00 00 40 00 00 00 00 00\n'
        d = convert_message_to_dict(line)
        target = dict(
            id='B5',
            dir='Tx',
            data=['00', '00', '40', '00', '00', '00', '00', '00'],
        )
        self.assertDictEqual(d, target)


class TestGetMessageData(unittest.TestCase):
    def test_count_asc(self):
        messages = get_message_data('test_CAN.asc')
        self.assertEqual(len(messages), 1995)
        self.assertIsInstance(messages, list)
        for message in messages:
            self.assertIsInstance(message, dict)
            for key in ['id', 'data', 'dir']:
                self.assertIn(key, message)

if __name__ == '__main__':
    unittest.main()
