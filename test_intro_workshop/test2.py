import unittest
# This is a file that will test the filter() function
# Change the name XXX below to the name of your python file
from XXX import filtered
# Then, run this file


class TestFiltered(unittest.TestCase):
    def setUp(self):
        self.message_list = [
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'}, 
            {'data': ['A5', '0B', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'}, 
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'}, 
            {'data': ['51', '0C', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'}, 
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'}, 
            {'data': ['CC', '0D', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'}, 
            {'data': ['F4', '80'], 'id': 'C0', 'dir': 'Rx'},
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'},
            {'data': ['76', '0E', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'},
            {'data': ['FB', 'F0', '00', '00', '00', 'FC', 'FF', 'FF'], 'id': '32', 'dir': 'Tx'},
            {'data': ['97', '00', '00', '00', '00', '00', 'E5', 'FF'], 'id': '3C', 'dir': 'Tx'},
            {'data': ['59', 'F0', '10', '00', '00', '00', 'FF', 'FF'], 'id': 'AB', 'dir': 'Tx'},
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'},
            {'data': ['BF', 'F0', '00', '25', '00', '25', '00', 'F0'], 'id': '163', 'dir': 'Tx'},
            {'data': ['83', '00', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'},
            {'data': ['64', 'E0', 'F0', 'FF', 'FF', 'FF', 'FF', 'FF'], 'id': '206', 'dir': 'Tx'},
            {'data': ['00', '00', '00', '00', '00', '00', '00', '00'], 'id': '23A', 'dir': 'Tx'},
            {'data': ['00', '00', '00', '00', '00', '00', '00', '00'], 'id': '26E', 'dir': 'Tx'},
            {'data': ['00', '00', '00', '00', '00', '00', '00', '00'], 'id': '2A0', 'dir': 'Tx'},
            {'data': ['00', '00'], 'id': '2B2', 'dir': 'Tx'},
            {'data': ['78', '00'], 'id': '2CA', 'dir': 'Tx'},
            {'data': ['00', '00', '00'], 'id': '2CE', 'dir': 'Tx'},
            {'data': ['00', '55', '00', '00', '00', '00', '00'], 'id': '2FC', 'dir': 'Tx'},
            {'data': ['00', '00', '00', '00', '00', '00', '00', '00'], 'id': '303', 'dir': 'Tx'},
            {'data': ['00', '00', '00', '00', '00', '00'], 'id': '328', 'dir': 'Tx'},
            {'data': ['00', '00'], 'id': '373', 'dir': 'Tx'},
            {'data': ['00', '00', '00'], 'id': '3B7', 'dir': 'Tx'},
            {'data': ['00', '00', '00'], 'id': '3B9', 'dir': 'Tx'},
            {'data': ['00', '00', '40', '00', '00', '00', '00', '00'], 'id': 'B5', 'dir': 'Tx'},
            {'data': ['1E', '01', '80', '0C', '81'], 'id': '1A1', 'dir': 'Tx'}]

    def test_no_filters(self):
        self.assertEqual(self.message_list, filtered(self.message_list))

    def test_single_id(self):
        f = filtered(self.message_list, filter_id='1A1')
        self.assertEqual(len(f), 6)
        for m in f:
            self.assertEqual(m['id'], '1A1')

    def test_multiple_id(self):
        f = filtered(self.message_list, filter_id=['1A1', '373', 'C0'])
        self.assertEqual(len(f), 8)
        for m in f:
            self.assertIn(m['id'], ['1A1', '373', 'C0'])

    def test_single_dir(self):
        f = filtered(self.message_list, filter_dir='Tx')
        self.assertEqual(len(f), 29)
        for m in f:
            self.assertEqual(m['dir'], 'Tx')

    def test_multiple_dir(self):
        f = filtered(self.message_list, filter_dir=['Tx', 'Rx'])
        self.assertEqual(len(f), 30)
        for m in f:
            self.assertIn(m['dir'], ['Tx', 'Rx'])

if __name__ == '__main__':
    unittest.main()
