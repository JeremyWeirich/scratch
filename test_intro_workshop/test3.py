import unittest
# This is a file that will test the get_message_data() and filtered() function
# You should now be able to handle either a .asc or .csv file
# Change the name XXX below to the name of your python file
from XXX import get_message_data, filtered
# Then, run this file


class TestGetMessageData(unittest.TestCase):
    def test_count_asc(self):
        messages = get_message_data('test_CAN.asc')
        self.assertEqual(len(messages), 1995)
        self.assertIsInstance(messages, list)
        for message in messages:
            self.assertIsInstance(message, dict)
            for key in ['id', 'data', 'dir']:
                self.assertIn(key, message)

    def test_count_csv(self):
        messages = get_message_data('test_CAN.csv')
        self.assertEqual(len(messages), 1995)
        self.assertIsInstance(messages, list)
        for message in messages:
            self.assertIsInstance(message, dict)
            for key in ['id', 'data', 'dir']:
                self.assertIn(key, message)

    def test_asc_equals_csv(self):
        for asc, csv in zip(get_message_data('test_CAN.asc'), get_message_data('test_CAN.csv')):
            self.assertDictEqual(asc, csv)


class TestFiltered(unittest.TestCase):
    def setUp(self):
        self.messages = get_message_data('test_CAN.asc')

    def test_no_filters(self):
        filt = filtered(self.messages)
        self.assertItemsEqual(filt, self.messages)

    def test_filter_dir_rx(self):
        filt = filtered(self.messages, filter_dir='Rx')
        self.assertEqual(len(filt), 96)

    def test_filter_dir_tx(self):
        filt = filtered(self.messages, filter_dir='Tx')
        self.assertEqual(len(filt), 1899)

    def test_filter_ids(self):
        for id, target in zip(['1A1', '2CA', 'B5', '6F', 'BBA', 'C0'], [369, 73, 378, 8, 0, 37]):
            actual = len(filtered(self.messages, filter_id=id))
            self.assertEqual(actual, target, 'expected {} counts of id {}, found {}'.format(target, id, actual))

    def test_filter_combined_rx(self):
        for id, target in zip(['1A1', '2CA', 'B5', '6F', 'BBA', 'C0'], [0, 0, 8, 8, 0, 37]):
            actual = len(filtered(self.messages, filter_id=id, filter_dir='Rx'))
            self.assertEqual(actual, target, 'expected {} counts of id {}, found {}'.format(target, id, actual))

    def test_filter_combined_tx(self):
        for id, target in zip(['1A1', '2CA', 'B5', '6F', 'BBA', 'C0'], [369, 73, 370, 0, 0, 0]):
            actual = len(filtered(self.messages, filter_id=id, filter_dir='Tx'))
            self.assertEqual(actual, target, 'expected {} counts of id {}, found {}'.format(target, id, actual))

if __name__ == '__main__':
    unittest.main()
