import re
import datetime
import string
from openpyxl import Workbook, load_workbook


# base = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM\VTC-P14106001_BMW_CDM_'
base = r'C:\Users\Jeremy Weirich\Desktop\CVS_personal\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM\VTC-P14106001_BMW_CDM_'

def read_prs_from_pr_tracker():
    print 'Loading PRs from PR Tracker'
    read = load_workbook('cdm_prs.xlsx', read_only=True)
    ws_read = read.active
    prs = []
    for f_row in ws_read.iter_rows(row_offset=1):
        row = [cell.value for cell in f_row]
        prs.append(
            [row[0],  # number
             row[1],  # status
             row[3],  # title
             row[5]]  # module
        )
    return prs


def read_prs_from_vtcs():
    
    functions = [
        'CID',
        'CoSi',
        'Curtain',
        'Diagnostics',
        'General',
        'HPW',
        'HSK',
        'KFS',
        'LIN_Master',
        'Volume',
        'ZuZi',
    ]

    vtc_prs = {}
    reg = re.compile(r'PR ?(\d+)')

    for function in functions:
        filename = base + function + '.xlsx'
        print "Opening {}".format(function)
        wb = load_workbook(filename, read_only=True, data_only=True)
        for sheet_name in wb.sheetnames:
            print "- checking sheet {}".format(sheet_name)
            sheet = wb.get_sheet_by_name(sheet_name)
            r_num = 0
            for row in sheet.iter_rows():
                r_num += 1
                if len(row) >= 10:
                    if "Auto" in sheet_name:
                        pr_index = 10
                    else:
                        pr_index = 7
                    pr_field = row[pr_index].value
                    if pr_field and isinstance(pr_field, basestring):
                        prs = re.findall(reg, pr_field)
                        if prs:
                            for pr in prs:
                                pr = int(pr)
                                if pr not in vtc_prs:
                                    vtc_prs[pr] = []
                                vtc_prs[pr].append(dict(
                                    row=r_num,
                                    document=function,
                                    sheet=sheet.title,
                                ))
    return vtc_prs


def create_pr_status_sheet(all_prs, vtc_prs):
    date = datetime.datetime.now()
    f_date = date.strftime('%m_%d_%Y')
    print 'Creating CDM PR Report'
    wb = Workbook()
    ws = wb.active
    ws.title = 'PRs'

    widths = [
        7.6,
        8.4,
        8.4,
        85,
        45,
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        ws.column_dimensions[column].width = width

    row = 1
    ws['A1'] = 'Number'
    ws['B1'] = 'Module'
    ws['C1'] = 'Status'
    ws['D1'] = 'Title'
    ws['E1'] = 'Location(s)'
    for number, status, title, module in all_prs:
        row += 1
        ws['A{}'.format(row)] = number
        ws['B{}'.format(row)] = module
        ws['C{}'.format(row)] = status
        ws['D{}'.format(row)] = title
        try:
            loc_str = ''
            for location in vtc_prs[number]:
                loc_str += '\n{}: {}, {}'.format(location['document'], location['sheet'], location['row'])
            ws['E{}'.format(row)] = loc_str[1:]
        except KeyError:
            ws['E{}'.format(row)] = '*Not Found'

    wb.save('CDM PR Report {}.xlsx'.format(f_date))
    print 'Finished'


if __name__ == '__main__':
    all_prs = read_prs_from_pr_tracker()
    vtc_prs = read_prs_from_vtcs()
    create_pr_status_sheet(all_prs, vtc_prs)
