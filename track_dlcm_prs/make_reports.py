import re
import datetime
import string
from openpyxl import Workbook, load_workbook


VTC = r'VTC-P15253010_FORD_DLCM.xlsx'
PR_EXPORT = r'PR lists for DLCM.xlsx'
CR_EXPORT = r'CR lists for DLCM.xlsx'

def read_prs_from_pr_export():
    print 'Loading PRs from PR export'
    read = load_workbook(PR_EXPORT, read_only=True, data_only=True)
    ws_read = read.active
    prs = []
    for f_row in ws_read.iter_rows(row_offset=1):
        row = [cell.value for cell in f_row]
        prs.append(
            [row[0],  # number
             row[1],  # status
             row[3],  # title
             row[4]]  # module
        )
    return prs


def read_crs_from_cr_export():
    print 'Loading CRs from CR export'
    read = load_workbook(CR_EXPORT, read_only=True, data_only=True)
    ws_read = read.active
    crs = []
    for f_row in ws_read.iter_rows(row_offset=1):
        row = [cell.value for cell in f_row]
        crs.append([
            row[0],  # number
            row[1],  # status
            row[3],  # title
        ])
    return crs


def read_data_from_vtc():
    vtc_prs = {}
    vtc_crs = {}
    pr_regex = re.compile(r'PR ?(\d+)')
    cr_regex = re.compile(r'CR ?(\d+)')

    print "Opening", VTC
    wb = load_workbook(VTC, read_only=True, data_only=True)
    for sheet_name in wb.sheetnames:
        print "- checking sheet {}".format(sheet_name)
        sheet = wb[sheet_name]
        r_num = 0
        for row in sheet.iter_rows():
            r_num += 1
            if len(row) >= 10:
                if "auto" in sheet_name.lower():
                    pr_index = 10
                else:
                    pr_index = 7
                pr_field = row[pr_index].value
                if pr_field and isinstance(pr_field, basestring):
                    prs = re.findall(pr_regex, pr_field)
                    if prs:
                        for pr in prs:
                            pr = int(pr)
                            if pr not in vtc_prs:
                                vtc_prs[pr] = []
                            vtc_prs[pr].append(dict(
                                row=r_num,
                                sheet=sheet.title,
                            ))
                    crs = re.findall(cr_regex, pr_field)
                    if crs:
                        for cr in crs:
                            cr = int(cr)
                            if cr not in vtc_crs:
                                vtc_crs[cr] = []
                            vtc_crs[cr].append(dict(
                                row=r_num,
                                sheet=sheet.title,
                            ))
    return vtc_prs, vtc_crs


def create_pr_status_sheet(all_prs, vtc_prs):
    date = datetime.datetime.now()
    f_date = date.strftime('%m_%d_%Y')
    print 'Creating PR Report'
    wb = Workbook()
    ws = wb.active
    ws.title = 'PRs'

    widths = [
        7.6,
        8.4,
        8.4,
        85,
        45,
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        ws.column_dimensions[column].width = width

    row = 1
    ws['A1'] = 'Number'
    ws['B1'] = 'Module'
    ws['C1'] = 'Status'
    ws['D1'] = 'Title'
    ws['E1'] = 'Location(s)'
    ws['F1'] = 'Missing:'
    ws['G1'] = '=COUNTIF(E:E, "*Not Found")'
    for number, status, title, module in all_prs:
        row += 1
        ws['A{}'.format(row)] = number
        ws['B{}'.format(row)] = module
        ws['C{}'.format(row)] = status
        ws['D{}'.format(row)] = title
        try:
            loc_str = ''
            for location in vtc_prs[number]:
                loc_str += '\n{}, {}'.format(location['sheet'], location['row'])
            ws['E{}'.format(row)] = loc_str[1:]
        except KeyError:
            ws['E{}'.format(row)] = '*Not Found'

    wb.save('PR Report {}.xlsx'.format(f_date))
    print 'Finished'


def create_cr_status_sheet(all_crs, vtc_crs):
    date = datetime.datetime.now()
    f_date = date.strftime('%m_%d_%Y')
    print 'Creating CR Report'
    wb = Workbook()
    ws = wb.active
    ws.title = 'CRs'

    widths = [
        7.6,
        8.4,
        85,
        45,
    ]
    columns = string.ascii_uppercase
    for column, width in zip(columns, widths):
        ws.column_dimensions[column].width = width

    row = 1
    ws['A1'] = 'Number'
    ws['B1'] = 'Status'
    ws['C1'] = 'Title'
    ws['D1'] = 'Location(s)'
    ws['E1'] = 'Missing:'
    ws['F1'] = '=COUNTIF(D:D, "*Not Found")'
    for number, status, title in all_crs:
        row += 1
        ws['A{}'.format(row)] = number
        ws['B{}'.format(row)] = status
        ws['C{}'.format(row)] = title
        try:
            loc_str = ''
            for location in vtc_crs[number]:
                loc_str += '\n{}, {}'.format(location['sheet'], location['row'])
            ws['D{}'.format(row)] = loc_str[1:]
        except KeyError:
            ws['D{}'.format(row)] = '*Not Found'

    wb.save('CR Report {}.xlsx'.format(f_date))
    print 'Finished'


if __name__ == '__main__':
    all_prs = read_prs_from_pr_export()
    all_crs = read_crs_from_cr_export()
    vtc_prs, vtc_crs = read_data_from_vtc()
    create_pr_status_sheet(all_prs, vtc_prs)
    create_cr_status_sheet(all_crs, vtc_crs)
