import json
from openpyxl import load_workbook

rct_tabs = [
    'General',
    'CID',
    'Curtain',
    'KFS',
    'CoSi',
    'HSK',
    'ZuZi',
    'Volume',
    'Mechanical',
]

rct_items = {name: {} for name in rct_tabs}

rct = load_workbook("build.xlsx", read_only=True, data_only=True)
for sheet_name in rct.sheetnames:
    if sheet_name in rct_tabs:
        sheet = rct.get_sheet_by_name(sheet_name)
        cur_rct = rct_items[sheet_name]
        for f_row in sheet.iter_rows():
            row = [cell.value for cell in f_row]
            if len(row) >= 4:
                if row.count(None) != len(row):
                    if sheet_name == "General":
                        try:
                            rnum = int(row[2])
                        except:
                            rnum = row[2]
                        req = row[3]
                    else:
                        try:
                            rnum = int(row[0])
                        except:
                            rnum = row[0]
                        req = row[1]
                    if rnum not in cur_rct and isinstance(rnum, int):
                        cur_rct[rnum] = req
                    else:
                        print "Duplicate RCT number: {}".format(rnum)


import pprint
pprint.pprint(rct_items)

with open('rct_items.txt', 'w') as items:
    items.write(json.dumps(rct_items))