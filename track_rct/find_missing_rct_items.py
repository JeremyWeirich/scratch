import json

with open('rct_items.txt') as f:
    all_rct = json.loads(f.read())
with open('vtc_rct_items.txt') as f:
    vtc_rct = json.loads(f.read())

found = {}
missing = {}

for function, data in all_rct.items():
    found[function] = []
    missing[function] = []
    for rct_id, req in data.items():
        rct_id = int(rct_id)
        lookup_id = "ID: {}_{}".format(function, rct_id)
        if lookup_id in vtc_rct:
            found[function].append(rct_id)
        else:
            missing[function].append([rct_id, req])
    found[function] = sorted(found[function])
    missing[function] = sorted(missing[function])
    print "{}:\n{} found\n{} missing".format(function, len(found[function]), len(missing[function]))

import pprint

with open('found.txt', 'w') as f:
    f.write(pprint.pformat(found))

with open('missing.txt', 'w') as f:
    f.write(pprint.pformat(missing))

