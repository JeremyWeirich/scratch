import json
import os
import glob
import re
from openpyxl import load_workbook

with open('rct_items.txt', 'r') as rct:
    rct_data = json.loads(rct.read())

vtc_path = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM'

os.chdir(vtc_path)
files = glob.glob('*.xlsx')
files = [file for file in files if file.startswith('VTC')]

found_rct_items = []

rct_finder = re.compile(r'RCT_BMW_CDM\n((ID: ?\w+_\d+\n?)+)')

for filename in files:
    print "opening {}".format(filename)
    wb = load_workbook(filename, data_only=True, read_only=True)
    simple_name = filename.split("BMW_CDM_")[1][:-5]
    for sheet_name in wb.sheetnames:
        sheet = wb.get_sheet_by_name(sheet_name)
        print '- searching {}'.format(sheet_name)
        for f_row in sheet.iter_rows():
            row = [cell.value for cell in f_row]
            if len(row) > 7 and row.count(None) != len(row):
                ref = row[7]
                if ref is not None and isinstance(ref, basestring):
                    rct_items = re.findall(rct_finder, ref)
                    if rct_items:
                        a = rct_items[0][:-1][0]
                        a = a.split("\n")
                        for item in a:
                            if item not in found_rct_items:
                                found_rct_items.append(item)

print found_rct_items
with open('vtc_rct_items.txt', 'w') as f:
    f.write(json.dumps(found_rct_items))
