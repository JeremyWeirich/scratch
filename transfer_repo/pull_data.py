import json

DATA_DIR = r'C:\Users\HIL Tester\Documents\transfer_release'


def create_dir(name, root=DATA_DIR):
    for invalid_char in ['<', '>', ':', '"', '/', '\\', '?', '*']:
        name = name.replace(invalid_char, '')
    p = os.path.join(root, name)
    if not os.path.exists(p):
        os.makedirs(p)
    return p


def log_suite(suite, path):
    json_suite = {
        'name:': suite.name,
        'release': suite.release.name,
        'module': suite.module.identifier if suite.module else '',
        'user': suite.user.username if suite.user else '',
        'hil': suite.hil.name,
        'created': suite.created,
        'updated': suite.updated,
        'sheet': suite.sheet,
        'filename': suite.filename,
        'state': suite.state,
        'children': [],
    }
    for requirement in suite.children.all():
        json_requirement = {
            'at': requirement.at,
            'state': requirement.state,
            'asil': requirement.asil,
            'description': requirement.description,
            'error': requirement.error,
            'created': requirement.created,
            'updated': requirement.updated,
            'parent': requirement.parent,
            'explanation': requirement.explanation,
            'spec_ref': requirement.spec_ref,
            'children': [],
        }
        for objective in requirement.children.all():
            json_objective = {
                'at': objective.at,
                'state': objective.state,
                'asil': objective.asil,
                'description': objective.description,
                'error': objective.error,
                'created': objective.created,
                'updated': objective.updated,
                'parent': objective.parent,
                'children': [],
            }
            for test in objective.children.all():
                json_test = {
                    'at': test.at,
                    'state': test.state,
                    'asil': test.asil,
                    'description': test.description,
                    'error': test.error,
                    'created': test.created,
                    'updated': test.updated,
                    'parent': test.parent,
                    'category': test.category,
                    'length': test.length,
                    'write_signals': [],
                    'verify_signals': [],
                }
                for write_signal in test.write_signals.all():
                    json_write_signal = {
                        'name': write_signal.name,
                        'test': write_signal.test,
                        'value': write_signal.value,
                    }
                    json_test['write_signals'].append(json_write_signal)
                for verify_signal in test.verify_signals.all():
                    json_verify_signal = {
                        'name': verify_signal.name,
                        'test': verify_signal.test,
                        'state': verify_signal.state,
                        'graph_data': verify_signal.graph_data,
                        'results': [],
                    }
                    for result in verify_signal.results.all():
                        json_result = {
                            'verify_signal': result.verify_signal,
                            'time': result.time,
                            'expected': result.expected,
                            'measured': result.measured,
                            'operator': result.operator,
                            'upper': result.upper,
                            'lower': result.lower,
                            'state': result.state,
                        }
                        json_verify_signal['results'].append(json_result)
                    json_test['verify_signals'].append(json_verify_signal)
                json_objective['children'].append(json_test)
            json_requirement['children'].append(json_objective)
        json_suite['children'].append(json_requirement)
    with open(os.path.join(path, '{}.json'.format(suite.id)), 'w') as f:
        f.write(json.dumps(json_suite, default=str))


if __name__ == '__main__':
    import os, time
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
    import django
    django.setup()
    from run import models

    for project in models.Project.objects.all():
        p_start = time.time()
        print 'working on project', project.name
        p = create_dir(project.name)
        for release in project.releases.all():
            r_start = time.time()
            print '  working on release', release.name
            r = create_dir(release.name, root=p)
            for suite in release.suites.all():
                start = time.time()
                log_suite(suite, r)
                print '    suite {} logged [{}]'.format(suite.name, time.time() - start)
            print '  release logged in', time.time() - r_start
        print 'project logged in', time.time() - p_start


    release = models.Release.objects.get(project=5, id=10)
