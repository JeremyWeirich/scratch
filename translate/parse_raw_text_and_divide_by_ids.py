import re
from collections import OrderedDict

id_regex = re.compile(r'^ID: ([a-zA-Z_\d]+)')


def prettify(value):
    temp = ''.join(value)
    temp = temp.replace('  ', ' ')
    temp = temp.replace('\n', '')
    return temp


def convert_to_readable(path):
    matches = OrderedDict()
    current_key = None
    with open(path) as f:
        for line in f.readlines():
            match = re.match(id_regex, line)
            if match:
                current_key = match.group()
                matches[match.group()] = []
            else:
                if current_key:
                    if line and line != '\n':
                        matches[current_key].append(line)
    return matches


if __name__ == '__main__':
    p = r'N:\Users\Jeremy Weirich\Python Projects\Scratch\translate\source2.txt'
    data = convert_to_readable(p)
    with open('cleaned.txt', 'w') as f:
        for key, value in data.iteritems():
            f.write(key)
            f.write('\n')
            f.write(prettify(value))
            f.write(3 * '\n')