import json

a = dict(
    name='jeremy',
    shirt='blue',
    good_things_happening_today=None,
)

with open('example.txt', 'w') as f1:
    encoded_data = json.dumps(a)
    f1.write(encoded_data)
    f1.write(json.dumps(a))

with open('example.txt', 'r') as f2:
    encoded_data = f2.read()
    unencoded_data = json.loads(encoded_data)

print unencoded_data