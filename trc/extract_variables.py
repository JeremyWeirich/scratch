import re
import json


trc_file_path = r'N:\Users\HIL Tester\Projects\BMW_CDM\BMW_CDM_CFD\Application_High\Build Results\BMW_CDM_Model.trc'
# trc_file_path = r'N:\Users\HIL Tester\Projects\P16G1501_SGM_GEM_FPC\P16G1501_SGM_GEM_FPC_CFD\P16G1501_SGM_GEM_FPC_Application\Build Results\P16G1501_SGM_GEM_FPC_Model.trc'

group_start = re.compile(r'^[\s]*group \"(?P<name>.+)\"')
group_end = re.compile(r'endgroup -- .*\"(?P<name>.+)\"')
alias = re.compile(r'^[\s]*alias:[\s]+\"(?P<name>.+)\"')
irritating_label = re.compile(r'^\s*endgroup\s*$')
variable_data = re.compile(r'^[\s]*type:[\s]+(?P<type>.+)\((?P<size>\d+)(\,.*)?\)$')

found_variables = []

with open(trc_file_path, 'r') as trc:
    current_path = ''
    current_type = ''
    current_size = 0
    line_num = 0
    for line in trc.readlines()[line_num:]:
        line_num += 1
        # start group
        start_match = re.search(group_start, line)
        if start_match:
            if current_path:
                current_path += '/' + start_match.group(1)
            else:
                current_path = start_match.group(1)

        # end group
        end_match = re.search(group_end, line)
        if end_match:
            assert current_path.endswith(end_match.group(1)), 'supposed to end with {} but is {}, line {}\n current path is {}'.format(end_match.group(1), current_path, line_num, current_path)
            slice_end = len(end_match.group(1))+1
            current_path = current_path[:-slice_end]

        # find variable
        alias_match = re.search(alias, line)
        if alias_match:
            path = alias_match.group(1)
            found_variables.append([current_path + '/' + path, current_type, current_size])

        # find irritating label
        label_end = re.search(irritating_label, line)
        if label_end:
            current_path = ''

        # find types
        type_match = re.search(variable_data, line)
        if type_match:
            type_match = type_match.groupdict()
            current_type = type_match['type'].lower() if 'type' in type_match else None
            if current_type == 'flt':
                current_type = 'float'
            current_size = type_match['size'] if 'size' in type_match else None

import pprint
pprint.pprint(found_variables)
# pprint.pprint(types)
# with open('found_variables.txt', 'w') as found:
    # found.write(json.dumps(found_variables))
