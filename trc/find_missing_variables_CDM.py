import json
import glob
from openpyxl import load_workbook

with open('found_variables.txt', 'r') as found:
    found_variables = json.loads(found.read())

with open('missing.txt', 'w') as missing:
    start = r'C:\Users\HIL Tester\Desktop\CVS HIL\P14106001_BMW_CDM\Software\Validation\Test_Plans\Test_Cases\VTC-P14106001_BMW_CDM\VTC'
    for filename in glob.glob('{}*.xlsx'.format(start)):
        short_name = filename.split('VTC-P14106001_BMW_CDM_')[-1][:-5]
        print 'checking {}'.format(short_name)
        missing.write('- ' + short_name + '\n')
        wb = load_workbook(filename, read_only=True)
        try:
            sheet = wb.get_sheet_by_name('VariablePool')
            a = sheet.iter_rows()
            a.next()
            for f_row in a:
                row = [cell.value for cell in f_row]
                path = row[6]
                if path:
                    if path not in found_variables:
                        missing.write(path+'\n')
        except KeyError:
            pass
        missing.write('\n\n')


