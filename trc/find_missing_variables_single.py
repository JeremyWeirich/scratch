import json
from openpyxl import load_workbook

filename = r'N:\Users\Amrutha\Copy of VTC-P16G1501_SGM_GEM_FPC_OMRON_Amrutha.xlsx'

with open('found_variables.txt', 'r') as found:
    found_variables = json.loads(found.read())

with open('missing.txt', 'w') as missing:
    count = 0
    print 'checking', filename
    missing.write('- ' + filename + '\n')
    wb = load_workbook(filename, read_only=True)
    try:
        sheet = wb.get_sheet_by_name('VariablePool')
        a = sheet.iter_rows()
        a.next()
        for i, f_row in enumerate(a):
            row = [cell.value for cell in f_row]
            path = row[6]
            if path:
                if path not in found_variables:
                    print path
                    missing.write(path+'\n')
                    count += 1
    except KeyError:
        pass
    missing.write('\n\n')
    print 'found', count, 'incorrect paths'


