import re
import json


trc_file_path = r'N:\Users\HIL Tester\Projects\Ford_ELSDM\FORD_ELSDM_CFD\Application_ELSDM\Build Results\ELSDM_Model.trc'

group_start = re.compile(r'^[\s]*group \"(?P<name>.+)\"')
group_end = re.compile(r'endgroup -- .*\"(?P<name>.+)\"')
alias = re.compile(r'^[\s]*alias:[\s]+\"(?P<name>.+)\"')
irritating_label = re.compile(r'^\s*endgroup\s*$')
type = re.compile(r'type:\s+([\S\s]+)\(')

found_variables = []
types = set()

with open(trc_file_path, 'r') as trc:
    current_path = ''
    line_num = 0
    current = dict(
        path=None,
        type=None,
    )
    for line in trc.readlines()[line_num:]:
        line_num += 1

        # start group
        start_match = re.search(group_start, line)
        if start_match:
            if current_path:
                current_path += '/' + start_match.group(1)
            else:
                current_path = start_match.group(1)

        # end group
        end_match = re.search(group_end, line)
        if end_match:
            assert current_path.endswith(end_match.group(1)), 'supposed to end with {} but is {}, line {}\n current path is {}'.format(end_match.group(1), current_path, line_num, current_path)
            slice_end = len(end_match.group(1))+1
            current_path = current_path[:-slice_end]

        # find variable
        alias_match = re.search(alias, line)
        if alias_match:
            name = alias_match.group(1)
            path = current_path + '/' + name if current_path else name
            if current['type']:  # this checks if the previous line was a type, which should always be true for an alias
                current['path'] = path
                found_variables.append([current['path'], current['type'].lower()])

        # find irritating label
        label_end = re.search(irritating_label, line)
        if label_end:
            current_path = ''

        # find type (this needs to go after alias check!)
        type_match = re.search(type, line)
        if type_match:
            current['type'] = type_match.group(1)
            types.add(type_match.group(1).lower())
        else:
            current['type'] = None



with open('found_variables_with_types.txt', 'w') as found:
    found.write(json.dumps(found_variables))

print 'found', len(found_variables), 'variables'
print 'types are', types
