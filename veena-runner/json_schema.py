import re

GLOBAL_REGEX = r'^\$\w+$'
VALID_FIU_CHANNELS = {
    "min": 1,
    "max": 39
}
VALID_FIU_NUMBERS = {
    "min": 1,
    "max": 3
}
VALID_COM_PORT = {
    "min": 1,
    "max": 256
}
FIU_NAME_REGEX = r'^(fiu|FIU)\W+\w+$'

COMMON_CATEGORY_ENUM = ["requirement", "stimulus", "suite", "objective"]

schema_suite = {
    "type": "object",
    "properties": {
        "database": {"type": "object"},
        "source": {"type": "object"},
        "common": {"type": "object"},
        "pools": {
            "type": "object",
            "properties": {
                "variables": {"type": ["object", "null"]},
                "globals": {"type": ["object", "null"]},
                "tolerances": {"type": ["object", "null"]},
                "defaults": {"type": ["object", "null"]},
                "fiu": {"type": ["object", "null"]}
            },
            "required": ["variables", "globals", "tolerances", "defaults", "fiu"]
        },
        "config": {"type": "object"},
        "children": {"type": ["array", "null"]},
    },
    "required": ["database", "source", "common", "pools", "config", "children"],
}

schema_common = {
    "type": "object",
    "properties": {
        "category": {"enum": COMMON_CATEGORY_ENUM},
        "map": {"type": ["integer", "null"]},
        "asil": {"type": ["string", "null"]},
        "description": {"type": ["string", "null"]},
        "repeat": {"type": "integer", "minimum": 1, "maximum": 10000},
        "duration": {"type": "number", "minimum": 0}, #todo Veena only  and muliple of 0.0005
        "properties": {
            "type": "object",
            "patternProperties": {
                "^.*$": {"type": ["string", "number"]}
            }
        },#todo Veena details of this field
        "name": {"type": "string"},
    },
    "required": ["category", "map", "asil", "description", "repeat", "duration", "properties", "name"],
}

schema_database = {
    "type": "object",
    "properties": {
        "module": {"type": ["number", "null"], "minimum": 0},
        "application": {"type": "integer", "minimum": 0},
        "variable_pool": {"type": ["integer", "null"], "minimum": 0},
        "user": {"type": ["integer", "null"], "minimum": 0},
        "release": {"type": ["integer", "null"], "minimum": 0},
        "tolerance_pool": {"type": ["integer", "null"], "minimum": 0},
        "hil": {"type": "integer", "minimum": 0},
    },
    "required": ["module", "application", "variable_pool", "user", "release", "tolerance_pool", "hil"],
}

schema_source = {
    "type": "object",
    "properties": {
        "category" : {"enum": ["excel", "qtest"]},
        "parameters": {"type": "object",
                      "properties" : {
                          "filename": {"type": "string"},
                          "sheets": {"type": "array", "minItems": 1},
                          "project_id":{"type": "string"},
                          "qtest_version": {"type": "array", "minItems": 1},
                      },
        }
    },
    "oneOf": [
        {
            "properties": {
                "category": {"enum": ["excel"]},
                "parameters": {
                    "required":["filename", "sheets"],
                }
            },
        },
        {
            "properties": {
                "category": {"enum": ["qtest"]},
                "parameters": {
                    "required": ["project_id", "qtest_version"]
                }
            },
        }
    ],
    "required": ["category", "parameters"],

}

schema_variables = {
    "type": "object",
    "properties": {},
    "additionalProperties": {
        "type": "string"
    }
}

schema_tolerances = {
    "type": "object",
    "properties": {},
    "additionalProperties" :{
        "type": "object",
        "properties": {
            "category": {"enum": ["Relative", "Absolute"]},
            "comment": {"type": ["string", "null"]},
            "value": {
                "oneOf":[
                    {"type": "number", "minimum": 0},
                    {"type": "string", "pattern": GLOBAL_REGEX}
                ]
            },
        },
        "required": ["category", "value"]
    }
}

schema_globals = {
    "type": "object",
    "patternProperties": {
        GLOBAL_REGEX:  {"type": "number"},
    },
    "additionalProperties": False
}

schema_defaults = {
    "type": "object",
    "properties": {
        "^.*$": {
            "oneOf": [
                {"type": "number", "minimum": 0},
                {"type": "string", "pattern": GLOBAL_REGEX}
            ]
        }
    },
}

schema_fiu = {
    "type": "object",
    "properties": {
        "channels": {
            "type": "object",
            "patternProperties": {
                FIU_NAME_REGEX: {
                    "oneOf": [
                        {"type": "integer", "minimum": VALID_FIU_CHANNELS['min'],
                         "maximum": VALID_FIU_CHANNELS['max']},
                        {"type": "string", "pattern": GLOBAL_REGEX}
                    ]
                }
            },
            "additionalProperties": False
        },
        "settings": {
            "type": "object",
            "properties": {
                "com_port": {
                    "oneOf": [
                        {"type": "integer", "minimum": VALID_COM_PORT['min'],
                        "maximum": VALID_COM_PORT['max']},
                        {"type": "string", "pattern": GLOBAL_REGEX}
                    ],
                },
                "fiu_number": {
                    "oneOf": [
                        {"type": "integer", "minimum": VALID_FIU_NUMBERS['min'],
                        "maximum": VALID_FIU_NUMBERS['max']},
                        {"type": "string", "pattern": GLOBAL_REGEX}
                    ],
                }
            },
            "required": ["com_port", "fiu_number"]
            },
    },
    "dependencies": {
        "channels": ["settings"],
        "settings": ["channels"]
    }
}

schema_config = {
    "type": "object",
    "properties": {
        "sample_time": {"type": "number", "minimum": 0, "multipleOf": 0.0005},
        "sequence_delay_after_write": {"type": "number", "minimum": 0},
        "error_recovery_period": {"type": "number", "minimum": 0},
        "stop_execution_after_exception": {"type": "boolean"}
    }
}


schema_children_upper= {
    "type": "array",
    "items":{
            "type": "object",
            "properties": {
                "common": {"type": "object"},
                "children": {"type": "array"}
            },
            "additionalProperties": False
    },
}

schema_children = {
    "type": "array",
    "items":{
            "type": "object",
            "properties": {},
            "allOf": [
                {"properties": {"common": {"type": "object"}}},
                {"properties": {"children": {"type": "array"}}},
                {"properties": {"write_signals": {"type": "object"}}},
                {"properties": {"faults": {"type": "object"}}},
                {"properties": {"verify_signals": {"type": "object"}}},
            ]
    },
}
