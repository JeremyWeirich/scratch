import logging
import pprint
from scan.src.error import ErrorCollection
from scan.src.handlers.common import validate_common
from scan.src.handlers.suite.validate import validate_suite
from scan.src.handlers.suite.validate import EXPECTED_KEYS as SUITE_EXPECTED_KEYS
from scan.src.handlers.container.validate import validate_container
from scan.src.handlers.sequence.validate import validate_sequence
from scan.src.handlers.stimulus.validate import validate_stimulus
from validate_New import ValidateSuite, ValidateCommonSuite,\
    ValidateDatabase, ValidateSource, ValidateConfig, ValidateVariables,\
    ValidateGlobals, ValidateDefaults, ValidateFiu, ValidateTolerances
from jsonschema import validate, Draft4Validator, exceptions
import json_schema

# def check_jsonschema(errors, suite, schema):
#     for error in Draft4Validator(schema).iter_errors(suite):
#         errors.add(error.message)

def validate(component):
    errors = ErrorCollection('suite')
    suite = ValidateSuite(errors, component, json_schema.schema_suite)
    if not errors.count():
        errors_common = errors.add_child('common')
        common = ValidateCommonSuite(errors_common, component['common'], json_schema.schema_common)
        if not errors_common.count():
            if common.get_category() != 'suite':
                errors_common.add("The outermost error must be suite")
        errors_database = errors.add_child('database')
        database = ValidateDatabase(errors_database, component['database'], json_schema.schema_database)
        errors_source = errors.add_child('source')
        source = ValidateSource(errors_source, component['source'], json_schema.schema_source)
        errors_config = errors.add_child('config')
        config = ValidateConfig(errors_config, component['config'], json_schema.schema_config)
        errors_globals = errors.add_child('globals')
        globals = ValidateGlobals(errors_config, component['pools']['globals'], json_schema.schema_globals)
        errors_variables = errors.add_child('variables')
        variables =  ValidateVariables(errors_variables, component['pools']['variables'], json_schema.schema_variables, database.app)
        errors_defaults = errors.add_child('defaults')
        component['pools']['_paths'] = variables.paths
        defaults = ValidateDefaults(errors_defaults, component['pools']['defaults'], json_schema.schema_defaults,\
                                    component['pools']['_paths'], component['pools']['globals'])
        errors_fiu = errors.add_child('fiu')
        fiu = ValidateFiu(errors_fiu, component['pools']['fiu'], json_schema.schema_fiu, component['pools']['globals'])
        errors_tolerances = errors.add_child('tolerances')
        tolerance = ValidateTolerances(errors_tolerances, component['pools']['tolerances'], json_schema.schema_tolerances, component['pools']['globals'])

    return errors



if __name__ == '__main__':
    import os
    os.environ.setdefault('DJANGO_SETTING_MODULE', 'config.settings')
    import django
    django.setup()
    from django.contrib.auth.models import User
    from run import models
    import json
    with open(r'C:\Bitbucket\perseus-runner\_suite.json') as f:
        s = json.loads(f.read())
        errors = validate(s)
    errors.show()