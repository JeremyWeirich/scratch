import json_schema
from jsonschema import validate
from jsonschema import exceptions
from jsonschema import validate, Draft4Validator, exceptions
from run import models
from django.contrib.auth.models import User
from numbers import Number
import copy

lookups = {
        'application': models.Application,
        'hil': models.HIL,
        'release': models.Release,
        'module': models.Module,
        'variable_pool': models.VariablePool,
        'tolerance_pool': models.TolerancePool,
        'user': User,
    }

def require_object(errors, model, props):
    """Helper function: gets object from database or raises assertion error"""
    try:
        return model.objects.get(**props), True
    except model.DoesNotExist:
        errors.add("%s with parameters %s not found in database" % (model.__name__, props))
    except:  # pylint: disable=broad-except
        errors.add("failed get with parameters for %s: %s" % (model.__name__, props))
    return None, False

def require_member(errors, element, options):
    """Confirm that element of given category is a member of options"""
    if element not in options:
        errors.add("invalid option: '%s'" % element)
        return False
    return True

def require_key(errors, key, dictionary):
    """Helper function: return value associated with key and boolean for if it was found or not"""
    value = None
    found = False
    if key in dictionary:
        value = dictionary[key]
        found = True
    else:
        errors.add("must contain key: '%s'" % (key))
    return value, found

def get_value_or_global(errors, value, _globals):
    """If value is numeric, return value.  If it's a string, return the value associated with that string in globals"""
    if _globals and isinstance(value, basestring):
        global_value, found = require_key(errors, value, _globals)
        if found:
            return global_value
        else:
            #errors.add("%s global variable is not found in globals tab" % value)
            return None
    return value

def require_writeable(errors, name, path):
    """Checks if a path is read-only"""
    if path.read_only:
        errors.add("%s is a read-only variable" % name)
        return False
    return True

def return_type(errors, value):
    if isinstance(value, Number):
        return type(value)
    else:
        errors.add("%s should be of type Number (int, unit or float" % value)
        return None

def require_size(errors, name, value, path):
    """Checks if a provided value can be safely written to a model variable"""
    value_type = return_type(errors, value)

    if value_type:
        if value_type == 'float' or value_type == 'int' and path.type == 'uint':
            errors.add("unsafe type coercion for %s: %s to %s" % (name, value_type, path.type))
            return False
    else:
        return False
    return True


class ValidateSuite(object):

    def __init__(self, errors, component, schema):
        self.component = component
        self.errors = errors
        self.schema = schema
        self._validate_against_schema()
        if not self.errors.count():
            self._validate_content()

    def _validate_against_schema(self):
        for error in Draft4Validator(self.schema).iter_errors(self.component):
            self.errors.add(error.message)

    def _validate_content(self):
        pass

class ValidateCommonSuite(ValidateSuite):
    def __init__(self, errors, component, schema):
        self.category = None
        ValidateSuite.__init__(self, errors, component, schema)


    def _validate_content(self):
        if not self.errors.count():
            self.category = self.component["category"]

    def get_category(self):
        return self.category



class ValidateDatabase(ValidateSuite):

    def __init__(self, errors, component, schema):
        self.app = None
        ValidateSuite.__init__(self, errors, component, schema)

    def _validate_content(self):
        share_a_project = ['release', 'module', 'tolerance_pool']
        if not self.errors.count():
            app_id = self.component['application']
            self.app, found = require_object(self.errors, models.Application, {'id': app_id})
            if found:
                for model, _id in self.component.items():
                    if _id is not None:
                        entry, found = require_object(self.errors, lookups[model], {'id': _id})
                        if model in share_a_project and found:
                            print model, entry.project, entry.id, entry.name
                        if found:
                            if model in share_a_project and entry.project != self.app.project:
                                self.errors.add("%s and application belong to different projects: %s [%s] vs %s [%s]" %
                                       (model, entry.project, entry.project.id, self.app.project, self.app.project.id))
                                if model == "variable_pool" and entry.trc.application != self.app:
                                    self.errors.add("variable pool does not belong to %s" % self.app.name)


class ValidateSource(ValidateSuite):
    pass



class ValidateGlobals(ValidateSuite):
    pass


class ValidateConfig(ValidateSuite):
    pass



class ValidateDefaults(ValidateSuite):

    def __init__(self, errors, component, schema, database_paths, globals):
        self._paths = database_paths
        self.globals = globals
        ValidateSuite.__init__(self, errors, component, schema)

    def _validate_content(self):

        for name, default in self.component.items():
            found = require_member(self.errors, name, self._paths)
            if found:
                default_val = get_value_or_global(self.errors, default, self.globals)
                if default_val:
                    writeable = require_writeable(self.errors, name, self._paths[name])
                    if writeable:
                        require_size(self.errors, name, default_val, self._paths[name])



class ValidateFiu(ValidateSuite):

    def __init__(self, errors, component, schema, globals):
        self.globals = globals
        ValidateSuite.__init__(self, errors, component, schema)

    def _validate_content(self):
        local_component = copy.deepcopy(self.component)
        if "channels" in local_component:
            for name, value in local_component['channels'].items():
                if isinstance(value, basestring):
                    global_val = get_value_or_global(self.errors, value, self.globals)
                    if global_val:
                        local_component['channels'][name] = global_val
            for name, value in local_component['settings'].items():
                if isinstance(value, basestring):
                    global_val = get_value_or_global(self.errors, value, self.globals)
                    if global_val:
                        local_component['settings'][name] = global_val
            self.component = copy.deepcopy(local_component)
            ValidateSuite._validate_against_schema(self)


class ValidateTolerances(ValidateSuite):
    def __init__(self, errors, component, schema, globals):
        self.globals = globals
        ValidateSuite.__init__(self, errors, component, schema)

    def _validate_content(self):
        for item in self.component.keys():
            if isinstance(self.component[item]['value'], basestring):
                global_val = get_value_or_global(self.errors, self.component[item]['value'], self.globals)







class ValidateVariables(ValidateSuite):
    def __init__(self, errors, component, schema, app):
        self.application = app
        self.paths = {}
        ValidateSuite.__init__(self, errors, component, schema)

    def _validate_content(self):
        for name, path in self.component.items():
            path, found = require_object(self.errors, models.Path, {'trc': self.application.trc, 'path': path})
            if found:
                self.paths[name] = path
