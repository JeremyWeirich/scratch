import glob
import os
import Tkinter as tk

from voice import play_audio
from voice import record_audio

CURRENT_FOLDER = ''
PAD = 2


def make_folder():
    folder =  folder_name.get()
    print('making folder %s' % folder)
    os.mkdir(folder)
    populate_folders()


def switch_folder():
    global CURRENT_FOLDER
    CURRENT_FOLDER = folders.get(folders.curselection())
    populate_voices()


def delete_folder():
    folder = folders.get(folders.curselection())
    print('deleting folder %s' % folder)
    os.remove(folder)
    populate_folders()


def populate_folders():
    folders.delete(0, tk.END)
    for folder in os.listdir(os.curdir):
        if os.path.isdir(folder) and not folder.startswith('__'):
            folders.insert(tk.END, folder)


def record_voice():
    voice = os.path.join(CURRENT_FOLDER, voice_name.get())
    length = int(duration.get())
    if not voice.endswith('.wav'):
        voice = voice + '.wav'
    print('recording %ss voice %s' % (length, voice))
    record_audio(voice, length)
    populate_voices()


def play_voice():
    voice = voices.get(voices.curselection())
    print('playing voice %s' % voice)
    voice_path = os.path.join(CURRENT_FOLDER, voice) + '.wav'
    play_audio(voice_path)
    print('%s finished' % voice)


def delete_voice():
    voice = voices.get(voices.curselection())
    print('deleting voice %s' % voice)
    os.remove(voice)
    populate_voices()


def populate_voices():
    voices.delete(0, tk.END)
    for voice_file in glob.glob('{}/*.wav'.format(CURRENT_FOLDER)):
        voice_file = os.path.basename(voice_file).replace('.wav', '')
        voices.insert(tk.END, voice_file)


# build frames
root = tk.Tk()
root.geometry("400x500")

# add a new folder
tk.Button(root, text="Add folder", command=make_folder).grid(row=0, column=0, padx=PAD, pady=PAD)
folder_name = tk.Entry(root)
folder_name.grid(row=0, column=1, sticky=tk.NSEW, padx=PAD, pady=PAD)

# list folders
folders = tk.Listbox(root)
folders.grid(row=1, column=0, rowspan=30, columnspan=2, sticky=tk.NSEW, padx=PAD, pady=PAD)
populate_folders()

# folder buttons
open_folder = tk.Button(root, text="Open", command=switch_folder)
open_folder.grid(row=31, column=0, sticky=tk.EW)
folders.bind('<Double-1>', lambda x: open_folder.invoke())
tk.Button(root, text="Delete", background='red', command=delete_folder).grid(row=31, column=1, sticky=tk.EW)

# add a new voice
tk.Button(root, text="Add voice", command=record_voice).grid(row=0, column=2, padx=PAD, pady=PAD)
voice_name = tk.Entry(root)
voice_name.grid(row=0, column=3, sticky=tk.EW, padx=PAD, pady=PAD)

# choose duration
tk.Label(root, text="Duration [s]").grid(row=1, column=2)
duration = tk.Entry(root)
duration.insert(tk.END, 8)
duration.grid(row=1, column=3, sticky=tk.EW, padx=PAD, pady=PAD)

# list voices
voices = tk.Listbox(root)
voices.grid(row=2, column=2, rowspan=30, columnspan=2, sticky=tk.NSEW, padx=PAD, pady=PAD)
populate_voices()

# voice buttons
play = tk.Button(root, text="Play", command=play_voice)
play.grid(row=31, column=2, sticky=tk.EW)
voices.bind('<Double-1>', lambda x: play.invoke())
tk.Button(root, text="Delete", background='red', command=delete_voice).grid(row=31, column=3, sticky=tk.EW)


tk.Button(text="Quit", command=quit).grid(row=32, columnspan=4, sticky=tk.EW, padx=PAD, pady=PAD)

root.rowconfigure(2, weight=1)  # stretch the listbox???
root.mainloop()