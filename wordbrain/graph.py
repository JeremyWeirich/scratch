def load_puzzle(filename):
    puzzle = []
    with open(filename) as f:
        size = int(f.readline())
        words = [int(i) for i in f.readline().split(' ')]
        assert size ** 2 == sum(words)
        for line in f.readlines():
            assert len(line.strip()) == size
            puzzle.append(line.strip())
    return size, tuple(puzzle), words


def ppuz(puzzle):
    for i in puzzle:
        print i
    print '\n'

def neighbors(puzzle, (x, y)):
    neighbors = []
    size = len(puzzle[0])
    for nx in range(max(0, x - 1), min(x + 2, size)):
        for ny in range(max(0, y - 1), min(y + 2, size)):
            if (nx, ny) != (x, y):
                neighbors.append((ny, nx))
    return neighbors


def make_graph(puzzle):
    graph = {}
    for y, row in enumerate(puzzle):
        for x, letter in enumerate(row):
            coords = (x, y)
            if letter not in graph:
                graph[letter] = dict(
                    coords=[],
                    neighbors=[],
                )
            graph[letter]['coords']
            graph[letter] = dict(
                coords=coords,
                neighbors=neighbors(puzzle, coords)
            )
    return graph


def crawlable(graph, word):
    for letter in word:
        if letter in graph:


if __name__  == '__main__':
    all_maps = {}
    filename = 'puzzles/penguin3.txt'
    psize, puzzle, lengths = load_puzzle(filename)
    ppuz(puzzle)
    puzzle_graph = make_graph(puzzle)
    import pprint
    pprint.pprint(puzzle_graph)