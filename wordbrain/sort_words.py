import json
from string import ascii_lowercase

wordlist = 'wordlist.txt'
sorted = 'sorted.txt'

if __name__ == '__main__':
    sorted_words = {}
    with open(wordlist) as f:
        for line in f.readlines():
            line = line.strip()
            if all([i in ascii_lowercase for i in line]):
                if len(line) not in sorted_words:
                    sorted_words[len(line)] = []
                sorted_words[len(line)].append(line)

    for length in sorted_words:
        print length, ':', len(sorted_words[length])

    with open(sorted, 'w') as f:
        f.write(json.dumps(sorted_words))
