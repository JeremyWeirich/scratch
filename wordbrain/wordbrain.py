import re


def load_puzzle(filename):
    puzzle = []
    with open(filename) as f:
        size = int(f.readline())
        words = [int(i) for i in f.readline().split(' ')]
        assert size ** 2 == sum(words)
        for line in f.readlines():
            assert len(line.strip()) == size
            puzzle.append(line.strip())
    return size, tuple(puzzle), words


def drop_letters(puzzle, path):
    import copy
    new = [list(i) for i in puzzle]
    # remove path items
    for (x, y) in path:
        new[y][x] = ' '
    # drop letters
    for i in range(len(puzzle[0])):  # very ugly
        for upper, lower in zip(new, new[1:]):
            for i, letter in enumerate(lower):
                if letter == ' ':
                    lower[i] = copy.copy(upper[i])
                    upper[i] = ' '
    new = [''.join(i) for i in new]
    return new


def next_letters_available(target, puzzle, (x, y)):
    for nx in range(max(0, x - 1), min(x + 2, psize)):
        for ny in range(max(0, y - 1), min(y + 2, psize)):
            if puzzle[ny][nx] == target:
                yield (nx, ny)


def crawl(current, candidate, puzzle, path):
    if current == candidate:
        current = candidate[0]
        yield path
    target = candidate[len(current)]
    for new_coords in next_letters_available(target, puzzle, path[-1]):
        if new_coords not in path:
            longer = current + target
            for result in crawl(longer, candidate, puzzle, path + [new_coords]):
                yield result


def can_be_spelled(puzzle, candidate):
    for y, row in enumerate(puzzle):
        for x, letter in enumerate(row):
            if letter == candidate[0]:
                for r in crawl(candidate[0], candidate, puzzle, path=[(x, y)]):
                    yield (candidate, r)


def get_possible_words(wordlist, puzzle, length):
    alphabet = ''.join(set(''.join(puzzle)))
    regex = '[' + alphabet + ']{' + str(length) + '}$'
    can_spell = re.compile(regex, re.M).match
    for candidate in wordlist[str(length)]:
        if can_spell(candidate):
            yield candidate


def solve(puzzle, lengths, solution):
    if lengths:
        candidates = get_possible_words(wordlist, puzzle, lengths[0])
        for candidate in candidates:
            for word, path in can_be_spelled(puzzle, candidate):
                new_puzzle = drop_letters(puzzle, path)
                for result in solve(new_puzzle, lengths[1:], solution + [(word, path)]):
                    yield result
    else:
        yield solution


def do_the_thing():
    for result in solve(puzzle, lengths, []):
        print result


if __name__  == '__main__':
    import json
    all_maps = {}
    filename = 'puzzles/7.txt'
    psize, puzzle, lengths = load_puzzle(filename)
    with open('sorted.txt') as f:
        wordlist = json.loads(f.read())
    do_the_thing()