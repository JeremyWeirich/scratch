from openpyxl import load_workbook

FILE = 'system.xlsx'
OFFSETS = {
    "TolerancePool": [0, 1],
    "Suite": [1, 4],
    "FIUPool": [0, 0],
}
ORDERING = {
    "Suite": {
        "verify_val": 7,
        "disable_flag": 0,
        "explanation": 3,
        "verify_operator": 6,
        "verify_var": 5,
        "verify_tol": 8,
        "description": 2,
        "pr": 9,
        "spec_ref": 9,
        "write_val": 4,
        "at": 1,
        "write_var": 3,
    },
    "FIUPool": {  # FIUpool ordering is by row, other orderings are by columns
        "fiu": 0,
        "com": 1,
        "channels": 4,
    }
}

def get_tabs(path):
    data = {}
    wb = load_workbook(path, read_only=True)
    for sheet in wb.worksheets:
        data[sheet.title] = []
        for line in sheet.rows:
            data[sheet.title].append([i.value for i in line])
    return data


if __name__ == '__main__':
    import pprint
    data = get_tabs(FILE)
    print data